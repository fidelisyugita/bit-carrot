import { Component, OnInit, group, Output, EventEmitter, AfterViewInit, ViewChild, SimpleChange, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from "@angular/router";
import { Location } from '@angular/common';
import * as Constanta from '../config/constanta';
import { DataTableDirective, DataTablesModule } from 'angular-datatables';
import { Subject } from 'rxjs';
import { AppComponent } from '../../app/app.component';
import * as Utility from '../config/utility';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';

import { User } from '../model/user';
import { Group } from '../model/group';
import { Transaction } from '../model/transaction';
import { Content } from '../model/content';

import { UserService } from '../service/user.service';
import { GroupService } from '../service/group.service';
import { TransactionService } from '../service/transaction.service';
import { ContentService } from '../service/content.service';
import { RequestInfoUtilities } from 'angular-in-memory-web-api';
// import { exists } from 'fs';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {

  displayedColumns = ['Name', 'E-mail', 'Join Date', 'Role', 'Active', 'Action'];
  displayedColumnsUnGroup = ['Name', 'E-mail', 'Join Date', 'Role', 'Active'];
  displayedColumnsAddEmp = ['select', 'Name', 'Join Date', 'Role', 'Active'];
  displayedColumnsOther = ['Name', 'E-mail', 'Join Date', 'Active', 'Action'];
  dataSource : MatTableDataSource<User>;
  dataSourceAddEmp: MatTableDataSource<User>;
  selection = new SelectionModel<User>(true, []);

  @ViewChild(MatPaginator) paginator:MatPaginator;
  // @ViewChild(MatPaginator) paginatorAv: MatPaginator; TODO : fixing paginator
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatSort) sortAddEmp:MatSort;

  dtOptions: DataTables.Settings = {};

  dtTrigger: Subject<any> = new Subject();

  @Output() parentComponent: EventEmitter<boolean> = new EventEmitter<boolean>();
  users : User[];
  unGroupUsers : User [];
  userLength : number;
  // newUsers : User[]= new Array<User>();
  selectedUser : User;
  selectedUGUser : User;
  loggedInManager : User;

  groups : Group[];
  groupTemp : Group= new Group();
  groupDisp : Group[]= new Array<Group>();
  groupsAftFiltered : Group[] = new Array<Group>();
  allGroups : Group[] = new Array<Group>();
  otherGroups : Group[] = new Array<Group>();

  utility = Utility;
  contents : Content[];
  contentsDisp : Content[] = new Array<Content>();
  unableContentsDisp : Content[] = new Array<Content>();
  contentLength: number;
  selectedContent : Content;

  selectedGroup : Group;
  selectedNewGroup : Group;

  inputedCarrot = false;
  
  contentPaginate: any;
  page:number = 1;
  amountValue: number = 1;
  

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private groupService: GroupService,
    private transactionService: TransactionService,
    private contentService: ContentService,
    private appComponent: AppComponent,
    private location: Location,
    private changeDetectorRefs: ChangeDetectorRef,
    private router: Router
  ) { }

  ngOnInit() {
    this.groupDisp = new Array<Group>();
    this.selectedUGUser = new User;
    // this.newUsers = new Array<User>();
    this.loggedInManager = this.userService.getLoggedInUser();
    if (!this.loggedInManager)
      this.router.navigate(['/']);

    this.userService.fetchUsers()
      .subscribe(users => {
        this.users = users;
        this.userLength = users.length;
      });

    this.refresh();

    this.contentService.fetchContentsByType(1)
      .subscribe(conts => {
        this.contents = conts;
        this.contentLength = this.contents.length;
        // this.loadContents();
      });

      this.dtOptions = {}
    // this.loadContents();
  }

  applyFilter(filterValue: string, div: number) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    if (div == 1) {
      this.dataSource.filter = filterValue;
    } else if (div == 2){
      this.dataSourceAddEmp.filter = filterValue;
    }
  }
  
  refresh(){
    this.groupService.getAllGroups()
      .subscribe(groups => {
        this.allGroups = groups;
        if (!this.changeDetectorRefs['destroyed']) {
          this.changeDetectorRefs.detectChanges();
        }
      });

    this.groupService.getGroupByManID(this.loggedInManager.id)
      .subscribe(groups => {
        console.log(groups);
        this.groups = groups;
        // console.log("my groups", this.groups)
        // if (!this.changeDetectorRefs['destroyed']) {
          this.changeDetectorRefs.detectChanges();
        // }
        this.loadFirstGroup();
    });

    this.userService.fetchUGUsers()
      .subscribe(users => {
        this.unGroupUsers = users;
        if (!this.changeDetectorRefs['destroyed']) {
          this.changeDetectorRefs.detectChanges();
        }
      });
  }

  createMatTable(groups : any){
    if (groups.managerId) {
      this.dataSource = new MatTableDataSource(groups.members);
    } else {
      this.dataSource = new MatTableDataSource(groups);
    }
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sortAddEmp;
  }

  loadFirstGroup(): void {
    this.selection.clear();
    this.onSelectGroup(this.groups[0]);
  }

  loadUnmanagedUsers(){
    this.createMatTable(this.unGroupUsers);
  }

  loadOtherGroups(){
    let members : User[] = new Array<User>();
    this.otherGroups = new Array<Group>();
    this.allGroups.forEach(gr => {
      if (gr.managerId != this.loggedInManager.id) {
        this.otherGroups.push(gr);
        // console.log("og", this.otherGroups);
      }
    });
    // this.otherGroups.forEach(g => {
    //   this.createMatTable(g.members);
    // })

    for (let i = 0; i < this.otherGroups.length; i++) {
      for (let j = 0; j < this.otherGroups[i].members.length; j++) {
        members.push(this.otherGroups[i].members[j]);        
      }
    }

    
    this.createMatTable(members);
    // this.createMatTable(this.otherGroups);
    // console.log(members);
  }

  onSelect(user: User): void{
    console.log(user);
    this.selectedUser = user;
    
    this.groups.forEach(g => {
      if (g.id != this.selectedGroup.id) {
        this.groupsAftFiltered.push(g);
      }
    })

    this.userService.fetchUserPlus(user).subscribe(us => {
      this.selectedUser = us;
      this.selectedGroup = us.group;
    });
  }

  onSelectGroup(group: Group): void{
    console.log("group1 ", group);
    this.selectedGroup = group;
    console.log("group2 ", this.selectedGroup);
    this.createMatTable(this.selectedGroup.members);
  }

  onSelectNewGroup(group: Group): void{
    this.selectedNewGroup = group;
  }

  selectNewGroup(idEmployee : string, oldGroup: Group, newGroup: Group):void{
    newGroup.membersId.push(idEmployee);
    this.groupService.updateGroup(newGroup).subscribe();
    this.onRemoveEmployee();
    // this.selectedNewManagerGroup = newManagerGroup;
  }

  onRemoveEmployee():void{
    let membersId = [];
    // let members : User[] = new Array<User>();
    this.selectedGroup.membersId.forEach(mem => {
      if (mem != this.selectedUser.id) {
        membersId.push(mem);
      }
    });
    this.selectedGroup.membersId = membersId;
    this.selectedGroup.manager = null;
    this.groupService.updateGroup(this.selectedGroup).subscribe( sg =>{
      if (sg) {
        this.appComponent.showSuccess(this.selectedUser.name + " is now removed from " + sg.name);
        this.selectedGroup = sg;
        this.refresh();
      }
      else{
        this.appComponent.showSuccess("Failed to remove" + this.selectedUser.name);
      }
    });
    // this.unGroupUsers.push(this.selectedUser);
    console.log("removed");
  }

  carrotvalue : number;

  calCarrot(con : Content):void{
    // this.selectedContent = con;
    this.carrotvalue = con.carrot;
    // console.log(con.carrot);
    // console.log(this.selectedContent);
    let min : number = this.loggedInManager.carrot - con.carrot;
    if(min < 0 ){
      this.appComponent.showError("We cannot process your activity now. You need more carrots.");
      // this.selectedContent = null;
    }
    else{
      this.selectedContent = con;
      if (min <= 500) {
        this.appComponent.showWarning("You have " + min + " carrots left");
      }

    }
  }

  re(){
    this.selectedContent = null;
  }

  // selectContent( content : Content){
  //   this.selectedContent = content;
  // }

  // sendCarrot(description : string){
  sendCarrot(){
    if (!this.loggedInManager) {
      this.loggedInManager = this.userService.getLoggedInUser();
    }
    let senderId = this.loggedInManager.id;
    let receiverId = this.selectedUser.id;
    let type = Constanta.ACHIEVEMENT_TYPE;
    let contentId = this.selectedContent.id;
    let amount = this.carrotvalue;
    let description = this.selectedContent.description;

    if (amount > 0) {
      this.transactionService.postTransaction({type, senderId, receiverId, amount, description, contentId} as Transaction)
        .subscribe(res => {
          console.log(res);
          if (res) {
            this.loggedInManager.carrot -= amount;
            this.selectedUser.carrot += amount;
            this.appComponent.showSuccess('Send Success!');
          } else {
            this.appComponent.showError('Send Failed!');
          }
        });
      
    } else {
      this.appComponent.showError('Send Failed!');
    }
  }

  // ngAfterViewInit(){
  //   this.dtTrigger.next();
  // }

  onSubmit(){
    if (this.selection.selected.length < 1) {
        this.appComponent.showError("Please Choose At Least One Employee");
    } else{
      for (let i = 0; i < this.selection.selected.length; i++) {
        this.selectedGroup.membersId.push(this.selection.selected[i].id);
      }
      this.selectedGroup.manager = null;
      this.groupService.updateGroup(this.selectedGroup).subscribe(sg => {
        if (sg) {
          for (let i = 0; i < this.selection.selected.length; i++) {
            this.appComponent.showSuccess(this.selection.selected[i].name + " added to " + this.selectedGroup.name + " successfully");
          }
          this.selectedGroup = sg;
          // this.selection = null;
          this.refresh();
        } else {
            this.appComponent.showError("Adding Employee(s) unsuccess");
        }
      });
    }
  }

  // ----------------------------------------------------------------------S

  saveBirthdayStatus()
  {
    // this.selectedGroup.birthdayCarrotManager = amount;
    console.log("auto Carrot set : ", this.selectedGroup.autoCarrotManager);
    console.log("mount of Carrot set : ", this.selectedGroup.birthdayCarrotManager);
    this.groupService.updateGroup(this.selectedGroup).subscribe();
    alert("Birthday Carrot Updated");
  }

  loadContents(){
    let exist : boolean = false;
    this.contentsDisp = new Array<Content>();
    this.unableContentsDisp = new Array<Content>();
    this.contents.forEach(c => {
      if (c.deleted == false) {
        exist = false;
        c.groupsId.forEach(g => {
          if (g == this.selectedGroup.id){
            exist = true;
          }
        });
        if (exist == false) {
          this.unableContentsDisp.push(c);
        } else if (exist == true){
          this.contentsDisp.push(c);
        }
      }
    });
  }

  onSelectContent(cont : Content){
    this.selectedContent = cont;
    this.contentsDisp = this.contentsDisp.filter((cG) =>{
      return cG.id != cont.id;
    });
    this.unableContentsDisp.push(cont);
  }

  onRemoveContent(cont : Content){
    this.selectedContent = cont;
    this.unableContentsDisp = this.unableContentsDisp.filter((cG) => {
      return cG.id != cont.id;
    });
    this.contentsDisp.push(cont);
  }

  onSubmitContent(){
    let groupsId = [];
    for (let i = 0; i < this.unableContentsDisp.length; i++) {
      for (let j = 0; j < this.unableContentsDisp[i].groupsId.length; j++) {
        if (this.unableContentsDisp[i].groupsId[j] == this.selectedGroup.id) {
          groupsId.pop();
        }
      }
      this.unableContentsDisp[i].groupsId = groupsId;
      this.contentService.postContent(this.unableContentsDisp[i]).subscribe();
    }
    groupsId = [];
    for (let i = 0; i < this.contentsDisp.length; i++) {
      for (let j = 0; j < this.contentsDisp[i].groupsId.length; j++) {
        if (this.contentsDisp[i].groupsId[j] == this.selectedGroup.id) {
          groupsId.push(this.selectedGroup.id);
        }
      }
      this.contentsDisp[i].groupsId = groupsId;
      console.log(this.contentsDisp[i].name);
      this.contentService.postContent(this.contentsDisp[i]).subscribe();
    }
  }

  // loadUnGroupUsers(){
  //   this.selectedUGUser = new User;
  //   this.unGroupUsers = new Array<User>();
  //   // this.newUsers = new Array<User>();
  // }

  changeAmount(amount: number){
    this.amountValue = amount;
    if (this.amountValue > this.loggedInManager.carrot) {
      this.amountValue = this.loggedInManager.carrot;
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  loadAddEmpl(){
    this.dataSourceAddEmp = new MatTableDataSource(this.unGroupUsers);
  }
}
