import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';


import { User } from '../../model/user';
import { UserService } from '../../service/user.service';
import { TransactionService } from '../../service/transaction.service';
import * as Utility from '../../config/utility';
import * as Constanta from '../../config/constanta';

import { Transaction } from '../../model/transaction';
import { AppComponent } from '../../app.component';
import { ConfigService } from '../../service/config.service';
import { Config } from '../../model/config';

@Component({
  selector: 'app-birthday',
  templateUrl: './birthday.component.html',
  styleUrls: ['./birthday.component.css']
})
export class BirthdayComponent implements OnInit {
  @ViewChild('closeBtn') closeBtn: ElementRef;

  constructor(
    private userService: UserService,
    private transactionService: TransactionService,
    private configService: ConfigService,
    private appComponent: AppComponent,
  ) { }

  users: User[];
  users2: User[];
  users3: User[];
  loggedInUser: User;
  selectedUser: User;
  modalShare: boolean;
  amountValue: number = 1;
  isExchangeLoading = false;
  config: Config;

  ngOnInit() {
    this.loggedInUser = this.userService.getLoggedInUser();
    
    this.getUsers();
    this.config = this.configService.getConfigData();
    if (this.loggedInUser && this.loggedInUser.carrot < this.config.carrotLimit) {
      this.config.carrotLimit = this.loggedInUser.carrot;
    }
  }

  Constanta = Constanta;
  Utility = Utility;

  getUsers(){
    // this.users = this.userService.getAll();

    if (!this.users) {
      this.userService.fetchBirthdayUsers(0)
        .subscribe(users => {
          console.log(users);
          // for(let user of users){
          //   user.dob = Utility.transformDate(user.dob, 'longDate');
          //   user.joinDate = Utility.transformDate(user.joinDate, 'longDate');
          // }
          this.users = users;
          // this.userService.setAll(users);
        });
    }
    this.userService.fetchBirthdayUsers(-1)
      .subscribe(users => {
        console.log(users);
        // for(let user of users){
        //   user.dob = Utility.transformDate(user.dob, 'longDate');
        //   user.joinDate = Utility.transformDate(user.joinDate, 'longDate');
        // }
        this.users2 = users;
        // this.userService.setAll(users);
      });
    this.userService.fetchBirthdayUsers(-2)
      .subscribe(users => {
        console.log(users);
        // for(let user of users){
        //   user.dob = Utility.transformDate(user.dob, 'longDate');
        //   user.joinDate = Utility.transformDate(user.joinDate, 'longDate');
        // }
        this.users3 = users;
        // this.userService.setAll(users);
      });
  }

  selectUser(user: User, modalShare: boolean){

    if (!this.loggedInUser) {
      this.loggedInUser = this.userService.getLoggedInUser();
    }

    console.log(modalShare);
    this.selectedUser = user;
    if(modalShare)
      this.modalShare = true;
    else
      this.modalShare = false;
  }

  sendCarrot(description: string){
    this.isExchangeLoading = true;

    if (!this.loggedInUser) {
      this.loggedInUser = this.userService.getLoggedInUser();
    }
    let senderId = this.loggedInUser.id;
    let receiverId = this.selectedUser.id;
    let type = Constanta.SHARED_TYPE;
    let amount = this.amountValue;

    if (amount > 0) {
      this.transactionService.postTransaction({type, senderId, receiverId, amount, description} as Transaction)
        .subscribe(res => {
          console.log(res);
          this.isExchangeLoading = false;
          this.closeModal();
          if (res) {
            this.loggedInUser.carrot -= amount;
            this.selectedUser.carrot += amount;
            this.appComponent.showSuccess('Send Success!');
          } else {
            this.appComponent.showError('Send Failed!');
          }
        });
      
    } else {
      this.appComponent.showError('Send Failed!');
    }
  }

  changeAmount(amount: number){
    this.amountValue = amount;
    if (this.amountValue > this.config.carrotLimit) {
      this.amountValue = this.config.carrotLimit;
    }
    // console.log(this.amountValue);
  }

  closeModal(){
    this.closeBtn.nativeElement.click();
  }

}
