import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { User } from '../model/user';
import { UserService } from '../service/user.service';
import * as Utility from '../config/utility';
import * as Constanta from '../config/constanta';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  user: User;
  isAchievement = false;
  isBazaar = true;
  isSocial = false;
  isGroup = false;
  isBirthday = false;
  isLoading: boolean = false;

  ngOnInit() {
    this.isLoading = true;
    this.user = this.userService.getLoggedInUser();

    if(!this.user) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              console.log(res);
              this.user = res;
              this.userService.setLoggedInUser(res);
              this.isLoading = false;
              if (res.role != Constanta.EMPLOYEE_ROLE || res.admin)
                this.router.navigate(['/']);
            } else {
              this.isLoading = false;
              this.router.navigate(['/']);
            }
          })
      } else {
        this.isLoading = false;
        this.router.navigate(['/']);
      }
    } else
      this.isLoading = false;
  }

  openAchievement(){
    this.isAchievement = true;
    this.isBazaar = false;
    this.isSocial = false;
    this.isSocial = false;
    this.isGroup = false;
    this.isBirthday = false;
  }

  openBazaar(){
    this.isBazaar = true;
    this.isAchievement = false;
    this.isSocial = false;
    this.isSocial = false;
    this.isGroup = false;
    this.isBirthday = false;
  }

  openBirthday(){
    this.isBirthday = true;
    this.isAchievement = false;
    this.isBazaar = false;
    this.isSocial = false;
    this.isGroup = false;
  }

  openGroup(){
    this.isBirthday = false;
    this.isAchievement = false;
    this.isBazaar = false;
    this.isGroup = true;
    this.isSocial = false;
  }

  openSocial(){
    this.isSocial = true;
    this.isAchievement = false;
    this.isBazaar = false;
    this.isGroup = false;
    this.isBirthday = false;
  }

}
