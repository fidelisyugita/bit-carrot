import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Subject } from 'rxjs';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { Transaction } from '../../model/transaction';
import { TransactionService } from '../../service/transaction.service';
import { UserService } from '../../service/user.service';
import * as Utility from '../../config/utility';
import * as Constanta from '../../config/constanta';
import { User } from '../../model/user';

import * as $ from 'jquery';
import 'datatables.net';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {

  constructor(
    private transactionService: TransactionService,
    private userService: UserService,
    private router: Router
  ) { }

  user: User;
  transactions: Transaction[];
  selectedTransaction: Transaction;
  utility = Utility;


  dtOptions: DataTables.Settings = {};
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject();

  ngOnInit() {
    this.user = this.userService.getLoggedInUser();

    if(!this.user) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              console.log(res);
              this.user = res;
              this.userService.setLoggedInUser(res);
              if (res.role != Constanta.EMPLOYEE_ROLE || res.admin)
                this.router.navigate(['/']);
              else
                this.fetchData();
            } else
              this.router.navigate(['/']);
          });
      } else 
        this.router.navigate(['/']);
    } else {
      this.fetchData();
    }

    this.dtOptions = {
      // pagingType: 'full_numbers',
      // pageLength: 3
    };
  }

  fetchData(){
    this.getTransactions();

    this.userService.fetchUserPlus(this.user)
      .subscribe(user => {
        this.user = user;
        this.userService.setLoggedInUser(user);
      })
  }



  getTransactions(){
    // this.transactions = this.transactionService.getAll();
    // if (!this.transactions) {
      this.transactionService.fetchTransactionsByUser(this.user ? this.user.id : null)
        .subscribe(transactions => {
          console.log(transactions);
          // for(let transaction of transactions){
          //   transaction.dateOccured = Utility.transformDate(transaction.dateOccured, 'longDate');
          // }
          this.transactions = transactions;
          this.dtTrigger.next();
          // this.transactionService.setAll(transactions);
        });
    // }
  }

  // getType(type: number){
  //   return Utility.getType(type);
  // }

  // transformDate(date: string, format: string){
  //   return Utility.transformDate(date, format);
  // }

  selectTransaction(transaction: Transaction){
    this.selectedTransaction = transaction;
  }

}
