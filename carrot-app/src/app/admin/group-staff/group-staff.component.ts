import { Component, OnInit, AfterViewInit, group } from '@angular/core';
import { Router } from "@angular/router";

import { ViewChild } from '@angular/core'
import { NgModule } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Location } from '@angular/common';
import { CommonModule } from '@angular/common';
import { DatePipe } from '@angular/common';

import { Content } from '../../model/content';
import { ContentChange } from '../../model/content-change';
import { User } from '../../model/user';
import { Group } from '../../model/group';
import { Transaction } from '../../model/transaction';
import { Notification } from '../../model/notification';

import { AdminService } from '../../service/admin.service';
import { GroupService } from '../../service/group.service';
import { TransactionService } from '../../service/transaction.service';
import { NotificationService } from '../../service/notification.service';

import {AppComponent} from '../../app.component';

import * as Utility from '../../config/utility';
import * as Constanta from '../../config/constanta';

import { forEach } from '@angular/router/src/utils/collection';
import { OrderPipe } from 'ngx-order-pipe';
import { UserService } from '../../service/user.service';
import { DataTableDirective } from 'angular-datatables';

import { Subject } from 'rxjs';

import * as $ from 'jquery';
import 'datatables.net';

@Component({
  selector: 'app-group-staff',
  templateUrl: './group-staff.component.html',
  styleUrls: ['./group-staff.component.css']
})
export class GroupStaffComponent implements OnInit  {
  isLoading: boolean = false;

//   temp : ContentChange [];
//   changes : ContentChange [];
//   rewards : Content [];
  users : User [];
//   staff : User [];
  managers : User [] = [];
  seniorManagers : User [] = [];
  groups : Group [];
  loggedInUser: User;
  groupTemp: Group = new Group();

  currentGroupChoice:string = "Employee";

  loadingGroup: boolean=true;
  selectedGroup : Group;
  selectedUser : User = new User;

  dtOptionsGroup: DataTables.Settings = {};
  dtTriggerGroup: Subject<any> = new Subject();

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  constructor(
    private http: HttpClient,
    private adminService: AdminService,
    private groupService: GroupService,
    private userService: UserService,
    private transactionService: TransactionService,
    private notificationService: NotificationService,
    private appComponent: AppComponent,
    private location: Location,
    public datepipe: DatePipe,
    private orderPipe: OrderPipe,
    private router: Router
  ){}

  ngOnInit() 
  {
    this.loggedInUser = this.userService.getLoggedInUser();

    if(!this.loggedInUser) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              this.loggedInUser = res;
              this.userService.setLoggedInUser(res);
              this.isLoading = false;
              if (!res.admin)
                this.router.navigate(['/']);
            } else
            {
              this.isLoading = false;
              this.router.navigate(['/']);
            }
          })
      } else {
        this.isLoading = false;
        this.router.navigate(['/']);
      }
    }
      else 
        this.isLoading = false;

    this.getUsers();
    this.getUngroupedManager();
    this.getGroups();
  }

  getUsers()
  {
    return this.adminService.getUsers().subscribe(users=> {
      this.users = users
    });
  }

  getUngroupedManager()
  {
    this.managers = [];
    this.seniorManagers = [];
    return this.userService.fetchUsers().subscribe(users => {
      this.users = users;
      this.users.forEach((user) => {
        if(user.role == 4)
          this.managers.push(user);

        if(user.role == 8)
          this.seniorManagers.push(user);
      });
    })
  }

  getGroups()
  {
    this.groups = [];
    return this.groupService.getAllGroups().subscribe(groups => {
        this.groups = groups;
        this.groupService.getAllManagerGroups().subscribe(mGroups =>{
          mGroups.forEach((group) =>{
            this.groups.push(group);
          });
          this.loadingGroup = false;
          this.dtTriggerGroup.next();
        });
      });
  }

  addGroup(groupName:string)
  {
    if(groupName != "")
    { 
      if(this.selectedUser.id != undefined && this.selectedUser.id != "" && groupName != "")
      {
        let newGroup : Group = new Group;
        newGroup.name = groupName;
        newGroup.managerId = this.selectedUser.id;
        newGroup.membersId = [];
        newGroup.members = [];
        newGroup.autoCarrotAdmin = true;
        newGroup.birthdayCarrotAdmin = 0;
        if(this.currentGroupChoice == "Employee")
        {
          this.groupService.updateGroup(newGroup).subscribe(group => {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.groups.push(group);
              this.dtTriggerGroup.next();
            });
          });
        }
        else
        {
          this.groupService.updateManagerGroup(newGroup).subscribe(group => {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.groups.push(group);
              this.dtTriggerGroup.next();
            });
          });
        }

        this.appComponent.showSuccess(groupName + " Staff Group has been Added."); 
      }
      else
        this.appComponent.showError("Please select a Manager and fill the group name");
    }
  }

    
  onSelectUser(managerId : string):void
  {
    if((managerId != "--SELECT A MANAGER--" || "--SELECT A SENIOR MANAGER--" ) && managerId != "")
    {    
      this.selectedUser.id = managerId;
    }
  }

  groupOnSelect(group : Group):void
  {
    this.selectedGroup = group;

    this.groupTemp.id = group.id;
    this.groupTemp.name = group.name;
    this.groupTemp.managerId = group.managerId;
    this.groupTemp.membersId = group.membersId;
    this.groupTemp.autoCarrotAdmin = group.autoCarrotAdmin;
    this.groupTemp.birthdayCarrotAdmin = group.birthdayCarrotAdmin;
    this.groupTemp.autoCarrotManager = group.autoCarrotManager;
    this.groupTemp.birthdayCarrotManager = group.birthdayCarrotManager;
    this.groupTemp.creationDate = group.creationDate;
    this.groupTemp.active = group.active;
    this.groupTemp.manager = group.manager;
    this.groupTemp.members = group.members;
  }

  saveBirthdayStatus()
  {
    this.groupService.updateGroup(this.selectedGroup).subscribe();
    this.appComponent.showSuccess("Birthday Carrot has been Updated"); 
  }


  editGroupName():void
  {
    if(this.selectedGroup.manager)
    {
      this.groupService.updateGroup(this.selectedGroup).subscribe((group) =>{
        this.appComponent.showSuccess("Group name now become " + this.selectedGroup.name + "."); 
      });
    }
    else
    {
      this.groupService.updateManagerGroup(this.selectedGroup).subscribe((group) =>{
        this.appComponent.showSuccess("Group name now become " + this.selectedGroup.name + "."); 
      });
    }
  }

  checkGroupType(type)
  {
    this.currentGroupChoice = type.target.value;
  }

  cancelEdit()
  {
    this.selectedGroup.name = this.groupTemp.name;
    this.selectedGroup.autoCarrotAdmin = this.groupTemp.autoCarrotAdmin;
    this.selectedGroup.birthdayCarrotAdmin = this.groupTemp.birthdayCarrotAdmin;
  }

//   //---------------------------------

  refresh(): void {
    window.location.reload();
  }
}
