import { Component, OnInit, AfterViewInit, group } from '@angular/core';
import { Router } from "@angular/router";

import { ViewChild } from '@angular/core'
import { NgModule } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Location } from '@angular/common';
import { CommonModule } from '@angular/common';

import { User } from '../model/user';
import { Transaction } from '../model/transaction';
import { Notification } from '../model/notification';

import {AppComponent} from '../app.component';

import * as Utility from '../config/utility';
import * as Constanta from '../config/constanta';

import { UserService } from '../service/user.service';

import { Subject } from 'rxjs';

import * as $ from 'jquery';
import 'datatables.net';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})


export class AdminComponent implements OnInit
{
  isContent: boolean;
  isApproval: boolean;
  isStaff: boolean;
  isGroup: boolean;
  isNewsletter: boolean;
  isLoading: boolean = false;

  loggedInUser: User;

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private appComponent: AppComponent,
    private router: Router
  ){}

  ngOnInit() 
  {
    this.loggedInUser = this.userService.getLoggedInUser();

    if(!this.loggedInUser) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              this.loggedInUser = res;
              this.userService.setLoggedInUser(res);
              this.isLoading = false;
              if (!res.admin)
                this.router.navigate(['/']);
            } else
            {
              this.isLoading = false;
              this.router.navigate(['/']);
            }
          })
      } else {
        this.isLoading = false;
        this.router.navigate(['/']);
      }
    }
      else 
        this.isLoading = false;

    this.isContent = true;
    this.isApproval = false;
    this.isStaff = false;
    this.isGroup = false;
    this.isNewsletter = false;
  }


  openContent()
  {
    this.isContent = true;
    this.isApproval = false;
    this.isStaff = false;
    this.isGroup = false;
    this.isNewsletter = false;
  }

  openApproval()
  {
    this.isContent = false;
    this.isApproval = true;
    this.isStaff = false;
    this.isGroup = false;
    this.isNewsletter = false;
  }

  openStaff()
  {
    this.isContent = false;
    this.isApproval = false;
    this.isStaff = true;
    this.isGroup = false;
    this.isNewsletter = false;
  }

  openGroup()
  {
    this.isContent = false;
    this.isApproval = false;
    this.isStaff = false;
    this.isGroup = true;
    this.isNewsletter = false;
  }

  openNewsletter()
  {
    this.isContent = false;
    this.isApproval = false;
    this.isStaff = false;
    this.isGroup = false;
    this.isNewsletter = true;
  }
}
