import { Component, OnInit, AfterViewInit, group } from '@angular/core';
import { Router } from "@angular/router";

import { ViewChild } from '@angular/core'
import { NgModule } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { User } from '../../model/user';

import { AdminService } from '../../service/admin.service';
import { UserService } from '../../service/user.service';

import {AppComponent} from '../../app.component';

import * as Utility from '../../config/utility';
import * as Constanta from '../../config/constanta';
import { DataTableDirective } from 'angular-datatables';

import { Subject } from 'rxjs';

import * as $ from 'jquery';
import 'datatables.net';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit { 
  isLoading: boolean = false;

  users : User [];
  staff : User [];

  loggedInUser: User;
  loadingStaff: boolean=true;
  selectedUser : User = new User;

  dtOptionsStaff: DataTables.Settings = {};
  dtTriggerStaff: Subject<any> = new Subject();

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  constructor(
    private http: HttpClient,
    private adminService: AdminService,
    private userService: UserService,
    private appComponent: AppComponent,
    private router: Router
  ){}

  ngOnInit() 
  {
    this.loggedInUser = this.userService.getLoggedInUser();

    if(!this.loggedInUser) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              this.loggedInUser = res;
              this.userService.setLoggedInUser(res);
              this.isLoading = false;
              if (!res.admin)
                this.router.navigate(['/']);
            } else
            {
              this.isLoading = false;
              this.router.navigate(['/']);
            }
          })
      } else {
        this.isLoading = false;
        this.router.navigate(['/']);
      }
    }
      else 
        this.isLoading = false;

    this.getUsers();
  }

  getUsers()
  {
    this.staff = [];
    return this.adminService.getUsers().subscribe(users=> {
      this.users = users
      this.users.forEach((user) => {
        if(user.role == 4 || user.role == 5)
        {
          this.staff.push(user);
        }
      });
      this.loadingStaff = false;
      this.dtTriggerStaff.next();
    });
  }

  getDate(type: string):string
  {
    return Utility.transformDate(type, 'dd-MM-yyyy');
  }

  getRoleType(type : number):string
  {
    return Utility.getRole(type);
  }

  getUserNameById(id:string):string
  {
    if(this.users)
    {
      let name: string = "";
      this.users.forEach(user => {
        if(user.id == id)
          name = user.name;
      });
      return name;
    }
  }
  
  onSelectUser(managerId : string):void
  {
    if(managerId != "--SELECT A MANAGER--" && managerId != "")
    {    
      this.selectedUser.id = managerId;
    }
  }

  setLate(user: User)
  {
    if(user.alwaysLate == true)
    {    
      user.alwaysLate = false;    
      this.appComponent.showSuccess(user.name + " is now flagged 'Not Late'."); 
    }
    else
    {
      this.appComponent.showSuccess(user.name + " is now flagged 'Late'."); 
      user.alwaysLate = true;
    }

    this.userService.addEdit(user).subscribe();
  }

  refresh(): void {
    window.location.reload();
  }
}
