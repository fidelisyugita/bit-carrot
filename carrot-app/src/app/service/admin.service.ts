import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DatePipe } from '@angular/common'

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Content } from '../model/content';
import { ContentChange } from '../model/content-change';
import { User } from '../model/user';
import * as Constanta from '../config/constanta';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AdminService 
{
  private rewardUrl = Constanta.API_URL + 'content/';
  private rewardUrlPlus = Constanta.API_URL + 'content/plus';
  //private approvalUrl = 'api/adminApprovals';
  private userUrl = Constanta.API_URL + 'user/plus';

  constructor(
    private http: HttpClient,
    public datepipe: DatePipe) { }

  getRewards(): Observable<Content[]> 
  {
    return this.http.get<Content[]>(this.rewardUrlPlus);
  }

  getUsers(): Observable<User[]> 
  {
    return this.http.get<User[]>(this.userUrl);
  }
  // getApprovals(): Observable<AdminApproval[]> 
  // {
  //   return this.http.get<AdminApproval[]>(this.approvalUrl);
  // }

  addReward (reward: Content): Observable<Content> 
  {
    console.log(reward);
    return this.http.post<Content>(this.rewardUrl, reward, httpOptions);
  }

  deleteReward(content: Content): Observable<Content>  
  {
    console.log(content);
    return this.http.post<Content>(this.rewardUrl, content, httpOptions);
  }
}