import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,  } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { fromPromise } from 'rxjs/observable/fromPromise';

import { Notification } from '../model/notification';
import * as Utility from '../config/utility';
import * as Constanta from '../config/constanta';

@Injectable()
export class NotificationService {
  private notificationUrl = Constanta.API_URL + 'notification';
  private notificationByUserUrl = Constanta.API_URL + 'notification/user/';

  constructor(private http: HttpClient) { 
    // // Create an Observable out of a promise
    // const data = fromPromise(fetch(this.getAllUsersUrl));
    // // Subscribe to begin listening for async result
    // data.subscribe({
    //   next(response) { console.log(response); },
    //   error(err) { console.error('Error: ' + err); },
    //   complete() { console.log('Completed'); }
    // });
  }

  getAll(): Observable<Notification[]> {
    try {
      let res = this.http.get<Notification[]>(this.notificationUrl);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  getByUser(userId: string): Observable<Notification[]> {
    try {
      let res = this.http.get<Notification[]>(this.notificationByUserUrl + userId);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  addEdit(notification: Notification): Observable<Notification> {
    try {
      let res = this.http.post<Notification>(this.notificationUrl, notification);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
    // return this.http.post<User>(this.signUpUrl, user, httpOptions)
    //   .pipe(
    //     catchError(this.handleError('signUp', user))
    //   );
  }

}
