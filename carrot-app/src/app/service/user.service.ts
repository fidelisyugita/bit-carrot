import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,  } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { fromPromise } from 'rxjs/observable/fromPromise';

import { User } from '../model/user';
import * as Utility from '../config/utility';
import * as Constanta from '../config/constanta';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {
  
  private userUrl = Constanta.API_URL + 'user/';
  private userPlusUrl = Constanta.API_URL + 'user/plus';
  private birthdayUsersUrl = Constanta.API_URL + 'user/birthday/';
  private usersBySupervisorUrl = Constanta.API_URL + 'user/supervisor/';
  private loginUrl = Constanta.API_URL + 'session/login';
  private loginWithTokenUrl = Constanta.API_URL + 'session/token';
  private uGUserUrl = Constanta.API_URL + 'user/ungroup-employee';
  private ungrouppedManagerUrl = Constanta.API_URL + 'user/ungroup-manager';

  users: User[];
  loggedInUser: User = null;

  constructor(private http: HttpClient) { 
    // // Create an Observable out of a promise
    // const data = fromPromise(fetch(this.getAllUsersUrl));
    // // Subscribe to begin listening for async result
    // data.subscribe({
    //   next(response) { console.log(response); },
    //   error(err) { console.error('Error: ' + err); },
    //   complete() { console.log('Completed'); }
    // });
  }

  // getAll(): User[] {
  //   return this.users;
  // }

  // setAll(users: User[]): void {
  //   if (users && users.length > 0) {
  //     this.users = users;
  //   }
  // }

  logout(): void {
    this.loggedInUser = null;
    localStorage.removeItem(Constanta.TOKEN_KEY_NAME);
  }

  setLoggedInUser(user: User): void {
    // user.subscribe(user => {
    //   console.log(user);
      // if(user && user.id){
        this.loggedInUser = user;
      // }
    // });
  }

  getLoggedInUser(): User {
    return this.loggedInUser;
  }

  getUngroupManager(): Observable<User[]>
  {
    return this.http.get<User[]>(this.ungrouppedManagerUrl);   
  }

  fetchUsers(): Observable<User[]> {
    try {
      let res = this.http.get<User[]>(this.userUrl);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  fetchUsersById(id : string): Observable<User> {
    try {
      let res = this.http.get<User>(this.userUrl + id);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  fetchUsersBySupervisor(supervisorId: string): Observable<User[]> {
    try {
      let res = this.http.get<User[]>(this.usersBySupervisorUrl + supervisorId);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  fetchBirthdayUsers(day: number): Observable<User[]> {
    if(!day)
      day = 0;
    try {
      let res = this.http.get<User[]>(this.birthdayUsersUrl + day);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  fetchUsersPlus(): Observable<User[]> {
    try {
      let res = this.http.get<User[]>(this.userPlusUrl);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  fetchUserPlus(user: User): Observable<User> {
    try {
      let res = this.http.post<User>(this.userPlusUrl, user);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  fetchUGUsers(): Observable<User[]> {
    try {
      let res = this.http.get<User[]>(this.uGUserUrl);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  /* POST */
  login(user: User): Observable<User> {
    try {
      let res = this.http.post<User>(this.loginUrl, user);
      console.log(res);
      // this.setLoggedInUser(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
    // return this.http.post<User>(this.loginUrl, myParams)
    //   .pipe(
    //     catchError(this.handleError<User>('login'))
    //   );
  }
  
  /* POST */
  loginWithToken(token: string): Observable<User> {
    try {
      const formData: FormData = new FormData();
      formData.append('token', token);
      // return this.http.post<User>(this.loginWithTokenUrl, formData)
      //   .map(res => {
      //     return res;
      //   });
      return this.http.post<User>(this.loginWithTokenUrl, formData);
      // this.setLoggedInUser(res);
      // return res;
    } catch (error) {
      console.log(error);
      return null;;
    }
    // return this.http.post<User>(this.loginUrl, myParams)
    //   .pipe(
    //     catchError(this.handleError<User>('login'))
    //   );
  }

  /**
   * 
   * @param user 
   * can be used for add or update
   */
  addEdit(user: User): Observable<User> {
    user.supervisor = null;
    try {
      let res = this.http.post<User>(this.userUrl, user, httpOptions);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
    // return this.http.post<User>(this.signUpUrl, user, httpOptions)
    //   .pipe(
    //     catchError(this.handleError('signUp', user))
    //   );
  }

  uploadFile(file: File) {
    try {
      const formData: FormData = new FormData();
      formData.append('file', file);
      formData.append('upload_preset', Constanta.UNSIGNED_UPLOAD_PRESET);

      let res = this.http.post(Constanta.UPLOAD_URL, formData);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }

    // var xhr = new XMLHttpRequest();
    // var fd = new FormData();
    // xhr.open('POST', url, true);
    // xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  
    // // // Reset the upload progress bar
    // //  document.getElementById('progress').style.width = 0;

    // // // Update progress (can be used to show progress indicator)
    // xhr.upload.addEventListener("progress", function(e) {
    //   // var progress = Math.round((e.loaded * 100.0) / e.total);
    //   // document.getElementById('progress').style.width = progress + "%";
  
    //   console.log(`fileuploadprogress data.loaded: ${e.loaded}, data.total: ${e.total}`);
    // });
  
    // xhr.onreadystatechange = function(e) {
    //   if (xhr.readyState == 4 && xhr.status == 200) {
    //     // File uploaded successfully
    //     var response = JSON.parse(xhr.responseText);
    //     console.log(response);
    //     // https://res.cloudinary.com/cloudName/image/upload/v1483481128/public_id.jpg
    //     var url = response.secure_url;
    //     // // Create a thumbnail of the uploaded image, with 150px width
    //     // var tokens = url.split('/');
    //     // tokens.splice(-2, 0, 'w_150,c_scale');
    //     // var img = new Image(); // HTML5 Constructor
    //     // img.src = tokens.join('/');
    //     // img.alt = response.public_id;
    //     // document.getElementById('gallery').appendChild(img);
    //   }
    // };
  
    // fd.append('upload_preset', Constanta.UNSIGNED_UPLOAD_PRESET);
    // // fd.append('tags', 'browser_upload'); // Optional - add tag for image admin in Cloudinary
    // fd.append('file', file);
    // xhr.send(fd);
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
