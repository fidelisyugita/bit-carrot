import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Content } from '../model/content';
import * as Utility from '../config/utility';
import * as Constanta from '../config/constanta';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ContentService {
  private contentsUrl = Constanta.API_URL + 'content/';
  private contentsByTypeUrl = Constanta.API_URL + 'content/type/';
  private contentPlusUrl = Constanta.API_URL + 'content/plus/';

  contents: Content[];

  constructor(private http: HttpClient) { 
    
    // if (!this.contents) {
    //   this.fetchContents()
    //   .subscribe(contents => {
    //     console.log(contents);
    //     for(let content of contents){
    //       content.expired = Utility.transformDate(content.expired, 'longDate');
    //     }
    //     this.contents = contents;
    //   });
    // }

  }

  // getAll(): Content[] {
  //   return this.contents;
  // }

  fetchContents(): Observable<Content[]> {
    try {
      let res = this.http.get<Content[]>(this.contentsUrl);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  fetchContentsByType(type: number, groupId: string = null): Observable<Content[]> {
    let url = this.contentsByTypeUrl + type + '/';
    if(groupId)
      url += groupId;
    
    console.log(url);

    try {
      let res = this.http.get<Content[]>(url);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  fetchContentPlus(type: number, userId: string): Observable<Content[]> {
    let url = this.contentPlusUrl + type + '/' + userId;
    
    console.log(url);

    try {
      let res = this.http.get<Content[]>(url);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  postContent(content: Content): Observable<Content> {
    try {
      let res = this.http.post<Content>(this.contentsUrl, content, httpOptions);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

}