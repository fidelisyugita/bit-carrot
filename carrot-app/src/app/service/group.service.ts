import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Group } from '../model/group';
import * as Constanta from '../config/constanta';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class GroupService {
  private groupsUrl = Constanta.API_URL + 'group/';
  private groupByManIDUrl = Constanta.API_URL + 'group/supervisor/';
  private managerGroupsUrl = Constanta.API_URL + 'manager-group/';

  constructor(private http: HttpClient) { }

  getAllGroups(): Observable<Group[]> {
    try {
      let res = this.http.get<Group[]>(this.groupsUrl);
      return res;
    } catch (error) {
      return error;
    }
  } 
  
  getAllManagerGroups(): Observable<Group[]> {
    try {
      let res = this.http.get<Group[]>(this.managerGroupsUrl);
      return res;
    } catch (error) {
      return error;
    }
  }

  getGroupById(id: string): Observable<Group> 
  {
    let url = this.groupsUrl + id;
    return this.http.get<Group>(url);
  }

  getGroupByManID(manID : string): Observable<Group[]> {
    try {
      let res = this.http.get<Group[]>(this.groupByManIDUrl+manID);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  updateGroup(group: Group): Observable<Group> 
  {
    return this.http.post<Group>(this.groupsUrl, group, httpOptions);
  }

  updateManagerGroup(group: Group): Observable<Group> 
  {
    return this.http.post<Group>(this.managerGroupsUrl, group, httpOptions);
  }
}
