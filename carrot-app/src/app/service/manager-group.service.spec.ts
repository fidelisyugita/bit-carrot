import { TestBed, inject } from '@angular/core/testing';

import { ManagerGroupService } from './manager-group.service';

describe('ManagerGroupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ManagerGroupService]
    });
  });

  it('should be created', inject([ManagerGroupService], (service: ManagerGroupService) => {
    expect(service).toBeTruthy();
  }));
});
