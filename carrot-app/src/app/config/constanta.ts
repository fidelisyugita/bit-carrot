// export const API_URL: string = "http://172.19.12.131:8846/api/"; //mitrais5
// export const API_URL: string = "http://172.19.13.46:8846/api/";  //mitrais
// export const API_URL: string = "http://192.168.26.2:8846/api/"; //byod
// export const API_URL: string = "https://carrot.mybluemix.net/api/";
export const API_URL: string = "/api/"; //build

// export const STAFF_ROLE = 1;
// export const ADMIN_ROLE = 2;
//	export const UNKNOWN_ROLE = 3;
export const MANAGER_ROLE = 4;
export const EMPLOYEE_ROLE = 5;
export const ROOT_ADMIN_ROLE = 6;
//	export const STAKEHOLDER_ROLE = 7;
export const SENIOR_MANAGER_ROLE = 8;


export const ACHIEVEMENT_TYPE: number = 1;
export const REWARD_TYPE: number = 2;
export const SOCIAL_TYPE: number = 3;
export const SHARED_TYPE: number = 4;

export const BIRTHDAY_NOTIF: number = 1;
export const NEWSLETTER_NOTIF: number = 2;
export const ADMIN_NOTIF: number = 3;

export const UNSIGNED_UPLOAD_PRESET = 'lurfv21d';
export const UPLOAD_URL = `https://api.cloudinary.com/v1_1/huzakerna/image/upload`;

export const TOKEN_KEY_NAME = 'carrot-token';