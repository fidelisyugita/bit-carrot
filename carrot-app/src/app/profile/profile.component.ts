import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Location } from '@angular/common';

import { User } from '../model/user';
import { UserService } from '../service/user.service';
import * as Constanta from '../config/constanta';
import * as Utility from '../config/utility';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(
    private userService: UserService,
    private location: Location,
    private router: Router
  ) { }

  user: User;
  errMsg;
  fileToUpload: File = null;

  ngOnInit() {
    // this.user = this.userService.getLoggedInUser();
    // if (!this.user)
    //   this.router.navigate(['/']);


    this.user = this.userService.getLoggedInUser();

    if(!this.user) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              console.log(res);
              this.user = res;
              this.userService.setLoggedInUser(res);
              // if (res.role != Constanta.EMPLOYEE_ROLE || res.admin)
              //   this.router.navigate(['/']);
            } else {
              this.router.navigate(['/']);
            }
          })
      } else {
        this.router.navigate(['/']);
      }
    }
  }

  Constanta = Constanta;
  Utility = Utility;
  isLoading: boolean = false;

  update(oldPassword: string, newPassword: string, confirmPassword: string): void{

    if (!Utility.isValidEmail(this.user.email)){
      this.errMsg = 'Email is invalid!';
      return;
    }

    if (oldPassword != this.user.password) {
      this.errMsg = 'Old password is invalid!';
      return;
    }
    if (confirmPassword != newPassword) {
      this.errMsg = 'New password is not equal!';
      return;
    }

    if (newPassword.length < 1) {
      this.errMsg = 'Please input new password!';
      return;
    }

    this.user.password = newPassword;

    this.updateUser();
    // this.userService.signUp(this.user)
    //   .subscribe(user => {
    //     if (user)
    //       this.location.back();
    //     else
    //       this.errMsg = 'Failed to update';
    //   });
  }

  updateAddress(address : string){
    this.user.address = address;
    this.updateUser();
  }

  back(){
    this.location.back();
  }

  updateUser(){
    this.isLoading = true;
    console.log(this.user);

    if (this.fileToUpload) {
      this.userService.uploadFile(this.fileToUpload)
        .subscribe(res => {
          console.log(res);
          if (this.user && res) {
            this.user.picture = res.url as string;

            this.userService.addEdit(this.user)
              .subscribe(user => {
                console.log(user);
                if (user)
                  this.router.navigate(['/']);
                else
                  this.errMsg = 'Failed to update';
      
                this.isLoading = false;
              });
          } else {
            this.errMsg = 'Failed to upload photo';
            this.isLoading = false;
          }
        });
    } else {
      this.userService.addEdit(this.user)
        .subscribe(user => {
          console.log(user);
          if (user)
            this.router.navigate(['/']);
          else
            this.errMsg = 'Failed to update';

          this.isLoading = false;
        });
    }
  }

  handleFileInput(files: FileList){
    // this.isLoading = true;
    console.log(files);
    if (files.length < 1) {
      // this.isLoading = false;
      return;
    }

    this.fileToUpload = files[0];

    // this.userService.uploadFile(this.fileToUpload)
    //   .subscribe(res => {
    //     console.log(res);
    //     if (this.user && res) {
    //       this.user.picture = res.url as string;
    //       this.isLoading = false;
    //       // this.updateUser();
    //     } else {
    //       this.errMsg = 'Failed to upload photo';
    //       this.isLoading = false;
    //     }
    //   });
  }

}
