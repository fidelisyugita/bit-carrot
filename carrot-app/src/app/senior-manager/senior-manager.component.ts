import { Component, OnInit, group, EventEmitter, AfterViewInit, ViewChild, Output, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from "@angular/router";
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';


import { Location } from '@angular/common';
import { User } from '../model/user';
import { Group } from '../model/group';
import { ManagerGroup } from "../model/manager-group";
import { UserService } from '../service/user.service';
import { GroupService } from '../service/group.service';
import { ManagerGroupService } from '../service/manager-group.service';
import { ManagerService } from '../service/manager.service';
import { TransactionService } from '../service/transaction.service'
import { AppComponent } from '../app.component';
import * as Constanta from '../config/constanta';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Transaction } from '../model/transaction';
import * as Utility from '../config/utility';


@Component({
  selector: 'app-senior-manager',
  templateUrl: './senior-manager.component.html',
  styleUrls: ['./senior-manager.component.css']
})
export class SeniorManagerComponent implements OnInit {

  displayedColumns = ['Name', 'E-mail', 'Join Date', 'Role', 'Active', 'Action'];
  displayedColumnsAvManager = ['Name', 'E-mail', 'Join Date', 'Role', 'Active'];
  dataSource : MatTableDataSource<User>;

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort:MatSort;


  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger: Subject<any> = new Subject();

  // @Output() parentComponent: EventEmitter<boolean> = new EventEmitter<boolean>();
  
  myGroups : ManagerGroup[];

  utility = Utility;

  users : User[];
  groups : Group[];
  managerStaffGroups = [];
  managerGroups : ManagerGroup[];
  managers : User[];
  unGroupManagers : User[];
  newManagers : User[] = new Array<User>();
  groupsAftFiltered : ManagerGroup[] = new Array<ManagerGroup>();
  loggedInManager : User;

  groupLength : number;
  manGroupLength : number;
  manLength : number;


  selectedUser : User;
  selectedUnGroupManager : User;
  selectedManagerGroup : ManagerGroup;
  selectedNewManagerGroup : ManagerGroup;
  selectedManager : User;
  amountValue: number = 0;

  inputedCarrot = false;
  
  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private groupService: GroupService,
    private managerGroupService: ManagerGroupService,
    private managerService: ManagerService,
    private transactionService: TransactionService,
    private appComponent: AppComponent,
    private location: Location,
    private changeDetectorRefs: ChangeDetectorRef,
    private router: Router
  ) { }

  ngOnInit() {
    this.loggedInManager = this.userService.getLoggedInUser();
    if (!this.loggedInManager)
      this.router.navigate(['/']);

      
    this.loggedInManager = this.userService.getLoggedInUser();

    this.managerGroupService.getAllManGroups()
      .subscribe(managerGroups =>{
        console.log(managerGroups);
        this.managerGroups = managerGroups;
      });

    this.managerService.getAllManager()
      .subscribe(managers =>{
        console.log(managers);
        this.managers = managers;
        this.manLength = this.managers.length;
      });

      // used ----------------------------------------------------------------
    this.refresh();

    // this.loadOtherGroups();

  }

  // used --------------------------------------------------------------------
  refresh(){
    this.managerGroupService.fetchMyGroup(this.loggedInManager.id)
      .subscribe(manGroup => {
        this.myGroups = manGroup;
        // console.log("my group", this.myGroups);
        this.loadFirstGroup();
        this.changeDetectorRefs.detectChanges();
      });

    this.managerService.getUnGroupManagers()
      .subscribe(managers =>{
        this.unGroupManagers = managers;
        this.changeDetectorRefs.detectChanges();
        // this.loadGroupAvManager();
      });
  }

  createMatTable(users : User[]){
    this.dataSource = new MatTableDataSource(users);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  onSelectGroup(group : ManagerGroup):void{
    this.selectedManagerGroup = group;
    this.createMatTable(group.members);
    this.myGroups.forEach(mg => {
      if (mg.id != this.selectedManagerGroup.id) {
        this.groupsAftFiltered.push(mg);        
      }
    })
  }

  onSelect(manager: User):void{
    this.selectedManager = manager;
    this.groups = [];
    this.groupService.getGroupByManID(this.selectedManager.id).subscribe(gr => {
      this.groups = gr;
    });
  }

  loadFirstGroup(){
    this.selectedManagerGroup = this.myGroups[0];
    this.createMatTable(this.selectedManagerGroup.members);
  }

      // -------------- Add Manager ------------------------------------------
  onSelectManagerToMove(manager : User){
    this.selectedUnGroupManager = manager;
    this.unGroupManagers = this.unGroupManagers.filter((man) => {
      return man.id != manager.id
    });
    this.newManagers.push(manager);
    console.log(this.newManagers);
  }

  onRemoveManagerToMove(manager: User){
    this.selectedUnGroupManager = manager;
    this.onRemoveManagerToMoves(this.selectedUnGroupManager);
  }

  onRemoveManagerToMoves(manager: User){
    this.newManagers = this.newManagers.filter((man) => {
      return man.id != manager.id
    });
    this.unGroupManagers.push(manager);
  }

  onSubmit(){
    this.newManagers.forEach(nm => {
      this.selectedManagerGroup.membersId.push(nm.id);
    })
    // this.parentComponent.emit(true);
    this.managerGroupService.addEdit(this.selectedManagerGroup).subscribe( sm => {
      this.selectedManagerGroup = sm;
      if (sm) {
          this.appComponent.showSuccess("SuccessFully added");
      } else {
        this.appComponent.showError("Unsuccessfully");
      }
      this.refresh();
    });
    this.newManagers = null;
  }

  onRemoveManager():void{
    let membersId = [];
    this.unGroupManagers.push(this.selectedManager);
    if (this.selectedManagerGroup.members.length > 0) {
      this.selectedManagerGroup.members.forEach(member => {
        if (this.selectedManager.id != member.id) {
          membersId.push(member.id);
        }
      });
      this.selectedManagerGroup.membersId = membersId;
      this.managerGroupService.addEdit(this.selectedManagerGroup).subscribe(man => {
        this.selectedManagerGroup = man;
        if (man) {
          this.appComponent.showSuccess("Successfully Removed");
        } else {
          this.appComponent.showError('Unsuccessfully Removed');
        }
        this.refresh();
      });
    }
  };

  loadOtherGroups(){
    let manGroup = new Array<ManagerGroup>();
    this.managerGroups.forEach(mg => {
      if (mg.seniorManagerId != this.loggedInManager.id) {
        manGroup.push(mg);
      }
    })
    this.managerGroups = new Array<ManagerGroup>();
    this.managerGroups = manGroup;
    let mana = new ManagerGroup;
    this.managerGroups.forEach(mg => {
      mg.members.forEach(mem => {
        mana.members.push(mem);
      })
    })
    // this.createMatTable(mana.members);
  }
  // --- --------------------------------------------------------------------

  loadGroupAvManager(){
    let membersid=[];
    let members = new Array<User>();
    for (let i = 0; i < this.unGroupManagers.length; i++) {
      membersid.push(this.unGroupManagers[i].id);
      members.push(this.unGroupManagers[i]);
    }
    this.createMatTable(members);
  }

  onSelectNewManagerGroup(managerGroup: ManagerGroup): void{
    this.selectedNewManagerGroup = managerGroup;
  }

  selectManagerGroup(managerGroup: ManagerGroup): void{
    this.selectedManagerGroup = new ManagerGroup;
    this.selectedManagerGroup = managerGroup;
    let groupMembers = new Array<User>();
    this.selectedManagerGroup.members.forEach(member => {
      groupMembers.push(member);
    });
  }

  selectNewManagerGroup():void{
    this.selectedNewManagerGroup.membersId.push(this.selectedManager.id);
    this.managerGroupService.addEdit(this.selectedNewManagerGroup);
    this.onRemoveManager();
  }
  
  carrotvalue : number;
  calCarrot(value: number):void{
    this.carrotvalue = value;
    console.log(value);
    if (value > 0)
      this.inputedCarrot = true;
    else 
      this.inputedCarrot = false;
  }

  sendCarrot(description : string){
    if (!this.loggedInManager) {
      this.loggedInManager = this.userService.getLoggedInUser();
    }
    let senderId = this.loggedInManager.id;
    let receiverId = this.selectedManager.id;
    let type = Constanta.SHARED_TYPE;
    let amount = this.amountValue;

    if (amount > 0) {
      this.transactionService.postTransaction({type, senderId, receiverId, amount, description} as Transaction)
        .subscribe(res => {
          console.log(res);
          if (res) {
            this.loggedInManager.carrot -= amount;
            this.selectedManager.carrot += amount;
            this.appComponent.showSuccess('Send Success!');
          } else {
            this.appComponent.showError('Send Failed!');
          }
        });
      
    } else {
      this.appComponent.showError('Send Failed!');
    }
  }

  moveToNoSelected(idManager : string):void{
    let groupsAftFiltered: ManagerGroup[] = new Array <ManagerGroup>();
    // this.groupsAftFiltered = (this.managerGroups);
    console.log(idManager);

    for (let index = 0; index < this.manGroupLength; index++) {
      if (this.managerGroups[index].id != idManager) {
        console.log(this.managerGroups[index]);
        groupsAftFiltered.push(this.managerGroups[index]);
      }
      
    }
    console.log(this.groupsAftFiltered);
    this.groupsAftFiltered = groupsAftFiltered;

  }


  changeAmount(amount: number){
    this.amountValue = amount;
    if (this.amountValue > this.loggedInManager.carrot) {
      this.amountValue = this.loggedInManager.carrot;
    }
  }

  applyFilter(filterValue: string, div: number) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
}
