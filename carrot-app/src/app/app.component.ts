import { Component, ViewContainerRef } from '@angular/core';
import { Router } from "@angular/router";

import { User } from './model/user';

import { Notification } from './model/notification';
import { NotificationService } from './service/notification.service';
import { UserService } from './service/user.service';

import * as Constanta from './config/constanta';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ConfigService } from './service/config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(
    private notificationService: NotificationService,
    private userService: UserService,
    private router: Router,
    private configService: ConfigService,
    public toastr: ToastsManager, 
    vcr: ViewContainerRef) { 
      this.toastr.setRootViewContainerRef(vcr)}

  notifications: Notification[];
  // loggedInUser: User;
  selectedNotification: Notification;
  isLoading: boolean = false;

  ngOnInit() {
    // this.notificationService.getAll()
    // .subscribe(notifications => {
    //   console.log(notifications);
    //   this.notifications = notifications;
    // });
    this.configService.getConfigData();
  }

  logout(){
    this.userService.logout();
    this.router.navigate(['/']);
  }

  isUserActive(): boolean{
    if(this.userService.getLoggedInUser())
      return true;
    if(localStorage.getItem(Constanta.TOKEN_KEY_NAME))
      return true;
    return false;  
  }

  onSelectNotification(notification: Notification){
    if (notification.type != Constanta.BIRTHDAY_NOTIF) {
      this.selectedNotification = notification;
      console.log(notification);
    } else {
      this.selectedNotification = null;
    }
  }

  showSuccess(text:string) {
    this.toastr.success(text);
  } 
  
  showError(text:string) {
    this.toastr.error(text);
  }

  showWarning(text:string){
    this.toastr.warning(text);
  }

  openNotif(){
    this.isLoading = true;
    let user = this.userService.getLoggedInUser();
    if (user) {
      this.notificationService.getByUser(user.id)
      .subscribe(notifications => {
        if(notifications){
          console.log(notifications);
          this.notifications = notifications.slice(0, 10);
          this.isLoading = false;
        }
      });
    } else
      this.isLoading = false;

  }

}
