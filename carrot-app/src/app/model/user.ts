import { Group } from "./group";

export class User {
  id: string;
  email: string;
  password: string;
  name: string;
  picture: string;
  dob: string;
  address: string;
  joinDate: string;
  role: number;
  carrot: number;
  supervisorId: string;
  groupId: string;
  creationDate: string;
  settings: Map<string, string>;
  groupMember: boolean;
  admin: boolean;
  approved: boolean;
  active: boolean;
  alwaysLate: boolean;
  
	carrotOfTheMonth: number;
	carrotOfTheYear: number;
	carrotToReward: number;
	carrotToSocial: number;
  carrotToOther: number;
  
  supervisor: User;
  group: Group;

  token: string;
}