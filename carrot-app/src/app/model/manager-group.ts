import { User } from "./user";

export class ManagerGroup {
  id: string;
  name: string;
  seniorManagerId: string;
  membersId: string[] = new Array<string>();
  creationDate: string;
  active: boolean;

  seniorManager: User;
  members: User[];
}