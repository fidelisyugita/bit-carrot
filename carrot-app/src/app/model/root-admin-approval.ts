export class RootAdminApproval {
    id : number
    type : string
    subject : string
    carrotAmount : number
    carrotTarget : number
    dateRequest : string
    status : string
 }