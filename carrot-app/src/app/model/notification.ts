export class Notification {
    id : string;
    type : number;
    adminId : string;
    userId : string;
    title : string;
    description : string;
    creationDate : string;
 }