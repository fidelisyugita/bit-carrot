import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { EmployeeComponent } from './employee/employee.component';
import { RegisterComponent } from './register/register.component';
import { ManagerComponent } from './manager/manager.component';
import { AdminComponent } from './admin/admin.component';
import { RootAdminComponent } from './root-admin/root-admin.component';
import { SeniorManagerComponent } from './senior-manager/senior-manager.component';
import { TransactionComponent } from './employee/transaction/transaction.component';
import { ProfileComponent } from './profile/profile.component';
import { PasswordComponent } from './password/password.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },

  { path: 'employee', component: EmployeeComponent },
  { path: 'employee/transaction', component: TransactionComponent },
  
  { path: 'manager', component: ManagerComponent },
  
  { path: 'admin', component: AdminComponent },

  { path: 'root-admin', component: RootAdminComponent },

  { path: 'senior-manager', component: SeniorManagerComponent },
  
  { path: 'profile', component: ProfileComponent },

  { path: 'password', component: PasswordComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
