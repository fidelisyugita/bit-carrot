import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { DatePipe, CommonModule } from '@angular/common'

import { OrderModule } from 'ngx-order-pipe';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { LoginComponent } from './login/login.component';
import { EmployeeComponent } from './employee/employee.component';
import { RegisterComponent } from './register/register.component';
import { UserService } from './service/user.service'
import { ManagerComponent } from './manager/manager.component';
import { AdminComponent } from './admin/admin.component';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { AdminService } from './service/admin.service';
import { BazaarComponent } from './employee/bazaar/bazaar.component';
import { BirthdayComponent } from './employee/birthday/birthday.component';
import { GroupService } from './service/group.service';
import { NotificationService } from './service/notification.service';
import { ConfigService } from './service/config.service';
import { DataTablesModule } from 'angular-datatables';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

import { SeniorManagerComponent } from './senior-manager/senior-manager.component'
import { ManagerGroupService } from './service/manager-group.service';
import { ManagerService } from './service/manager.service';
import { RootAdminComponent } from './root-admin/root-admin.component';
import { RootAdminService } from './service/root-admin.service';
import { TransactionComponent } from './employee/transaction/transaction.component';
import { TransactionService } from './service/transaction.service';
import { ProfileComponent } from './profile/profile.component';
import { ContentService } from './service/content.service';
import { SocialComponent } from './employee/social/social.component';
import { GroupComponent } from './employee/group/group.component';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AchievementComponent } from './employee/achievement/achievement.component';
import { MatTableModule, MatPaginatorModule, MatSortModule, MatInputModule, MatSelectModule, MatCheckboxModule } from '@angular/material';
import { ContentComponent } from './admin/content/content.component';
import { ApprovalComponent } from './admin/approval/approval.component';
import { StaffComponent } from './admin/staff/staff.component';
import { NewsletterComponent } from './admin/newsletter/newsletter.component';
import { GroupStaffComponent } from './admin/group-staff/group-staff.component';
import { AdminListComponent } from './root-admin/admin-list/admin-list.component';
import { ApprovalManagerComponent } from './root-admin/approval-manager/approval-manager.component';
import { ApprovalAdminComponent } from './root-admin/approval-admin/approval-admin.component';
import { ApprovalSocialComponent } from './root-admin/approval-social/approval-social.component';
import { CarrotGiveComponent } from './root-admin/carrot-give/carrot-give.component';
import { CarrotSetComponent } from './root-admin/carrot-set/carrot-set.component';
import { PasswordComponent } from './password/password.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmployeeComponent,
    RegisterComponent,
    ManagerComponent,
    AdminComponent,
    BazaarComponent,
    BirthdayComponent,
    RootAdminComponent,
    SeniorManagerComponent,
    TransactionComponent,
    ProfileComponent,
    SocialComponent,
    GroupComponent,
    AchievementComponent,
    ContentComponent,
    ApprovalComponent,
    StaffComponent,
    NewsletterComponent,
    GroupStaffComponent,
    AdminListComponent,
    ApprovalManagerComponent,
    ApprovalAdminComponent,
    ApprovalSocialComponent,
    CarrotGiveComponent,
    CarrotSetComponent,
    AchievementComponent,
    PasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    CommonModule,
    OrderModule,
    DataTablesModule,
    NgxPaginationModule,
    MatProgressSpinnerModule,
    NgxPaginationModule,
    FroalaEditorModule.forRoot(), 
    FroalaViewModule.forRoot(),
    BrowserAnimationsModule,
    MatInputModule, 
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    MatCheckboxModule,
    ToastModule.forRoot()
  ],
  // exports: [
  //   MatProgressSpinnerModule,
  // ],
  providers: [
    UserService,
    AdminService,
    GroupService,
    RootAdminService,
    ManagerGroupService,
    ManagerService,
    DatePipe,
    TransactionService,
    ContentService,
    ConfigService,
    NotificationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
