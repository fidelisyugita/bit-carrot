import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrotGiveComponent } from './carrot-give.component';

describe('CarrotGiveComponent', () => {
  let component: CarrotGiveComponent;
  let fixture: ComponentFixture<CarrotGiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarrotGiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarrotGiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
