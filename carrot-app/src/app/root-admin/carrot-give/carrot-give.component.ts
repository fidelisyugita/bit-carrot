import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";

import { User } from '../../model/user';
import { Transaction } from '../../model/transaction';
import { Config } from '../../model/config';
import { Content } from '../../model/content';

import * as Constanta from '../../config/constanta';

import { RootAdminService } from '../../service/root-admin.service';
import { UserService } from '../../service/user.service';
import { TransactionService } from '../../service/transaction.service';
import { ConfigService } from '../../service/config.service';

import { AppComponent } from '../../app.component'

import { Subject } from 'rxjs';


@Component({
  selector: 'app-carrot-give',
  templateUrl: './carrot-give.component.html',
  styleUrls: ['./carrot-give.component.css']
})
export class CarrotGiveComponent implements OnInit {
  users: User [];
  sManager: User[] = [];
  configs: Config [];
  config: Config;

  transactionTemp: Transaction = new Transaction;
  selectedUser:User;

  loadingSeniorManager:boolean = true;

  dtOptionsSeniorManager: DataTables.Settings = {};
  dtTriggerSeniorManager: Subject<any> = new Subject();

  constructor(
    private rootAdminService: RootAdminService,
    private userService:UserService,
    private transactionService:TransactionService,
    private configService:ConfigService,
    private router: Router,
    private appComponent: AppComponent
  ) { }

    
  userTemp: User = new User;
  loggedInUser:User = new User;

  ngOnInit() 
  {
    this.loggedInUser = this.userService.getLoggedInUser();
    
    if(!this.loggedInUser) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              this.loggedInUser = res;
              this.userService.setLoggedInUser(res);
            } else
              this.router.navigate(['/']);
          })
      } else 
        this.router.navigate(['/']);
    }

    this.getUsers();
    this.getConfig();
    // this.getSocial()
  }

  getUsers(): void 
  {
    this.rootAdminService.getUsers().subscribe(users=> {
      this.users = users
      this.users.forEach((user) => {
        if(user.admin == false)
          if(user.role == 8 && user.approved == true)
            this.sManager.push(user);
        
      });
      this.loadingSeniorManager = false;

      this.dtTriggerSeniorManager.next();
    });
  }

  getConfig(): void 
  {
    this.configs = [];
    this.configService.getConfig().subscribe(configs=> this.config = configs[0]);
  }

  onSelect(user:User):void
  {
    this.selectedUser = user;
  }

  send(carrot:number):void
  {
    if(carrot != null)
    {
      if(isNaN(carrot))
        this.appComponent.showError("Only number is permitted.");
      else
      { 
        if(this.config.stockCarrot >= carrot)
        {
          this.transactionTemp.senderId = this.loggedInUser.id;
          this.transactionTemp.receiverId = this.selectedUser.id;
          this.transactionTemp.amount = carrot;
          this.transactionTemp.description = "Senior Manager Supply from Root Admin";
          this.transactionTemp.approved = true;
          this.transactionTemp.type = 4;
          this.transactionService.postTransaction(this.transactionTemp).subscribe(transaction => {
          if(transaction)
          {
            this.config.stockCarrot -= carrot;
            this.configService.updateConfig(this.config).subscribe(config =>
            {
              this.appComponent.showSuccess(carrot + " carrot(s) has been sent to " + this.selectedUser.name + ".");
            });
          }
          else
            this.appComponent.showError("Sending Failed.");
          });  
        }
        else
          this.appComponent.showError("Not Enough Stock Carrot");
      }
    }
    else
      this.appComponent.showError("Please fill the carrot number.");
  }
}
