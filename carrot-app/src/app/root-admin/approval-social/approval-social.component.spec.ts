import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalSocialComponent } from './approval-social.component';

describe('ApprovalSocialComponent', () => {
  let component: ApprovalSocialComponent;
  let fixture: ComponentFixture<ApprovalSocialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalSocialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalSocialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
