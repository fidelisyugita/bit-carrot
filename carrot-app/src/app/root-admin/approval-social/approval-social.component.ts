import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";

import { User } from '../../model/user';
import { Transaction } from '../../model/transaction';
import { Config } from '../../model/config';
import { Content } from '../../model/content';

import * as Constanta from '../../config/constanta';

import { RootAdminService } from '../../service/root-admin.service';
import { AdminService } from '../../service/admin.service';
import { UserService } from '../../service/user.service';
import { TransactionService } from '../../service/transaction.service';

import { AppComponent } from '../../app.component'

import { Subject } from 'rxjs';

@Component({
  selector: 'app-approval-social',
  templateUrl: './approval-social.component.html',
  styleUrls: ['./approval-social.component.css']
})
export class ApprovalSocialComponent implements OnInit {
  rewards: Content[];

  social: Content[] = [];

  loadingSocial:boolean = true;
  
  dtOptionsSocial: DataTables.Settings = {};
  dtTriggerSocial: Subject<any> = new Subject();

  constructor(
    private rootAdminService: RootAdminService,
    private adminService:AdminService,
    private userService:UserService,
    private transactionService:TransactionService,
    private router: Router,
    private appComponent: AppComponent
  ) { }

    
  userTemp: User = new User;
  loggedInUser:User = new User;

  ngOnInit() 
  {
    this.loggedInUser = this.userService.getLoggedInUser();
    
    if(!this.loggedInUser) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              this.loggedInUser = res;
              this.userService.setLoggedInUser(res);
            } else
              this.router.navigate(['/']);
          })
      } else 
        this.router.navigate(['/']);
    }
    this.getSocial()
  }

  getSocial(): void 
  {
    this.social = [];
    console.log(this.social);
    this.adminService.getRewards().subscribe(rewards => {
      this.rewards = rewards
      this.rewards.forEach((reward) => {
        if(reward.type == 3 && reward.currentCarrot >= reward.carrot)
        {
          this.social.push(reward);
        }
      });
      this.loadingSocial = false;
      this.dtTriggerSocial.next();
    });
    
  }

  approveSocial(content: Content): void
  {
    content.currentCarrot -= content.carrot;
    this.adminService.addReward(content).subscribe(content =>{
      this.appComponent.showSuccess(content.carrot + " Carrot(s) has been donated to '" + content.name + "' Social Foundation.");
    });
  }
}
