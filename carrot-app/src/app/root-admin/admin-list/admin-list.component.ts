import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";

import { User } from '../../model/user';
import { Transaction } from '../../model/transaction';
import { Config } from '../../model/config';
import { Content } from '../../model/content';

import * as Constanta from '../../config/constanta';

import { RootAdminService } from '../../service/root-admin.service';
import { AdminService } from '../../service/admin.service';
import { UserService } from '../../service/user.service';

import { AppComponent } from '../../app.component'

import { Subject } from 'rxjs';

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.css']
})
export class AdminListComponent implements OnInit {
  users: User [];
  admin : User[] = [];
  nonAdmin: User[] = [];

  selectedUser:User;

  loadingAdmin:boolean = true;

  dtOptionsAdmin: DataTables.Settings = {};
  dtTriggerAdmin: Subject<any> = new Subject();

  constructor(
    private rootAdminService: RootAdminService,
    private userService:UserService,
    private router: Router,
    private appComponent: AppComponent
  ) { }

    
  userTemp: User = new User;
  loggedInUser:User = new User;

  ngOnInit() 
  {
    this.loggedInUser = this.userService.getLoggedInUser();
    
    if(!this.loggedInUser) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              this.loggedInUser = res;
              this.userService.setLoggedInUser(res);
            } else
              this.router.navigate(['/']);
          })
      } else 
        this.router.navigate(['/']);
    }

    this.getUsers();
  }

  getUsers(): void 
  {
    this.rootAdminService.getUsers().subscribe(users=> {
      this.users = users
      this.users.forEach((user) => {
        if(user.admin == true)
          this.admin.push(user);
        else
          this.nonAdmin.push(user);
      });
      this.loadingAdmin = false;

      this.dtTriggerAdmin.next();
    });
  }

  onSelect(user:User):void
  {
    this.selectedUser = user;
  }

  revoke(user: User): void 
  {
    user.admin = false;
    this.rootAdminService.updateUser(user).subscribe(user => {
      this.nonAdmin.push(user);
      this.admin = this.admin.filter(r => {
        return r.id != user.id;
      });
      this.appComponent.showSuccess(user.name + " is not an Admin anymore.");
    });
  }

}
