import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";

import { User } from '../../model/user';
import { Transaction } from '../../model/transaction';
import { Config } from '../../model/config';
import { Content } from '../../model/content';

import * as Constanta from '../../config/constanta';

import { RootAdminService } from '../../service/root-admin.service';
import { UserService } from '../../service/user.service';

import { AppComponent } from '../../app.component'

import { Subject } from 'rxjs';

@Component({
  selector: 'app-approval-manager',
  templateUrl: './approval-manager.component.html',
  styleUrls: ['./approval-manager.component.css']
})
export class ApprovalManagerComponent implements OnInit {
  users: User [];
  manager: User[] = [];
  selectedUser:User;

  loadingManager:boolean = true;
  dtOptionsManager: DataTables.Settings = {};
  dtTriggerManager: Subject<any> = new Subject();

  constructor(
    private rootAdminService: RootAdminService,
    private userService:UserService,
    private router: Router,
    private appComponent: AppComponent
  ) { }

    
  userTemp: User = new User;
  loggedInUser:User = new User;

  ngOnInit() 
  {
    this.loggedInUser = this.userService.getLoggedInUser();
    
    if(!this.loggedInUser) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              this.loggedInUser = res;
              this.userService.setLoggedInUser(res);
            } else
              this.router.navigate(['/']);
          })
      } else 
        this.router.navigate(['/']);
    }

    this.getUsers();
  }

  getUsers(): void 
  {
    this.rootAdminService.getUsers().subscribe(users=> {
      this.users = users
      this.users.forEach((user) => {
        if(user.admin == false)

          if(user.role == 4 && user.approved == false)
            this.manager.push(user);
        });
        this.loadingManager = false;
        this.dtTriggerManager.next();
      });    
  }

  approveManager(user: User): void
  {
    user.approved = true;
    this.userService.addEdit(user).subscribe(user => {
      this.manager = this.manager.filter(r => {
        return r.id != user.id;
      });
      this.appComponent.showSuccess(user.name + " is now registered as Manager.");
    });
  }
}
