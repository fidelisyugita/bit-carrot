import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";

import { User } from '../../model/user';
import { Transaction } from '../../model/transaction';
import { Config } from '../../model/config';
import { Content } from '../../model/content';

import * as Constanta from '../../config/constanta';

import { UserService } from '../../service/user.service';
import { ConfigService } from '../../service/config.service';

import { AppComponent } from '../../app.component'

import { Subject } from 'rxjs';


@Component({
  selector: 'app-carrot-set',
  templateUrl: './carrot-set.component.html',
  styleUrls: ['./carrot-set.component.css']
})
export class CarrotSetComponent implements OnInit {
  configs: Config [];
  config: Config;

  constructor(
    private userService:UserService,
    private configService:ConfigService,
    private router: Router,
    private appComponent: AppComponent
  ) { }

    
  userTemp: User = new User;
  loggedInUser:User = new User;

  ngOnInit() 
  {
    this.loggedInUser = this.userService.getLoggedInUser();
    
    if(!this.loggedInUser) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              this.loggedInUser = res;
              this.userService.setLoggedInUser(res);
            } else
              this.router.navigate(['/']);
          })
      } else 
        this.router.navigate(['/']);
    }

    this.getConfig();
  }

  getConfig(): void 
  {
    this.configs = [];
    this.configService.getConfig().subscribe(configs=> this.config = configs[0]);
  }

  updateConfig(yearlyCarrot:number, carrotLimit:number)
  {
    this.config.yearlyCarrot = yearlyCarrot;
    this.config.carrotLimit = carrotLimit;
    this.configService.updateConfig(this.config).subscribe(config => {
      if(config)
      {
        this.appComponent.showSuccess("Config Updated");
        this.ngOnInit;
      }
      else
        this.appComponent.showError("Update Failed.");
      });  
    
  }

  
}
