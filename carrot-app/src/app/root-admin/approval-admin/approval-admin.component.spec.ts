import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalAdminComponent } from './approval-admin.component';

describe('ApprovalAdminComponent', () => {
  let component: ApprovalAdminComponent;
  let fixture: ComponentFixture<ApprovalAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
