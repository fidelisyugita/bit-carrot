import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";

import { User } from '../../model/user';
import { Transaction } from '../../model/transaction';
import { Config } from '../../model/config';
import { Content } from '../../model/content';

import * as Constanta from '../../config/constanta';

import { RootAdminService } from '../../service/root-admin.service';
import { UserService } from '../../service/user.service';

import { AppComponent } from '../../app.component'

import { Subject } from 'rxjs';

@Component({
  selector: 'app-approval-admin',
  templateUrl: './approval-admin.component.html',
  styleUrls: ['./approval-admin.component.css']
})
export class ApprovalAdminComponent implements OnInit {
  users: User [];
  admin : User[] = [];
  nonAdmin: User[] = [];

  selectedUser:User;

  loadingStaff:boolean = true;

  dtOptionsStaff: DataTables.Settings = {};
  dtTriggerStaff: Subject<any> = new Subject();

  constructor(
    private rootAdminService: RootAdminService,
    private userService:UserService,
    private router: Router,
    private appComponent: AppComponent
  ) { }

  userTemp: User = new User;
  loggedInUser:User = new User;

  ngOnInit() 
  {
    this.loggedInUser = this.userService.getLoggedInUser();
    
    if(!this.loggedInUser) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              this.loggedInUser = res;
              this.userService.setLoggedInUser(res);
            } else
              this.router.navigate(['/']);
          })
      } else 
        this.router.navigate(['/']);
    }

    this.getUsers();
  }

  getUsers(): void 
  {
    this.rootAdminService.getUsers().subscribe(users=> {
      this.users = users
      this.users.forEach((user) => {
        if(user.admin == true)
          this.admin.push(user);
        else
          this.nonAdmin.push(user);
      });
      this.loadingStaff = false;

      this.dtTriggerStaff.next();
    });
  }

  onSelect(user:User):void
  {
    this.selectedUser = user;
  }

  approveAdmin(user: User): void
  {
    user.admin = true;
    this.rootAdminService.updateUser(user).subscribe(user => {
      this.admin.push(user);         
      this.nonAdmin = this.nonAdmin.filter(r => {
        return r.id != user.id;
      });   
      this.appComponent.showSuccess(user.name + " is now an Admin.");
    });
  }
}

