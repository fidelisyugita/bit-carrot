import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { User } from '../model/user';
import { UserService } from '../service/user.service';
import * as Constanta from '../config/constanta';
import * as Utility from '../config/utility';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(
    private userService: UserService,
    private router: Router
  ) { }


  user: User;
  errMsg: string = null;

  ngOnInit() {
    this.user = this.userService.getLoggedInUser();
    if (this.user)
      this.move(this.user);
  }

  Constanta = Constanta;
  Utility = Utility;

  signUp(name: string, email: string, dob: string, joinDate: string, role: number, password: string, cpassword: string): void {

    if (!name || !email || !dob || !joinDate || !role || !password || !cpassword ) {
      this.errMsg = 'input all field!';
      return;
    }

    email = email.trim();
    if (!Utility.isValidEmail(email)){
      this.errMsg = 'input valid email!';
      return;
    }

    if (password != cpassword) {
      this.errMsg = 'password is not equal!';
      return;
    }

    let approved = true;

    if (role == Constanta.MANAGER_ROLE)
      approved = false;
    
    this.userService.addEdit({ name, email, dob, joinDate, role, password, approved } as User)
      .subscribe(user => {
        this.router.navigate(['login']);
      });
  }

  
  move(user: User){
    if (user) {
      this.userService.setLoggedInUser(user);
      
      if(user.admin)
        this.router.navigate(['admin']);
      else {
        switch (user.role) {
          case Constanta.MANAGER_ROLE:
            this.router.navigate(['manager']);
            break;
          case Constanta.EMPLOYEE_ROLE:
            this.router.navigate(['employee']);
            break;
          case Constanta.ROOT_ADMIN_ROLE:
            this.router.navigate(['root-admin']);
            break;
          case Constanta.SENIOR_MANAGER_ROLE:
            this.router.navigate(['senior-manager']);
            break;
          
          default:
            break;
        }
      }
    }
  }

}
