import React, {Component} from 'react';
import {
  Modal, 
  Text, 
  TouchableHighlight, 
  View,
  Image,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import { 
  Button, 
  FormLabel, 
  FormInput, 
  FormValidationMessage,
  Header,
  Icon,
  Avatar,
  List,
  ListItem,
  Badge,
} from 'react-native-elements';

import * as Constanta from '../config/constanta';
import * as Utility from '../config/utility';

export default class Profile extends Component {
  state = {
    // modalVisible: false,
    user: null,
  };

  // setModalVisible(visible) {
  //   this.setState({modalVisible: visible});
  // }

  async componentDidMount(){
    let user = this.props.navigation.state.params.user;
    this.setState({ user });
    // console.log(user);

    user = await Utility.sendData(Constanta.USER_PLUS_API_URL, user);
    // console.log(user);
    this.setState({ user });
  }

  openTransaction = (type) => {
    // let direction = Utility.getRole(this.state.user);
    this.props.navigation.navigate('Transaction', {'user': this.state.user, 'type': type});
  }

  render() {
    const { user } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: Constanta.COLOR_DARK_GRAY }} >
        {/* <Header
          statusBarProps={{ barStyle: 'light-content' }}
          // leftComponent={{ text: 'Login', style: { color: '#fff' } }}
          // centerComponent={{ text: 'Login', style: { color: '#fff' } }}
          outerContainerStyles={{ backgroundColor: Constanta.COLOR_GRAY, height: 0, }}
          innerContainerStyles={{ justifyContent: 'space-around' }}
        /> */}

        <View style = {{ flex: 1, margin: 5, marginTop: 25,
          borderRadius: 5, backgroundColor: Constanta.COLOR_GRAY }} >
          <View style={{flex: 1, backgroundColor: Constanta.BG_COLOR, 
            justifyContent: 'center', alignItems: 'center', 
            borderTopLeftRadius: 5, borderTopRightRadius: 5 }} >
            {/* <Image 
              source = { require('../assets/img/icon.png') }
              style = {{ width: 80, height: 80 }}
            /> */}
            <Avatar
              large
              rounded
              // icon={{name: 'person'}}
              source={user && user.picture ? {uri: user.picture} : require('../assets/img/profile.png')}
              // avatarStyle={{resizeMode: 'contain', resizeMethod: 'resize'}}
              // onPress={() => console.log("Works!")}
              activeOpacity={0.7}
              containerStyle={{ }}
            />
            <Text style={{color: Constanta.COLOR_GRAY, marginTop: 8}} >
              {user && user.name ? user.name : 'name'}
            </Text>
            <Text style={{color: Constanta.COLOR_GRAY, marginTop: 0}} >
              {user && user.address ? user.address : 'address'}
            </Text>
            <Badge 
              containerStyle={{
                backgroundColor: Constanta.COLOR_YELLOW, 
                flexDirection: 'row', alignItems:'center'
              }}
              onPress={() => this.openTransaction(null)}
            >
              <Icon name='euro' type='font-awesome' color= {Constanta.COLOR_LIGHT_GRAY} size={12} />
              <Text style={{color: Constanta.COLOR_LIGHT_GRAY, marginLeft: 5, fontSize: 14}} >
                {/* Carrot:  */}
                {user ? user.carrot : '0'}
              </Text>
            </Badge>
          </View>
          
          <ScrollView style={{flex: 1 }} >
            <List containerStyle={{marginBottom: 20, flex: 1}} >
              <ListItem
                key={0}
                onPress={() => this.openTransaction(Constanta.ACHIEVEMENT_TYPE)}
                title={
                  <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}} >
                    <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 8}} >
                      <Icon 
                        name='gift'
                        type='font-awesome' 
                        size={30} 
                        color={Constanta.COLOR_DARK} 
                        containerStyle={{width: 30}}
                      />
                      <Text style={{marginLeft: 20, color: Constanta.COLOR_DARK}} >Earned</Text>
                    </View>
                    {
                      user && user.carrotOfTheYear != null ? (
                        <Text style={{color: Constanta.COLOR_DARK}} >{user.carrotOfTheYear}</Text>
                      ) : (
                        <ActivityIndicator size = 'small' 
                          color = { Constanta.COLOR_ORANGE } />
                      )
                    }
                  </View>
                }
                containerStyle={{backgroundColor: Constanta.COLOR_GRAY}}
              />
              <ListItem
                key={1}
                onPress={() => this.openTransaction(Constanta.REWARD_TYPE)}
                title={
                  <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}} >
                    <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 8}} >
                      <Icon 
                        name='shopping-cart'
                        type='font-awesome' 
                        size={30} 
                        color={Constanta.COLOR_DARK} 
                        containerStyle={{width: 30}}
                      />
                      <Text style={{marginLeft: 20, color: Constanta.COLOR_DARK}} >Reward</Text>
                    </View>
                    {
                      user && user.carrotToReward != null ? (
                        <Text style={{color: Constanta.COLOR_DARK}} >{user.carrotToReward}</Text>
                      ) : (
                        <ActivityIndicator size = 'small' 
                          color = { Constanta.COLOR_ORANGE } />
                      )
                    }
                  </View>
                }
                containerStyle={{backgroundColor: Constanta.COLOR_GRAY}}
              />
              <ListItem
                key={2}
                onPress={() => this.openTransaction(Constanta.SOCIAL_TYPE)}
                title={
                  <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}} >
                    <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 8}} >
                      <Icon 
                        name='slideshare'
                        type='font-awesome' 
                        size={30} 
                        color={Constanta.COLOR_DARK} 
                        containerStyle={{width: 30}}
                      />
                      <Text style={{marginLeft: 20, color: Constanta.COLOR_DARK}} >Social</Text>
                    </View>
                    {
                      user && user.carrotToSocial != null ? (
                        <Text style={{color: Constanta.COLOR_DARK}} >{user.carrotToSocial}</Text>
                      ) : (
                        <ActivityIndicator size = 'small' 
                          color = { Constanta.COLOR_ORANGE } />
                      )
                    }
                  </View>
                }
                containerStyle={{backgroundColor: Constanta.COLOR_GRAY}}
              />
              <ListItem
                key={3}
                onPress={() => this.openTransaction(Constanta.SHARED_TYPE)}
                title={
                  <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}} >
                    <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 8}} >
                      <Icon 
                        name='social-myspace'
                        type='foundation' 
                        size={30} 
                        color={Constanta.COLOR_DARK} 
                        containerStyle={{width: 30}}
                      />
                      <Text style={{marginLeft: 20, color: Constanta.COLOR_DARK}} >Shared</Text>
                    </View>
                    {
                      user && user.carrotToOther != null ? (
                        <Text style={{color: Constanta.COLOR_DARK}} >{user.carrotToOther}</Text>
                      ) : (
                        <ActivityIndicator size = 'small' 
                          color = { Constanta.COLOR_ORANGE } />
                      )
                    }
                  </View>
                }
                containerStyle={{backgroundColor: Constanta.COLOR_GRAY}}
              />
            </List>
          </ScrollView>
        </View>

        {/* {
          (user && Utility.getRole(user) == Utility.getRole({'role': Constanta.EMPLOYEE_ROLE})) ? (
            <View style={{
              height: 38, justifyContent: 'flex-end', 
              backgroundColor: Constanta.BG_COLOR_PRIMARY
            }} >
              <View style={{flex: 1, justifyContent: 'center', margin: 'auto'}} >
                <View style={{flexDirection: 'row', justifyContent: 'space-around'}} >
                  <View style={{}} >
                    <Icon 
                      name='home'
                      size={30}
                      color={Constanta.BG_COLOR_BASIC}
                      // containerStyle={{width: 50}}
                      onPress={this.openHome}
                    />
                  </View>
                  <View style={{}} >
                    <Icon 
                      // reverse
                      name='people'
                      size={30}
                      color={Constanta.BG_COLOR_BASIC}
                      // containerStyle={{ width: 50}}
                    />
                  </View>
                  <View style={{}} >
                    <Icon 
                      // raised
                      name='cake'
                      size={30}
                      color={Constanta.BG_COLOR_BASIC}
                      // containerStyle={{width: 50}}
                    />
                  </View>
                  <View style={{}} >
                    <Icon 
                      name='notifications'
                      size={30}
                      color={Constanta.BG_COLOR_BASIC}
                      // containerStyle={{width: 50}}
                    />
                  </View>
                  <View style={{}} >
                    <Icon 
                      name='person'
                      size={30}
                      color={Constanta.COLOR_PRIMARY}
                      // containerStyle={{ width: 50}}
                    />
                  </View>
                </View>
              </View>
              
            </View>
          ) : (
            <View></View>
          )
        } */}
      </View>
    );
  }
}
