import React, {Component} from 'react';
import {
  Modal, 
  Text, 
  TouchableHighlight, 
  View,
  Image,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import { 
  Button, 
  FormLabel, 
  FormInput, 
  FormValidationMessage,
  Header,
  Icon,
  Avatar,
  List,
  ListItem,
} from 'react-native-elements';

import * as Constanta from '../../config/constanta';
import * as Utility from '../../config/utility';

export default class Notification extends Component {
  state = {
    isLoading: true,
    user: null,
    notifications: null,

  };

  async componentDidMount(){
    let user = this.props.navigation.state.params.user;
    let notifications = await Utility.getData(Constanta.NOTIFICATION_BY_USER_API_URL + user.id);
    // console.log(notifications);
    this.setState({ isLoading: false, user, notifications });
  }

  render() {
    const { isLoading, user, notifications } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: Constanta.COLOR_DARK_GRAY }} >
        {/* <Header
          statusBarProps={{ barStyle: 'light-content' }}
          // leftComponent={{ text: 'Login', style: { color: '#fff' } }}
          // centerComponent={{ text: 'Login', style: { color: '#fff' } }}
          outerContainerStyles={{ backgroundColor: Constanta.COLOR_GRAY, height: 0, }}
          innerContainerStyles={{ justifyContent: 'space-around' }}
        /> */}

        <View style = {{ flex: 1, margin: 5, marginTop: 25,
          borderRadius: 5, backgroundColor: Constanta.COLOR_GRAY }} >
          <ScrollView style={{flex: 1 }} >
            {
              isLoading ? 
                <ActivityIndicator size = 'large' 
                  color = { Constanta.COLOR_ORANGE } 
                  style = {{ marginTop: 30}} />
              : (
                <List containerStyle={{marginBottom: 20, flex: 1}} >
                  {
                    notifications && (
                      notifications.map((notification, i) => (
                        <ListItem
                          key={i}
                          // roundAvatar
                          avatar={notification.picture ? {uri: notification.picture} : require('../../assets/img/gift.png')}
                          // avatarStyle={{resizeMode: 'contain', resizeMethod: 'resize'}}
                          // title={notification.description ? notification.description : 'Description'}
                          title={notification.title ? notification.title : notification.description}
                          subtitle={Utility.getFormattedDate(notification.creationDate)}
                          containerStyle={{backgroundColor: Constanta.COLOR_GRAY}}
                        />
                      ))
                    )
                  }
                </List>
              )
            }
          </ScrollView>
        </View>

      </View>
    );
  }
}
