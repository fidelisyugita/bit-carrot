import React, {Component} from 'react';
import {
  Modal, 
  Text, 
  TouchableHighlight, 
  View,
  Image,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import { 
  Button, 
  FormLabel, 
  FormInput, 
  FormValidationMessage,
  Header,
  Icon,
  Avatar,
  List,
  ListItem,
  Badge,
} from 'react-native-elements';

import * as Constanta from '../../config/constanta';
import * as Utility from '../../config/utility';

export default class Birthday extends Component {
  state = {
    isLoading: true,
    user: null,
    birthdayStaffs: null,
    birthdayStaffs2: null,
    birthdayStaffs3: null,
    selectedStaff: null,
    modalVisible: false,
    isModalLoading: false,
    sendAmount: 0,
    sendMessage: '',

  };

  async componentDidMount(){
    let user = this.props.navigation.state.params.user;
    let birthdayStaffs = await Utility.getData(Constanta.BIRTHDAY_USER_API_URL);
    let birthdayStaffs2 = await Utility.getData(Constanta.BIRTHDAY_USER_API_URL + '-1');
    let birthdayStaffs3 = await Utility.getData(Constanta.BIRTHDAY_USER_API_URL + '-2');

    this.setState({ isLoading: false, user, birthdayStaffs, birthdayStaffs2, birthdayStaffs3 });
  }

  sendCarrotMenu = (selectedStaff) => {
    // console.log(selectedStaff);
    this.setState({ selectedStaff });
    this.toggleModalVisible();
  }

  toggleModalVisible = () => {
    this.setState({modalVisible: !this.state.modalVisible});
  }

  sendCarrot = async() => {
    this.setState({isModalLoading: true});

    let transaction = {
      'senderId': this.state.user.id,
      'receiverId': this.state.selectedStaff.id,
      'amount': this.state.sendAmount,
      'description': this.state.sendMessage,
      'type': Constanta.SHARED_TYPE,
    }

    let res = await Utility.sendData(Constanta.TRANSACTION_API_URL, transaction);

    this.setState({isModalLoading: false});

    if (res) {
      alert('Send success!');
      this.toggleModalVisible();
    } else {
      alert('Send failed!');
      // this.toggleModalVisible();
    }
  }

  messageChange = (message) => {
    this.setState({ sendMessage: message });
  }

  amountChange = (amount) => {
    // console.log(amount);
    if(!isNaN(amount)){
      this.setState({ sendAmount: amount });
      // console.log(amount);
    }
  }

  render() {
    const { isLoading, user, birthdayStaffs, birthdayStaffs2, birthdayStaffs3, selectedStaff, isModalLoading } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: Constanta.COLOR_DARK_GRAY }} >
        {/* <Header
          statusBarProps={{ barStyle: 'light-content' }}
          // leftComponent={{ text: 'Login', style: { color: '#fff' } }}
          // centerComponent={{ text: 'Login', style: { color: '#fff' } }}
          outerContainerStyles={{ backgroundColor: Constanta.COLOR_GRAY, height: 0, }}
          innerContainerStyles={{ justifyContent: 'space-around' }}
        /> */}
        <Modal
          transparent = { true }
          visible = { this.state.modalVisible }
          onRequestClose={this.toggleModalVisible}
        >
          <View style = {{ flex: 1, justifyContent: 'center', alignItems: 'center',
            backgroundColor: Constanta.COLOR_TRANSPARANT }} >
            <View style = {{ width: 300, height: 350,
              backgroundColor: Constanta.COLOR_LIGHT_GRAY, borderRadius: 5 }} >

              <View style={{flex: 1}} >
                <FormLabel>To: {selectedStaff ? selectedStaff.name : 'Name'}</FormLabel>

                <FormLabel>Message</FormLabel>
                <FormInput onChangeText = { (message) => this.messageChange(message) } />

                <FormLabel>Amount</FormLabel>
                <FormInput 
                  // value = {this.state.sendAmount}
                  // onChange = {(amount) => this.amountChange(amount)} 
                  onChangeText = { (amount) => this.amountChange(amount) } 
                />
              </View>
              {
                isModalLoading ? 
                  <ActivityIndicator size = 'large' 
                    color = { Constanta.COLOR_ORANGE } 
                    style = {{justifyContent: 'flex-end', marginBottom: 30 }} />
                : (
                  <View style={{justifyContent: 'flex-end', }} >
                    <Button 
                      title = 'SEND' 
                      onPress = { this.sendCarrot } 
                      backgroundColor = { Constanta.COLOR_ORANGE } 
                      borderRadius = { 5 }
                      buttonStyle = {{ }}
                    />
                    <Button 
                      title = 'Cancel' 
                      onPress = { this.toggleModalVisible } 
                      backgroundColor = { Constanta.COLOR_GRAY }
                      color = {Constanta.COLOR_ORANGE} 
                      borderRadius = { 5 }
                      buttonStyle = {{ marginBottom: 10, marginTop: 10 }}
                    />
                  </View>
                )
              }
            </View>
          </View>
        </Modal>

        <View style = {{ flex: 1, margin: 5, marginTop: 25,
          borderRadius: 5, backgroundColor: Constanta.COLOR_GRAY }} >
          <ScrollView style={{flex: 1 }} >
            {
              isLoading ? 
                <ActivityIndicator size = 'large' 
                  color = { Constanta.COLOR_ORANGE } 
                  style = {{ marginTop: 30}} />
              : (
                <View>
                  <List containerStyle={{marginBottom: 20, flex: 1}} >
                    <Badge containerStyle={{ backgroundColor: Constanta.COLOR_GREEN}}>
                      <Text h2 style={{color: Constanta.COLOR_LIGHT_GRAY, fontWeight: 'bold'}} >
                        Today
                      </Text>
                    </Badge>
                    {
                      birthdayStaffs && (
                        birthdayStaffs.map((staff, i) => (
                          staff.id != user.id && (
                            <ListItem
                              key={i}
                              hideChevron={true}
                              title={
                                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                                  <View style={{flexDirection: 'row', alignItems: 'center'}} >
                                    <Avatar
                                      small
                                      rounded
                                      // icon={{name: 'person'}}
                                      source={staff && staff.picture ? 
                                        {uri: staff.picture} : require('../../assets/img/profile.png')}
                                      // avatarStyle={{resizeMode: 'contain', resizeMethod: 'resize'}}
                                      // onPress={() => console.log("Works!")}
                                      activeOpacity={0.7}
                                      containerStyle={{ }}
                                    />
                                    <View style={{flexDirection: 'column', flex: 1, marginLeft: 10, marginRight: 10}} >
                                      <Text style={{color: Constanta.COLOR_DARK, fontSize: 14, }} >
                                        {staff.name ? staff.name : 'Name'}
                                      </Text>
                                      <Text style={{color: Constanta.COLOR_DARK, fontSize: 12,  fontWeight: 'bold'}} >
                                        {Utility.getFormattedDate(staff.dob)}
                                      </Text>
                                    </View>
                                    <Badge 
                                      containerStyle={{
                                        backgroundColor: Constanta.COLOR_YELLOW, 
                                        flexDirection: 'row', alignItems:'center'
                                      }}
                                      onPress={() => this.sendCarrotMenu(staff)}
                                    >
                                      <Text style={{color: Constanta.COLOR_LIGHT_GRAY}} >
                                        Send carrot
                                      </Text>
                                    </Badge>
                                  </View>
                                </View>
                              }
                              containerStyle={{backgroundColor: Constanta.COLOR_GRAY}}
                            />
                          )
                        ))
                      )
                    }
                  </List>

                  <List containerStyle={{marginBottom: 20, flex: 1}} >
                    <Badge containerStyle={{ backgroundColor: Constanta.COLOR_GREEN}}>
                      <Text h2 style={{color: Constanta.COLOR_LIGHT_GRAY, fontWeight: 'bold'}} >
                        Yesterday
                      </Text>
                    </Badge>
                  {
                    birthdayStaffs2 && (
                      birthdayStaffs2.map((staff, i) => (
                        staff.id != user.id && (
                          <ListItem
                            key={i}
                            hideChevron={true}
                            title={
                              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                                <View style={{flexDirection: 'row', alignItems: 'center'}} >
                                  <Avatar
                                    small
                                    rounded
                                    // icon={{name: 'person'}}
                                    source={staff && staff.picture ? 
                                      {uri: staff.picture} : require('../../assets/img/profile.png')}
                                    // avatarStyle={{resizeMode: 'contain', resizeMethod: 'resize'}}
                                    // onPress={() => console.log("Works!")}
                                    activeOpacity={0.7}
                                    containerStyle={{ }}
                                  />
                                  <View style={{flexDirection: 'column', flex: 1, marginLeft: 10, marginRight: 10}} >
                                    <Text style={{color: Constanta.COLOR_DARK, fontSize: 14, }} >
                                      {staff.name ? staff.name : 'Name'}
                                    </Text>
                                    <Text style={{color: Constanta.COLOR_DARK, fontSize: 12,  fontWeight: 'bold'}} >
                                      {Utility.getFormattedDate(staff.dob)}
                                    </Text>
                                  </View>
                                  <Badge 
                                    containerStyle={{
                                      backgroundColor: Constanta.COLOR_YELLOW, 
                                      flexDirection: 'row', alignItems:'center'
                                    }}
                                    onPress={() => this.sendCarrotMenu(staff)}
                                  >
                                    <Text style={{color: Constanta.COLOR_LIGHT_GRAY}} >
                                      Send carrot
                                    </Text>
                                  </Badge>
                                </View>
                              </View>
                            }
                            containerStyle={{backgroundColor: Constanta.COLOR_GRAY}}
                          />
                        )
                      ))
                    )
                  }
                  </List>

                  <List containerStyle={{marginBottom: 20, flex: 1}} >
                    <Badge containerStyle={{ backgroundColor: Constanta.COLOR_GREEN}}>
                      <Text h2 style={{color: Constanta.COLOR_LIGHT_GRAY, fontWeight: 'bold'}} >
                        Two days ago
                      </Text>
                    </Badge>
                    {
                      birthdayStaffs3 && (
                        birthdayStaffs3.map((staff, i) => (
                          staff.id != user.id && (
                            <ListItem
                              key={i}
                              hideChevron={true}
                              title={
                                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                                  <View style={{flexDirection: 'row', alignItems: 'center'}} >
                                    <Avatar
                                      small
                                      rounded
                                      // icon={{name: 'person'}}
                                      source={staff && staff.picture ? 
                                        {uri: staff.picture} : require('../../assets/img/profile.png')}
                                      // avatarStyle={{resizeMode: 'contain', resizeMethod: 'resize'}}
                                      // onPress={() => console.log("Works!")}
                                      activeOpacity={0.7}
                                      containerStyle={{ }}
                                    />
                                    <View style={{flexDirection: 'column', flex: 1, marginLeft: 10, marginRight: 10}} >
                                      <Text style={{color: Constanta.COLOR_DARK, fontSize: 14, }} >
                                        {staff.name ? staff.name : 'Name'}
                                      </Text>
                                      <Text style={{color: Constanta.COLOR_DARK, fontSize: 12,  fontWeight: 'bold'}} >
                                        {Utility.getFormattedDate(staff.dob)}
                                      </Text>
                                    </View>
                                    <Badge 
                                      containerStyle={{
                                        backgroundColor: Constanta.COLOR_YELLOW, 
                                        flexDirection: 'row', alignItems:'center'
                                      }}
                                      onPress={() => this.sendCarrotMenu(staff)}
                                    >
                                      <Text style={{color: Constanta.COLOR_LIGHT_GRAY}} >
                                        Send carrot
                                      </Text>
                                    </Badge>
                                  </View>
                                </View>
                              }
                              containerStyle={{backgroundColor: Constanta.COLOR_GRAY}}
                            />
                          )
                        ))
                      )
                    }
                  </List>
                </View>
              )
            }
          </ScrollView>
        </View>

      </View>
    );
  }
}
