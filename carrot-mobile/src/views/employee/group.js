import React, {Component} from 'react';
import {
  Modal, 
  Text, 
  TouchableHighlight, 
  View,
  Image,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import { 
  Button, 
  FormLabel, 
  FormInput, 
  FormValidationMessage,
  Header,
  Icon,
  Avatar,
  List,
  ListItem,
  Badge,
} from 'react-native-elements';

import * as Constanta from '../../config/constanta';
import * as Utility from '../../config/utility';

export default class Group extends Component {
  state = {
    isLoading: true,
    user: null,
    collaegues: null,
    selectedCollaegue: null,
    modalVisible: false,
    isModalLoading: false,
    sendAmount: 0,
    sendMessage: '',

  };

  async componentDidMount(){
    let user = this.props.navigation.state.params.user;
    let collaegues = await Utility.getData(Constanta.USER_BY_SUPERVISOR_API_URL + user.supervisorId);

    this.setState({ isLoading: false, user, collaegues });
  }

  sendCarrotMenu = (selectedCollaegue) => {
    // console.log(selectedStaff);
    this.setState({ selectedCollaegue });
    this.toggleModalVisible();
  }

  toggleModalVisible = () => {
    this.setState({modalVisible: !this.state.modalVisible});
  }

  sendCarrot = async() => {
    this.setState({isModalLoading: true});

    let transaction = {
      'senderId': this.state.user.id,
      'receiverId': this.state.selectedCollaegue.id,
      'amount': this.state.sendAmount,
      'description': this.state.sendMessage,
      'type': Constanta.SHARED_TYPE,
    }

    let res = await Utility.sendData(Constanta.TRANSACTION_API_URL, transaction);

    this.setState({isModalLoading: false});

    if (res) {
      alert('Send success!');
      this.toggleModalVisible();
    } else {
      alert('Send failed!');
      // this.toggleModalVisible();
    }
  }

  messageChange = (message) => {
    this.setState({ sendMessage: message });
  }

  amountChange = (amount) => {
    // console.log(amount);
    if(!isNaN(amount)){
      this.setState({ sendAmount: amount });
      // console.log(amount);
    }
  }

  render() {
    const { isLoading, user, collaegues, selectedCollaegue, isModalLoading } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: Constanta.COLOR_DARK_GRAY }} >
        {/* <Header
          statusBarProps={{ barStyle: 'light-content' }}
          // leftComponent={{ text: 'Login', style: { color: '#fff' } }}
          // centerComponent={{ text: 'Login', style: { color: '#fff' } }}
          outerContainerStyles={{ backgroundColor: Constanta.COLOR_GRAY, height: 0, }}
          innerContainerStyles={{ justifyContent: 'space-around' }}
        /> */}
        <Modal
          transparent = { true }
          visible = { this.state.modalVisible }
          onRequestClose={this.toggleModalVisible}
        >
          <View style = {{ flex: 1, justifyContent: 'center', alignItems: 'center',
            backgroundColor: Constanta.COLOR_TRANSPARANT }} >
            <View style = {{ width: 300, height: 350,
              backgroundColor: Constanta.COLOR_LIGHT_GRAY, borderRadius: 5 }} >

              <View style={{flex: 1}} >
                <FormLabel>To: {selectedCollaegue ? selectedCollaegue.name : 'Name'}</FormLabel>

                <FormLabel>Message</FormLabel>
                <FormInput onChangeText = { (message) => this.messageChange(message) } />

                <FormLabel>Amount</FormLabel>
                <FormInput 
                  // value = {this.state.sendAmount}
                  // onChange = {(amount) => this.amountChange(amount)} 
                  onChangeText = { (amount) => this.amountChange(amount) } 
                />
              </View>
              {
                isModalLoading ? 
                  <ActivityIndicator size = 'large' 
                    color = { Constanta.COLOR_ORANGE } 
                    style = {{justifyContent: 'flex-end', marginBottom: 30 }} />
                : (
                  <View style={{justifyContent: 'flex-end', }} >
                    <Button 
                      title = 'SEND' 
                      onPress = { this.sendCarrot } 
                      backgroundColor = { Constanta.COLOR_ORANGE } 
                      borderRadius = { 5 }
                      buttonStyle = {{ }}
                    />
                    <Button 
                      title = 'Cancel' 
                      onPress = { this.toggleModalVisible } 
                      backgroundColor = { Constanta.COLOR_GRAY }
                      color = {Constanta.COLOR_ORANGE} 
                      borderRadius = { 5 }
                      buttonStyle = {{ marginBottom: 10, marginTop: 10 }}
                    />
                  </View>
                )
              }
            </View>
          </View>
        </Modal>

        <View style = {{ flex: 1, margin: 5, marginTop: 25,
          borderRadius: 5, backgroundColor: Constanta.COLOR_GRAY }} >
          <ScrollView style={{flex: 1 }} >
            {
              isLoading ? 
                <ActivityIndicator size = 'large' 
                  color = { Constanta.COLOR_ORANGE } 
                  style = {{ marginTop: 30}} />
              : (
                <List containerStyle={{marginBottom: 20, flex: 1}} >
                  {
                    collaegues && (
                      collaegues.map((collaegue, i) => (
                        collaegue.id != user.id && (
                          <ListItem
                            key={i}
                            hideChevron={true}
                            title={
                              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                                <View style={{flexDirection: 'row', alignItems: 'center'}} >
                                  <Avatar
                                    small
                                    rounded
                                    // icon={{name: 'person'}}
                                    source={collaegue && collaegue.picture ? 
                                      {uri: collaegue.picture} : require('../../assets/img/profile.png')}
                                    // avatarStyle={{resizeMode: 'contain', resizeMethod: 'resize'}}
                                    // onPress={() => console.log("Works!")}
                                    activeOpacity={0.7}
                                    containerStyle={{ }}
                                  />
                                  <View style={{flexDirection: 'column', flex: 1, marginLeft: 10, marginRight: 10}} >
                                    <Text style={{color: Constanta.COLOR_DARK, fontSize: 14, }} >
                                      {collaegue.name ? collaegue.name : 'Name'}
                                    </Text>
                                    <Text style={{color: Constanta.COLOR_DARK, fontSize: 12,  fontWeight: 'bold'}} >
                                      {Utility.getFormattedDate(collaegue.dob)}
                                    </Text>
                                  </View>
                                  {/* <Badge 
                                    containerStyle={{
                                      backgroundColor: Constanta.COLOR_GREEN, 
                                      flexDirection: 'row', alignItems:'center'
                                    }}
                                    // onPress={this.openTransaction}
                                  >
                                    <Text style={{color: Constanta.COLOR_LIGHT_GRAY}} >
                                      Detail
                                    </Text>
                                  </Badge> */}
                                  <Badge 
                                    containerStyle={{
                                      backgroundColor: Constanta.COLOR_YELLOW, 
                                      flexDirection: 'row', alignItems:'center'
                                    }}
                                    onPress={() => this.sendCarrotMenu(collaegue)}
                                  >
                                    {/* <Icon name='euro' type='font-awesome' color= {Constanta.COLOR_LIGHT_GRAY} size={12} /> */}
                                    <Text style={{color: Constanta.COLOR_LIGHT_GRAY}} >
                                      Send carrot
                                    </Text>
                                  </Badge>
                                </View>
                              </View>
                            }
                            containerStyle={{backgroundColor: Constanta.COLOR_GRAY}}
                          />
                        )
                      ))
                    )
                  }
                </List>
              )
            }
          </ScrollView>
        </View>

      </View>
    );
  }
}
