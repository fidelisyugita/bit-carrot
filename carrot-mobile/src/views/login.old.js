import React, { Component } from 'react';
import { 
  View, 
  Text, 
  Image, 
  ActivityIndicator,
  KeyboardAvoidingView,
  Modal,
} from 'react-native';
import { 
  Button, 
  FormLabel, 
  FormInput, 
  FormValidationMessage,
  Header, 
} from 'react-native-elements';

import * as Constanta from '../config/constanta';
import * as Utility from '../config/utility';

export default class Login extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      isLoading: false,
      email: null,
      password: null,

    };
  };

  componentDidMount(){
  }

  emailChange = (email) => {
    if (!Utility.isValidEmail(email)) {
      this.setState({ email, emailErrMsg: 'Nice email, change it!' })
    } else
      this.setState({ email, emailErrMsg: null });
  }

  passwordChange = (password) => {
    if (password == '') {
      this.setState({ password, pwdErrMsg: 'Input your password please!' })
    } else
      this.setState({ password, pwdErrMsg: null });
  }

  login = async() => {
    this.setState({ isLoading: true });
    let user = {
      email: this.state.email,
      password: this.state.password,
    }
    let userRes = await Utility.sendData(Constanta.LOGIN_API_URL, user);
    this.setState({ isLoading: false });
    
    if (userRes) {
      let direction = Utility.getRole(userRes);
      this.props.navigation.navigate(direction, {'user': userRes});
    } else {
      this.setState({ 
        emailErrMsg: 'Input right email!', 
        pwdErrMsg: 'Input right password!' 
      });
    }
  }
  
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: Constanta.COLOR_DARK_GRAY }} >
        {/* <Header
          statusBarProps={{ barStyle: 'light-content' }}
          // leftComponent={{ text: 'Login', style: { color: '#fff' } }}
          // centerComponent={{ text: 'Login', style: { color: '#fff' } }}
          outerContainerStyles={{ backgroundColor: Constanta.COLOR_DARK_GRAY, height: 0 }}
          // innerContainerStyles={{ justifyContent: 'space-around' }}
        /> */}
        {/* <Modal
          transparent = { true }
          visible = { this.state.isLoading }
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}
        >
          <View style = {{ flex: 1, justifyContent: 'center', alignItems: 'center',
            backgroundColor: Constanta.COLOR_TRANSPARANT }} >
            <View style = {{ width: 300, height: 200,
              backgroundColor: Constanta.BG_COLOR_PRIMARY }} >
              <ActivityIndicator size = 'large' color = { Constanta.COLOR_PRIMARY }
                style = {{ justifyContent: 'center', alignItems: 'center' }} />
            </View>
          </View>
        </Modal> */}
        <View style = {{ flex: 1, margin: 5, marginTop: 25,
          borderRadius: 5, backgroundColor: Constanta.COLOR_GRAY }} >
          
          <KeyboardAvoidingView style = {{ flex: 1 }} behavior = 'padding' enabled >
            <View style = {{ flex: 3, justifyContent: 'flex-end', alignItems: 'center' }} >
              <Image 
                source = { require('../assets/img/icon.png') }
                style = {{ width: 180, height: 180 }}
                resizeMode = 'contain'
                resizeMethod = 'resize'
              />
            </View>
            <View style = {{ flex: 4, margin: 20 }} >
              <FormLabel>Email</FormLabel>
              <FormInput keyboardType='email-address' onChangeText = { (email) => this.emailChange(email) } />
              <FormValidationMessage>{ this.state.emailErrMsg }</FormValidationMessage>

              <FormLabel>Password</FormLabel>
              <FormInput secureTextEntry = { true } 
                onChangeText = { (password) => this.passwordChange(password) } />
              <FormValidationMessage>{ this.state.pwdErrMsg }</FormValidationMessage>

              {
                this.state.isLoading ? 
                  <ActivityIndicator size = 'large' 
                    color = { Constanta.COLOR_ORANGE } 
                    style = {{ marginTop: 30}} />
                : (
                  <Button 
                    title = 'LOGIN' 
                    onPress = { this.login } 
                    backgroundColor = { Constanta.COLOR_ORANGE } 
                    borderRadius = { 5 }
                    buttonStyle = {{ marginTop: 30, }}
                  />
                )
              }
              
            </View>
          </KeyboardAvoidingView>
        </View>
      </View>
    );
  }
}