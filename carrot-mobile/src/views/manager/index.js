import React, {Component} from 'react';
import {
  Modal, 
  Text, 
  TouchableHighlight, 
  View,
  Image
} from 'react-native';
import { 
  Button, 
  FormLabel, 
  FormInput, 
  FormValidationMessage,
  Header,
  Icon,
} from 'react-native-elements';

import * as Constanta from '../../config/constanta';
import * as Utility from '../../config/utility';

export default class Manager extends Component {
  state = {
    // modalVisible: false,
    user: null,
  };

  // setModalVisible(visible) {
  //   this.setState({modalVisible: visible});
  // }

  componentDidMount(){
    let user = this.props.navigation.state.params.user;
    this.setState({ user });
  }

  openProfile = () => {
    this.props.navigation.navigate('Profile', {'user': this.state.user});
  }

  render() {
    return (
      <View style={{flex: 1, }} >
        <Header
          statusBarProps={{ barStyle: 'light-content' }}
          // leftComponent={{ text: 'Login', style: { color: '#fff' } }}
          // centerComponent={{ text: 'Login', style: { color: '#fff' } }}
          outerContainerStyles={{ backgroundColor: Constanta.BG_COLOR_BASIC, height: 0, }}
          innerContainerStyles={{ justifyContent: 'space-around' }}
        />

        <View style={{flex: 1, }} >
          <Image 
            source = { require('../../assets/img/icon.png') }
            style = {{ width: 180, height: 180 }}
          />
        </View>
        <View style={{
          height: 38, justifyContent: 'flex-end', 
          backgroundColor: Constanta.BG_COLOR_PRIMARY
        }} >
          <View style={{flex: 1, justifyContent: 'center', margin: 'auto'}} >
            <View style={{flexDirection: 'row', justifyContent: 'space-around'}} >
              <View style={{}} >
                <Icon 
                  name='home'
                  size={30}
                  color={Constanta.COLOR_PRIMARY}
                  // containerStyle={{width: 50}}
                />
              </View>
              <View style={{}} >
                <Icon 
                  // reverse
                  name='people'
                  size={30}
                  color={Constanta.BG_COLOR_BASIC}
                  // containerStyle={{ width: 50}}
                />
              </View>
              <View style={{}} >
                <Icon 
                  // raised
                  name='cake'
                  size={30}
                  color={Constanta.BG_COLOR_BASIC}
                  // containerStyle={{width: 50}}
                />
              </View>
              <View style={{}} >
                <Icon 
                  name='notifications'
                  size={30}
                  color={Constanta.BG_COLOR_BASIC}
                  // containerStyle={{width: 50}}
                />
              </View>
              <View style={{}} >
                <Icon 
                  name='person'
                  size={30}
                  color={Constanta.BG_COLOR_BASIC}
                  // containerStyle={{ width: 50}}
                  onPress={this.openProfile}
                />
              </View>
            </View>
          </View>
          
        </View>
      </View>
    );
  }
}
