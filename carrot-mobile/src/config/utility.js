import React from 'react';
import * as Constanta from './constanta';
import moment from 'moment';

export async function getData(url, debug) {
  if(debug)
    console.log(url);
  try {
    let response = await fetch(url);
    let responseJson = await response.json();
    if(debug)
      console.log(responseJson);
    return responseJson;
  } catch (error) {
    if(debug)
      console.log(error);
    return null;
  }
}

// params = {
//   firstParam: 'yourValue',
//   secondParam: 'yourOtherValue
// }
export async function sendData(url, params = {}, debug) {
  let options = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(params)
  }

  if(debug){
    console.log(url);
    console.log(params);
  }

  try {
    let response = await fetch(url, options);
    let responseJson = await response.json();
    if(debug)
      console.log(responseJson);
    return responseJson;
  } catch (error) {
    if(debug)
      console.log(error);
    return null;
  }
}

/**
 * 
 * @param {*} user 
 * can be used for get routing name
 */
export function getRole(user) {
  if (user.admin)
    return 'Admin';

  switch (user.role) {
    case Constanta.MANAGER_ROLE:
      return 'Manager';
    case Constanta.EMPLOYEE_ROLE:
      return 'Employee';
    case Constanta.ROOT_ADMIN_ROLE:
      return 'RootAdmin';
    case Constanta.SENIOR_MANAGER_ROLE:
      return 'SeniorManager';
    default:
      return 'Employee';
  }
}

export function isValidEmail(email) {
  let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

export function getFormattedDate(date, format) {
  if(!date)
    return '-';
  
  if(!format)
    format = 'MMM D, YYYY';
  
  return moment(date).format(format);
}

export function getAge(dob, withSuffix) {
  if(!dob)
    return '-';

  let age = moment().get('year') - moment(dob).get('year')
  return `${age} ${withSuffix && 'years'}`;
}

export function getIconName(type) {
  switch (type) {
    case Constanta.ACHIEVEMENT_TYPE:
      return 'gift';
    case Constanta.REWARD_TYPE:
      return 'shopping-cart';
    case Constanta.SOCIAL_TYPE:
      return 'slideshare';
    case Constanta.SHARED_TYPE:
      return 'social-myspace';
  
    default:
      return 'gift';
  }
}

export function getIconType(type) {
  switch (type) {
    // case Constanta.ACHIEVEMENT_TYPE:
    //   return 'font-awesome';
    // case Constanta.REWARD_TYPE:
    //   return 'font-awesome';
    // case Constanta.SOCIAL_TYPE:
    //   return 'font-awesome';
    case Constanta.SHARED_TYPE:
      return 'foundation';
  
    default:
      return 'font-awesome';
  }
}

export function getType(type) {
  switch (type) {
    case Constanta.ACHIEVEMENT_TYPE:
      return 'Achievement';
    case Constanta.REWARD_TYPE:
      return 'Reward';
    case Constanta.SOCIAL_TYPE:
      return 'Social';
    case Constanta.SHARED_TYPE:
      return 'Shared';
  
    default:
      return 'Achievement';
  }
  
}