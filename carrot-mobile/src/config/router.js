import React from 'react';
import { 
  createStackNavigator, 
  createBottomTabNavigator 
} from 'react-navigation';
import { 
  Icon,
} from 'react-native-elements';

import * as Constanta from '../config/constanta';

import Login from '../views/login';
import Profile from '../views/profile';

import Employee from '../views/employee/index';
import Group from '../views/employee/group';
import Birthday from '../views/employee/birthday';
import Notification from '../views/employee/notification';
import Transaction from '../views/employee/transaction';

import Manager from '../views/manager/index';
import SeniorManager from '../views/senior-manager/index';
import Admin from '../views/admin/index';
import RootAdmin from '../views/root-admin/index';

const EmployeeTab = createBottomTabNavigator(
  {
    Home: {
      screen: Employee,
    },
    Group: {
      screen: Group,
    },
    Birthday: {
      screen: Birthday,
    },
    Notification: {
      screen: Notification,
    },
    Profile: {
      screen: Profile,
    },
  },
  {
    initialRouteName: 'Home',
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        switch (routeName) {
          case 'Home':
            iconName = 'home';
            break;
          case 'Group':
            iconName = 'people';
            break;
          case 'Birthday':
            iconName = 'cake';
            break;
          case 'Notification':
            iconName = 'notifications';
            break;
          case 'Profile':
            iconName = 'person';
            break;
        
          default:
            iconName = 'home';
            break;
        }
        // if (routeName === 'Home') {
        //   // iconName = `ios-information-circle${focused ? '' : '-outline'}`;
        // } else if (routeName === 'Profile') {
        //   // iconName = `ios-information-circle${focused ? '' : '-outline'}`;
        // }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Icon name={iconName} size={30} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: Constanta.COLOR_ORANGE,
      inactiveTintColor: Constanta.COLOR_DARK_GRAY,
      style: {
        backgroundColor: Constanta.COLOR_GRAY,
        height: 38,
      },
      showLabel: false,
    },
  }
)

export const Root = createStackNavigator(
  {
    Login: {
      screen: Login,
      // navigationOptions: {
      //   header:null,
      //   headerTitle: 'sad',
      // }
    },
    Profile: {
      screen: Profile,
    },
    Employee: EmployeeTab,
    // Employee: {
    //   screen: Employee,
    // },
    Transaction: {
      screen: Transaction,
    },
    Manager: {
      screen: Manager,
    },
    SeniorManager: {
      screen: SeniorManager,
    },
    Admin: {
      screen: Admin,
    },
    RootAdmin: {
      screen: RootAdmin,
    },
  },
  {
    navigationOptions: {
      header: null,
    },
    // initialRouteName: 'Employee',
  }
);

// export default Root;