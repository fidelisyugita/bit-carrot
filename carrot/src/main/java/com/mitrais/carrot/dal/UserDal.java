package com.mitrais.carrot.dal;

import java.util.Date;
import java.util.List;

import com.mitrais.carrot.model.User;

public interface UserDal {

	List<User> findAll();

//	List<User> getBirthdayUsers();

	List<User> getSeniorManagers();

	List<User> getManagers();

	List<User> getEmployees();

	List<User> getAdmin();

	List<User> getUngroupUsers();

	List<User> getUngroupEmployees();

	List<User> getUngroupManagers();

	List<User> getUsersBySupervisor(String supervisorId);

	List<User> getUsersByGroup(String groupId);

//	List<User> getBirthdayUsers();

	User getUserById(String id);

	User getUserByEmail(String email);

	User addEditUser(User user);

	List<User> getByBirthdate(Date date);

	Object getAllUserSettings(String id);

	String getUserSetting(String id, String key);

	String addUserSetting(String id, String key, String value);
}
