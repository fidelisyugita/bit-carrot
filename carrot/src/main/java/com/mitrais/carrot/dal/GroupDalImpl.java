package com.mitrais.carrot.dal;

import com.mitrais.carrot.model.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GroupDalImpl implements GroupDal {
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Group> findAll() {
		Query query = new Query();
		query.addCriteria(Criteria.where("active").is(true));
		return mongoTemplate.find(query, Group.class);
	}

	@Override
	public List<Group> getGroupsBySupervisor(String supervisorId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("managerId").is(supervisorId));
		return mongoTemplate.find(query, Group.class);
	}

	@Override
	public Group getGroupById(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, Group.class);
	}

	@Override
	public Group getGroupByName(String name) {
		Query query = new Query();
		query.addCriteria(Criteria.where("name").is(name));
		return mongoTemplate.findOne(query, Group.class);
	}

	@Override
	public Group addEditGroup(Group group) {
		mongoTemplate.save(group);
		return group;
	}
}
