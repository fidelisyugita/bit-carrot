package com.mitrais.carrot.dal;

import com.mitrais.carrot.config.Constanta;
import com.mitrais.carrot.model.Content;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class ContentDalImpl implements ContentDal {
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Content> findAll() {
		Query query = new Query();

		Criteria criteria = new Criteria().orOperator(
				Criteria.where("deleted").exists(false),
				Criteria.where("deleted").is(false)
		);

		query.addCriteria(criteria);
		return mongoTemplate.find(query, Content.class);
//		return mongoTemplate.findAll(Content.class);
	}

	@Override
	public Content getContentById(String id) {
		Query query = new Query();

		query.addCriteria(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, Content.class);
	}

	@Override
	public boolean deleteContentById(String id) {
		Query query = new Query();

		query.addCriteria(Criteria.where("id").is(id));
		DeleteResult res = mongoTemplate.remove(query, Content.class);
		if (res.wasAcknowledged())
			return true;
		return false;
	}

	@Override
	public List<Content> getContentByType(int type) {
		Query query = new Query();

		Criteria cDeleted = new Criteria().orOperator(
				Criteria.where("deleted").exists(false),
				Criteria.where("deleted").is(false)
		);

		Criteria cActive = new Criteria().orOperator(
				Criteria.where("active").exists(false),
				Criteria.where("active").is(true)
		);

		Criteria criteria = new Criteria().andOperator(
				cDeleted, cActive, Criteria.where("type").is(type)
		);

		query.addCriteria(criteria);
		return mongoTemplate.find(query, Content.class);
	}

	@Override
	public List<Content> getContentExpiredAndNotClosed() {
		Query query = new Query();

		Criteria cDeleted = new Criteria().orOperator(
				Criteria.where("deleted").exists(false),
				Criteria.where("deleted").is(false)
		);

		Criteria cActive = new Criteria().orOperator(
				Criteria.where("active").exists(false),
				Criteria.where("active").is(true)
		);

		Criteria criteria = new Criteria().andOperator(
				cDeleted, cActive, Criteria.where("type").is(Constanta.REWARD_TYPE), Criteria.where("expired").lt(new Date())
		);

		query.addCriteria(criteria);
		return mongoTemplate.find(query, Content.class);
	}

	@Override
	public Content getContentByName(String name) {
		Query query = new Query();
		query.addCriteria(Criteria.where("name").is(name));
		return mongoTemplate.findOne(query, Content.class);
	}

	@Override
	public Content addEditContent(Content content) {
		mongoTemplate.save(content);
		return content;
	}
}
