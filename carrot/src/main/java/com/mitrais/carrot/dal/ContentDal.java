package com.mitrais.carrot.dal;

import com.mitrais.carrot.model.Content;
import com.mongodb.client.result.DeleteResult;

import java.util.List;

public interface ContentDal {

	List<Content> findAll();

	Content getContentById(String id);

	boolean deleteContentById(String id);

//	List<Content> getContentByGroup(String groupId);

	List<Content> getContentByType(int type);

	List<Content> getContentExpiredAndNotClosed();

	Content getContentByName(String name);

	Content addEditContent(Content content);
}
