package com.mitrais.carrot.dal;

import com.mitrais.carrot.model.Content;
import com.mitrais.carrot.model.Notification;

import java.util.Date;
import java.util.List;

public interface NotificationDal {

	List<Notification> findAll();

	Notification getById(String id);

	List<Notification> getByType(int type);

	List<Notification> getByUser(String userId);

	List<Notification> getByTypeAndDate(int type, Date date);

	Notification addEdit(Notification notification);

	boolean isDuplicated(Notification notification);

}
