package com.mitrais.carrot.dal;

import com.mitrais.carrot.model.Group;
import com.mitrais.carrot.model.Transaction;

import java.util.Date;
import java.util.List;

public interface TransactionDal {

	List<Transaction> findAll();

	List<Transaction> getTransactionsByContent(String contentId);

	List<Transaction> getTransactionsByContent(String contentId, int year, int month);

	Boolean haveSendToday(String senderId, String receiverId);

	List<Transaction> getTransactionsBySender(String senderId);

	List<Transaction> getTransactionsByReceiver(String receiverId);

	List<Transaction> getTransaction(String receiverId, String contentId);

	List<Transaction> getTransactionsByUser(String userId);

	List<Transaction> getTransactions(String userId, int type);

	Transaction getTransactionById(String id);

	List<Transaction> getTransactionByType(int type);

	Transaction addEdit(Transaction group);
}
