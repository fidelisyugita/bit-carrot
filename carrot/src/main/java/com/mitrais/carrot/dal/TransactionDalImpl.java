package com.mitrais.carrot.dal;

import com.mitrais.carrot.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Repository
public class TransactionDalImpl implements TransactionDal {
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Transaction> findAll() {
		Query query = new Query();
//		query.addCriteria(Criteria.where("active").is(true));
		return mongoTemplate.find(query, Transaction.class);
	}

	@Override
	public List<Transaction> getTransactionsByContent(String contentId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("contentId").is(contentId));
		return mongoTemplate.find(query, Transaction.class);
	}

	@Override
	public List<Transaction> getTransactionsByContent(String contentId, int year, int month) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd");
//		String dateText = sdf.format(date);
		String dateText = year + "-" + month + "-00";
		System.out.println(dateText);
		try {
			Date minDate = sdf.parse(dateText);
//			Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));

			Criteria criteria = new Criteria().andOperator(
					Criteria.where("contentId").is(contentId),
					Criteria.where("dateOccured").gt(minDate)
//					Criteria.where("dateOccured").lt(maxDate)
			);
			Query query = new Query();
			query.addCriteria(criteria);
			return mongoTemplate.find(query, Transaction.class);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Boolean haveSendToday(String senderId, String receiverId) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd");
		String dateText = sdf.format(new Date());
		System.out.println(dateText);
		try {
			Date minDate = sdf.parse(dateText);
			Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));

			Criteria criteria = new Criteria().andOperator(
					Criteria.where("senderId").is(senderId),
					Criteria.where("receiverId").is(receiverId),
					Criteria.where("dateOccured").gt(minDate),
					Criteria.where("dateOccured").lt(maxDate)
			);
			Query query = new Query();
			query.addCriteria(criteria);
			List<Transaction> transactions = mongoTemplate.find(query, Transaction.class);
			if (transactions.size() > 0)
				return true;
			else
				return false;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public List<Transaction> getTransactionsBySender(String senderId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("senderId").is(senderId));
		return mongoTemplate.find(query, Transaction.class);
	}

	@Override
	public List<Transaction> getTransactionsByReceiver(String receiverId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("receiverId").is(receiverId));
		return mongoTemplate.find(query, Transaction.class);
	}

	@Override
	public List<Transaction> getTransaction(String receiverId, String contentId) {
		Query query = new Query();
		query.addCriteria(new Criteria().andOperator(
				Criteria.where("receiverId").is(receiverId), Criteria.where("contentId").is(contentId)
		));
		return mongoTemplate.find(query, Transaction.class);
	}

	@Override
	public List<Transaction> getTransactionsByUser(String userId) {
		Query query = new Query();
		query.addCriteria(new Criteria().orOperator(
				Criteria.where("senderId").is(userId),
				Criteria.where("receiverId").is(userId)
		));
		return mongoTemplate.find(query, Transaction.class);
	}

	@Override
	public List<Transaction> getTransactions(String userId, int type) {
		Query query = new Query();

		Criteria cUser = new Criteria().orOperator(
				Criteria.where("senderId").is(userId),
				Criteria.where("receiverId").is(userId)
		);

		Criteria criteria = new Criteria().andOperator(
				cUser, Criteria.where("type").is(type)
		);

		query.addCriteria(criteria);
		return mongoTemplate.find(query, Transaction.class);
	}

	@Override
	public Transaction getTransactionById(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, Transaction.class);
	}

	@Override
	public List<Transaction> getTransactionByType(int type) {
		Query query = new Query();
		query.addCriteria(Criteria.where("type").is(type));
		return mongoTemplate.find(query, Transaction.class);
	}

	@Override
	public Transaction addEdit(Transaction transaction) {
		if (transaction.getAmount() < 1)
			return null;
		mongoTemplate.save(transaction);
		return transaction;
	}
}
