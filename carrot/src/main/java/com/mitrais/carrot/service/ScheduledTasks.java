package com.mitrais.carrot.service;

import com.mitrais.carrot.api.ContentApi;
import com.mitrais.carrot.config.Constanta;
import com.mitrais.carrot.config.Utility;
import com.mitrais.carrot.dal.*;
import com.mitrais.carrot.model.*;
import com.mitrais.carrot.repository.ConfigRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.util.Calendar;
import java.util.List;


@Component
public class ScheduledTasks {
    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    @Autowired
    private TransactionDal transactionDal;
    @Autowired
    private GroupDal groupDal;
    @Autowired
    private UserDal userDal;
    @Autowired
    private ContentDal contentDal;
    @Autowired
    private ContentApi contentApi;
    @Autowired
    private NotificationDal notificationDal;
    @Autowired
    private ManagerGroupDal managerGroupDal;
    @Autowired
    private ConfigRepository configRepository;
    @Autowired
    private JavaMailSender javaMailSender;

//    @Scheduled(cron = "0 0 1 * * *")
    @Scheduled(fixedRate = 1000 * 60 * 60 * 24)
//    @Scheduled(fixedRate = 1000 * 10)
    public void birthdayGiftManager(){
        log.info("------ autosend birthday gift manager start ------");

//        Date dateNow = new Date();
        Calendar dateNow = Calendar.getInstance();
        dateNow.set(Calendar.DATE, dateNow.get(Calendar.DATE) - 0);
//        MonthDay now = MonthDay.of(dateNow.getMonth(), dateNow.getDay());

        List<ManagerGroup> groups = managerGroupDal.findAll();

        for (ManagerGroup group : groups){
            for (String memberId : group.getMembersId()) {

                User user = userDal.getUserById(memberId);

//                log.info(user.getName() + " = " + user.getDob());


                if (user.getDob() != null) {
                    Calendar dob = Calendar.getInstance();
                    dob.setTime(user.getDob());

                    if (dateNow.get(Calendar.MONTH) == dob.get(Calendar.MONTH) &&
                            dateNow.get(Calendar.DATE) == dob.get(Calendar.DATE)) {

                        log.info("Birthday gift to: " + user.getName());

//                        Create birthday notification
                        String desc = "It's " + user.getName() + "'s birthday!";
                        Notification notification = new Notification(Constanta.BIRTHDAY_NOTIF, user.getId(), desc, dateNow.getTime());
                        if (!notificationDal.isDuplicated(notification))
                            notificationDal.addEdit(notification);
                    }
                }
//                if (user.getDob() != null &&
//                        (dateNow.getMonth() == user.getDob().getMonth() &&
//                                dateNow.getDate() == user.getDob().getDate())
//                        ) {
//
//                    log.info("Birthday gift to: " + user.getName());
//
////                        Create birthday notification
//                    String desc = "It's " + user.getName() + "'s birthday!";
//                    Notification notification = new Notification(Constanta.BIRTHDAY_NOTIF, user.getId(), desc);
//                    if (!notificationDal.isDuplicated(notification))
//                        notificationDal.addEdit(notification);
//
//                }
            }
        }

    }

//    @Scheduled(cron = "0 0 1 * * *")
    @Scheduled(fixedRate = 1000 * 60 * 60 * 24)
//    @Scheduled(fixedRate = 1000 * 10)
    public void birthdayGiftStaffGroup(){
        log.info("------ autosend birthday gift staff start ------");

//        Date dateNow = new Date();
        Calendar dateNow = Calendar.getInstance();
        dateNow.set(Calendar.DATE, dateNow.get(Calendar.DATE) - 0);
//        MonthDay now = MonthDay.of(dateNow.getMonth(), dateNow.getDay());

        List<Group> groups = groupDal.findAll();
        Config config = configRepository.findAll().get(0);

        for (Group group : groups){
            User manager = userDal.getUserById(group.getManagerId());
            for (String memberId : group.getMembersId()) {

                User user = userDal.getUserById(memberId);
//                    System.out.println(user.getName() + " - " + user.getDob());

//                    if (user.getDob() != null) {
//                        MonthDay birthday = MonthDay.of(user.getDob().getMonth(), user.getDob().getDay());
//                        System.out.println(user.getName() + " - " + birthday + " - " + now);
//                        if (birthday.compareTo(now) == 0){
//                            System.out.println("cihuy");
//                        }
//                    }

//                log.info(user.getName() + " - " + user.getDob());

                if (user.getDob() != null) {
                    Calendar dob = Calendar.getInstance();
                    dob.setTime(user.getDob());

                    if (dateNow.get(Calendar.MONTH) == dob.get(Calendar.MONTH) &&
                            dateNow.get(Calendar.DATE) == dob.get(Calendar.DATE)) {

                        log.info("Birthday gift to: " + user.getName());

//                        Create birthday notification
                        String desc = "It's " + user.getName() + "'s birthday!";
                        Notification notification = new Notification(Constanta.BIRTHDAY_NOTIF, user.getId(), desc, dateNow.getTime());
                        if (!notificationDal.isDuplicated(notification)) {
                            notificationDal.addEdit(notification);

//                        Admin autosend
                            if (group.isAutoCarrotAdmin()) {
                                Transaction t1 = new Transaction(
                                        null, user.getId(), null, group.getBirthdayCarrotAdmin(), "Birthday gift from Mitrais",
                                        Constanta.ACHIEVEMENT_TYPE, true);
                                t1 = transactionDal.addEdit(t1);
                                if (t1 != null) {
                                    log.info("Transaction saved");

                                    user.setCarrot(user.getCarrot() + group.getBirthdayCarrotAdmin());
                                    user = userDal.addEditUser(user);
                                    if (user != null)
                                        log.info("User carrot updated");
                                }
                            }

//                        Manager autosend
                            if (group.isAutoCarrotManager()) {
                                Transaction t2 = new Transaction(
                                        group.getManagerId(), user.getId(), null, group.getBirthdayCarrotManager(), "Birthday gift from Manager",
                                        Constanta.ACHIEVEMENT_TYPE, true);
                                t2 = transactionDal.addEdit(t2);
                                if (t2 != null) {
                                    log.info("Transaction saved");

                                    user.setCarrot(user.getCarrot() + group.getBirthdayCarrotManager());
                                    user = userDal.addEditUser(user);
                                    if (user != null) {
                                        log.info("User carrot updated");
                                        if (manager.getCarrot() < (config != null ? config.getWarningCarrot() : 100)) {
                                            try {
                                                Utility.sendEmail(javaMailSender, manager.getEmail(), "Stock carrot warning", "Your carrot is low, please use it wisely :)");
                                            } catch (MessagingException e) {
                                                e.printStackTrace();
                                            }
                                            notificationDal.addEdit(new Notification(Constanta.ADMIN_NOTIF, manager.getId(), "Stock carrot warning", "Your carrot is low, please use it wisely :)"));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

//                if (user.getDob() != null &&
//                        (dateNow.getMonth() == user.getDob().getMonth() &&
//                                dateNow.getDate() == user.getDob().getDate())
//                        ) {
//
//                    log.info("Birthday gift to: " + user.getName());
//
////                        Create birthday notification
//                    String desc = "It's " + user.getName() + "'s birthday!";
//                    Notification notification = new Notification(Constanta.BIRTHDAY_NOTIF, user.getId(), desc);
//                    if (!notificationDal.isDuplicated(notification)) {
//                        notificationDal.addEdit(notification);
//
////                        Admin autosend
//                        if (group.isAutoCarrotAdmin()) {
//                            Transaction t1 = new Transaction(
//                                    null, user.getId(), null, group.getBirthdayCarrotAdmin(), "Birthday gift from Mitrais",
//                                    Constanta.ACHIEVEMENT_TYPE, true);
//                            t1 = transactionDal.addEdit(t1);
//                            if (t1 != null) {
//                                log.info("Transaction saved");
//
//                                user.setCarrot(user.getCarrot() + group.getBirthdayCarrotAdmin());
//                                user = userDal.addEditUser(user);
//                                if (user != null)
//                                    log.info("User carrot updated");
//                            }
//                        }
//
////                        Manager autosend
//                        if (group.isAutoCarrotManager()) {
//                            Transaction t2 = new Transaction(
//                                    group.getManagerId(), user.getId(), null, group.getBirthdayCarrotManager(), "Birthday gift from Manager",
//                                    Constanta.ACHIEVEMENT_TYPE, true);
//                            t2 = transactionDal.addEdit(t2);
//                            if (t2 != null) {
//                                log.info("Transaction saved");
//
//                                user.setCarrot(user.getCarrot() + group.getBirthdayCarrotManager());
//                                user = userDal.addEditUser(user);
//                                if (user != null) {
//                                    log.info("User carrot updated");
//                                    if (manager.getCarrot() < (config != null ? config.getWarningCarrot() : 100)) {
//                                        try {
//                                            Utility.sendEmail(javaMailSender, manager.getEmail(), "Stock carrot warning", "Your carrot is low, please use it wisely :)");
//                                        } catch (MessagingException e) {
//                                            e.printStackTrace();
//                                        }
//                                        notificationDal.addEdit(new Notification(Constanta.ADMIN_NOTIF, manager.getId(), "Stock carrot warning", "Your carrot is low, please use it wisely :)"));
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
            }
        }

    }

    //    @Scheduled(cron = "0 0 1 * * *")
    @Scheduled(fixedRate = 1000 * 60 * 60 * 24)
//    @Scheduled(fixedRate = 1000 * 10)
    public void birthdayGiftUngroup() {
        log.info("------ autosend birthday gift ungroup start ------");

//        Date dateNow = new Date();
        Calendar dateNow = Calendar.getInstance();
        dateNow.set(Calendar.DATE, dateNow.get(Calendar.DATE) - 0);

        List<User> users = userDal.getUngroupUsers();
        for (User user: users){

//            log.info(user.getName() + " ~ " + user.getDob());

//            if (user.getDob() != null &&
//                    (dateNow.getMonth() == user.getDob().getMonth() &&
//                            dateNow.getDate() == user.getDob().getDate())
//                    ) {

            if (user.getDob() != null) {
                Calendar dob = Calendar.getInstance();
                dob.setTime(user.getDob());

                if (dateNow.get(Calendar.MONTH) == dob.get(Calendar.MONTH) &&
                        dateNow.get(Calendar.DATE) == dob.get(Calendar.DATE)) {

                    log.info("Birthday gift to: " + user.getName());

//                        Create birthday notification
                    String desc = "It's " + user.getName() + "'s birthday!";
                    Notification notification = new Notification(Constanta.BIRTHDAY_NOTIF, user.getId(), desc, dateNow.getTime());
                    if (!notificationDal.isDuplicated(notification))
                        notificationDal.addEdit(notification);
                }
            }

        }
    }

    //    @Scheduled(cron = "0 0 1 * * *")
    @Scheduled(fixedRate = 1000 * 60 * 60 * 24)
//    @Scheduled(fixedRate = 1000 * 10)
//
    public void closeExpiredContent() {
        log.info("------ autoclose expired content start ------");

        List<Content> contents = contentDal.getContentExpiredAndNotClosed();
        for (Content content: contents){

            log.info(content.getName() + " ~ " + content.getExpired());

            content.setActive(false);   //closed
            content = contentApi.contentConfig(content);

        }
    }

}
