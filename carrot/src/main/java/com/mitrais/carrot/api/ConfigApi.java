package com.mitrais.carrot.api;

import com.mitrais.carrot.model.Config;
import com.mitrais.carrot.repository.ConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping({"/api/config"})
public class ConfigApi {

	@Autowired
	private ConfigRepository configRepository;

	@GetMapping(value = "")
	public List<Config> getAll() {
		List<Config> listConfig = configRepository.findAll();

		return listConfig;
	}

	@PostMapping(value = "")
	public Config addEditConfig(@RequestBody Config config) {
		return configRepository.save(config);
	}

}
