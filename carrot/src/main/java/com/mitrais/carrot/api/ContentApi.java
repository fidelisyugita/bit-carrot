package com.mitrais.carrot.api;

import com.mitrais.carrot.config.Constanta;
import com.mitrais.carrot.config.Utility;
import com.mitrais.carrot.dal.ContentDal;
import com.mitrais.carrot.dal.GroupDal;
import com.mitrais.carrot.dal.TransactionDal;
import com.mitrais.carrot.dal.UserDal;
import com.mitrais.carrot.model.Content;
import com.mitrais.carrot.model.Transaction;
import com.mitrais.carrot.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping({"/api/content"})
public class ContentApi {

	@Autowired
	private ContentDal contentDal;
	@Autowired
	private GroupDal groupDal;
	@Autowired
	private TransactionDal transactionDal;
	@Autowired
	private UserDal userDal;
	@Autowired
	private JavaMailSender javaMailSender;

	@GetMapping(value = "")
	public List<Content> getAll() {
		List<Content> listContent = contentDal.findAll();

//		for (Content content: listContent){
//			List<Group> groups = new ArrayList<>();
//			for (String groupId: content.getGroupsId()){
//				Group group = groupDal.getGroupById(groupId);
//				groups.add(group);
//			}
//			content.setGroups(groups);
//		}

		return listContent;
	}

	@PostMapping(value = "")
	public Content addEditContent(@RequestBody Content c) {
//		c.setGroups(null);
//
//		boolean newContent = false;
//		boolean closeContent = false;
//
//		if (c.getId() == null)
//			newContent = true;
//
//		Content content = contentDal.addEditContent(c);
//
//		if (content == null)
//			return null;
//		else if (!c.isActive())
//			closeContent = true;
//
//		if (newContent || closeContent) {
//			List<User> listEmployee = new ArrayList<>();
//
//			String subject = newContent ? "New " : "";
//			String message = null;
//
//			if (content.getType() == Constanta.ACHIEVEMENT_TYPE) {
//				subject += "Achievement" + (closeContent ? " closed" : "");
//				message = "Achievement \"" + content.getName() + "\" is" + (closeContent ? " not" : "") + " available!";
//
//				for (String groupId: content.getGroupsId()){
//					listEmployee.addAll(userDal.getUsersByGroup(groupId));
//				}
//
//			}else {
//				listEmployee = userDal.getEmployees();
//
//				if (content.getType() == Constanta.REWARD_TYPE) {
//					subject += "Reward" + (closeContent ? " closed" : "");
//					message = "Reward \"" + content.getName() + "\" is" + (closeContent ? " not" : "") + " available!";
//				} else if (content.getType() == Constanta.SOCIAL_TYPE) {
//					subject += "Social foundation" + (closeContent ? " closed" : "");
//					message = "Social foundation \"" + content.getName() + "\" is" + (closeContent ? " not" : "") + " available!";
//				}
//			}
//
//			for (User employee: listEmployee) {
//				System.out.println("to: " + employee.getEmail());
//				System.out.println("message: " + message);
//
//				try {
//					Utility.sendEmail(javaMailSender, employee.getEmail(), subject, message);
//				} catch (MessagingException e) {
//					e.printStackTrace();
//				}
//			}
//		}

		return contentConfig(c);
	}

	@GetMapping(value = "/{contentId}")
	public Content getContent(@PathVariable String contentId) {
		return contentDal.getContentById(contentId);
	}


	@GetMapping(value = "/plus")
	public List<Content> getContentsPlus() {
		List<Content> listContent = contentDal.findAll();

		int year = YearMonth.now().getYear();
		int month = YearMonth.now().getMonthValue();

		System.out.println(year + " --- " + month);

		for (Content content: listContent){
			List<Transaction> transactions = new ArrayList<>();
			if (content.getType() == Constanta.ACHIEVEMENT_TYPE)
				transactions = transactionDal.getTransactionsByContent(content.getId(), year, month);
			else
				transactions = transactionDal.getTransactionsByContent(content.getId());

			if (content.getType() == Constanta.SOCIAL_TYPE){
				int amount = 0;
				for (Transaction t: transactions){
					amount += t.getAmount();
				}
				content.setTotalUsage(amount);
			} else
				content.setTotalUsage(transactions.size());
		}

		return listContent;
	}
//	@DeleteMapping(value = "/{contentId}")
//	public boolean deleteContent(@PathVariable String contentId) {
//		return contentDal.deleteContentById(contentId);
//	}

	@GetMapping(value = {"/type/{type}", "/type/{type}/{groupId}"})
	public List<Content> getContentByType(@PathVariable int type, @PathVariable Optional<String > groupId) {
		List<Content> contents = contentDal.getContentByType(type);

		if (groupId.isPresent()){
			List<Content> temp = new ArrayList<>();

			contents.forEach(content -> {
				for (String gId: content.getGroupsId()){
					if (gId.equals(groupId.get())){
						temp.add(content);
						break;
					}
				}
			});
			contents = temp;
		}

		return contents;
	}

	@GetMapping(value = {"/plus/{type}", "/plus/{type}/{userId}"})
	public List<Content> getContentPlus(@PathVariable int type, @PathVariable Optional<String> userId) {
		List<Content> contents = contentDal.getContentByType(type);

		if (userId.isPresent()){
			List<Content> temp = new ArrayList<>();

			User user = userDal.getUserById(userId.get());

			contents.forEach(content -> {
				for (String gId: content.getGroupsId()){
					if (user.getGroupId().equals(gId)){
						if (transactionDal.getTransaction(user.getId(), content.getId()).size() > 0)
							content.setCurrentCarrot(1);
						else
							content.setCurrentCarrot(0);
						temp.add(content);
						break;
					}
				}
			});
			contents = temp;
		}

		return contents;
	}

	@PostMapping(value = "/name/{name}")
	public Content getContentByName(@PathVariable String name) {
		return contentDal.getContentByName(name);
	}

	public Content contentConfig(Content c){

		c.setGroups(null);

		boolean newContent = false;
		boolean closeContent = false;

		if (c.getId() == null)
			newContent = true;

		Content content = contentDal.addEditContent(c);

		if (content == null)
			return null;
		else if (!c.isActive())
			closeContent = true;

		if (newContent || closeContent) {
			List<User> listEmployee = new ArrayList<>();

			String subject = newContent ? "New " : "";
			String message = null;

			if (content.getType() == Constanta.ACHIEVEMENT_TYPE) {
				subject += "Achievement" + (closeContent ? " closed" : "");
				message = "Achievement \"" + content.getName() + "\" is" + (closeContent ? " not" : "") + " available!";

				for (String groupId: content.getGroupsId()){
					listEmployee.addAll(userDal.getUsersByGroup(groupId));
				}

			}else {
				listEmployee = userDal.getEmployees();

				if (content.getType() == Constanta.REWARD_TYPE) {
					subject += "Reward" + (closeContent ? " closed" : "");
					message = "Reward \"" + content.getName() + "\" is" + (closeContent ? " not" : "") + " available!";
				} else if (content.getType() == Constanta.SOCIAL_TYPE) {
					subject += "Social foundation" + (closeContent ? " closed" : "");
					message = "Social foundation \"" + content.getName() + "\" is" + (closeContent ? " not" : "") + " available!";
				}
			}

			for (User employee: listEmployee) {
				System.out.println("to: " + employee.getEmail());
				System.out.println("message: " + message);

				try {
					Utility.sendEmail(javaMailSender, employee.getEmail(), subject, message);
				} catch (MessagingException e) {
					e.printStackTrace();
				}
			}
		}

		return content;
	}

}
