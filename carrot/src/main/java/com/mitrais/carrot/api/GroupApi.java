package com.mitrais.carrot.api;

import com.mitrais.carrot.dal.GroupDal;
import com.mitrais.carrot.dal.UserDal;
import com.mitrais.carrot.model.Group;
import com.mitrais.carrot.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping({"/api/group"})
public class GroupApi {

	@Autowired
	private GroupDal groupDal;
	@Autowired
	private UserDal userDal;

	@GetMapping(value = "")
	public List<Group> getAll() {
		List<Group> listGroup = groupDal.findAll();

		for(Group group: listGroup){
			User supervisor = userDal.getUserById(group.getManagerId());
			group.setManager(supervisor);

			List<User> members = new ArrayList<>();
			for (String userId: group.getMembersId()){
				User member = userDal.getUserById(userId);
				if (!member.isAdmin())
					members.add(member);
			}
			group.setMembers(members);
		}

		return listGroup;
	}

	@PostMapping(value = "")
	public Group addEditGroup(@RequestBody Group group) {
		group.setManager(null);
		group.setMembers(null);
		List<User> oldMembers = userDal.getUsersBySupervisor(group.getManagerId());
		for (User user: oldMembers){
			user.setSupervisorId(null);
			user.setGroupMember(false);
			user.setGroupId(null);
			user.setSupervisor(null);
			user.setGroup(null);
			user.setToken(null);
			user.setCarrotOfTheMonth(null);
			user.setCarrotOfTheYear(null);
			user.setCarrotToOther(null);
			user.setCarrotToSocial(null);
			user.setCarrotToReward(null);
			userDal.addEditUser(user);
		}

		for (String memberId: group.getMembersId()){
			User user = userDal.getUserById(memberId);
			user.setSupervisorId(group.getManagerId());
			user.setGroupMember(true);
			user.setGroupId(group.getId());
			user.setSupervisor(null);
			user.setGroup(null);
			user.setToken(null);
			user.setCarrotOfTheMonth(null);
			user.setCarrotOfTheYear(null);
			user.setCarrotToOther(null);
			user.setCarrotToSocial(null);
			user.setCarrotToReward(null);
			userDal.addEditUser(user);
		}
		return groupDal.addEditGroup(group);
	}

	@GetMapping(value = "/supervisor/{supervisorId}")
	public List<Group> getGroupsBySupervisor(@PathVariable String supervisorId) {
		List<Group> listGroup = groupDal.getGroupsBySupervisor(supervisorId);

		for(Group group: listGroup){
			User supervisor = userDal.getUserById(group.getManagerId());
			group.setManager(supervisor);

			List<User> members = new ArrayList<>();
			for (String userId: group.getMembersId()){
				User member = userDal.getUserById(userId);
				if (!member.isAdmin())
					members.add(member);
			}
			group.setMembers(members);
		}

		return listGroup;
	}

	@GetMapping(value = "/{groupId}")
	public Group getGroup(@PathVariable String groupId) {
		return groupDal.getGroupById(groupId);
	}

	@PostMapping(value = "/name/{name}")
	public Group getGroupByName(@PathVariable String name) {
		return groupDal.getGroupByName(name);
	}

}
