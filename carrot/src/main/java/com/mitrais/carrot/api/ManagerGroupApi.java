package com.mitrais.carrot.api;

import com.mitrais.carrot.dal.ManagerGroupDal;
import com.mitrais.carrot.dal.UserDal;
import com.mitrais.carrot.model.ManagerGroup;
import com.mitrais.carrot.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping({"/api/manager-group"})
public class ManagerGroupApi {

	@Autowired
	private ManagerGroupDal groupDal;
	@Autowired
	private UserDal userDal;

	@GetMapping(value = "")
	public List<ManagerGroup> getAll() {
		List<ManagerGroup> listGroup = groupDal.findAll();

		for(ManagerGroup group: listGroup){
			User supervisor = userDal.getUserById(group.getSeniorManagerId());
			group.setSeniorManager(supervisor);

			List<User> members = new ArrayList<>();
			for (String userId: group.getMembersId()){
				User member = userDal.getUserById(userId);
				members.add(member);
			}
			group.setMembers(members);
		}

		return listGroup;
	}

	@PostMapping(value = "")
	public ManagerGroup addEditGroup(@RequestBody ManagerGroup group) {
		group.setSeniorManager(null);
		group.setMembers(null);
		List<User> oldMembers = userDal.getUsersBySupervisor(group.getSeniorManagerId());
		for (User user: oldMembers){
			user.setSupervisorId(null);
			user.setGroupMember(false);
			user.setGroupId(null);
			user.setSupervisor(null);
			user.setGroup(null);
			user.setToken(null);
			user.setCarrotOfTheMonth(null);
			user.setCarrotOfTheYear(null);
			user.setCarrotToOther(null);
			user.setCarrotToSocial(null);
			user.setCarrotToReward(null);
			userDal.addEditUser(user);
		}

		for (String memberId: group.getMembersId()){
			User user = userDal.getUserById(memberId);
			user.setSupervisorId(group.getSeniorManagerId());
			user.setGroupMember(true);
			user.setSupervisor(null);
			user.setGroup(null);
			user.setGroupId(group.getId());
			user.setToken(null);
			user.setCarrotOfTheMonth(null);
			user.setCarrotOfTheYear(null);
			user.setCarrotToOther(null);
			user.setCarrotToSocial(null);
			user.setCarrotToReward(null);
			userDal.addEditUser(user);
		}

		return groupDal.addEditGroup(group);
	}

	@GetMapping(value = "/supervisor/{supervisorId}")
	public List<ManagerGroup> getGroupsBySupervisor(@PathVariable String supervisorId) {
		List<ManagerGroup> listGroup = groupDal.getGroupsBySupervisor(supervisorId);

		for(ManagerGroup group: listGroup){
			User supervisor = userDal.getUserById(group.getSeniorManagerId());
			group.setSeniorManager(supervisor);

			List<User> members = new ArrayList<>();
			for (String userId: group.getMembersId()){
				User member = userDal.getUserById(userId);
				members.add(member);
			}
			group.setMembers(members);
		}

		return listGroup;
	}

	@GetMapping(value = "/{groupId}")
	public ManagerGroup getGroup(@PathVariable String groupId) {
		return groupDal.getGroupById(groupId);
	}

	@PostMapping(value = "/name/{name}")
	public ManagerGroup login(@PathVariable String name) {
		return groupDal.getGroupByName(name);
	}

}
