package com.mitrais.carrot.config;

public class Constanta {


//	public static final int STAFF_ROLE = 1;
//	public static final int ADMIN_ROLE = 2;
//	public static final int UNKNOWN_ROLE = 3;
	public static final int MANAGER_ROLE = 4;
	public static final int EMPLOYEE_ROLE = 5;
	public static final int ROOT_ADMIN_ROLE = 6;
//	public static final int STAKEHOLDER_ROLE = 7;
	public static final int SENIOR_MANAGER_ROLE = 8;

	public static final int ACHIEVEMENT_TYPE = 1;
	public static final int REWARD_TYPE = 2;
	public static final int SOCIAL_TYPE = 3;
	public static final int SHARING_TYPE = 4;

	public static final int BIRTHDAY_NOTIF = 1;
	public static final int NEWSLETTER_NOTIF = 2;
	public static final int ADMIN_NOTIF = 3;

}
