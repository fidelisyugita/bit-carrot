package com.mitrais.carrot.model;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ManagerGroup {

    @Id
    private String id;
    private String name;
    private String seniorManagerId;
    private List<String> membersId = new ArrayList<>();
    private Date creationDate = new Date();
    private boolean active = true;

    private User seniorManager = null;
    private List<User> members = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeniorManagerId() {
        return seniorManagerId;
    }

    public void setSeniorManagerId(String seniorManagerId) {
        this.seniorManagerId = seniorManagerId;
    }

    public List<String> getMembersId() {
        return membersId;
    }

    public void setMembersId(List<String> membersId) {
        this.membersId = membersId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public User getSeniorManager() {
        return seniorManager;
    }

    public void setSeniorManager(User seniorManager) {
        this.seniorManager = seniorManager;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }
}
