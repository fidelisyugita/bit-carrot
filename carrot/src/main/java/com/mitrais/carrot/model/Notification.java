package com.mitrais.carrot.model;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class Notification {


    @Id
    private String id;
    private int type;
//    private String contentId;
    private String adminId;
    private String userId;
    private String title;
    private String description;
    private Date creationDate = new Date();

    public Notification() {
    }

    public Notification(int type, String userId, String title, Date creationDate) {
        this.type = type;
        this.userId = userId;
        this.title = title;
        this.creationDate = creationDate;
    }

    public Notification(int type, String userId, String title, String description) {
        this.type = type;
        this.userId = userId;
        this.title = title;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
