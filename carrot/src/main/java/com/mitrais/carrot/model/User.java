package com.mitrais.carrot.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mitrais.carrot.config.Constanta;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class User {

	@Id
	private String id;
//	@Email(message = "sorry, email is not valid!")
	private String email;
	@NotNull
	private String password;
	private String name;
	private String picture = null;
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dob;
	private String address;
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date joinDate;
	private int role = Constanta.EMPLOYEE_ROLE;
	private int carrot = 0;
	private String supervisorId = null;
	private String groupId = null;
	private Date creationDate = new Date();
	private Map<String, String> settings = new HashMap<>();

	private boolean groupMember = false;
	private boolean admin = false;
	private boolean approved = true;
	private boolean active = true;
	private boolean alwaysLate = false;

	private Integer carrotOfTheMonth = null;
	private Integer carrotOfTheYear = null;
	private Integer carrotToReward = null;
	private Integer carrotToSocial = null;
	private Integer carrotToOther = null;

	private String token = null;

	private User supervisor;
	private Group group;

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	public int getRole() {
		return role;
	}
	public void setRole(int role) {
		this.role = role;
	}
	public int getCarrot() {
		return carrot;
	}
	public void setCarrot(int carrot) {
		this.carrot = carrot;
	}

	public String getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Map<String, String> getSettings() {
		return settings;
	}
	public void setSettings(Map<String, String> settings) {
		this.settings = settings;
	}

	public boolean isGroupMember() {
		return groupMember;
	}

	public void setGroupMember(boolean groupMember) {
		this.groupMember = groupMember;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Integer getCarrotOfTheMonth() {
		return carrotOfTheMonth;
	}

	public void setCarrotOfTheMonth(Integer carrotOfTheMonth) {
		this.carrotOfTheMonth = carrotOfTheMonth;
	}

	public Integer getCarrotOfTheYear() {
		return carrotOfTheYear;
	}

	public void setCarrotOfTheYear(Integer carrotOfTheYear) {
		this.carrotOfTheYear = carrotOfTheYear;
	}

	public Integer getCarrotToReward() {
		return carrotToReward;
	}

	public void setCarrotToReward(Integer carrotToReward) {
		this.carrotToReward = carrotToReward;
	}

	public Integer getCarrotToSocial() {
		return carrotToSocial;
	}

	public void setCarrotToSocial(Integer carrotToSocial) {
		this.carrotToSocial = carrotToSocial;
	}

	public Integer getCarrotToOther() {
		return carrotToOther;
	}

	public void setCarrotToOther(Integer carrotToOther) {
		this.carrotToOther = carrotToOther;
	}

	public boolean isAlwaysLate() {
		return alwaysLate;
	}

	public void setAlwaysLate(boolean alwaysLate) {
		this.alwaysLate = alwaysLate;
	}

	public User getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(User supervisor) {
		this.supervisor = supervisor;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}
}
