package com.mitrais.carrot.model;

import org.springframework.data.annotation.Id;

import java.util.*;

public class Group {

    @Id
    private String id;
    private String name;
    private String managerId;
    private List<String> membersId = new ArrayList<>();
    private int birthdayCarrotAdmin = 0;
    private boolean autoCarrotAdmin = true;
    private int birthdayCarrotManager = 0;
    private boolean autoCarrotManager = true;
    private Date creationDate = new Date();
    private boolean active = true;

    private User manager = null;
    private List<User> members = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public List<String> getMembersId() {
        return membersId;
    }

    public void setMembersId(List<String> membersId) {
        this.membersId = membersId;
    }

    public int getBirthdayCarrotAdmin() {
        return birthdayCarrotAdmin;
    }

    public void setBirthdayCarrotAdmin(int birthdayCarrotAdmin) {
        this.birthdayCarrotAdmin = birthdayCarrotAdmin;
    }

    public boolean isAutoCarrotAdmin() {
        return autoCarrotAdmin;
    }

    public void setAutoCarrotAdmin(boolean autoCarrotAdmin) {
        this.autoCarrotAdmin = autoCarrotAdmin;
    }

    public int getBirthdayCarrotManager() {
        return birthdayCarrotManager;
    }

    public void setBirthdayCarrotManager(int birthdayCarrotManager) {
        this.birthdayCarrotManager = birthdayCarrotManager;
    }

    public boolean isAutoCarrotManager() {
        return autoCarrotManager;
    }

    public void setAutoCarrotManager(boolean autoCarrotManager) {
        this.autoCarrotManager = autoCarrotManager;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public User getManager() {
        return manager;
    }

    public void setManager(User manager) {
        this.manager = manager;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }
}
