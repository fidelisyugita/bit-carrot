package com.mitrais.carrot.model;

import java.util.Date;

public class ContentChange {

    private String parameterChanged;
    private String from;
    private Date on = new Date();

    public String getParameterChanged() {
        return parameterChanged;
    }

    public void setParameterChanged(String parameterChanged) {
        this.parameterChanged = parameterChanged;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Date getOn() {
        return on;
    }

    public void setOn(Date on) {
        this.on = on;
    }
}
