package com.mitrais.carrot.model;

import org.springframework.data.annotation.Id;

public class Config {

    @Id
    private String id;
    private int carrotLimit;
    private int yearlyCarrot;
    private int stockCarrot;
    private int warningCarrot;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCarrotLimit() {
        return carrotLimit;
    }

    public void setCarrotLimit(int carrotLimit) {
        this.carrotLimit = carrotLimit;
    }

    public int getYearlyCarrot() {
        return yearlyCarrot;
    }

    public void setYearlyCarrot(int yearlyCarrot) {
        this.yearlyCarrot = yearlyCarrot;
    }

    public int getStockCarrot() {
        return stockCarrot;
    }

    public void setStockCarrot(int stockCarrot) {
        this.stockCarrot = stockCarrot;
    }

    public int getWarningCarrot() {
        return warningCarrot;
    }

    public void setWarningCarrot(int warningCarrot) {
        this.warningCarrot = warningCarrot;
    }

}
