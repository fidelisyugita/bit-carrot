webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/admin/admin.component.css":
/***/ (function(module, exports) {

module.exports = ".managers {\r\n    color: #444;\r\n    font-family: Arial, Helvetica, sans-serif;\r\n    font-weight: lighter;\r\n    margin: 0 0 2em 0;\r\n    list-style-type: none;\r\n    padding: .3em 0;\r\n    width: 15em;\r\n  }\r\n  .managers li {\r\n    cursor: pointer;\r\n    position: relative;\r\n    left: 0;\r\n    background-color: #EEE;\r\n    margin: 1em;\r\n    /* padding: .3em 0; */\r\n    height: 1.6em;\r\n    border-radius: 4px;\r\n  }\r\n  .managers li.selected:hover {\r\n    background-color: #BBD8DC !important;\r\n    color: white;\r\n  }\r\n  .managers li:hover {\r\n    color: #607D8B;\r\n    background-color: #DDD;\r\n    left: .1em;\r\n  }\r\n  .managers .text {\r\n    position: relative;\r\n    top: -3px;\r\n  }\r\n  .managers .badge {\r\n    display: inline-block;\r\n    font-size: small;\r\n    color: white;\r\n    padding: 0.8em 0.7em 0 0.7em;\r\n    background-color: #607D8B;\r\n    line-height: 1em;\r\n    position: relative;\r\n    left: -1px;\r\n    top: -4px;\r\n    height: 1.8em;\r\n    margin-right: .8em;\r\n    border-radius: 4px 0 0 4px;\r\n  }\r\n  a:not([href]):not([tabindex]) {\r\n    color: #ff5722;\r\n    cursor: pointer;\r\n  }"

/***/ }),

/***/ "./src/app/admin/admin.component.html":
/***/ (function(module, exports) {

module.exports = "<!doctype html>\r\n<html lang=\"en\">\r\n<head>\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\r\n    <meta name=\"description\" content=\"\">\r\n    <meta name=\"author\" content=\"\">\r\n</head>\r\n\r\n<body>\r\n    <div id=\"header\"></div>\r\n\r\n    <!-- Begin page content -->\r\n    <main role=\"main\" class=\"container\">\r\n        <h2 class=\"mt-4 pl-0 text-grey ml-0\">\r\n        ADMINISTRATOR DASHBOARD\r\n       </h2>\r\n    </main>\r\n    <section class=\"admin-tabs py-3\">\r\n        <div class=\"container\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12 px-md-0\">\r\n                    <ul class=\"nav nav-pills mb-3\" id=\"myTab\" role=\"tablist\">\r\n                        <li class=\"nav-item\">\r\n                            <a class=\"nav-link\" (click)=\"openContent()\" [ngClass]=\"{'btn-success': isContent, 'btn-outline-success': !isContent }\">Content List</a>\r\n                        </li>\r\n                        <li class=\"nav-item\">\r\n                            <a class=\"nav-link\" (click)=\"openApproval()\" [ngClass]=\"{'btn-success': isApproval, 'btn-outline-success': !isApproval }\">Approval List</a>\r\n                        </li>\r\n                        <li class=\"nav-item\">\r\n                            <a class=\"nav-link\" (click)=\"openStaff()\" [ngClass]=\"{'btn-success': isStaff, 'btn-outline-success': !isStaff }\">Staff List</a>\r\n                        </li>\r\n                        <li class=\"nav-item\">\r\n                            <a class=\"nav-link\" (click)=\"openGroup()\" [ngClass]=\"{'btn-success': isGroup, 'btn-outline-success': !isGroup }\">Staff Group List</a>\r\n                        </li>\r\n                        <li class=\"nav-item\">\r\n                            <a class=\"nav-link\" (click)=\"openNewsletter()\" [ngClass]=\"{'btn-success': isNewsletter, 'btn-outline-success': !isNewsletter }\">Newsletter</a>\r\n                        </li>\r\n                    </ul>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section>\r\n\r\n    <div>\r\n        <app-content *ngIf=\"isContent\" ></app-content>\r\n        <app-approval *ngIf=\"isApproval\" ></app-approval>\r\n        <app-staff *ngIf=\"isStaff\" ></app-staff>\r\n        <app-group-staff *ngIf=\"isGroup\" ></app-group-staff>\r\n        <app-newsletter *ngIf=\"isNewsletter\" ></app-newsletter>\r\n    </div>\r\n    \r\n    <div id=\"footer\"></div>\r\n</body>\r\n\r\n</html>"

/***/ }),

/***/ "./src/app/admin/admin.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_datatables_net__ = __webpack_require__("./node_modules/datatables.net/js/jquery.dataTables.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_datatables_net___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_datatables_net__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AdminComponent = /** @class */ (function () {
    function AdminComponent(http, userService, appComponent, router) {
        this.http = http;
        this.userService = userService;
        this.appComponent = appComponent;
        this.router = router;
        this.isLoading = false;
    }
    AdminComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInUser = this.userService.getLoggedInUser();
        if (!this.loggedInUser) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_4__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        _this.loggedInUser = res;
                        _this.userService.setLoggedInUser(res);
                        _this.isLoading = false;
                        if (!res.admin)
                            _this.router.navigate(['/']);
                    }
                    else {
                        _this.isLoading = false;
                        _this.router.navigate(['/']);
                    }
                });
            }
            else {
                this.isLoading = false;
                this.router.navigate(['/']);
            }
        }
        else
            this.isLoading = false;
        this.isContent = true;
        this.isApproval = false;
        this.isStaff = false;
        this.isGroup = false;
        this.isNewsletter = false;
    };
    AdminComponent.prototype.openContent = function () {
        this.isContent = true;
        this.isApproval = false;
        this.isStaff = false;
        this.isGroup = false;
        this.isNewsletter = false;
    };
    AdminComponent.prototype.openApproval = function () {
        this.isContent = false;
        this.isApproval = true;
        this.isStaff = false;
        this.isGroup = false;
        this.isNewsletter = false;
    };
    AdminComponent.prototype.openStaff = function () {
        this.isContent = false;
        this.isApproval = false;
        this.isStaff = true;
        this.isGroup = false;
        this.isNewsletter = false;
    };
    AdminComponent.prototype.openGroup = function () {
        this.isContent = false;
        this.isApproval = false;
        this.isStaff = false;
        this.isGroup = true;
        this.isNewsletter = false;
    };
    AdminComponent.prototype.openNewsletter = function () {
        this.isContent = false;
        this.isApproval = false;
        this.isStaff = false;
        this.isGroup = false;
        this.isNewsletter = true;
    };
    AdminComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin',
            template: __webpack_require__("./src/app/admin/admin.component.html"),
            styles: [__webpack_require__("./src/app/admin/admin.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], AdminComponent);
    return AdminComponent;
}());



/***/ }),

/***/ "./src/app/admin/approval/approval.component.css":
/***/ (function(module, exports) {

module.exports = "a:not([href]):not([tabindex]) {\r\n  color: #FFF;\r\n  cursor: pointer;\r\n}"

/***/ }),

/***/ "./src/app/admin/approval/approval.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"admin-tabs py-3\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12 px-md-0\">\r\n\r\n                <div class=\"tab-content search-bo\" id=\"myTabContent\">\r\n                    <div class=\"tab-pane fade show active\" id=\"profile\" role=\"tabpanel\" aria-labelledby=\"profile-tab\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-12\">\r\n                                <hr class=\"box-title-hr\">\r\n                                <h4 class=\"my-2 box-title\">APPROVAL LIST</h4>\r\n                            </div>\r\n                            <div class=\"col-md-12\">\r\n                                <mat-progress-spinner *ngIf=\"loadingApproval\"\r\n                                class=\"text-center m-auto\"\r\n                                color=\"warn\"\r\n                                mode=\"indeterminate\" >\r\n                                </mat-progress-spinner> \r\n                                <table class=\"table table-hover mt-3\" datatable [dtOptions]=\"dtOptionsApproval\" [dtTrigger]=\"dtTriggerApproval\">\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th scope=\"col\">User</th>\r\n                                            <th scope=\"col\">Reward</th>\r\n                                            <th scope=\"col\">Date Request</th>\r\n                                            <th scope=\"col\">Carrot</th>\r\n                                            <th scope=\"col\">Action</th>\r\n                                        </tr>\r\n                                    </thead>\r\n                                    <tbody>\r\n                                        <tr *ngFor=\"let approval of approvals\" >\r\n                                            <td>{{approval.sender.name}}</td>\r\n                                            <td>{{getContentById(approval.contentId)}}</td>\r\n                                            <td>{{getDate(approval.dateOccured)}}</td>\r\n                                            <td>{{approval.amount}}</td>\r\n                                            <td><a class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#approvedMessage\" (click)=\"onSelectApproval(approval.sender, approval)\">Approve</a> <a class=\"btn btn-danger\" data-toggle=\"modal\" data-target=\"#disapprovedMessage\" (click)=\"onSelectApproval(approval.sender, approval)\">Disapprove</a></td>\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n<!-- Modal -->\r\n<!-- Approve Message Modal -->\r\n<div class=\"modal fade\" id=\"approvedMessage\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Send Message to {{selectedUser.name}} regarding Reward Acceptance</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <form>\r\n                    <label for=\"text1\">Message</label>\r\n                    <input #messageApprove type=\"text\" id=\"messageApprove\" name = \"messageApprove\">\r\n                </form>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\" (click)=\"messageApprove.value==''\">Cancel</button>\r\n                <button type=\"button\" class=\"btn btn-carrot radius-5\" (click)=\"setApprove(messageApprove.value); \" data-dismiss=\"modal\">Approve</button>\r\n            </div>\r\n        </div>\r\n    </div> \r\n</div>\r\n\r\n<!-- Disapprove Message Modal -->\r\n<div class=\"modal fade\" id=\"disapprovedMessage\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Send Message to {{selectedUser.name}} regarding Reward Rejection</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <form>\r\n                    <label for=\"text1\">Message</label>\r\n                    <input #messageDisapprove type = \"text\" id=\"messageDisapprove\" name = \"messageDisapprove\">\r\n                </form>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\" (click)=\"messageDisapprove.value==''\">Cancel</button>\r\n                <button type=\"button\" class=\"btn btn-carrot radius-5\" (click)=\"setDisapprove(messageDisapprove.value);messageDisapprove.value = ''\" data-dismiss=\"modal\">Disapprove</button>\r\n            </div>\r\n        </div>\r\n    </div> \r\n</div>"

/***/ }),

/***/ "./src/app/admin/approval/approval.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApprovalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_user__ = __webpack_require__("./src/app/model/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__model_notification__ = __webpack_require__("./src/app/model/notification.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_admin_service__ = __webpack_require__("./src/app/service/admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_transaction_service__ = __webpack_require__("./src/app/service/transaction.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_notification_service__ = __webpack_require__("./src/app/service/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__config_utility__ = __webpack_require__("./src/app/config/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angular_datatables__ = __webpack_require__("./node_modules/angular-datatables/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_datatables_net__ = __webpack_require__("./node_modules/datatables.net/js/jquery.dataTables.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_datatables_net___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_datatables_net__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var ApprovalComponent = /** @class */ (function () {
    function ApprovalComponent(http, adminService, userService, transactionService, notificationService, appComponent, router) {
        this.http = http;
        this.adminService = adminService;
        this.userService = userService;
        this.transactionService = transactionService;
        this.notificationService = notificationService;
        this.appComponent = appComponent;
        this.router = router;
        this.isLoading = false;
        this.transactions = [];
        this.loadingApproval = true;
        this.selectedUser = new __WEBPACK_IMPORTED_MODULE_3__model_user__["a" /* User */];
        this.dtOptionsApproval = {};
        this.dtTriggerApproval = new __WEBPACK_IMPORTED_MODULE_13_rxjs__["Subject"]();
    }
    ApprovalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInUser = this.userService.getLoggedInUser();
        if (!this.loggedInUser) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_10__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        _this.loggedInUser = res;
                        _this.userService.setLoggedInUser(res);
                        _this.isLoading = false;
                        if (!res.admin)
                            _this.router.navigate(['/']);
                    }
                    else {
                        _this.isLoading = false;
                        _this.router.navigate(['/']);
                    }
                });
            }
            else {
                this.isLoading = false;
                this.router.navigate(['/']);
            }
        }
        else
            this.isLoading = false;
        this.getTransaction();
        this.getUsers();
    };
    ApprovalComponent.prototype.getUsers = function () {
        var _this = this;
        return this.adminService.getUsers().subscribe(function (users) {
            _this.users = users;
        });
    };
    ApprovalComponent.prototype.getDate = function (type) {
        return __WEBPACK_IMPORTED_MODULE_9__config_utility__["transformDate"](type, 'dd-MM-yyyy');
    };
    ApprovalComponent.prototype.getTransaction = function () {
        var _this = this;
        this.approvals = [];
        this.transactionService.fetchTransactions().subscribe(function (transactions) {
            _this.transactions = transactions;
            _this.transactions.forEach(function (transaction) {
                if (transaction.type == 2 && transaction.approved == false && transaction.rejected == false)
                    _this.approvals.push(transaction);
            });
            _this.loadingApproval = false;
            _this.dtTriggerApproval.next();
        });
    };
    ApprovalComponent.prototype.getUserNameById = function (id) {
        if (this.users) {
            var name_1 = "";
            this.users.forEach(function (user) {
                if (user.id == id)
                    name_1 = user.name;
            });
            return name_1;
        }
    };
    ApprovalComponent.prototype.getContentById = function (id) {
        if (this.rewards) {
            var name_2 = "";
            this.rewards.forEach(function (reward) {
                if (reward.id == id)
                    name_2 = reward.name;
            });
            return name_2;
        }
    };
    ApprovalComponent.prototype.onSelectApproval = function (user, approval) {
        this.selectedUser = user;
        this.selectedApproval = approval;
    };
    ApprovalComponent.prototype.setApprove = function (message) {
        var _this = this;
        console.log(message);
        if (message != "") {
            this.selectedApproval.approved = true;
            this.transactionService.postTransactionReject(this.selectedApproval).subscribe(function (transactions) {
                _this.dtElement.dtInstance.then(function (dtInstance) {
                    dtInstance.destroy();
                    _this.approvals = [];
                    _this.transactions.forEach(function (transaction) {
                        if (transaction.type == 2 && transaction.approved == false && transaction.rejected == false)
                            _this.approvals.push(transaction);
                    });
                    _this.dtTriggerApproval.next();
                });
                var notifTemp = new __WEBPACK_IMPORTED_MODULE_4__model_notification__["a" /* Notification */];
                notifTemp.adminId = _this.loggedInUser.id;
                notifTemp.userId = _this.selectedUser.id;
                notifTemp.title = "Transaction Approved";
                notifTemp.description = message;
                notifTemp.type = 3;
                _this.notificationService.addEdit(notifTemp).subscribe(function (notif) {
                    if (notif)
                        _this.appComponent.showSuccess(_this.getUserNameById(_this.selectedApproval.senderId) + "'s transaction is approved.");
                });
            });
        }
        else
            this.appComponent.showError("Please fill the message.");
    };
    ApprovalComponent.prototype.setDisapprove = function (message) {
        var _this = this;
        if (message != "") {
            this.selectedApproval.rejected = true;
            this.transactionService.postTransactionReject(this.selectedApproval).subscribe(function (transactions) {
                _this.dtElement.dtInstance.then(function (dtInstance) {
                    dtInstance.destroy();
                    _this.approvals = [];
                    _this.transactions.forEach(function (transaction) {
                        if (transaction.type == 2 && transaction.approved == false && transaction.rejected == false)
                            _this.approvals.push(transaction);
                    });
                    _this.dtTriggerApproval.next();
                });
                var notifTemp = new __WEBPACK_IMPORTED_MODULE_4__model_notification__["a" /* Notification */];
                notifTemp.adminId = _this.loggedInUser.id;
                notifTemp.userId = _this.selectedUser.id;
                notifTemp.title = "Transaction Rejected";
                notifTemp.description = message;
                notifTemp.type = 3;
                _this.notificationService.addEdit(notifTemp).subscribe(function (notif) {
                    if (notif)
                        _this.appComponent.showSuccess(_this.getUserNameById(_this.selectedApproval.senderId) + "'s transaction is rejected.");
                });
            });
        }
        else
            this.appComponent.showError("Please fill the message.");
    };
    ApprovalComponent.prototype.refresh = function () {
        window.location.reload();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_12_angular_datatables__["a" /* DataTableDirective */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_12_angular_datatables__["a" /* DataTableDirective */])
    ], ApprovalComponent.prototype, "dtElement", void 0);
    ApprovalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-approval',
            template: __webpack_require__("./src/app/admin/approval/approval.component.html"),
            styles: [__webpack_require__("./src/app/admin/approval/approval.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__service_admin_service__["a" /* AdminService */],
            __WEBPACK_IMPORTED_MODULE_11__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_6__service_transaction_service__["a" /* TransactionService */],
            __WEBPACK_IMPORTED_MODULE_7__service_notification_service__["a" /* NotificationService */],
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], ApprovalComponent);
    return ApprovalComponent;
}());



/***/ }),

/***/ "./src/app/admin/content/content.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n  a:not([href]):not([tabindex]) {\r\n    color: #FFF;\r\n    cursor: pointer;\r\n  }"

/***/ }),

/***/ "./src/app/admin/content/content.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"admin-tabs py-3\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12 px-md-0\">\r\n\r\n                <div class=\"tab-content search-bo\" id=\"myTabContent\">\r\n                    <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">\r\n                        <div class=\"row\" >\r\n                            <div class=\"col-md-12\">\r\n                                <hr class=\"box-title-hr\">\r\n                                <h4 class=\"my-2 box-title\">CONTENT LIST</h4>\r\n                            </div>\r\n                            <div class=\"col-md-12\" > \r\n                                <mat-progress-spinner *ngIf=\"!rewards\"\r\n                                class=\"text-center m-auto\"\r\n                                color=\"warn\"\r\n                                mode=\"indeterminate\" >\r\n                                </mat-progress-spinner> \r\n                                <table class=\"table table-hover mt-3\" datatable  [dtOptions]=\"dtOptionsContent\" [dtTrigger]=\"dtTriggerContent\">\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th scope=\"col\">Content Type</th>\r\n                                            <th scope=\"col\">User Occupation</th>\r\n                                            <th scope=\"col\">Content Name</th>\r\n                                            <th scope=\"col\">Carrot</th>\r\n                                            <th scope=\"col\">(Achievement) This Month Achiever Count</th>\r\n                                            <th scope=\"col\">(Reward) Redeem Count</th>\r\n                                            <th scope=\"col\">(Social Foundation) Donation Amount</th>\r\n                                            <th scope=\"col\">Status</th>\r\n                                            <th scope=\"col\">Action</th>\r\n                                        </tr>\r\n                                    </thead>\r\n                                    <tbody>           \r\n                                        <tr *ngFor=\"let reward of rewards\" >\r\n                                            <td  data-toggle=\"modal\" data-target=\"#seeDetail\" (click)=\"onSelect(reward)\">{{getRewardType(reward.type)}}</td>\r\n                                            <td  data-toggle=\"modal\" data-target=\"#seeDetail\" (click)=\"onSelect(reward)\">{{getRoleType(reward.role)}}</td>\r\n                                            <td  data-toggle=\"modal\" data-target=\"#seeDetail\" (click)=\"onSelect(reward)\">{{reward.name}}</td>\r\n                                            <td  data-toggle=\"modal\" data-target=\"#seeDetail\" (click)=\"onSelect(reward)\">{{reward.carrot}}</td>\r\n                                            <td  *ngIf=\"reward.type == 1\" data-toggle=\"modal\" data-target=\"#seeDetail\" (click)=\"onSelect(reward)\">{{reward.totalUsage}}</td><td  *ngIf=\"reward.type != 1\" data-toggle=\"modal\" data-target=\"#seeDetail\" (click)=\"onSelect(reward)\">-</td>\r\n                                            <td  *ngIf=\"reward.type == 2\" data-toggle=\"modal\" data-target=\"#seeDetail\" (click)=\"onSelect(reward)\">{{reward.totalUsage}}</td><td  *ngIf=\"reward.type != 2\" data-toggle=\"modal\" data-target=\"#seeDetail\" (click)=\"onSelect(reward)\">-</td>\r\n                                            <td  *ngIf=\"reward.type == 3\" data-toggle=\"modal\" data-target=\"#seeDetail\" (click)=\"onSelect(reward)\">{{reward.totalUsage}}</td><td  *ngIf=\"reward.type != 3\" data-toggle=\"modal\" data-target=\"#seeDetail\" (click)=\"onSelect(reward)\">-</td>\r\n                                            <td  data-toggle=\"modal\" data-target=\"#seeDetail\" (click)=\"onSelect(reward)\">{{getStatus(reward.active)}}</td>\r\n                                            <td><span *ngIf=checkClose(reward.active)><a class=\"btn btn-success\" (click)=\"open(reward)\">Open</a></span> <span *ngIf=checkOpen(reward.active)><a class=\"btn btn-secondary\" data-toggle=\"modal\" data-target=\"#setCloseMessage\" (click)=\"setClose(reward)\">Close</a></span> <a class=\"btn btn-danger\" (click)=\"delete(reward)\">Delete</a> <a class=\"btn btn-warning\" data-toggle=\"modal\" data-target=\"#editReward\" (click)=\"onSelect(reward)\"> Edit</a></td>\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table>\r\n                                <div class=\"text-center mb-3\">\r\n                                <button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#newReward\">\r\n                                <i class=\"fa fa-plus-circle\"></i> ADD NEW CONTENT\r\n                                </button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n<!-- Modal -->\r\n<!-- New Content Modal -->\r\n<div class=\"modal fade\" id=\"newReward\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Add New Reward</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <form>\r\n                    <div class=\"form-group\">\r\n                      <label for=\"text1\">Reward Type</label><br>\r\n                      <select name=\"type\" #type (change)= \"checkContent($event)\">\r\n                        <option value=\"Achievement\" >Achievement</option>\r\n                        <option value=\"Social\">Social Foundation</option>\r\n                        <option value=\"Reward\" selected>Reward</option>\r\n                      </select>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"file\">Choose Image for Content</label>\r\n                        <input type=\"file\" id=\"file\" (change)=\"handleFileInputContent($event.target.files)\">\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"text2\">Target User</label><br>\r\n                        <select name=\"user\" #user>\r\n                          <option value=5 selected>Employee</option>\r\n                          <option *ngIf = \"currentChoice == 'Reward'\" value=4>Manager</option>\r\n                        </select>\r\n                    </div>  \r\n                    <div class=\"form-group\">\r\n                        <label for=\"text5\">Reward Name</label>\r\n                        <input #name id=\"name\" name=\"name\" type=\"text\" class=\"form-control here\">\r\n                    </div> \r\n                    <div class=\"form-group\">\r\n                        <label for=\"text5\">Reward Description</label>\r\n                        <input #description id=\"description\" name=\"description\" type=\"text\" class=\"form-control here\">\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"text6\" *ngIf = \"currentChoice == 'Reward'\">Carrot Needed</label><label for=\"text6\" *ngIf = \"currentChoice == 'Achievement'\">Carrot Gained</label><label for=\"text6\" *ngIf = \"currentChoice == 'Social'\">Carrot Target</label>\r\n                        <input #carrot id=\"carrot\" name=\"carrot\" type=\"number\" class=\"form-control here\">\r\n                    </div>\r\n                    <div *ngIf = \"currentChoice == 'Reward'\">\r\n                        <div class=\"form-group\">\r\n                            <label for=\"text3\">Redeem Limit</label>\r\n                            <input [(ngModel)]=\"limit\" name=\"limit\" type=\"number\" class=\"form-control here\">\r\n                        </div>          \r\n                        <div class=\"form-group\">\r\n                            <label for=\"text4\">Expire Date</label>\r\n                            <input [(ngModel)]=\"expired\" name=\"expired\" type=\"date\" >\r\n                        </div>  \r\n                    </div>\r\n                </form>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\">Cancel</button>\r\n                <button *ngIf = \"currentChoice == 'Reward'\" type=\"button\" class=\"btn btn-carrot radius-5\" (click)=\"addReward(user.value, name.value, description.value, carrot.value); user.value=''; name.value==''; description.value=''; carrot.value=''\" data-dismiss=\"modal\">ADD REWARD</button>\r\n                <button *ngIf = \"currentChoice == 'Achievement'\" type=\"button\" class=\"btn btn-carrot radius-5\" (click)=\"addAchievement(user.value, name.value, description.value, carrot.value); user.value=''; name.value==''; description.value=''; carrot.value=''\" data-dismiss=\"modal\">ADD ACHIEVEMENT</button>\r\n                <button *ngIf = \"currentChoice == 'Social'\" type=\"button\" class=\"btn btn-carrot radius-5\" (click)=\"addSocial(user.value, name.value, description.value, carrot.value); user.value=''; name.value==''; description.value=''; carrot.value=''\" data-dismiss=\"modal\">ADD SOCIAL FOUNDATION</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<!-- Edit Content Modal -->\r\n<div class=\"modal fade\" id=\"editReward\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Edit Reward</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\" *ngIf = \"selectedReward\">\r\n                <form>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"text1\">Reward Name</label>\r\n                        <input [(ngModel)]=\"selectedReward.name\" name=\"rewardName\" type=\"text\" class=\"form-control here\"  >\r\n                    </div> \r\n                    <div class=\"form-group\">\r\n                        <label for=\"file\">Edit content Image</label>\r\n                        <input type=\"file\" id=\"file\" (change)=\"handleFileInputContent($event.target.files)\">\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"text1\">Reward Description</label>\r\n                        <input [(ngModel)]=\"selectedReward.description\" name=\"rewardDescription\" type=\"text\" class=\"form-control here\"  >\r\n                    </div>\r\n                    <div *ngIf=\"selectedReward.type == 2\" class=\"form-group\">\r\n                        <label for=\"text1\">Expire Date</label><br>\r\n                        <input [(ngModel)]=\"selectedReward.expired\" type=\"date\" name=\"expire\">\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"text1\">Carrot</label><br>\r\n                        <input [(ngModel)]=\"selectedReward.carrot\" name=\"carrot\" type=\"text\" class=\"form-control here\"  >\r\n                    </div>\r\n                    <div *ngIf=\"selectedReward.type == 2\" class=\"form-group\">\r\n                        <label for=\"text1\">Redeem Limit</label><br>\r\n                        <input [(ngModel)]=\"selectedReward.currentCarrot\" type=\"text\" name=\"limit\">\r\n                    </div>\r\n                </form>\r\n            </div>           \r\n            <div class=\"row col-md-12\">\r\n                <label for=\"text1\">Click on the Group name to move it.</label><br>\r\n                <div class=\"col-md-6\">\r\n                    <table class=\"table table-hover mt-3\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th rowspan=\"1\">Not Able</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <ul class=\"managers\">\r\n                                <li *ngFor=\"let group of disabledGroup\"\r\n                                    [class.selected]=\"group === selectedGroup\"\r\n                                    (click)=\"groupOnSelectToMove(group)\">\r\n                                        {{group.name}}\r\n                                </li>\r\n                            </ul>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                    <table class=\"table table-hover mt-3\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th rowspan=\"1\">Able</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <ul class=\"managers\">\r\n                                <li *ngFor=\"let group of enabledGroup\"\r\n                                    [class.selected]=\"group === selectedGroup\"\r\n                                    (click)=\"groupOnSelectToRemove(group)\">\r\n                                        {{group.name}}\r\n                                </li>\r\n                            </ul>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\" (click)=\"cancelSave()\">Cancel</button>\r\n                <button type=\"button\" class=\"btn btn-carrot radius-5\" (click)=\"save()\" data-dismiss=\"modal\">SAVE EDIT</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<!-- Content Detail Modal -->\r\n<div class=\"modal fade\" id=\"seeDetail\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Reward Detail</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\" *ngIf = \"selectedReward\">\r\n                <form>\r\n                    <div class=\"form-group\">\r\n                        <img src=\"{{selectedReward && selectedReward.picture ? selectedReward.picture : '../../assets/img/user.png'}}\" alt=\"\" width=\"300\" height=\"300\"><br>\r\n                        <label >Type : </label>{{getRewardType(selectedReward.type)}}<br>\r\n                        <label >User : </label>{{getRoleType(selectedReward.role)}}<br>\r\n                        <label *ngIf=\"selectedReward.type == 1\">Achievement Name : </label><label *ngIf=\"selectedReward.type == 2\">Reward Name : </label><label *ngIf=\"selectedReward.type == 3\">Social Foundation Name : </label>{{selectedReward.name}}<br>\r\n                        <label >Description : </label><br>{{selectedReward.description}}<br>\r\n                        <label *ngIf=\"selectedReward.type == 1\">Carrot Gained : </label><label *ngIf=\"selectedReward.type == 2\">Carrot Needed : </label><label *ngIf=\"selectedReward.type == 3\">Carrot Target : </label>{{selectedReward.carrot}}<br>\r\n                        <div *ngIf=\"selectedReward.type == 3\"><label>Current Carrot : </label>{{selectedReward.currentCarrot}}</div>\r\n                        <div *ngIf=\"selectedReward.type == 2\"><label>Redeem Limit Left : </label>{{selectedReward.currentCarrot}}</div>            \r\n                        <div *ngIf=\"selectedReward.type == 2\"><label >Expired Date : </label>{{getDate(selectedReward.expired)}}</div>\r\n                        <label >Status : </label>{{getStatus(selectedReward.active)}}<br>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n            <div class=\"row col-md-12\">\r\n                <div class=\"col-md-6\">\r\n                    <table class=\"table table-hover mt-3\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th rowspan=\"1\">Able</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <ul >\r\n                                <li *ngFor=\"let group of enabledGroup\"\r\n                                    [class.selected]=\"group === selectedGroup\">\r\n                                        {{group.name}}\r\n                                </li>\r\n                            </ul>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n            <div>\r\n                <div *ngFor=\"let change of changes\" >\r\n                    <div *ngIf=\"change.parameterChanged != 'Groups Able' && change.parameterChanged != 'Groups Disable' \">{{getDate(change.on)}} : \"{{change.parameterChanged}}\" changed from \"{{change.from}}\".</div>\r\n                    <div *ngIf=\"change.parameterChanged == 'Groups Able'\">{{getDate(change.on)}} : Group \"{{change.from}}\" now able to use this content.</div>\r\n                    <div *ngIf=\"change.parameterChanged == 'Groups Disable'\">{{getDate(change.on)}} : Group \"{{change.from}}\" now unable to use this content.</div>\r\n                </div>\r\n            </div> \r\n            <div class=\"modal-footer\">               \r\n              <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\">Close</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<!-- Set Close Modal -->\r\n<div class=\"modal fade\" id=\"setCloseMessage\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Close Reward</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <form>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"text1\">Set Close Message</label>\r\n                        <input  [(ngModel)]=\"closeMessage\" type=\"text\" name = \"closeMessage\" class=\"form-control here\">\r\n                    </div>\r\n                </form>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\">Cancel</button>\r\n                <button type=\"button\" class=\"btn btn-carrot radius-5\" (click)=\"close()\" data-dismiss=\"modal\">Close it</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n        \r\n\r\n"

/***/ }),

/***/ "./src/app/admin/content/content.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_content__ = __webpack_require__("./src/app/model/content.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__model_notification__ = __webpack_require__("./src/app/model/notification.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_admin_service__ = __webpack_require__("./src/app/service/admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_group_service__ = __webpack_require__("./src/app/service/group.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_notification_service__ = __webpack_require__("./src/app/service/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__config_utility__ = __webpack_require__("./src/app/config/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angular_datatables__ = __webpack_require__("./node_modules/angular-datatables/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_datatables_net__ = __webpack_require__("./node_modules/datatables.net/js/jquery.dataTables.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_datatables_net___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_datatables_net__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var ContentComponent = /** @class */ (function () {
    function ContentComponent(http, adminService, groupService, userService, notificationService, appComponent, router) {
        this.http = http;
        this.adminService = adminService;
        this.groupService = groupService;
        this.userService = userService;
        this.notificationService = notificationService;
        this.appComponent = appComponent;
        this.router = router;
        this.disabledGroup = [];
        this.enabledGroup = [];
        this.fileToUpload = null;
        this.rewardTemp = new __WEBPACK_IMPORTED_MODULE_3__model_content__["a" /* Content */]();
        this.currentChoice = "Reward";
        this.currentGroupChoice = "Employee";
        this.changeGroupCheck = false;
        this.closeMessage = "";
        this.dtOptionsContent = {};
        this.dtTriggerContent = new __WEBPACK_IMPORTED_MODULE_12_rxjs__["Subject"]();
    }
    ContentComponent.prototype.ngOnInit = function () {
        this.loggedInUser = this.userService.getLoggedInUser();
        this.getRewards();
        this.getGroups();
    };
    ContentComponent.prototype.getRewards = function () {
        var _this = this;
        this.adminService.getRewards().subscribe(function (rewards) {
            _this.rewards = rewards;
            _this.dtTriggerContent.next();
        });
    };
    ContentComponent.prototype.getGroups = function () {
        var _this = this;
        this.groups = [];
        return this.groupService.getAllGroups().subscribe(function (groups) {
            _this.groups = groups;
            _this.groupService.getAllManagerGroups().subscribe(function (mGroups) {
                mGroups.forEach(function (group) {
                    _this.groups.push(group);
                });
            });
        });
    };
    ContentComponent.prototype.getRewardType = function (type) {
        return __WEBPACK_IMPORTED_MODULE_9__config_utility__["getType"](type);
    };
    ContentComponent.prototype.getDate = function (type) {
        return __WEBPACK_IMPORTED_MODULE_9__config_utility__["transformDate"](type, 'dd-MM-yyyy');
    };
    ContentComponent.prototype.getStatus = function (active) {
        if (active == true)
            return "Open";
        else
            return "Closed";
    };
    ContentComponent.prototype.getRoleType = function (type) {
        return __WEBPACK_IMPORTED_MODULE_9__config_utility__["getRole"](type);
    };
    ContentComponent.prototype.getContentById = function (id) {
        if (this.rewards) {
            var name_1 = "";
            this.rewards.forEach(function (reward) {
                if (reward.id == id)
                    name_1 = reward.name;
            });
            return name_1;
        }
    };
    ContentComponent.prototype.addReward = function (role, rewardName, rewardDesc, carrot) {
        var _this = this;
        if (role != null || rewardName != "" || carrot != null) {
            var rewardLimit = this.limit;
            var expiredDate = this.expired;
            if (!isNaN(carrot)) {
                rewardName = rewardName.trim();
                this.rewardTemp.type = 2;
                if (!isNaN(rewardLimit) && rewardLimit && expiredDate) {
                    this.rewardTemp.currentCarrot = rewardLimit;
                    this.rewardTemp.expired = expiredDate.toString();
                    this.rewardTemp.role = role;
                    this.rewardTemp.name = rewardName;
                    this.rewardTemp.description = rewardDesc;
                    this.rewardTemp.groupsId = [];
                    this.rewardTemp.changes = [];
                    var groupsId_1 = new Array();
                    groupsId_1 = this.rewardTemp.groupsId;
                    this.groups.forEach(function (group) {
                        groupsId_1.push(group.id);
                    });
                    this.rewardTemp.groupsId = groupsId_1;
                    this.rewardTemp.carrot = carrot;
                    this.rewardTemp.active = true;
                    this.adminService.addReward(this.rewardTemp)
                        .subscribe(function (reward) {
                        if (reward) {
                            _this.appComponent.showSuccess("'" + rewardName + "'" + " is added.");
                            _this.dtElement.dtInstance.then(function (dtInstance) {
                                dtInstance.destroy();
                                _this.rewards.push(reward);
                                _this.dtTriggerContent.next();
                            });
                        }
                        else
                            _this.appComponent.showSuccess("Sending Failed.");
                    });
                }
                else
                    this.appComponent.showError("Redeem Limit must be number and please Fill the date and Redeem Limit");
            }
            else
                this.appComponent.showError("Carrot must be a Number");
        }
        else
            this.appComponent.showError("Please fill all the field");
    };
    ContentComponent.prototype.addSocial = function (role, rewardName, rewardDesc, carrot) {
        var _this = this;
        if (role != null || rewardName != "" || carrot != null) {
            var currentCarrot = 0;
            if (!isNaN(carrot)) {
                rewardName = rewardName.trim();
                this.rewardTemp.type = 3;
                this.rewardTemp.role = role;
                this.rewardTemp.name = rewardName;
                this.rewardTemp.description = rewardDesc;
                this.rewardTemp.groupsId = [];
                this.rewardTemp.changes = [];
                var groupsId_2 = new Array();
                groupsId_2 = this.rewardTemp.groupsId;
                this.groups.forEach(function (group) {
                    groupsId_2.push(group.id);
                });
                this.rewardTemp.groupsId = groupsId_2;
                this.rewardTemp.carrot = carrot;
                this.rewardTemp.currentCarrot = currentCarrot;
                this.rewardTemp.active = true;
                this.adminService.addReward(this.rewardTemp)
                    .subscribe(function (reward) {
                    _this.dtElement.dtInstance.then(function (dtInstance) {
                        dtInstance.destroy();
                        _this.rewards.push(reward);
                        _this.dtTriggerContent.next();
                        _this.appComponent.showSuccess("'" + rewardName + "'" + " is added.");
                    });
                });
            }
            else
                this.appComponent.showError("Carrot must be a Number");
        }
        else
            this.appComponent.showError("Please fill all the field");
    };
    ContentComponent.prototype.addAchievement = function (role, rewardName, rewardDesc, carrot) {
        var _this = this;
        if (role != null || rewardName != "" || carrot != null) {
            if (!isNaN(carrot)) {
                rewardName = rewardName.trim();
                var flag = true;
                this.rewardTemp.type = 1;
                this.rewardTemp.role = role;
                this.rewardTemp.name = rewardName;
                this.rewardTemp.description = rewardDesc;
                this.rewardTemp.groupsId = [];
                this.rewardTemp.changes = [];
                var groupsId_3 = new Array();
                groupsId_3 = this.rewardTemp.groupsId;
                this.groups.forEach(function (group) {
                    groupsId_3.push(group.id);
                });
                this.rewardTemp.groupsId = groupsId_3;
                this.rewardTemp.carrot = carrot;
                this.rewardTemp.active = true;
                this.adminService.addReward(this.rewardTemp)
                    .subscribe(function (reward) {
                    _this.dtElement.dtInstance.then(function (dtInstance) {
                        dtInstance.destroy();
                        _this.rewards.push(reward);
                        _this.dtTriggerContent.next();
                        _this.appComponent.showSuccess("'" + rewardName + "'" + " is added.");
                    });
                });
            }
            else
                this.appComponent.showError("Carrot must be a Number");
        }
        else
            this.appComponent.showError("Please fill all the field");
    };
    ContentComponent.prototype.addChange = function (reward, parameterChanged, from) {
        reward.changes.push({ parameterChanged: parameterChanged, from: from });
        return reward;
    };
    ContentComponent.prototype.handleFileInputContent = function (files) {
        var _this = this;
        if (files.length < 1) {
            return;
        }
        this.fileToUpload = files[0];
        this.userService.uploadFile(this.fileToUpload)
            .subscribe(function (res) {
            if (_this.rewardTemp && res) {
                _this.rewardTemp.picture = res.url;
                // this.updateUser();
            }
            else {
                _this.appComponent.showError('Failed to upload photo');
            }
        });
    };
    ContentComponent.prototype.open = function (reward) {
        var _this = this;
        reward.active = true;
        this.adminService.addReward(reward).subscribe(function (reward) {
            _this.appComponent.showSuccess(reward.name + " is now open.");
        });
    };
    ContentComponent.prototype.close = function () {
        var _this = this;
        this.soonToBeClosedReward.active = false;
        this.adminService.addReward(this.soonToBeClosedReward).subscribe(function (reward) {
            var notifTemp = new __WEBPACK_IMPORTED_MODULE_4__model_notification__["a" /* Notification */];
            var userTemp = _this.userService.getLoggedInUser();
            _this.appComponent.showSuccess(reward.name + " is now closed.");
            notifTemp.type = 3;
            notifTemp.adminId = userTemp.id;
            notifTemp.title = _this.soonToBeClosedReward.name + " is now closed.";
            notifTemp.description = _this.closeMessage + ". \n Closed by " + userTemp.name + ".";
            _this.notificationService.addEdit(notifTemp).subscribe(function (notif) {
            });
        });
    };
    ContentComponent.prototype.delete = function (reward) {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            reward.deleted = true;
            _this.adminService.deleteReward(reward).subscribe(function (r) {
                if (r) {
                    dtInstance.destroy();
                    _this.rewards = _this.rewards.filter(function (r) {
                        return r.id != reward.id;
                    });
                    _this.dtTriggerContent.next();
                    _this.appComponent.showSuccess(reward.name + " is deleted.");
                }
                else
                    _this.appComponent.showError("Deleting " + reward.name + " is not succeed.");
            });
        });
    };
    ContentComponent.prototype.onSelect = function (reward) {
        var _this = this;
        this.selectedReward = reward;
        this.rewardTemp.id = reward.id;
        this.rewardTemp.type = reward.type;
        this.rewardTemp.role = reward.role;
        this.rewardTemp.name = reward.name;
        this.rewardTemp.expired = reward.expired;
        this.rewardTemp.changes = reward.changes;
        this.rewardTemp.groupsId = reward.groupsId;
        this.rewardTemp.carrot = reward.carrot;
        this.rewardTemp.active = reward.active;
        this.disabledGroup = [];
        this.enabledGroup = [];
        this.changes = reward.changes;
        this.groups.forEach(function (group) {
            var flag = false;
            for (var i = 0; i < _this.selectedReward.groupsId.length; i++) {
                if (group.id == _this.selectedReward.groupsId[i]) {
                    _this.enabledGroup.push(group);
                    flag = true;
                    break;
                }
            }
            if (flag == false)
                _this.disabledGroup.push(group);
        });
    };
    ContentComponent.prototype.save = function () {
        var _this = this;
        if (this.rewardTemp.expired != this.selectedReward.expired) {
            this.selectedReward = this.addChange(this.selectedReward, "Expired Date", this.rewardTemp.expired);
        }
        if (this.rewardTemp.carrot != this.selectedReward.carrot) {
            this.selectedReward = this.addChange(this.selectedReward, "Maximum Carrot", this.rewardTemp.carrot.toString());
        }
        if (this.rewardTemp.name != this.selectedReward.name) {
            this.selectedReward = this.addChange(this.selectedReward, "Content Name", this.rewardTemp.name);
        }
        this.selectedReward.groupsId = [];
        if (this.checkGroupType) {
            this.enabledGroup.forEach(function (group) {
                _this.selectedReward.groupsId.push(group.id);
                _this.selectedReward = _this.addChange(_this.selectedReward, "Groups Able", group.name);
            });
            this.disabledGroup.forEach(function (group) {
                _this.selectedReward = _this.addChange(_this.selectedReward, "Groups Disable", group.name);
            });
        }
        this.changeGroupCheck = false;
        this.adminService.addReward(this.selectedReward).subscribe(function (res) {
            console.log(res);
            if (res) {
                _this.selectedReward = res;
                _this.selectedReward.changes[_this.selectedReward.changes.length - 1].on = new Date().toString();
            }
        });
        this.appComponent.showSuccess("'" + this.selectedReward.name + "'" + " has been updated.");
    };
    ContentComponent.prototype.groupOnSelectToMove = function (group) {
        this.changeGroupCheck = true;
        this.selectedGroup = group;
        this.onEnabledGroup(this.selectedGroup);
    };
    ContentComponent.prototype.groupOnSelectToRemove = function (group) {
        this.changeGroupCheck = true;
        this.selectedGroup = group;
        this.onRemoveGroupToMoves(this.selectedGroup);
    };
    ContentComponent.prototype.onEnabledGroup = function (group) {
        this.disabledGroup = this.disabledGroup.filter(function (us) {
            return us.id != group.id;
        });
        this.enabledGroup.push(group);
    };
    ContentComponent.prototype.onRemoveGroupToMoves = function (group) {
        this.enabledGroup = this.enabledGroup.filter(function (man) {
            return man.id != group.id;
        });
        this.disabledGroup.push(group);
    };
    ContentComponent.prototype.setLate = function (user) {
        if (user.alwaysLate == true) {
            user.alwaysLate = false;
            this.appComponent.showSuccess(user.name + " is now flagged 'Not Late'.");
        }
        else {
            this.appComponent.showSuccess(user.name + " is now flagged 'Late'.");
            user.alwaysLate = true;
        }
        this.userService.addEdit(user).subscribe();
    };
    ContentComponent.prototype.setClose = function (reward) {
        this.soonToBeClosedReward = reward;
        this.adminService.addReward(reward).subscribe();
    };
    ContentComponent.prototype.checkOpen = function (status) {
        if (status == true)
            return true;
    };
    ContentComponent.prototype.checkClose = function (status) {
        if (status == false)
            return true;
    };
    ContentComponent.prototype.checkContent = function (type) {
        this.currentChoice = type.target.value;
    };
    ContentComponent.prototype.checkGroupType = function (type) {
        this.currentGroupChoice = type.target.value;
    };
    ContentComponent.prototype.refresh = function () {
        window.location.reload();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_11_angular_datatables__["a" /* DataTableDirective */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_11_angular_datatables__["a" /* DataTableDirective */])
    ], ContentComponent.prototype, "dtElement", void 0);
    ContentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-content',
            template: __webpack_require__("./src/app/admin/content/content.component.html"),
            styles: [__webpack_require__("./src/app/admin/content/content.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__service_admin_service__["a" /* AdminService */],
            __WEBPACK_IMPORTED_MODULE_6__service_group_service__["a" /* GroupService */],
            __WEBPACK_IMPORTED_MODULE_10__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_7__service_notification_service__["a" /* NotificationService */],
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], ContentComponent);
    return ContentComponent;
}());



/***/ }),

/***/ "./src/app/admin/group-staff/group-staff.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n  a:not([href]):not([tabindex]) {\r\n    color: #FFF;\r\n    cursor: pointer;\r\n  }"

/***/ }),

/***/ "./src/app/admin/group-staff/group-staff.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"admin-tabs py-3\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12 px-md-0\">\r\n\r\n                <div class=\"tab-content search-bo\" >\r\n                    <div class=\"tab-pane fade show active\" >\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-12\">\r\n                                <hr class=\"box-title-hr\">\r\n                                <h4 class=\"my-2 box-title\">STAFF GROUP LIST</h4>\r\n                            </div>\r\n                            <div class=\"col-md-12\">\r\n                                <mat-progress-spinner *ngIf=\"loadingGroup\"\r\n                                class=\"text-center m-auto\"\r\n                                color=\"warn\"\r\n                                mode=\"indeterminate\" >\r\n                                </mat-progress-spinner> \r\n                                <table class=\"table table-hover mt-3\" datatable [dtOptions]=\"dtOptionsGroup\" [dtTrigger]=\"dtTriggerGroup\">\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th scope=\"col\">Group Name</th>\r\n                                            <th scope=\"col\">No of Staff</th>\r\n                                            <th scope=\"col\">Action</th>\r\n                                        </tr>\r\n                                    </thead>\r\n                                    <tbody>\r\n                                        <tr *ngFor=\"let group of groups\">\r\n                                            <td>{{group.name}}</td>\r\n                                            <td>{{(group.members) ? group.members.length + 1 : 1}}</td>\r\n                                            <td><a class=\"btn btn-warning\" data-toggle=\"modal\" data-target=\"#editGroupName\" (click)=\"groupOnSelect(group)\">Edit Name</a> <a *ngIf=\"group.manager\" class=\"btn btn-warning\" data-toggle=\"modal\" data-target=\"#editBirthday\" (click)=\"groupOnSelect(group)\">Edit Birthday Config</a></td>\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table> \r\n                                <div class=\"text-center mb-3\">\r\n                                    <button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#newGroup\">\r\n                                        <i class=\"fa fa-plus-circle\"></i> CREATE NEW STAFF GROUP\r\n                                    </button>\r\n                                </div>                               \r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n<!-- Modal -->\r\n<!-- Edit Group Name Modal -->\r\n<div class=\"modal fade\" id=\"editGroupName\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Edit Group Name</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\" *ngIf = \"selectedGroup\">\r\n                <form>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"text1\">New Group Name</label>\r\n                      <input [(ngModel)]=\"selectedGroup.name\" name=\"groupName\" type=\"text\" class=\"form-control here\"  >\r\n                    </div>\r\n                </form>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-link\" (click)=\"cancelEdit()\" data-dismiss=\"modal\">Cancel</button>\r\n                <button type=\"button\" class=\"btn btn-carrot radius-5\" (click)=\"editGroupName()\" data-dismiss=\"modal\">Change</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<!-- Edit Birthday Modal -->\r\n<div class=\"modal fade\" id=\"editBirthday\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Edit Birthday Configuration</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\" *ngIf = \"selectedGroup\">\r\n                <form>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"text1\">Auto-send carrot on staff birthday</label>\r\n                        <input #autoStatus type=\"checkbox\" [checked]=\"selectedGroup.autoCarrotAdmin\" (change)=\"selectedGroup.autoCarrotAdmin = !selectedGroup.autoCarrotAdmin\">\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"text1\">Amount of carrot sent</label><br>\r\n                        <input #carrotAmount [(ngModel)]=\"selectedGroup.birthdayCarrotAdmin\" [attr.disabled]=\"selectedGroup.autoCarrotAdmin ? null : selectedGroup.birthdayCarrotAdmin\" id=\"text1\" name=\"text1\" type=\"text\" class=\"form-control here\">\r\n                    </div>\r\n                </form>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\" (click)=\"cancelEdit()\">Cancel</button>\r\n                <button type=\"button\" class=\"btn btn-carrot radius-5\" (click)=\"saveBirthdayStatus()\" data-dismiss=\"modal\">SAVE CONFIG</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n  </div>\r\n\r\n<!-- Edit Birthday Modal -->\r\n  <div class=\"modal fade\" id=\"newGroup\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n      <div class=\"modal-dialog\" role=\"document\">\r\n          <div class=\"modal-content\">\r\n              <div class=\"modal-header\">\r\n                  <h5 class=\"modal-title\" id=\"exampleModalLabel\">Create New Staff Group</h5>\r\n                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                      <span aria-hidden=\"true\">&times;</span>\r\n                  </button>\r\n              </div>\r\n              <div class=\"modal-body\">\r\n                  <form>\r\n                      <div class=\"form-group\">\r\n                          <label for=\"text1\">Group Type</label><br>\r\n                          <select name=\"typeGroup\" #typeGroup (change)= \"checkGroupType($event)\">\r\n                              <option value=\"Employee\" selected>Employee</option>\r\n                              <option value=\"Manager\">Manager</option>\r\n                          </select>\r\n                      </div>\r\n                      <div class=\"form-group\">\r\n                          <label for=\"text1\">Group Name</label>\r\n                          <input #groupName type=\"Text\" id=\"groupName\" name=\"groupName\">\r\n                      </div>\r\n                      <div *ngIf = \"currentGroupChoice == 'Employee'\" class=\"form-group\">\r\n                          <label>Assigned Manager: </label>\r\n                          <select (change)=\"onSelectUser($event.target.value)\">\r\n                              <option>--SELECT A MANAGER--</option>\r\n                              <option *ngFor=\"let user of managers\" value={{user.id}}>\r\n                                  {{user.name}}\r\n                              </option>\r\n                          </select>\r\n                      </div>\r\n                      <div *ngIf = \"currentGroupChoice == 'Manager'\" class=\"form-group\">\r\n                          <label>Assigned Senior Manager: </label>\r\n                          <select (change)=\"onSelectUser($event.target.value)\">\r\n                              <option>--SELECT A SENIOR MANAGER--</option>\r\n                              <option *ngFor=\"let user of seniorManagers\" value={{user.id}}>\r\n                                  {{user.name}}\r\n                              </option>\r\n                          </select>\r\n                      </div>\r\n                  </form>\r\n              </div>\r\n              <div class=\"modal-footer\">\r\n                  <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\" (click)=\"groupName.value==''\">Cancel</button>\r\n                  <button type=\"button\" class=\"btn btn-carrot radius-5\" (click)=\"addGroup(groupName.value)\" data-dismiss=\"modal\">Create Group</button>\r\n              </div>\r\n          </div>\r\n      </div>\r\n  </div>\r\n\r\n"

/***/ }),

/***/ "./src/app/admin/group-staff/group-staff.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupStaffComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__model_user__ = __webpack_require__("./src/app/model/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__model_group__ = __webpack_require__("./src/app/model/group.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_admin_service__ = __webpack_require__("./src/app/service/admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_group_service__ = __webpack_require__("./src/app/service/group.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_transaction_service__ = __webpack_require__("./src/app/service/transaction.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__service_notification_service__ = __webpack_require__("./src/app/service/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_ngx_order_pipe__ = __webpack_require__("./node_modules/ngx-order-pipe/ngx-order-pipe.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_angular_datatables__ = __webpack_require__("./node_modules/angular-datatables/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_datatables_net__ = __webpack_require__("./node_modules/datatables.net/js/jquery.dataTables.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_datatables_net___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_datatables_net__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



















var GroupStaffComponent = /** @class */ (function () {
    function GroupStaffComponent(http, adminService, groupService, userService, transactionService, notificationService, appComponent, location, datepipe, orderPipe, router) {
        this.http = http;
        this.adminService = adminService;
        this.groupService = groupService;
        this.userService = userService;
        this.transactionService = transactionService;
        this.notificationService = notificationService;
        this.appComponent = appComponent;
        this.location = location;
        this.datepipe = datepipe;
        this.orderPipe = orderPipe;
        this.router = router;
        this.isLoading = false;
        //   staff : User [];
        this.managers = [];
        this.seniorManagers = [];
        this.groupTemp = new __WEBPACK_IMPORTED_MODULE_5__model_group__["a" /* Group */]();
        this.currentGroupChoice = "Employee";
        this.loadingGroup = true;
        this.selectedUser = new __WEBPACK_IMPORTED_MODULE_4__model_user__["a" /* User */];
        this.dtOptionsGroup = {};
        this.dtTriggerGroup = new __WEBPACK_IMPORTED_MODULE_15_rxjs__["Subject"]();
    }
    GroupStaffComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInUser = this.userService.getLoggedInUser();
        if (!this.loggedInUser) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_11__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        _this.loggedInUser = res;
                        _this.userService.setLoggedInUser(res);
                        _this.isLoading = false;
                        if (!res.admin)
                            _this.router.navigate(['/']);
                    }
                    else {
                        _this.isLoading = false;
                        _this.router.navigate(['/']);
                    }
                });
            }
            else {
                this.isLoading = false;
                this.router.navigate(['/']);
            }
        }
        else
            this.isLoading = false;
        this.getUsers();
        this.getUngroupedManager();
        this.getGroups();
    };
    GroupStaffComponent.prototype.getUsers = function () {
        var _this = this;
        return this.adminService.getUsers().subscribe(function (users) {
            _this.users = users;
        });
    };
    GroupStaffComponent.prototype.getUngroupedManager = function () {
        var _this = this;
        this.managers = [];
        this.seniorManagers = [];
        return this.userService.fetchUsers().subscribe(function (users) {
            _this.users = users;
            _this.users.forEach(function (user) {
                if (user.role == 4)
                    _this.managers.push(user);
                if (user.role == 8)
                    _this.seniorManagers.push(user);
            });
        });
    };
    GroupStaffComponent.prototype.getGroups = function () {
        var _this = this;
        this.groups = [];
        return this.groupService.getAllGroups().subscribe(function (groups) {
            _this.groups = groups;
            _this.groupService.getAllManagerGroups().subscribe(function (mGroups) {
                mGroups.forEach(function (group) {
                    _this.groups.push(group);
                });
                _this.loadingGroup = false;
                _this.dtTriggerGroup.next();
            });
        });
    };
    GroupStaffComponent.prototype.addGroup = function (groupName) {
        var _this = this;
        if (groupName != "") {
            if (this.selectedUser.id != undefined && this.selectedUser.id != "" && groupName != "") {
                var newGroup = new __WEBPACK_IMPORTED_MODULE_5__model_group__["a" /* Group */];
                newGroup.name = groupName;
                newGroup.managerId = this.selectedUser.id;
                newGroup.membersId = [];
                newGroup.members = [];
                newGroup.autoCarrotAdmin = true;
                newGroup.birthdayCarrotAdmin = 0;
                if (this.currentGroupChoice == "Employee") {
                    this.groupService.updateGroup(newGroup).subscribe(function (group) {
                        _this.dtElement.dtInstance.then(function (dtInstance) {
                            dtInstance.destroy();
                            _this.groups.push(group);
                            _this.dtTriggerGroup.next();
                        });
                    });
                }
                else {
                    this.groupService.updateManagerGroup(newGroup).subscribe(function (group) {
                        _this.dtElement.dtInstance.then(function (dtInstance) {
                            dtInstance.destroy();
                            _this.groups.push(group);
                            _this.dtTriggerGroup.next();
                        });
                    });
                }
                this.appComponent.showSuccess(groupName + " Staff Group has been Added.");
            }
            else
                this.appComponent.showError("Please select a Manager and fill the group name");
        }
    };
    GroupStaffComponent.prototype.onSelectUser = function (managerId) {
        if ((managerId != "--SELECT A MANAGER--" || "--SELECT A SENIOR MANAGER--") && managerId != "") {
            this.selectedUser.id = managerId;
        }
    };
    GroupStaffComponent.prototype.groupOnSelect = function (group) {
        this.selectedGroup = group;
        this.groupTemp.id = group.id;
        this.groupTemp.name = group.name;
        this.groupTemp.managerId = group.managerId;
        this.groupTemp.membersId = group.membersId;
        this.groupTemp.autoCarrotAdmin = group.autoCarrotAdmin;
        this.groupTemp.birthdayCarrotAdmin = group.birthdayCarrotAdmin;
        this.groupTemp.autoCarrotManager = group.autoCarrotManager;
        this.groupTemp.birthdayCarrotManager = group.birthdayCarrotManager;
        this.groupTemp.creationDate = group.creationDate;
        this.groupTemp.active = group.active;
        this.groupTemp.manager = group.manager;
        this.groupTemp.members = group.members;
    };
    GroupStaffComponent.prototype.saveBirthdayStatus = function () {
        this.groupService.updateGroup(this.selectedGroup).subscribe();
        this.appComponent.showSuccess("Birthday Carrot has been Updated");
    };
    GroupStaffComponent.prototype.editGroupName = function () {
        var _this = this;
        if (this.selectedGroup.manager) {
            this.groupService.updateGroup(this.selectedGroup).subscribe(function (group) {
                _this.appComponent.showSuccess("Group name now become " + _this.selectedGroup.name + ".");
            });
        }
        else {
            this.groupService.updateManagerGroup(this.selectedGroup).subscribe(function (group) {
                _this.appComponent.showSuccess("Group name now become " + _this.selectedGroup.name + ".");
            });
        }
    };
    GroupStaffComponent.prototype.checkGroupType = function (type) {
        this.currentGroupChoice = type.target.value;
    };
    GroupStaffComponent.prototype.cancelEdit = function () {
        this.selectedGroup.name = this.groupTemp.name;
        this.selectedGroup.autoCarrotAdmin = this.groupTemp.autoCarrotAdmin;
        this.selectedGroup.birthdayCarrotAdmin = this.groupTemp.birthdayCarrotAdmin;
    };
    //   //---------------------------------
    GroupStaffComponent.prototype.refresh = function () {
        window.location.reload();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_14_angular_datatables__["a" /* DataTableDirective */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_14_angular_datatables__["a" /* DataTableDirective */])
    ], GroupStaffComponent.prototype, "dtElement", void 0);
    GroupStaffComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-group-staff',
            template: __webpack_require__("./src/app/admin/group-staff/group-staff.component.html"),
            styles: [__webpack_require__("./src/app/admin/group-staff/group-staff.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_6__service_admin_service__["a" /* AdminService */],
            __WEBPACK_IMPORTED_MODULE_7__service_group_service__["a" /* GroupService */],
            __WEBPACK_IMPORTED_MODULE_13__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_8__service_transaction_service__["a" /* TransactionService */],
            __WEBPACK_IMPORTED_MODULE_9__service_notification_service__["a" /* NotificationService */],
            __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_3__angular_common__["DatePipe"],
            __WEBPACK_IMPORTED_MODULE_12_ngx_order_pipe__["b" /* OrderPipe */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], GroupStaffComponent);
    return GroupStaffComponent;
}());



/***/ }),

/***/ "./src/app/admin/newsletter/newsletter.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/admin/newsletter/newsletter.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"admin-tabs py-3\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12 px-md-0\">\r\n\r\n                <div class=\"tab-content search-bo\" >\r\n                    <div class=\"tab-pane fade show active\" >\r\n                        <div class=\"row\">\r\n                            <div>\r\n                                <div class=\"tab-pane fade show active\" id=\"list\" role=\"tabpanel\" aria-labelledby=\"list-tab\">\r\n                                    <div class=\"row\" >\r\n                                        <div class=\"col-md-12\">\r\n                                            <hr class=\"box-title-hr\">\r\n                                            <h4 class=\"my-2 box-title\">NEWSLETTER AND CLOSE MESSAGE LIST</h4>\r\n                                        </div>\r\n                                        <div class=\"col-md-12\">\r\n                                            <mat-progress-spinner *ngIf=\"loadingNewsletters\"\r\n                                            class=\"text-center m-auto\"\r\n                                            color=\"warn\"\r\n                                            mode=\"indeterminate\" >\r\n                                            </mat-progress-spinner> \r\n                                            <table class=\"table table-hover mt-3\" datatable  [dtOptions]=\"dtOptionsNewsletter\" [dtTrigger]=\"dtTriggerNewsletter\">\r\n                                                <thead>\r\n                                                    <tr>\r\n                                                        <th scope=\"col\">Title</th>\r\n                                                        <th scope=\"col\">Publish Date</th>\r\n                                                        <th scope=\"col\">By</th>\r\n                                                    </tr>\r\n                                                </thead>\r\n                                                <tbody>\r\n                                                    <tr *ngFor=\"let newsletter of newsletters\" >\r\n                                                        <td data-toggle=\"modal\" data-target=\"#newsletterDetail\" (click)=\"onSelectNewsletter(newsletter)\">{{newsletter.title}}</td>\r\n                                                        <td data-toggle=\"modal\" data-target=\"#newsletterDetail\" (click)=\"onSelectNewsletter(newsletter)\">{{getDate(newsletter.creationDate)}}</td>\r\n                                                        <td data-toggle=\"modal\" data-target=\"#newsletterDetail\" (click)=\"onSelectNewsletter(newsletter)\">{{getUserNameById(newsletter.adminId)}}</td> \r\n                                                    </tr>\r\n                                                </tbody>\r\n                                            </table>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div>\r\n                                <div class=\"row\" >\r\n                                    <div class=\"col-md-12\">\r\n                                        <hr class=\"box-title-hr\">\r\n                                        <h4 class=\"my-2 box-title\">CREATE NEWSLETTER</h4>\r\n                                    </div>\r\n                                    <div class=\"col-md-12\">\r\n                                        <label for=\"text1\">Title</label><br>\r\n                                        <input #title type=\"text\" name=\"title\" id=\"title\"><br>\r\n                                        <label for=\"text1\">Content</label><br>\r\n                                        <textarea [froalaEditor] [(froalaModel)]=\"editorContent\"></textarea>\r\n                                        <div class=\"text-center mb-3\">\r\n                                            <button type=\"button\" class=\"btn btn-info\" (click) = \"publish(title.value); editorContent = ''; title.value = ''\">\r\n                                            <i class=\"fa fa-plus-circle\"></i> PUBLISH\r\n                                            </button>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n<!-- Modal -->\r\n<!-- Edit Group Name Modal -->\r\n<div class=\"modal fade\" id=\"newsletterDetail\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Create New Staff Group</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <form>\r\n                    <label for=\"text1\">Title</label>\r\n                    <div class=\"form-group\">\r\n                        {{selectedNewsletter.title}}\r\n                    </div>\r\n                    <label for=\"text1\">Content</label>\r\n                    <div class=\"form-group\" [froalaView]=\"selectedNewsletter.description\"></div>\r\n                </form>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/admin/newsletter/newsletter.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsletterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__model_notification__ = __webpack_require__("./src/app/model/notification.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_admin_service__ = __webpack_require__("./src/app/service/admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_group_service__ = __webpack_require__("./src/app/service/group.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_transaction_service__ = __webpack_require__("./src/app/service/transaction.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_notification_service__ = __webpack_require__("./src/app/service/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__config_utility__ = __webpack_require__("./src/app/config/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_ngx_order_pipe__ = __webpack_require__("./node_modules/ngx-order-pipe/ngx-order-pipe.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_angular_datatables__ = __webpack_require__("./node_modules/angular-datatables/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_datatables_net__ = __webpack_require__("./node_modules/datatables.net/js/jquery.dataTables.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_datatables_net___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_datatables_net__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



















var NewsletterComponent = /** @class */ (function () {
    function NewsletterComponent(http, adminService, groupService, userService, transactionService, notificationService, appComponent, location, datepipe, orderPipe, router) {
        this.http = http;
        this.adminService = adminService;
        this.groupService = groupService;
        this.userService = userService;
        this.transactionService = transactionService;
        this.notificationService = notificationService;
        this.appComponent = appComponent;
        this.location = location;
        this.datepipe = datepipe;
        this.orderPipe = orderPipe;
        this.router = router;
        this.isLoading = false;
        this.editorContent = "";
        this.loadingNewsletters = true;
        this.loaded = false;
        this.selectedNewsletter = new __WEBPACK_IMPORTED_MODULE_4__model_notification__["a" /* Notification */];
        this.dtOptionsNewsletter = {};
        this.dtTriggerNewsletter = new __WEBPACK_IMPORTED_MODULE_15_rxjs__["Subject"]();
        this.isList = true;
        this.isCreate = false;
    }
    NewsletterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInUser = this.userService.getLoggedInUser();
        if (!this.loggedInUser) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_11__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        _this.loggedInUser = res;
                        _this.userService.setLoggedInUser(res);
                        _this.isLoading = false;
                        if (!res.admin)
                            _this.router.navigate(['/']);
                    }
                    else {
                        _this.isLoading = false;
                        _this.router.navigate(['/']);
                    }
                });
            }
            else {
                this.isLoading = false;
                this.router.navigate(['/']);
            }
        }
        else
            this.isLoading = false;
        //   this.getRewards();
        //   this.getTransaction();
        this.getUsers();
        //   this.getUngroupedManager();
        //   this.getGroups();
        this.getNewsletter();
        //   this.disabledGroup = [];
        //   this.enabledGroup = [];
    };
    //   //----------GET LIST METHOD----------
    //   getRewards(): void 
    //   {
    //     this.adminService.getRewards().subscribe(rewards=> {
    //       this.rewards = rewards;
    //       this.dtTriggerContent.next();
    //     });
    //   }
    NewsletterComponent.prototype.openList = function () {
        this.isList = true;
        this.isCreate = false;
    };
    NewsletterComponent.prototype.openCreate = function () {
        this.isList = false;
        this.isCreate = true;
    };
    NewsletterComponent.prototype.getUsers = function () {
        var _this = this;
        return this.adminService.getUsers().subscribe(function (users) {
            _this.users = users;
        });
    };
    //   getUngroupedManager()
    //   {
    //     this.managers = [];
    //     this.seniorManagers = [];
    //     return this.userService.fetchUsers().subscribe(users => {
    //       this.users = users;
    //       this.users.forEach((user) => {
    //         if(user.role == 4)
    //           this.managers.push(user);
    //         if(user.role == 8)
    //           this.seniorManagers.push(user);
    //       });
    //     })
    //   }
    //   getGroups()
    //   {
    //     this.groups = [];
    //     return this.groupService.getAllGroups().subscribe(groups => {
    //         this.groups = groups;
    //         this.groupService.getAllManagerGroups().subscribe(mGroups =>{
    //           mGroups.forEach((group) =>{
    //             this.groups.push(group);
    //           });
    //           this.loadingGroup = false;
    //           this.dtTriggerGroup.next();
    //         });
    //       });
    //   }
    NewsletterComponent.prototype.getNewsletter = function () {
        var _this = this;
        this.newsletters = [];
        return this.notificationService.getAll().subscribe(function (notifications) {
            notifications.forEach(function (notification) {
                if (notification.type == 2)
                    _this.newsletters.push(notification);
            });
            _this.loaded = true;
            _this.loadingNewsletters = false;
            _this.dtTriggerNewsletter.next();
        });
    };
    //   getTransaction()
    //   {
    //     this.approvals = [];
    //     this.transactionService.fetchTransactions().subscribe(transactions => {
    //       this.transactions = transactions;
    //       this.transactions.forEach((transaction) => {
    //         if(transaction.type == 2 && transaction.approved == false && transaction.rejected == false)
    //           this.approvals.push(transaction);
    //       });   
    //       this.loadingApproval = false;
    //       this.dtTriggerApproval.next();
    //     });
    //   }
    //   //-----------------------------------
    //   //----------GET TYPE METHOD----------
    //   getRewardType(type : number):string
    //   {
    //     return Utility.getType(type);
    //   }
    NewsletterComponent.prototype.getDate = function (type) {
        return __WEBPACK_IMPORTED_MODULE_10__config_utility__["transformDate"](type, 'dd-MM-yyyy');
    };
    //   getStatus(active : boolean):string
    //   {
    //     if(active == true)
    //       return "Open";
    //     else
    //       return "Closed";
    //   }
    //   getRoleType(type : number):string
    //   {
    //     return Utility.getRole(type);
    //   }
    NewsletterComponent.prototype.getUserNameById = function (id) {
        if (this.users) {
            var name_1 = "";
            this.users.forEach(function (user) {
                if (user.id == id)
                    name_1 = user.name;
            });
            return name_1;
        }
    };
    //   getContentById(id:string):string
    //   {
    //     if(this.rewards)
    //     {
    //       let name: string = "";
    //       this.rewards.forEach(reward => {
    //         if(reward.id == id)
    //           name = reward.name;
    //       });
    //       return name;
    //     }
    //   }
    //   //--------------------------------
    //   //----------ADD METHOD----------
    //   addReward(role: number, rewardName: string, rewardDesc:string, carrot: number): void 
    //   {
    //     if(role != null || rewardName != "" || carrot != null)
    //     {
    //       let rewardLimit = this.limit;
    //       let expiredDate = this.expired;
    //       if(!isNaN(carrot))
    //       {
    //         rewardName = rewardName.trim();
    //         this.rewardTemp.type = 2;
    //         if(!isNaN(rewardLimit) && rewardLimit && expiredDate)
    //         {
    //           this.rewardTemp.currentCarrot = rewardLimit;
    //           this.rewardTemp.expired = expiredDate.toString();
    //           this.rewardTemp.role = role;
    //           this.rewardTemp.name = rewardName;
    //           this.rewardTemp.description = rewardDesc;
    //           this.rewardTemp.groupsId = [];
    //           let groupsId: string[] = new Array<string>();
    //           groupsId = this.rewardTemp.groupsId;
    //           this.groups.forEach((group) => {
    //             groupsId.push(group.id);
    //           });
    //           this.rewardTemp.groupsId = groupsId;
    //           this.rewardTemp.carrot = carrot;
    //           this.rewardTemp.active = true;
    //           this.adminService.addReward(this.rewardTemp)
    //             .subscribe(reward =>{
    //               if(reward)
    //               {
    //                 this.appComponent.showSuccess("'" + rewardName + "'" +  " is added.");
    //                 this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    //                   dtInstance.destroy();
    //                   this.rewards.push(reward);
    //                   this.dtTriggerContent.next();
    //                 });
    //               }
    //               else
    //                 this.appComponent.showSuccess("Sending Failed.");
    //             });
    //         }
    //         else
    //           this.appComponent.showError("Redeem Limit must be number and please Fill the date and Redeem Limit")
    //       }
    //       else
    //         this.appComponent.showError("Carrot must be a Number");     
    //     }
    //     else
    //       this.appComponent.showError("Please fill all the field");  
    //   }
    //   addSocial(role: number, rewardName: string, rewardDesc:string, carrot: number): void 
    //   {  
    //     if(role != null || rewardName != "" || carrot != null)
    //     {
    //       let currentCarrot = 0;
    //       if(!isNaN(carrot))
    //       {
    //         rewardName = rewardName.trim();
    //         this.rewardTemp.type = 3;
    //         this.rewardTemp.role = role;
    //         this.rewardTemp.name = rewardName;
    //         this.rewardTemp.description = rewardDesc;
    //         this.rewardTemp.groupsId = [];
    //         let groupsId: string[] = new Array<string>();
    //         groupsId = this.rewardTemp.groupsId;
    //         this.groups.forEach((group) => {
    //           groupsId.push(group.id);
    //         });
    //         this.rewardTemp.groupsId = groupsId;
    //         this.rewardTemp.carrot = carrot;
    //         this.rewardTemp.currentCarrot = currentCarrot;
    //         this.rewardTemp.active = true;
    //         this.adminService.addReward(this.rewardTemp)
    //           .subscribe(reward => {
    //             this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    //               dtInstance.destroy();
    //               this.rewards.push(reward);
    //               this.dtTriggerContent.next();
    //               this.appComponent.showSuccess("'" + rewardName + "'" +  " is added.");
    //             });
    //           });
    //       }
    //       else
    //         this.appComponent.showError("Carrot must be a Number");
    //     }
    //     else
    //       this.appComponent.showError("Please fill all the field");  
    //     /*
    //     this.dtElementContent.dtInstance.then((dtInstance: DataTables.Api) => {
    //       let currentCarrot = 0;
    //       if(!isNaN(carrot))
    //       {
    //         rewardName = rewardName.trim();
    //         this.rewardTemp.type = 3;
    //         this.rewardTemp.role = role;
    //         this.rewardTemp.name = rewardName;
    //         this.rewardTemp.groupsId = [];
    //         let groupsId: string[] = new Array<string>();
    //         groupsId = this.rewardTemp.groupsId;
    //         this.groups.forEach((group) => {
    //           groupsId.push(group.id);
    //         });
    //         this.rewardTemp.groupsId = groupsId;
    //         this.rewardTemp.carrot = carrot;
    //         this.rewardTemp.currentCarrot = currentCarrot;
    //         this.rewardTemp.active = true;
    //         this.adminService.addReward(this.rewardTemp)
    //           .subscribe(reward => {
    //             dtInstance.destroy();
    //             this.rewards.push(reward);
    //             this.dtTriggerContent.next();
    //           });
    //         this.appComponent.showSuccess("'" + rewardName + "'" +  " is added.");
    //       }
    //       else
    //         this.appComponent.showError("Carrot must be a Number");
    //     });
    //     */
    //   }
    //   addAchievement(role: number, rewardName: string, rewardDesc:string, carrot: number): void 
    //   {
    //     if(role != null || rewardName != "" || carrot != null)
    //     {
    //       if(!isNaN(carrot))
    //       {
    //         rewardName = rewardName.trim();
    //         let flag : boolean = true;
    //         this.rewardTemp.type = 1;
    //         this.rewardTemp.role = role;
    //         this.rewardTemp.name = rewardName;
    //         this.rewardTemp.description = rewardDesc;
    //         this.rewardTemp.groupsId = [];
    //         let groupsId: string[] = new Array<string>();
    //         groupsId = this.rewardTemp.groupsId;
    //         this.groups.forEach((group) => {
    //           groupsId.push(group.id);
    //         });
    //         this.rewardTemp.groupsId = groupsId;
    //         this.rewardTemp.carrot = carrot;
    //         this.rewardTemp.active = true;
    //         this.adminService.addReward(this.rewardTemp)
    //           .subscribe(reward => {
    //             this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    //               dtInstance.destroy();
    //               this.rewards.push(reward);
    //               this.dtTriggerContent.next();
    //               this.appComponent.showSuccess("'" + rewardName + "'" +  " is added.");
    //             });
    //           });   
    //       }
    //       else
    //         this.appComponent.showError("Carrot must be a Number");
    //     }
    //     else
    //       this.appComponent.showError("Please fill all the field");  
    //   }
    //   addChange(reward: Content, parameterChanged: string, from: string) :Content
    //   {
    //     reward.changes.push({parameterChanged, from} as ContentChange);
    //     return reward;
    //   }
    //   addGroup(groupName:string)
    //   {
    //     if(groupName != "")
    //     { 
    //       if(this.selectedUser.id != undefined && this.selectedUser.id != "" && groupName != "")
    //       {
    //         let newGroup : Group = new Group;
    //         newGroup.name = groupName;
    //         newGroup.managerId = this.selectedUser.id;
    //         newGroup.membersId = [];
    //         newGroup.members = [];
    //         newGroup.autoCarrotAdmin = true;
    //         newGroup.birthdayCarrotAdmin = 0;
    //         if(this.currentGroupChoice == "Employee")
    //         {
    //           this.groupService.updateGroup(newGroup).subscribe(group => {
    //             this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    //               dtInstance.destroy();
    //               this.groups.push(group);
    //               this.dtTriggerContent.next();
    //             });
    //           });
    //         }
    //         else
    //         {
    //           this.groupService.updateManagerGroup(newGroup).subscribe(group => {
    //             this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    //               dtInstance.destroy();
    //               this.groups.push(group);
    //               this.dtTriggerContent.next();
    //             });
    //           });
    //         }
    //         this.appComponent.showSuccess(groupName + " Staff Group has been Added."); 
    //       }
    //       else
    //         this.appComponent.showError("Please select a Manager and fill the group name");
    //     }
    //   }
    NewsletterComponent.prototype.publish = function (title) {
        var _this = this;
        var notifTemp = new __WEBPACK_IMPORTED_MODULE_4__model_notification__["a" /* Notification */];
        var userTemp = this.userService.getLoggedInUser();
        if (title != "" && this.editorContent != "") {
            notifTemp.type = 2;
            notifTemp.adminId = userTemp.id;
            notifTemp.title = "(Newsletter) " + title;
            notifTemp.description = this.editorContent;
            this.notificationService.addEdit(notifTemp).subscribe(function (notification) {
                // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                //   dtInstance.destroy();
                //   this.newsletters.push(notification);
                //   this.dtTriggerNewsletter.next();
                // });
                _this.newsletters.push(notification);
                _this.appComponent.showSuccess(title + " is now published.");
            });
        }
        else
            this.appComponent.showError("Please fill the title and the content");
    };
    //   handleFileInputContent(files: FileList){
    //     if (files.length < 1) {
    //       return;
    //     }
    //     this.fileToUpload = files[0];
    //     this.userService.uploadFile(this.fileToUpload)
    //       .subscribe(res => {
    //         if (this.rewardTemp && res) {
    //           this.rewardTemp.picture = res.url as string;
    //           // this.updateUser();
    //         } else {
    //           this.appComponent.showError('Failed to upload photo');
    //         }
    //       });
    //   }
    //   //------------------------------
    //   //----------CONTENT ONLY METHOD----------
    //   open(reward: Content): void
    //   {
    //     reward.active = true;
    //     this.adminService.addReward(reward).subscribe(reward => {
    //       this.appComponent.showSuccess(reward.name + " is now open.");
    //     });
    //   }
    //   close(): void
    //   {
    //     this.soonToBeClosedReward.active = false;
    //     this.adminService.addReward(this.soonToBeClosedReward).subscribe(reward => {
    //       let notifTemp : Notification = new Notification;
    //       let userTemp : User = this.userService.getLoggedInUser();
    //       this.appComponent.showSuccess(reward.name + " is now closed.");
    //       notifTemp.type = 3;
    //       notifTemp.adminId = userTemp.id;
    //       notifTemp.title = this.soonToBeClosedReward.name + " is now closed.";
    //       notifTemp.description = this.closeMessage + ". \n Closed by " + userTemp.name + ".";
    //       this.notificationService.addEdit(notifTemp).subscribe(notif =>{
    //       });
    //     });
    //   }
    //   delete(reward: Content): void 
    //   {
    //     this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => { 
    //       reward.deleted = true;
    //       this.adminService.deleteReward(reward).subscribe(r => {
    //         if(r){
    //           dtInstance.destroy();
    //           this.rewards = this.rewards.filter(r => {
    //             return r.id != reward.id;
    //           })
    //           this.dtTriggerContent.next();
    //           this.appComponent.showSuccess(reward.name + " is deleted.");
    //         }
    //         else
    //           this.appComponent.showError("Deleting " + reward.name + " is not succeed.");
    //       });
    //     });
    //   }
    //   //---------------------------------------
    //   //----------ON SELECT METHOD----------
    //   onSelect(reward : Content):void
    //   {
    //     this.selectedReward = reward;
    //     this.rewardTemp.id = reward.id;
    //     this.rewardTemp.type = reward.type;
    //     this.rewardTemp.role = reward.role;
    //     this.rewardTemp.name = reward.name;
    //     this.rewardTemp.expired = reward.expired;
    //     this.rewardTemp.changes = reward.changes;
    //     this.rewardTemp.groupsId = reward.groupsId;
    //     this.rewardTemp.carrot = reward.carrot;
    //     this.rewardTemp.active = reward.active;
    //     this.disabledGroup = [];
    //     this.enabledGroup = [];
    //     this.changes = reward.changes;
    //     this.groups.forEach((group) => {
    //       let flag:boolean = false;
    //       for(let i = 0; i< this.selectedReward.groupsId.length; i++)
    //       {
    //         if(group.id == this.selectedReward.groupsId[i])
    //         {
    //           this.enabledGroup.push(group);
    //           flag = true;
    //           break;
    //         }
    //       }
    //       if(flag == false)
    //         this.disabledGroup.push(group);
    //     });
    //   }
    //   onSelectUser(managerId : string):void
    //   {
    //     if(managerId != "--SELECT A MANAGER--" && managerId != "")
    //     {    
    //       this.selectedUser.id = managerId;
    //     }
    //   }
    //   groupOnSelect(group : Group):void
    //   {
    //     this.selectedGroup = group;
    //     this.groupTemp.id = group.id;
    //     this.groupTemp.name = group.name;
    //     this.groupTemp.managerId = group.managerId;
    //     this.groupTemp.membersId = group.membersId;
    //     this.groupTemp.autoCarrotAdmin = group.autoCarrotAdmin;
    //     this.groupTemp.birthdayCarrotAdmin = group.birthdayCarrotAdmin;
    //     this.groupTemp.autoCarrotManager = group.autoCarrotManager;
    //     this.groupTemp.birthdayCarrotManager = group.birthdayCarrotManager;
    //     this.groupTemp.creationDate = group.creationDate;
    //     this.groupTemp.active = group.active;
    //     this.groupTemp.manager = group.manager;
    //     this.groupTemp.members = group.members;
    //   }
    //   groupOnSelectToMove(group : Group):void
    //   {
    //     this.changeGroupCheck = true;
    //     this.selectedGroup = group;   
    //     this.onEnabledGroup(this.selectedGroup);
    //   }
    //   groupOnSelectToRemove(group:Group)
    //   {
    //     this.changeGroupCheck = true;
    //     this.selectedGroup = group;
    //     this.onRemoveGroupToMoves(this.selectedGroup);
    //   }
    NewsletterComponent.prototype.onSelectNewsletter = function (newsletter) {
        this.selectedNewsletter = newsletter;
    };
    //   onSelectApproval(user:User, approval:Transaction)
    //   {
    //     this.selectedUser = user;
    //     this.selectedApproval = approval;
    //   }
    //   //------------------------------------
    //   //----------EDIT METHOD----------
    //   save() 
    //   {
    //     if(this.rewardTemp.expired != this.selectedReward.expired)
    //     {
    //       this.selectedReward =  this.addChange(this.selectedReward, "Expired Date", this.rewardTemp.expired)
    //     }
    //     if(this.rewardTemp.carrot != this.selectedReward.carrot)
    //     {
    //       this.selectedReward =  this.addChange(this.selectedReward, "Maximum Carrot", this.rewardTemp.carrot.toString())
    //     }
    //     if(this.rewardTemp.name != this.selectedReward.name)
    //     {
    //       this.selectedReward =  this.addChange(this.selectedReward, "Content Name", this.rewardTemp.name);
    //     } 
    //     this.selectedReward.groupsId = [];
    //     if(this.checkGroupType)
    //     {
    //       this.enabledGroup.forEach((group) => {
    //         this.selectedReward.groupsId.push(group.id);
    //         this.selectedReward = this.addChange(this.selectedReward, "Groups Able", group.name);
    //       })
    //       this.disabledGroup.forEach((group) =>{
    //         this.selectedReward = this.addChange(this.selectedReward, "Groups Disable", group.name);
    //       })
    //     }
    //     this.changeGroupCheck = false;
    //     this.adminService.addReward(this.selectedReward).subscribe(res => {
    //       console.log(res);
    //       if(res)
    //       {
    //         this.selectedReward = res;
    //         this.selectedReward.changes[this.selectedReward.changes.length-1].on = new Date().toString();
    //       }
    //     });
    //     this.appComponent.showSuccess("'" + this.selectedReward.name + "'" +  " has been updated.");
    //   }
    //   saveBirthdayStatus()
    //   {
    //     this.groupService.updateGroup(this.selectedGroup).subscribe();
    //     this.appComponent.showSuccess("Birthday Carrot has been Updated"); 
    //   }
    //   editGroupName():void
    //   {
    //     this.groupService.updateGroup(this.selectedGroup).subscribe();
    //     this.appComponent.showSuccess("Group name now become " + this.selectedGroup.name + "."); 
    //   }
    //   //-------------------------------
    //   //----------MOVING STAFF GROUP ON EDIT METHOD----------
    //   onEnabledGroup(group: Group)
    //   {
    //     this.disabledGroup = this.disabledGroup.filter((us) => {
    //       return us.id != group.id;
    //     });
    //     this.enabledGroup.push(group);
    //   }
    //   onRemoveGroupToMoves(group: Group){
    //     this.enabledGroup = this.enabledGroup.filter((man) => {
    //       return man.id != group.id
    //     });
    //     this.disabledGroup.push(group);
    //   }
    //   //-----------------------------------------------------
    //   //----------SET METHOD----------
    //   setLate(user: User)
    //   {
    //     if(user.alwaysLate == true)
    //     {    
    //       user.alwaysLate = false;    
    //       this.appComponent.showSuccess(user.name + " is now flagged 'Not Late'."); 
    //     }
    //     else
    //     {
    //       this.appComponent.showSuccess(user.name + " is now flagged 'Late'."); 
    //       user.alwaysLate = true;
    //     }
    //     this.userService.addEdit(user).subscribe();
    //   }
    //   setClose(reward: Content): void
    //   {
    //     this.soonToBeClosedReward = reward;
    //     this.adminService.addReward(reward).subscribe();
    //   }
    //   setApprove(message : string)
    //   { 
    //     console.log(message);
    //     if(message != "")
    //     {
    //       this.selectedApproval.approved = true;
    //       this.transactionService.postTransactionReject(this.selectedApproval).subscribe(transactions => {
    //         this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    //           dtInstance.destroy(); 
    //           this.approvals = [];
    //           this.transactions.forEach((transaction) => {
    //             if(transaction.type == 2 && transaction.approved == false && transaction.rejected == false)
    //               this.approvals.push(transaction);
    //           });
    //           this.dtTriggerContent.next();
    //         });  
    //         let notifTemp : Notification = new Notification;
    //         notifTemp.adminId = this.loggedInUser.id;
    //         notifTemp.userId = this.selectedUser.id;
    //         notifTemp.title = "Transaction Approved";
    //         notifTemp.description = message;
    //         notifTemp.type = 3;
    //         this.notificationService.addEdit(notifTemp).subscribe(notif =>{
    //           if(notif)
    //             this.appComponent.showSuccess(this.getUserNameById(this.selectedApproval.senderId)+  "'s transaction is approved.");
    //         })
    //       });
    //     }
    //     else
    //       this.appComponent.showError("Please fill the message.");
    //   }
    //   setDisapprove(message: string)
    //   {
    //     if(message != "")
    //     {
    //       this.selectedApproval.rejected = true;
    //       this.transactionService.postTransactionReject(this.selectedApproval).subscribe(transactions =>  {
    //         this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    //           dtInstance.destroy(); 
    //           this.approvals = [];
    //           this.transactions.forEach((transaction) => {
    //             if(transaction.type == 2 && transaction.approved == false && transaction.rejected == false)
    //               this.approvals.push(transaction);
    //           });
    //           this.dtTriggerContent.next();
    //         });  
    //         let notifTemp : Notification = new Notification;
    //         notifTemp.adminId = this.loggedInUser.id;
    //         notifTemp.userId = this.selectedUser.id;
    //         notifTemp.title = "Transaction Rejected";
    //         notifTemp.description = message;
    //         notifTemp.type = 3;
    //         this.notificationService.addEdit(notifTemp).subscribe(notif =>{
    //           if(notif)
    //           this.appComponent.showSuccess(this.getUserNameById(this.selectedApproval.senderId)+  "'s transaction is rejected.");
    //         })
    //       });
    //     }
    //     else
    //       this.appComponent.showError("Please fill the message.");
    //   }
    //   //------------------------------
    //   //----------CHECK METHOD----------
    //   checkOpen(status)
    //   {
    //     if(status == true)
    //       return true;
    //   } 
    //   checkClose(status)
    //   {
    //     if(status == false)
    //       return true;
    //   }
    //   checkContent(type)
    //   {
    //     this.currentChoice = type.target.value;
    //   }
    //   checkGroupType(type)
    //   {
    //     this.currentGroupChoice = type.target.value;
    //   }
    //   //--------------------------------
    //   //----------CANCEL METHOD----------
    //   cancelSave()
    //   {
    //     this.selectedReward.name = this.rewardTemp.name;
    //     this.selectedReward.expired = this.rewardTemp.expired;
    //     this.selectedReward.carrot = this.rewardTemp.carrot;
    //   }
    //   cancelEdit()
    //   {
    //     this.selectedGroup.name = this.groupTemp.name;
    //     this.selectedGroup.autoCarrotAdmin = this.groupTemp.autoCarrotAdmin;
    //     this.selectedGroup.birthdayCarrotAdmin = this.groupTemp.birthdayCarrotAdmin;
    //   }
    //---------------------------------
    NewsletterComponent.prototype.refresh = function () {
        window.location.reload();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_14_angular_datatables__["a" /* DataTableDirective */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_14_angular_datatables__["a" /* DataTableDirective */])
    ], NewsletterComponent.prototype, "dtElement", void 0);
    NewsletterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-newsletter',
            template: __webpack_require__("./src/app/admin/newsletter/newsletter.component.html"),
            styles: [__webpack_require__("./src/app/admin/newsletter/newsletter.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__service_admin_service__["a" /* AdminService */],
            __WEBPACK_IMPORTED_MODULE_6__service_group_service__["a" /* GroupService */],
            __WEBPACK_IMPORTED_MODULE_13__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_7__service_transaction_service__["a" /* TransactionService */],
            __WEBPACK_IMPORTED_MODULE_8__service_notification_service__["a" /* NotificationService */],
            __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_3__angular_common__["DatePipe"],
            __WEBPACK_IMPORTED_MODULE_12_ngx_order_pipe__["b" /* OrderPipe */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], NewsletterComponent);
    return NewsletterComponent;
}());



/***/ }),

/***/ "./src/app/admin/staff/staff.component.css":
/***/ (function(module, exports) {

module.exports = "a:not([href]):not([tabindex]) {\r\ncolor: #FFF;\r\ncursor: pointer;\r\n}"

/***/ }),

/***/ "./src/app/admin/staff/staff.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"admin-tabs py-3\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12 px-md-0\">\r\n\r\n                <div class=\"tab-content search-bo\" id=\"myTabContent\">\r\n                    <div class=\"tab-pane fade show active\" id=\"profile\" role=\"tabpanel\" aria-labelledby=\"profile-tab\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-12\">\r\n                                <hr class=\"box-title-hr\">\r\n                                <h4 class=\"my-2 box-title\">STAFF LIST</h4>\r\n                            </div>\r\n                            <div class=\"col-md-12\">\r\n                                <mat-progress-spinner *ngIf=\"loadingStaff\"\r\n                                class=\"text-center m-auto\"\r\n                                color=\"warn\"\r\n                                mode=\"indeterminate\" >\r\n                                </mat-progress-spinner> \r\n                                <table class=\"table table-hover mt-3\" datatable [dtOptions]=\"dtOptionsStaff\" [dtTrigger]=\"dtTriggerStaff\">\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th scope=\"col\">Name</th>\r\n                                            <th scope=\"col\">Email</th>\r\n                                            <th scope=\"col\">Birthday</th>\r\n                                            <th scope=\"col\">Role</th>\r\n                                            <th scope=\"col\">Late</th>\r\n                                            <th scope=\"col\">Carrot(s)</th>\r\n                                            <th scope=\"col\">Carrot(s) in this Month</th>\r\n                                            <th scope=\"col\">Carrot(s) in this Year </th>\r\n                                            <th scope=\"col\">Carrot(s) from Reward</th>\r\n                                            <th scope=\"col\">Carrot(s) Sent to Social Foundation</th>\r\n                                            <th scope=\"col\">Carrot(s) Shared</th>\r\n                                            <th scope=\"col\">Action</th>\r\n                                        </tr>\r\n                                    </thead>\r\n                                    <tbody>\r\n                                        <tr *ngFor=\"let user of staff\">\r\n                                            <td>{{user.name}}</td>\r\n                                            <td>{{user.email}}</td>\r\n                                            <td>{{getDate(user.dob)}}</td>\r\n                                            <td>{{getRoleType(user.role)}}</td>\r\n                                            <td>{{user.alwaysLate}}</td>\r\n                                            <td>{{user.carrot}}</td>\r\n                                            <td>{{user.carrotOfTheMonth}}</td>\r\n                                            <td>{{user.carrotOfTheYear}}</td>\r\n                                            <td>{{user.carrotToReward}}</td>\r\n                                            <td>{{user.carrotToSocial}}</td>\r\n                                            <td>{{user.carrotToOther}}</td>\r\n                                            <td><span><a class=\"btn btn-primary\" (click)=\"setLate(user)\">Change Late Flag</a></span></td>\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>"

/***/ }),

/***/ "./src/app/admin/staff/staff.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StaffComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_user__ = __webpack_require__("./src/app/model/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_admin_service__ = __webpack_require__("./src/app/service/admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__config_utility__ = __webpack_require__("./src/app/config/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angular_datatables__ = __webpack_require__("./node_modules/angular-datatables/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_datatables_net__ = __webpack_require__("./node_modules/datatables.net/js/jquery.dataTables.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_datatables_net___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_datatables_net__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var StaffComponent = /** @class */ (function () {
    function StaffComponent(http, adminService, userService, appComponent, router) {
        this.http = http;
        this.adminService = adminService;
        this.userService = userService;
        this.appComponent = appComponent;
        this.router = router;
        this.isLoading = false;
        this.loadingStaff = true;
        this.selectedUser = new __WEBPACK_IMPORTED_MODULE_3__model_user__["a" /* User */];
        this.dtOptionsStaff = {};
        this.dtTriggerStaff = new __WEBPACK_IMPORTED_MODULE_10_rxjs__["Subject"]();
    }
    StaffComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInUser = this.userService.getLoggedInUser();
        if (!this.loggedInUser) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_8__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        _this.loggedInUser = res;
                        _this.userService.setLoggedInUser(res);
                        _this.isLoading = false;
                        if (!res.admin)
                            _this.router.navigate(['/']);
                    }
                    else {
                        _this.isLoading = false;
                        _this.router.navigate(['/']);
                    }
                });
            }
            else {
                this.isLoading = false;
                this.router.navigate(['/']);
            }
        }
        else
            this.isLoading = false;
        this.getUsers();
    };
    StaffComponent.prototype.getUsers = function () {
        var _this = this;
        this.staff = [];
        return this.adminService.getUsers().subscribe(function (users) {
            _this.users = users;
            _this.users.forEach(function (user) {
                if (user.role == 4 || user.role == 5) {
                    _this.staff.push(user);
                }
            });
            _this.loadingStaff = false;
            _this.dtTriggerStaff.next();
        });
    };
    StaffComponent.prototype.getDate = function (type) {
        return __WEBPACK_IMPORTED_MODULE_7__config_utility__["transformDate"](type, 'dd-MM-yyyy');
    };
    StaffComponent.prototype.getRoleType = function (type) {
        return __WEBPACK_IMPORTED_MODULE_7__config_utility__["getRole"](type);
    };
    StaffComponent.prototype.getUserNameById = function (id) {
        if (this.users) {
            var name_1 = "";
            this.users.forEach(function (user) {
                if (user.id == id)
                    name_1 = user.name;
            });
            return name_1;
        }
    };
    StaffComponent.prototype.onSelectUser = function (managerId) {
        if (managerId != "--SELECT A MANAGER--" && managerId != "") {
            this.selectedUser.id = managerId;
        }
    };
    StaffComponent.prototype.setLate = function (user) {
        if (user.alwaysLate == true) {
            user.alwaysLate = false;
            this.appComponent.showSuccess(user.name + " is now flagged 'Not Late'.");
        }
        else {
            this.appComponent.showSuccess(user.name + " is now flagged 'Late'.");
            user.alwaysLate = true;
        }
        this.userService.addEdit(user).subscribe();
    };
    StaffComponent.prototype.refresh = function () {
        window.location.reload();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_9_angular_datatables__["a" /* DataTableDirective */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_9_angular_datatables__["a" /* DataTableDirective */])
    ], StaffComponent.prototype, "dtElement", void 0);
    StaffComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-staff',
            template: __webpack_require__("./src/app/admin/staff/staff.component.html"),
            styles: [__webpack_require__("./src/app/admin/staff/staff.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_4__service_admin_service__["a" /* AdminService */],
            __WEBPACK_IMPORTED_MODULE_5__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], StaffComponent);
    return StaffComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login_component__ = __webpack_require__("./src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__employee_employee_component__ = __webpack_require__("./src/app/employee/employee.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__register_register_component__ = __webpack_require__("./src/app/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__manager_manager_component__ = __webpack_require__("./src/app/manager/manager.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__admin_admin_component__ = __webpack_require__("./src/app/admin/admin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__root_admin_root_admin_component__ = __webpack_require__("./src/app/root-admin/root-admin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__senior_manager_senior_manager_component__ = __webpack_require__("./src/app/senior-manager/senior-manager.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__employee_transaction_transaction_component__ = __webpack_require__("./src/app/employee/transaction/transaction.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__profile_profile_component__ = __webpack_require__("./src/app/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__password_password_component__ = __webpack_require__("./src/app/password/password.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_2__login_login_component__["a" /* LoginComponent */] },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_4__register_register_component__["a" /* RegisterComponent */] },
    { path: 'employee', component: __WEBPACK_IMPORTED_MODULE_3__employee_employee_component__["a" /* EmployeeComponent */] },
    { path: 'employee/transaction', component: __WEBPACK_IMPORTED_MODULE_9__employee_transaction_transaction_component__["a" /* TransactionComponent */] },
    { path: 'manager', component: __WEBPACK_IMPORTED_MODULE_5__manager_manager_component__["a" /* ManagerComponent */] },
    { path: 'admin', component: __WEBPACK_IMPORTED_MODULE_6__admin_admin_component__["a" /* AdminComponent */] },
    { path: 'root-admin', component: __WEBPACK_IMPORTED_MODULE_7__root_admin_root_admin_component__["a" /* RootAdminComponent */] },
    { path: 'senior-manager', component: __WEBPACK_IMPORTED_MODULE_8__senior_manager_senior_manager_component__["a" /* SeniorManagerComponent */] },
    { path: 'profile', component: __WEBPACK_IMPORTED_MODULE_10__profile_profile_component__["a" /* ProfileComponent */] },
    { path: 'password', component: __WEBPACK_IMPORTED_MODULE_11__password_password_component__["a" /* PasswordComponent */] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forRoot(routes)
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]],
            declarations: []
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<header>\r\n  <!-- Fixed navbar -->\r\n  <nav class=\"navbar navbar-expand-md navbar-light fixed-top bg-light\">\r\n    <a class=\"navbar-brand\" routerLink=\"/\">\r\n        <img src=\"../assets/img/mitrais-logo.png\" alt=\"\">\r\n    </a>\r\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarCollapse\" aria-controls=\"navbarCollapse\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n    </button>\r\n    <div *ngIf=\"isUserActive()\" class=\"collapse navbar-collapse\" id=\"navbarCollapse\">\r\n      <ul class=\"navbar-nav ml-auto\">\r\n        <li class=\"nav-item dropdown\">\r\n          <a class=\"nav-link dropdown-toggle\" (click)=\"openNotif()\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n            <i class=\"fa fa-bell notif-icon\"></i>\r\n            <div class=\"notif-active\"></div>\r\n          </a>\r\n          <div class=\"dropdown-menu right-0\" aria-labelledby=\"navbarDropdown\" >\r\n            <mat-progress-spinner *ngIf=\"isLoading\"\r\n              class=\"text-center m-auto\"\r\n              color=\"warn\"\r\n              diameter=28\r\n              mode=\"indeterminate\" >\r\n            </mat-progress-spinner>\r\n            <div *ngIf=\"!isLoading\" >\r\n              <a *ngIf=\"(notifications < 1)\" class=\"dropdown-item\" ><strong>No notifications!</strong></a>\r\n              <a *ngFor=\"let notification of notifications\" class=\"dropdown-item\" style=\"padding: 0px\"\r\n                data-toggle=\"modal\" data-target=\"#modalNewslatter\" (click)=\"onSelectNotification(notification)\">\r\n                {{notification.title ? notification.title : notification.description}}\r\n              </a>\r\n            </div>\r\n          </div>\r\n        </li>\r\n        <li class=\"nav-item dropdown\">\r\n          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n            <i class=\"fa fa-user notif-icon\"></i>\r\n          </a>\r\n          <div class=\"dropdown-menu right-0\" aria-labelledby=\"navbarDropdown\">\r\n            <a routerLink=\"/profile\" class=\"dropdown-item\" ><strong>Profile</strong></a>\r\n            <a (click)=\"logout()\" class=\"dropdown-item text-gray-dark\" ><i class=\"fa fa-sign-out ml-3\"></i> Logout</a>\r\n          </div>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </nav>\r\n</header>\r\n\r\n<div id=\"container\" class=\"h-100\">\r\n    <router-outlet></router-outlet>\r\n\r\n    <!-- See Newsletter Modal -->\r\n    <div *ngIf=\"selectedNotification\" class=\"modal fade\" id=\"modalNewslatter\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"newslatterModalLabel\" aria-hidden=\"true\">\r\n        <div class=\"modal-dialog\" role=\"document\">\r\n            <div class=\"modal-content\">\r\n                <div class=\"modal-header\">\r\n                  <h5 class=\"modal-title\" id=\"newslatterModalLabel\">Newsletter</h5>\r\n                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                  </button>\r\n                </div>\r\n                <div class=\"modal-body\">\r\n                  <form>\r\n                    <div class=\"form-group\">\r\n                      <label for=\"title\">{{selectedNotification.title}}</label>\r\n                    </div>\r\n                    <div class=\"form-group\" [froalaView]=\"selectedNotification.description\"></div>\r\n                  </form>\r\n                </div>\r\n                <div class=\"modal-footer\">\r\n                  <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\">Close</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n</div>\r\n\r\n<footer class=\"footer text-center\">\r\n  <div class=\"container\">\r\n    <span class=\"text-muted\"><small>Copyright &copy; 2018 Mitrais. All rights reserved.</small></span>\r\n  </div>\r\n</footer>"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_notification_service__ = __webpack_require__("./src/app/service/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__ = __webpack_require__("./node_modules/ng2-toastr/ng2-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_config_service__ = __webpack_require__("./src/app/service/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AppComponent = /** @class */ (function () {
    function AppComponent(notificationService, userService, router, configService, toastr, vcr) {
        this.notificationService = notificationService;
        this.userService = userService;
        this.router = router;
        this.configService = configService;
        this.toastr = toastr;
        this.isLoading = false;
        this.toastr.setRootViewContainerRef(vcr);
    }
    AppComponent.prototype.ngOnInit = function () {
        // this.notificationService.getAll()
        // .subscribe(notifications => {
        //   console.log(notifications);
        //   this.notifications = notifications;
        // });
        this.configService.getConfigData();
    };
    AppComponent.prototype.logout = function () {
        this.userService.logout();
        this.router.navigate(['/']);
    };
    AppComponent.prototype.isUserActive = function () {
        if (this.userService.getLoggedInUser())
            return true;
        if (localStorage.getItem(__WEBPACK_IMPORTED_MODULE_4__config_constanta__["TOKEN_KEY_NAME"]))
            return true;
        return false;
    };
    AppComponent.prototype.onSelectNotification = function (notification) {
        if (notification.type != __WEBPACK_IMPORTED_MODULE_4__config_constanta__["BIRTHDAY_NOTIF"]) {
            this.selectedNotification = notification;
            console.log(notification);
        }
        else {
            this.selectedNotification = null;
        }
    };
    AppComponent.prototype.showSuccess = function (text) {
        this.toastr.success(text);
    };
    AppComponent.prototype.showError = function (text) {
        this.toastr.error(text);
    };
    AppComponent.prototype.showWarning = function (text) {
        this.toastr.warning(text);
    };
    AppComponent.prototype.openNotif = function () {
        var _this = this;
        this.isLoading = true;
        var user = this.userService.getLoggedInUser();
        if (user) {
            this.notificationService.getByUser(user.id)
                .subscribe(function (notifications) {
                if (notifications) {
                    console.log(notifications);
                    _this.notifications = notifications.slice(0, 10);
                    _this.isLoading = false;
                }
            });
        }
        else
            this.isLoading = false;
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__service_notification_service__["a" /* NotificationService */],
            __WEBPACK_IMPORTED_MODULE_3__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_6__service_config_service__["a" /* ConfigService */],
            __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__["ToastsManager"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_order_pipe__ = __webpack_require__("./node_modules/ngx-order-pipe/ngx-order-pipe.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_pagination__ = __webpack_require__("./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_routing_module__ = __webpack_require__("./src/app/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__login_login_component__ = __webpack_require__("./src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__employee_employee_component__ = __webpack_require__("./src/app/employee/employee.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__register_register_component__ = __webpack_require__("./src/app/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__manager_manager_component__ = __webpack_require__("./src/app/manager/manager.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__admin_admin_component__ = __webpack_require__("./src/app/admin/admin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__service_admin_service__ = __webpack_require__("./src/app/service/admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__employee_bazaar_bazaar_component__ = __webpack_require__("./src/app/employee/bazaar/bazaar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__employee_birthday_birthday_component__ = __webpack_require__("./src/app/employee/birthday/birthday.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__service_group_service__ = __webpack_require__("./src/app/service/group.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__service_notification_service__ = __webpack_require__("./src/app/service/notification.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__service_config_service__ = __webpack_require__("./src/app/service/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21_angular_datatables__ = __webpack_require__("./node_modules/angular-datatables/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22_angular_froala_wysiwyg__ = __webpack_require__("./node_modules/angular-froala-wysiwyg/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__senior_manager_senior_manager_component__ = __webpack_require__("./src/app/senior-manager/senior-manager.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__service_manager_group_service__ = __webpack_require__("./src/app/service/manager-group.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__service_manager_service__ = __webpack_require__("./src/app/service/manager.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__root_admin_root_admin_component__ = __webpack_require__("./src/app/root-admin/root-admin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__service_root_admin_service__ = __webpack_require__("./src/app/service/root-admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__employee_transaction_transaction_component__ = __webpack_require__("./src/app/employee/transaction/transaction.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__service_transaction_service__ = __webpack_require__("./src/app/service/transaction.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__profile_profile_component__ = __webpack_require__("./src/app/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__service_content_service__ = __webpack_require__("./src/app/service/content.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__employee_social_social_component__ = __webpack_require__("./src/app/employee/social/social.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__employee_group_group_component__ = __webpack_require__("./src/app/employee/group/group.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__angular_material_progress_spinner__ = __webpack_require__("./node_modules/@angular/material/esm5/progress-spinner.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35_ng2_toastr_ng2_toastr__ = __webpack_require__("./node_modules/ng2-toastr/ng2-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_35_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__employee_achievement_achievement_component__ = __webpack_require__("./src/app/employee/achievement/achievement.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__admin_content_content_component__ = __webpack_require__("./src/app/admin/content/content.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__admin_approval_approval_component__ = __webpack_require__("./src/app/admin/approval/approval.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__admin_staff_staff_component__ = __webpack_require__("./src/app/admin/staff/staff.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__admin_newsletter_newsletter_component__ = __webpack_require__("./src/app/admin/newsletter/newsletter.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__admin_group_staff_group_staff_component__ = __webpack_require__("./src/app/admin/group-staff/group-staff.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__root_admin_admin_list_admin_list_component__ = __webpack_require__("./src/app/root-admin/admin-list/admin-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__root_admin_approval_manager_approval_manager_component__ = __webpack_require__("./src/app/root-admin/approval-manager/approval-manager.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__root_admin_approval_admin_approval_admin_component__ = __webpack_require__("./src/app/root-admin/approval-admin/approval-admin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__root_admin_approval_social_approval_social_component__ = __webpack_require__("./src/app/root-admin/approval-social/approval-social.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__root_admin_carrot_give_carrot_give_component__ = __webpack_require__("./src/app/root-admin/carrot-give/carrot-give.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__root_admin_carrot_set_carrot_set_component__ = __webpack_require__("./src/app/root-admin/carrot-set/carrot-set.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__password_password_component__ = __webpack_require__("./src/app/password/password.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_9__login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_10__employee_employee_component__["a" /* EmployeeComponent */],
                __WEBPACK_IMPORTED_MODULE_11__register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_13__manager_manager_component__["a" /* ManagerComponent */],
                __WEBPACK_IMPORTED_MODULE_14__admin_admin_component__["a" /* AdminComponent */],
                __WEBPACK_IMPORTED_MODULE_16__employee_bazaar_bazaar_component__["a" /* BazaarComponent */],
                __WEBPACK_IMPORTED_MODULE_17__employee_birthday_birthday_component__["a" /* BirthdayComponent */],
                __WEBPACK_IMPORTED_MODULE_26__root_admin_root_admin_component__["a" /* RootAdminComponent */],
                __WEBPACK_IMPORTED_MODULE_23__senior_manager_senior_manager_component__["a" /* SeniorManagerComponent */],
                __WEBPACK_IMPORTED_MODULE_28__employee_transaction_transaction_component__["a" /* TransactionComponent */],
                __WEBPACK_IMPORTED_MODULE_30__profile_profile_component__["a" /* ProfileComponent */],
                __WEBPACK_IMPORTED_MODULE_32__employee_social_social_component__["a" /* SocialComponent */],
                __WEBPACK_IMPORTED_MODULE_33__employee_group_group_component__["a" /* GroupComponent */],
                __WEBPACK_IMPORTED_MODULE_37__employee_achievement_achievement_component__["a" /* AchievementComponent */],
                __WEBPACK_IMPORTED_MODULE_39__admin_content_content_component__["a" /* ContentComponent */],
                __WEBPACK_IMPORTED_MODULE_40__admin_approval_approval_component__["a" /* ApprovalComponent */],
                __WEBPACK_IMPORTED_MODULE_41__admin_staff_staff_component__["a" /* StaffComponent */],
                __WEBPACK_IMPORTED_MODULE_42__admin_newsletter_newsletter_component__["a" /* NewsletterComponent */],
                __WEBPACK_IMPORTED_MODULE_43__admin_group_staff_group_staff_component__["a" /* GroupStaffComponent */],
                __WEBPACK_IMPORTED_MODULE_44__root_admin_admin_list_admin_list_component__["a" /* AdminListComponent */],
                __WEBPACK_IMPORTED_MODULE_45__root_admin_approval_manager_approval_manager_component__["a" /* ApprovalManagerComponent */],
                __WEBPACK_IMPORTED_MODULE_46__root_admin_approval_admin_approval_admin_component__["a" /* ApprovalAdminComponent */],
                __WEBPACK_IMPORTED_MODULE_47__root_admin_approval_social_approval_social_component__["a" /* ApprovalSocialComponent */],
                __WEBPACK_IMPORTED_MODULE_48__root_admin_carrot_give_carrot_give_component__["a" /* CarrotGiveComponent */],
                __WEBPACK_IMPORTED_MODULE_49__root_admin_carrot_set_carrot_set_component__["a" /* CarrotSetComponent */],
                __WEBPACK_IMPORTED_MODULE_37__employee_achievement_achievement_component__["a" /* AchievementComponent */],
                __WEBPACK_IMPORTED_MODULE_50__password_password_component__["a" /* PasswordComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_8__app_routing_module__["a" /* AppRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_5_ngx_order_pipe__["a" /* OrderModule */],
                __WEBPACK_IMPORTED_MODULE_21_angular_datatables__["b" /* DataTablesModule */],
                __WEBPACK_IMPORTED_MODULE_6_ngx_pagination__["a" /* NgxPaginationModule */],
                __WEBPACK_IMPORTED_MODULE_34__angular_material_progress_spinner__["a" /* MatProgressSpinnerModule */],
                __WEBPACK_IMPORTED_MODULE_6_ngx_pagination__["a" /* NgxPaginationModule */],
                __WEBPACK_IMPORTED_MODULE_22_angular_froala_wysiwyg__["a" /* FroalaEditorModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_22_angular_froala_wysiwyg__["b" /* FroalaViewModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_36__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_38__angular_material__["b" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_38__angular_material__["i" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_38__angular_material__["d" /* MatPaginatorModule */],
                __WEBPACK_IMPORTED_MODULE_38__angular_material__["g" /* MatSortModule */],
                __WEBPACK_IMPORTED_MODULE_38__angular_material__["e" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_38__angular_material__["a" /* MatCheckboxModule */],
                __WEBPACK_IMPORTED_MODULE_35_ng2_toastr_ng2_toastr__["ToastModule"].forRoot()
            ],
            // exports: [
            //   MatProgressSpinnerModule,
            // ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_12__service_user_service__["a" /* UserService */],
                __WEBPACK_IMPORTED_MODULE_15__service_admin_service__["a" /* AdminService */],
                __WEBPACK_IMPORTED_MODULE_18__service_group_service__["a" /* GroupService */],
                __WEBPACK_IMPORTED_MODULE_27__service_root_admin_service__["a" /* RootAdminService */],
                __WEBPACK_IMPORTED_MODULE_24__service_manager_group_service__["a" /* ManagerGroupService */],
                __WEBPACK_IMPORTED_MODULE_25__service_manager_service__["a" /* ManagerService */],
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["DatePipe"],
                __WEBPACK_IMPORTED_MODULE_29__service_transaction_service__["a" /* TransactionService */],
                __WEBPACK_IMPORTED_MODULE_31__service_content_service__["a" /* ContentService */],
                __WEBPACK_IMPORTED_MODULE_20__service_config_service__["a" /* ConfigService */],
                __WEBPACK_IMPORTED_MODULE_19__service_notification_service__["a" /* NotificationService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/config/constanta.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "API_URL", function() { return API_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MANAGER_ROLE", function() { return MANAGER_ROLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EMPLOYEE_ROLE", function() { return EMPLOYEE_ROLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROOT_ADMIN_ROLE", function() { return ROOT_ADMIN_ROLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SENIOR_MANAGER_ROLE", function() { return SENIOR_MANAGER_ROLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACHIEVEMENT_TYPE", function() { return ACHIEVEMENT_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "REWARD_TYPE", function() { return REWARD_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SOCIAL_TYPE", function() { return SOCIAL_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SHARED_TYPE", function() { return SHARED_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BIRTHDAY_NOTIF", function() { return BIRTHDAY_NOTIF; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NEWSLETTER_NOTIF", function() { return NEWSLETTER_NOTIF; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADMIN_NOTIF", function() { return ADMIN_NOTIF; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UNSIGNED_UPLOAD_PRESET", function() { return UNSIGNED_UPLOAD_PRESET; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPLOAD_URL", function() { return UPLOAD_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TOKEN_KEY_NAME", function() { return TOKEN_KEY_NAME; });
// export const API_URL: string = "http://172.19.12.131:8846/api/"; //mitrais5
// export const API_URL: string = "http://172.19.13.46:8846/api/";  //mitrais
// export const API_URL: string = "http://192.168.26.2:8846/api/"; //byod
// export const API_URL: string = "https://carrot.mybluemix.net/api/";
var API_URL = "/api/"; //build
// export const STAFF_ROLE = 1;
// export const ADMIN_ROLE = 2;
//	export const UNKNOWN_ROLE = 3;
var MANAGER_ROLE = 4;
var EMPLOYEE_ROLE = 5;
var ROOT_ADMIN_ROLE = 6;
//	export const STAKEHOLDER_ROLE = 7;
var SENIOR_MANAGER_ROLE = 8;
var ACHIEVEMENT_TYPE = 1;
var REWARD_TYPE = 2;
var SOCIAL_TYPE = 3;
var SHARED_TYPE = 4;
var BIRTHDAY_NOTIF = 1;
var NEWSLETTER_NOTIF = 2;
var ADMIN_NOTIF = 3;
var UNSIGNED_UPLOAD_PRESET = 'lurfv21d';
var UPLOAD_URL = "https://api.cloudinary.com/v1_1/huzakerna/image/upload";
var TOKEN_KEY_NAME = 'carrot-token';


/***/ }),

/***/ "./src/app/config/utility.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["transformDate"] = transformDate;
/* harmony export (immutable) */ __webpack_exports__["getDate"] = getDate;
/* harmony export (immutable) */ __webpack_exports__["getType"] = getType;
/* harmony export (immutable) */ __webpack_exports__["getRole"] = getRole;
/* harmony export (immutable) */ __webpack_exports__["isValidEmail"] = isValidEmail;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__constanta__ = __webpack_require__("./src/app/config/constanta.ts");


function transformDate(date, format) {
    if (!format)
        format = 'MMM dd, yyyy';
    var pipe = new __WEBPACK_IMPORTED_MODULE_0__angular_common__["DatePipe"]('en-US');
    return pipe.transform(date, format);
}
function getDate(date, format) {
    if (!format)
        format = 'MMM dd, yyyy';
    return new Date(transformDate(date, format));
}
function getType(type) {
    switch (type) {
        case __WEBPACK_IMPORTED_MODULE_1__constanta__["ACHIEVEMENT_TYPE"]:
            return 'Achievement';
        case __WEBPACK_IMPORTED_MODULE_1__constanta__["REWARD_TYPE"]:
            return 'Reward';
        case __WEBPACK_IMPORTED_MODULE_1__constanta__["SOCIAL_TYPE"]:
            return 'Social';
        case __WEBPACK_IMPORTED_MODULE_1__constanta__["SHARED_TYPE"]:
            return 'Shared';
        default:
            return '-';
    }
}
function getRole(type) {
    switch (type) {
        case __WEBPACK_IMPORTED_MODULE_1__constanta__["MANAGER_ROLE"]:
            return 'Manager';
        case __WEBPACK_IMPORTED_MODULE_1__constanta__["EMPLOYEE_ROLE"]:
            return 'Employee';
        case __WEBPACK_IMPORTED_MODULE_1__constanta__["ROOT_ADMIN_ROLE"]:
            return 'Root Admin';
        case __WEBPACK_IMPORTED_MODULE_1__constanta__["SENIOR_MANAGER_ROLE"]:
            return 'Senior Manager';
        default:
            return '-';
    }
}
function isValidEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}


/***/ }),

/***/ "./src/app/employee/achievement/achievement.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee/achievement/achievement.component.html":
/***/ (function(module, exports) {

module.exports = "\n\n<section class=\"bazaar-2-items mb-4\">\n    <div class=\"container search-box pb-4\">\n      <div class=\"row\">\n        <div class=\"col-md-12\">\n          <hr class=\"box-title-hr\">\n          <h4 class=\"my-2 box-title\">Achievement</h4>\n        </div>\n        <mat-progress-spinner *ngIf=\"!contents\"\n          class=\"text-center m-auto\"\n          color=\"warn\"\n          mode=\"indeterminate\" >\n        </mat-progress-spinner>\n        <div *ngFor=\"let content of contents\" class=\"col-md-6 br-1\">\n          <div class=\"text-center\">\n            <img src=\"{{content.picture ? content.picture : '../../assets/img/na.jpg'}}\" class=\"img-fluid p-3 bazaar-item mb-3\" alt=\"\">\n          </div>\n          <div class=\"px-3\">\n            <h3>{{content.name}}</h3>\n            <h4><strong class=\"carrot-orange\">{{content.carrot}} Carrots</strong></h4>\n            <p>\n              {{content.description ? content.description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse eius velit dolores exercitationem porro tempore, ipsa suscipit quia. Libero voluptate quibusdam neque numquam error quas ipsa hic voluptatem aliquam, necessitatibus.'}}\n            </p>\n            <button (click)=\"onSelect(content)\" data-toggle=\"modal\" data-target=\"#modalContent\" class=\"btn btn-carrot radius-5\" >\n              {{'Detail'}}\n            </button>\n            {{content.currentCarrot > 0 ? 'Accomplished' : 'Not Accomplished yet'}}\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n  \n  <!-- Modal -->\n  <div *ngIf=\"selectedContent\" class=\"modal fade\" id=\"modalContent\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalContentLabel\" aria-hidden=\"true\">\n      <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n          <div class=\"modal-header\">\n            <h5 class=\"modal-title\" id=\"modalContentLabel\">Detail</h5>\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n              <span aria-hidden=\"true\">&times;</span>\n            </button>\n          </div>\n          <div class=\"modal-body\">\n            <form>\n              <div class=\"form-group\">\n                <label for=\"name\">Achievement</label> \n                <input name=\"name\" type=\"name\" class=\"form-control here\" [(ngModel)]=\"selectedContent.name\" disabled />\n              </div>\n              <div class=\"form-group\">\n                <label for=\"description\">Description</label> \n                <textarea id=\"description\" name=\"description\" cols=\"40\" rows=\"8\" class=\"form-control\" placeholder=\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse eius velit dolores exercitationem porro tempore, ipsa suscipit quia. Libero voluptate quibusdam neque numquam error quas ipsa hic voluptatem aliquam, necessitatibus.\" >\n                  {{selectedContent.description && selectedContent.description}}\n                </textarea>\n              </div> \n              <div class=\"form-group\">\n                <label for=\"carrot\">Carrot</label> \n                <input name=\"carrot\" type=\"number\" class=\"form-control here\" [(ngModel)]=\"selectedContent.carrot\" disabled  >\n              </div> \n            </form>\n          </div>\n          <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-link float-left\" data-dismiss=\"modal\">Close</button>\n            <!-- <button (click)=\"user.carrot > selectedContent.carrot && exchange(selectedContent)\" data-dismiss=\"modal\" type=\"button\" class=\"btn btn-carrot radius-5 float-right\"\n              [disabled]=\"user.carrot <= selectedContent.carrot\"  >\n              {{user.carrot > selectedContent.carrot ? 'Exchange' : 'Insufficient Carrot'}}\n            </button> -->\n          </div>\n        </div>\n      </div>\n    </div>"

/***/ }),

/***/ "./src/app/employee/achievement/achievement.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AchievementComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_content_service__ = __webpack_require__("./src/app/service/content.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AchievementComponent = /** @class */ (function () {
    function AchievementComponent(userService, contentService) {
        this.userService = userService;
        this.contentService = contentService;
    }
    AchievementComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.userService.getLoggedInUser();
        if (this.user) {
            this.contentService.fetchContentPlus(__WEBPACK_IMPORTED_MODULE_3__config_constanta__["ACHIEVEMENT_TYPE"], this.user.id)
                .subscribe(function (res) {
                console.log(res);
                _this.contents = res;
            });
        }
    };
    AchievementComponent.prototype.onSelect = function (content) {
        this.selectedContent = content;
    };
    AchievementComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-achievement',
            template: __webpack_require__("./src/app/employee/achievement/achievement.component.html"),
            styles: [__webpack_require__("./src/app/employee/achievement/achievement.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_2__service_content_service__["a" /* ContentService */]])
    ], AchievementComponent);
    return AchievementComponent;
}());



/***/ }),

/***/ "./src/app/employee/bazaar/bazaar.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee/bazaar/bazaar.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<section class=\"bazaar-2-items mb-4\">\r\n  <div class=\"container search-box pb-4\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\">\r\n        <hr class=\"box-title-hr\">\r\n        <h4 class=\"my-2 box-title\">Bazaar</h4>\r\n      </div>\r\n      <mat-progress-spinner *ngIf=\"!contents\"\r\n        class=\"text-center m-auto\"\r\n        color=\"warn\"\r\n        mode=\"indeterminate\" >\r\n      </mat-progress-spinner>\r\n      <div *ngFor=\"let content of contents\" class=\"col-md-6 br-1\">\r\n        <div class=\"text-center\">\r\n          <img src=\"{{content.picture ? content.picture : '../../assets/img/na.jpg'}}\" class=\"img-fluid p-3 bazaar-item mb-3\" alt=\"\">\r\n        </div>\r\n        <div class=\"px-3\">\r\n          <h3>{{content.name}}</h3>\r\n          <h4><strong class=\"carrot-orange\">{{content.carrot}} Carrots</strong></h4>\r\n          <p>\r\n            {{content.description ? content.description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse eius velit dolores exercitationem porro tempore, ipsa suscipit quia. Libero voluptate quibusdam neque numquam error quas ipsa hic voluptatem aliquam, necessitatibus.'}}\r\n          </p>\r\n          <button (click)=\"content.currentCarrot > 0 && onSelect(content)\" data-toggle=\"modal\" data-target=\"#modalContent\" [disabled]=\"content.currentCarrot < 1\" class=\"btn btn-carrot radius-5\" >\r\n            {{content.currentCarrot > 0 ? 'Exchange' : 'Sold Out'}}\r\n          </button>\r\n          {{content.currentCarrot}} left\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n\r\n<!-- Modal -->\r\n<div *ngIf=\"selectedContent\" class=\"modal fade\" id=\"modalContent\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalContentLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n      <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n          <h5 class=\"modal-title\" id=\"modalContentLabel\">Exchange</h5>\r\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" #closeBtn>\r\n            <span aria-hidden=\"true\">&times;</span>\r\n          </button>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n          <form>\r\n            <div class=\"form-group\">\r\n              <label for=\"name\">Reward</label> \r\n              <input name=\"name\" type=\"name\" class=\"form-control here\" [(ngModel)]=\"selectedContent.name\" disabled />\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label for=\"description\">Description</label> \r\n              <textarea id=\"description\" name=\"description\" cols=\"40\" rows=\"8\" class=\"form-control\" placeholder=\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse eius velit dolores exercitationem porro tempore, ipsa suscipit quia. Libero voluptate quibusdam neque numquam error quas ipsa hic voluptatem aliquam, necessitatibus.\" >\r\n                {{selectedContent.description && selectedContent.description}}\r\n              </textarea>\r\n            </div> \r\n            <div class=\"form-group\">\r\n              <label for=\"carrot\">Carrot</label> \r\n              <input name=\"carrot\" type=\"number\" class=\"form-control here\" [(ngModel)]=\"selectedContent.carrot\" disabled  >\r\n            </div> \r\n          </form>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n          <mat-progress-spinner *ngIf=\"isExchangeLoading\"\r\n            class=\"text-center m-auto\"\r\n            color=\"warn\"\r\n            diameter=34\r\n            mode=\"indeterminate\" >\r\n          </mat-progress-spinner>\r\n          <div *ngIf=\"!isExchangeLoading\" >\r\n            <button type=\"button\" class=\"btn btn-link float-left\" data-dismiss=\"modal\">Cancel</button>\r\n            <button (click)=\"user.carrot > selectedContent.carrot && exchange(selectedContent)\" type=\"button\" class=\"btn btn-carrot radius-5 float-right\"\r\n              [disabled]=\"user.carrot <= selectedContent.carrot\"  >\r\n              {{user.carrot > selectedContent.carrot ? 'Exchange' : 'Insufficient Carrot'}}\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "./src/app/employee/bazaar/bazaar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BazaarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_transaction_service__ = __webpack_require__("./src/app/service/transaction.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_content_service__ = __webpack_require__("./src/app/service/content.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__model_transaction__ = __webpack_require__("./src/app/model/transaction.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("./src/app/app.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var BazaarComponent = /** @class */ (function () {
    function BazaarComponent(transactionService, userService, contentService, appComponent) {
        this.transactionService = transactionService;
        this.userService = userService;
        this.contentService = contentService;
        this.appComponent = appComponent;
        this.isExchangeLoading = false;
    }
    BazaarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.userService.getLoggedInUser();
        var groupId = this.user && this.user.groupId ? this.user.groupId : null;
        this.contentService.fetchContentsByType(__WEBPACK_IMPORTED_MODULE_4__config_constanta__["REWARD_TYPE"], groupId)
            .subscribe(function (res) {
            console.log(res);
            // this.contents = res.filter(content => {
            //   return content.active && !content.deleted
            // });
            _this.contents = res;
        });
    };
    BazaarComponent.prototype.onSelect = function (content) {
        if (!this.user) {
            this.user = this.userService.getLoggedInUser();
        }
        this.selectedContent = content;
    };
    BazaarComponent.prototype.exchange = function (content) {
        var _this = this;
        if (content.carrot > this.user.carrot) {
            this.appComponent.showError('Not enough carrot!');
            return;
        }
        this.isExchangeLoading = true;
        var transaction = new __WEBPACK_IMPORTED_MODULE_5__model_transaction__["a" /* Transaction */]();
        transaction.type = __WEBPACK_IMPORTED_MODULE_4__config_constanta__["REWARD_TYPE"];
        transaction.senderId = this.user.id;
        transaction.contentId = content.id;
        transaction.amount = content.carrot;
        transaction.description = 'Reward: ' + content.name;
        transaction.approved = false;
        this.transactionService.postTransaction(transaction)
            .subscribe(function (res) {
            console.log(res);
            _this.isExchangeLoading = false;
            _this.closeModal();
            if (res) {
                _this.user.carrot -= transaction.amount;
                content.currentCarrot -= 1;
                _this.appComponent.showSuccess('Exchange Success!');
            }
            else {
                _this.appComponent.showError('Exchange Failed!');
            }
        });
    };
    BazaarComponent.prototype.closeModal = function () {
        this.closeBtn.nativeElement.click();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('closeBtn'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], BazaarComponent.prototype, "closeBtn", void 0);
    BazaarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-bazaar',
            template: __webpack_require__("./src/app/employee/bazaar/bazaar.component.html"),
            styles: [__webpack_require__("./src/app/employee/bazaar/bazaar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__service_transaction_service__["a" /* TransactionService */],
            __WEBPACK_IMPORTED_MODULE_1__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_3__service_content_service__["a" /* ContentService */],
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]])
    ], BazaarComponent);
    return BazaarComponent;
}());



/***/ }),

/***/ "./src/app/employee/birthday/birthday.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee/birthday/birthday.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"bazaar-1-item mb-4 pb-5\">\r\n  <div class=\"container search-box pb-4\">\r\n    <div class=\"row d-flex\">\r\n      <div class=\"col-md-12\">\r\n        <hr class=\"box-title-hr\">\r\n        <h4 class=\"my-2 box-title\">Today Birthday</h4>\r\n      </div>\r\n      \r\n      <mat-progress-spinner *ngIf=\"!users\"\r\n        class=\"text-center m-auto\"\r\n        color=\"warn\"\r\n        mode=\"indeterminate\" >\r\n      </mat-progress-spinner>\r\n      <div *ngFor=\"let user of users\" class=\"col-md-12 card\">\r\n        <div class=\"row soft-shadow p-0 m-2\">\r\n          <div class=\"col-md-2 my-auto\">\r\n            <img src=\"{{user && user.picture ? user.picture : '../../assets/img/user.png'}}\" alt=\"\" class=\"img-fluid rounded-circle\">\r\n          </div>\r\n          <div class=\"col-md-10 my-auto\">\r\n            <a data-toggle=\"modal\" (click)=\"selectUser(user)\" data-target=\"#modalProfile\"><h4 class=\"mb-0 \">{{user.name}}</h4></a>\r\n            <p class=\" \">{{Utility.transformDate(user.dob, 'longDate')}}</p>\r\n            <a class=\"badge badge-orange\" data-toggle=\"modal\" (click)=\"selectUser(user, true)\" data-target=\"#modalShare\">Send carrot</a>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n\r\n<section class=\"bazaar-1-item mb-4 pb-5\">\r\n  <div class=\"container search-box pb-4\">\r\n    <div class=\"row d-flex\">\r\n      <div class=\"col-md-12\">\r\n        <hr class=\"box-title-hr\">\r\n        <h4 class=\"my-2 box-title\">Yesterday</h4>\r\n      </div>\r\n      <mat-progress-spinner *ngIf=\"!users2\"\r\n        class=\"text-center m-auto\"\r\n        color=\"warn\"\r\n        mode=\"indeterminate\" >\r\n      </mat-progress-spinner>\r\n      <div *ngFor=\"let user of users2\" class=\"col-md-12 card\">\r\n        <div class=\"row soft-shadow p-0 m-2\">\r\n          <div class=\"col-md-2 my-auto\">\r\n            <img src=\"{{user && user.picture ? user.picture : '../../assets/img/user.png'}}\" alt=\"\" class=\"img-fluid rounded-circle\">\r\n          </div>\r\n          <div class=\"col-md-10 my-auto\">\r\n            <a data-toggle=\"modal\" (click)=\"selectUser(user)\" data-target=\"#modalProfile\"><h4 class=\"mb-0 \">{{user.name}}</h4></a>\r\n            <p class=\" \">{{Utility.transformDate(user.dob, 'longDate')}}</p>\r\n            <a class=\"badge badge-orange\" data-toggle=\"modal\" (click)=\"selectUser(user, true)\" data-target=\"#modalShare\">Send carrot</a>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n\r\n\r\n<section class=\"bazaar-1-item mb-4 pb-5\">\r\n  <div class=\"container search-box pb-4\">\r\n    <div class=\"row d-flex\">\r\n      <div class=\"col-md-12\">\r\n        <hr class=\"box-title-hr\">\r\n        <h4 class=\"my-2 box-title\">2 days ago</h4>\r\n      </div>\r\n      \r\n      <mat-progress-spinner *ngIf=\"!users3\"\r\n        class=\"text-center m-auto\"\r\n        color=\"warn\"\r\n        mode=\"indeterminate\" >\r\n      </mat-progress-spinner>\r\n      <div *ngFor=\"let user of users3\" class=\"col-md-12 card\">\r\n        <div class=\"row soft-shadow p-0 m-2\">\r\n          <div class=\"col-md-2 my-auto\">\r\n            <img src=\"{{user && user.picture ? user.picture : '../../assets/img/user.png'}}\" alt=\"\" class=\"img-fluid rounded-circle\">\r\n          </div>\r\n          <div class=\"col-md-10 my-auto\">\r\n            <a data-toggle=\"modal\" (click)=\"selectUser(user)\" data-target=\"#modalProfile\"><h4 class=\"mb-0 \">{{user.name}}</h4></a>\r\n            <p class=\" \">{{Utility.transformDate(user.dob, 'longDate')}}</p>\r\n            <a class=\"badge badge-orange\" data-toggle=\"modal\" (click)=\"selectUser(user, true)\" data-target=\"#modalShare\">Send carrot</a>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n  \r\n<!-- Modal Share -->\r\n<div *ngIf=\"selectedUser && modalShare\" class=\"modal fade\" id=\"modalShare\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalShareLabel\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"modalShareLabel\">Share your carrot!</h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" #closeBtn >\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <form>\r\n          <div class=\"form-group\">\r\n            <label for=\"name\">Recipient</label> \r\n            <input name=\"name\" type=\"text\" class=\"form-control here\" placeholder=\"Autocomplete + dropdown\" [(ngModel)]=\"selectedUser.name\" disabled />\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <label for=\"description\">Comments</label> \r\n            <textarea #description id=\"description\" name=\"description\" cols=\"40\" rows=\"5\" class=\"form-control\" placeholder=\"Happy birthday broooo!!!!!!\" required ></textarea>\r\n          </div> \r\n          <div class=\"form-group\">\r\n            <label for=\"amount\">Carrot</label> \r\n            <input #amount (input)=\"changeAmount(amount.value)\" name=\"amount\" type=\"number\" class=\"form-control here\" [min]=1 [max]=\"config.carrotLimit\" value={{amountValue}} required >\r\n          </div> \r\n        </form>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <mat-progress-spinner *ngIf=\"isExchangeLoading\"\r\n          class=\"text-center m-auto\"\r\n          color=\"warn\"\r\n          diameter=34\r\n          mode=\"indeterminate\" >\r\n        </mat-progress-spinner>\r\n        <div *ngIf=\"!isExchangeLoading\" >\r\n          <button type=\"button\" class=\"btn btn-link float-left\" data-dismiss=\"modal\">Cancel</button>\r\n          <button (click)=\"sendCarrot(description.value)\" type=\"button\" class=\"btn btn-carrot radius-5 float-right\">Send Carrot</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n  \r\n\r\n<!-- Modal Profile -->\r\n<div *ngIf=\"selectedUser && !modalShare\" class=\"modal fade\" id=\"modalProfile\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"exampleModalLabel\">{{selectedUser.name}}</h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <form>\r\n          <div class=\"form-group\">\r\n            <label for=\"text\">Date of birth</label> \r\n            <input name=\"dob\" type=\"text\" class=\"form-control here\" [(ngModel)]=\"selectedUser.dob\" disabled />\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <label for=\"text\">Join Date</label> \r\n            <input name=\"joinDate\" type=\"text\" class=\"form-control here\" [(ngModel)]=\"selectedUser.joinDate\" disabled />\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <label for=\"textarea\">Address</label> \r\n            <textarea id=\"textarea\" name=\"textarea\" cols=\"40\" rows=\"5\" class=\"form-control\" [(ngModel)]=\"selectedUser.address\" disabled ></textarea>\r\n          </div> \r\n          <div class=\"form-group\">\r\n            <label for=\"text1\">Carrot</label> \r\n            <input id=\"text1\" name=\"text1\" type=\"number\" class=\"form-control here\"  [(ngModel)]=\"selectedUser.carrot\"  disabled />\r\n          </div>\r\n        </form>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-link float-left\" data-dismiss=\"modal\">Close</button>\r\n        <!-- <button type=\"button\" class=\"btn btn-carrot radius-5 float-right\">Send Carrot</button> -->\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/employee/birthday/birthday.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BirthdayComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_transaction_service__ = __webpack_require__("./src/app/service/transaction.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_utility__ = __webpack_require__("./src/app/config/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_config_service__ = __webpack_require__("./src/app/service/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var BirthdayComponent = /** @class */ (function () {
    function BirthdayComponent(userService, transactionService, configService, appComponent) {
        this.userService = userService;
        this.transactionService = transactionService;
        this.configService = configService;
        this.appComponent = appComponent;
        this.amountValue = 1;
        this.isExchangeLoading = false;
        this.Constanta = __WEBPACK_IMPORTED_MODULE_4__config_constanta__;
        this.Utility = __WEBPACK_IMPORTED_MODULE_3__config_utility__;
    }
    BirthdayComponent.prototype.ngOnInit = function () {
        this.loggedInUser = this.userService.getLoggedInUser();
        this.getUsers();
        this.config = this.configService.getConfigData();
        if (this.loggedInUser && this.loggedInUser.carrot < this.config.carrotLimit) {
            this.config.carrotLimit = this.loggedInUser.carrot;
        }
    };
    BirthdayComponent.prototype.getUsers = function () {
        // this.users = this.userService.getAll();
        var _this = this;
        if (!this.users) {
            this.userService.fetchBirthdayUsers(0)
                .subscribe(function (users) {
                console.log(users);
                // for(let user of users){
                //   user.dob = Utility.transformDate(user.dob, 'longDate');
                //   user.joinDate = Utility.transformDate(user.joinDate, 'longDate');
                // }
                _this.users = users;
                // this.userService.setAll(users);
            });
        }
        this.userService.fetchBirthdayUsers(-1)
            .subscribe(function (users) {
            console.log(users);
            // for(let user of users){
            //   user.dob = Utility.transformDate(user.dob, 'longDate');
            //   user.joinDate = Utility.transformDate(user.joinDate, 'longDate');
            // }
            _this.users2 = users;
            // this.userService.setAll(users);
        });
        this.userService.fetchBirthdayUsers(-2)
            .subscribe(function (users) {
            console.log(users);
            // for(let user of users){
            //   user.dob = Utility.transformDate(user.dob, 'longDate');
            //   user.joinDate = Utility.transformDate(user.joinDate, 'longDate');
            // }
            _this.users3 = users;
            // this.userService.setAll(users);
        });
    };
    BirthdayComponent.prototype.selectUser = function (user, modalShare) {
        if (!this.loggedInUser) {
            this.loggedInUser = this.userService.getLoggedInUser();
        }
        console.log(modalShare);
        this.selectedUser = user;
        if (modalShare)
            this.modalShare = true;
        else
            this.modalShare = false;
    };
    BirthdayComponent.prototype.sendCarrot = function (description) {
        var _this = this;
        this.isExchangeLoading = true;
        if (!this.loggedInUser) {
            this.loggedInUser = this.userService.getLoggedInUser();
        }
        var senderId = this.loggedInUser.id;
        var receiverId = this.selectedUser.id;
        var type = __WEBPACK_IMPORTED_MODULE_4__config_constanta__["SHARED_TYPE"];
        var amount = this.amountValue;
        if (amount > 0) {
            this.transactionService.postTransaction({ type: type, senderId: senderId, receiverId: receiverId, amount: amount, description: description })
                .subscribe(function (res) {
                console.log(res);
                _this.isExchangeLoading = false;
                _this.closeModal();
                if (res) {
                    _this.loggedInUser.carrot -= amount;
                    _this.selectedUser.carrot += amount;
                    _this.appComponent.showSuccess('Send Success!');
                }
                else {
                    _this.appComponent.showError('Send Failed!');
                }
            });
        }
        else {
            this.appComponent.showError('Send Failed!');
        }
    };
    BirthdayComponent.prototype.changeAmount = function (amount) {
        this.amountValue = amount;
        if (this.amountValue > this.config.carrotLimit) {
            this.amountValue = this.config.carrotLimit;
        }
        // console.log(this.amountValue);
    };
    BirthdayComponent.prototype.closeModal = function () {
        this.closeBtn.nativeElement.click();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('closeBtn'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], BirthdayComponent.prototype, "closeBtn", void 0);
    BirthdayComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-birthday',
            template: __webpack_require__("./src/app/employee/birthday/birthday.component.html"),
            styles: [__webpack_require__("./src/app/employee/birthday/birthday.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_2__service_transaction_service__["a" /* TransactionService */],
            __WEBPACK_IMPORTED_MODULE_6__service_config_service__["a" /* ConfigService */],
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]])
    ], BirthdayComponent);
    return BirthdayComponent;
}());



/***/ }),

/***/ "./src/app/employee/employee.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee/employee.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Begin page content -->\r\n<main role=\"main\" class=\"container mt-3\">\r\n  <div class=\"row d-flex\">\r\n    <div class=\"col-md-6\">\r\n      <h2 class=\"mt-4 pl-0 text-grey ml-0\">EMPLOYEE DASHBOARD</h2>\r\n    </div>\r\n    <div class=\"col-md-6\">\r\n      <div class=\"btn-group mt-4 pull-right \" role=\"group\" aria-label=\"Basic example\">\r\n        <button (click)=\"openAchievement()\" [ngClass]=\"{'btn-success': isAchievement, 'btn-outline-success': !isAchievement }\" class=\"btn radius-5\">Achievement</button>\r\n        <button (click)=\"openBazaar()\" [ngClass]=\"{'btn-success': isBazaar, 'btn-outline-success': !isBazaar }\" class=\"btn radius-5\">Bazaar</button>\r\n        <button (click)=\"openSocial()\" [ngClass]=\"{'btn-success': isSocial, 'btn-outline-success': !isSocial }\" class=\"btn radius-5\">Social</button>\r\n        <button (click)=\"openGroup()\" [ngClass]=\"{'btn-success': isGroup, 'btn-outline-success': !isGroup }\" class=\"btn radius-5\">Group</button>\r\n        <button (click)=\"openBirthday()\" [ngClass]=\"{'btn-success': isBirthday, 'btn-outline-success': !isBirthday }\" class=\"btn radius-5\">Birthday</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</main>\r\n\r\n<section class=\"mini-dashboard my-4\">\r\n  <div class=\"container\">\r\n    <mat-progress-spinner *ngIf=\"!user\"\r\n      class=\"text-center m-auto\"\r\n      color=\"warn\"\r\n      mode=\"indeterminate\" >\r\n    </mat-progress-spinner>\r\n\r\n    <div class=\"row\" *ngIf=\"user\" >\r\n      <div class=\"col-md-4\">\r\n        <div class=\"row box-profile soft-shadow px-0 mr-0\">\r\n          <div class=\"col-md-4 my-auto\">\r\n            <img src=\"{{user && user.picture ? user.picture : '../../assets/img/user.png'}}\" alt=\"\" class=\"img-fluid rounded-circle\">\r\n          </div>\r\n          <div class=\"col-md-8 my-auto\">\r\n            <h4 class=\"mb-0 text-white\">{{user ? user.name : 'Name'}}</h4>\r\n            <p class=\"text-white\">Mitrais Employee</p>\r\n            <a routerLink=\"/profile\" class=\"badge badge-white\">Edit Profile</a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-4\">\r\n        <div class=\"row box-carrot px-0 mr-0\">\r\n          <div class=\"col-md-4 my-auto\">\r\n            <img src=\"../../assets/img/mc-icon-carrot.png\" alt=\"\" class=\"img-fluid rounded-circle\">\r\n          </div>\r\n          <div class=\"col-md-8 my-auto\">\r\n            <h4 class=\"text-white\">You have {{user ? user.carrot : '0'}} carrots!</h4>\r\n            <!-- <a class=\"badge badge-white\" data-toggle=\"modal\" data-target=\"#exampleModal\">\r\n              Share carrot!\r\n            </a> -->\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-4\">\r\n        <div class=\"row box-additional px-0\">\r\n          <div class=\"col-md-4 my-auto\">\r\n            <img src=\"../../assets/img/mc-icon-transaction.png\" alt=\"\" class=\"img-fluid rounded-circle\">\r\n          </div>\r\n          <div class=\"col-md-8 my-auto\">\r\n            <h4 class=\"text-white\">Carrots Transaction History</h4>\r\n            <a routerLink=\"/employee/transaction\" class=\"badge badge-white\">View</a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </div> \r\n</section>\r\n\r\n<div *ngIf=\"!isLoading\" >\r\n  <app-achievement *ngIf=\"isAchievement\" ></app-achievement>\r\n  <app-bazaar *ngIf=\"isBazaar\" ></app-bazaar>\r\n  <app-social *ngIf=\"isSocial\" ></app-social>\r\n  <app-group *ngIf=\"isGroup\" ></app-group>\r\n  <app-birthday *ngIf=\"isBirthday\" ></app-birthday>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/employee/employee.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmployeeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EmployeeComponent = /** @class */ (function () {
    function EmployeeComponent(userService, router) {
        this.userService = userService;
        this.router = router;
        this.isAchievement = false;
        this.isBazaar = true;
        this.isSocial = false;
        this.isGroup = false;
        this.isBirthday = false;
        this.isLoading = false;
    }
    EmployeeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isLoading = true;
        this.user = this.userService.getLoggedInUser();
        if (!this.user) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_3__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        console.log(res);
                        _this.user = res;
                        _this.userService.setLoggedInUser(res);
                        _this.isLoading = false;
                        if (res.role != __WEBPACK_IMPORTED_MODULE_3__config_constanta__["EMPLOYEE_ROLE"] || res.admin)
                            _this.router.navigate(['/']);
                    }
                    else {
                        _this.isLoading = false;
                        _this.router.navigate(['/']);
                    }
                });
            }
            else {
                this.isLoading = false;
                this.router.navigate(['/']);
            }
        }
        else
            this.isLoading = false;
    };
    EmployeeComponent.prototype.openAchievement = function () {
        this.isAchievement = true;
        this.isBazaar = false;
        this.isSocial = false;
        this.isSocial = false;
        this.isGroup = false;
        this.isBirthday = false;
    };
    EmployeeComponent.prototype.openBazaar = function () {
        this.isBazaar = true;
        this.isAchievement = false;
        this.isSocial = false;
        this.isSocial = false;
        this.isGroup = false;
        this.isBirthday = false;
    };
    EmployeeComponent.prototype.openBirthday = function () {
        this.isBirthday = true;
        this.isAchievement = false;
        this.isBazaar = false;
        this.isSocial = false;
        this.isGroup = false;
    };
    EmployeeComponent.prototype.openGroup = function () {
        this.isBirthday = false;
        this.isAchievement = false;
        this.isBazaar = false;
        this.isGroup = true;
        this.isSocial = false;
    };
    EmployeeComponent.prototype.openSocial = function () {
        this.isSocial = true;
        this.isAchievement = false;
        this.isBazaar = false;
        this.isGroup = false;
        this.isBirthday = false;
    };
    EmployeeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-employee',
            template: __webpack_require__("./src/app/employee/employee.component.html"),
            styles: [__webpack_require__("./src/app/employee/employee.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], EmployeeComponent);
    return EmployeeComponent;
}());



/***/ }),

/***/ "./src/app/employee/group/group.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee/group/group.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"bazaar-1-item mb-4 pb-5\">\n  <div class=\"container search-box pb-4\">\n    <div class=\"row d-flex\">\n      <div class=\"col-md-12\">\n        <hr class=\"box-title-hr\">\n        <h4 class=\"my-2 box-title\">{{loggedInUser && loggedInUser.group && loggedInUser.group.name ? loggedInUser.group.name : 'Colleagues'}}</h4>\n      </div>\n\n      <mat-progress-spinner *ngIf=\"!users\"\n        class=\"text-center m-auto\"\n        color=\"warn\"\n        mode=\"indeterminate\" >\n      </mat-progress-spinner>\n      <div *ngFor=\"let user of users\" class=\"col-md-12 card\">\n        <div class=\"row soft-shadow p-0 m-2\">\n          <div class=\"col-md-2 my-auto\">\n            <img src=\"{{user && user.picture ? user.picture : '../../assets/img/user.png'}}\" alt=\"\" class=\"img-fluid rounded-circle\">\n          </div>\n          <div class=\"col-md-10 my-auto\">\n            <a data-toggle=\"modal\" (click)=\"selectUser(user)\" data-target=\"#modalProfile\"><h4 class=\"mb-0 \">{{user.name}}</h4></a>\n            <p class=\" \">{{Utility.transformDate(user.dob, 'longDate')}}</p>\n            <a class=\"badge badge-orange\" data-toggle=\"modal\" (click)=\"selectUser(user, true)\" data-target=\"#modalShare\">Send carrot</a>\n          </div>\n        </div>\n\n      </div>\n    </div>\n  </div>\n</section>\n  \n<!-- Modal Share -->\n<div *ngIf=\"selectedUser && modalShare\" class=\"modal fade\" id=\"modalShare\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalShareLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5 class=\"modal-title\" id=\"modalShareLabel\">Share your carrot!</h5>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" #closeBtn >\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <form>\n          <div class=\"form-group\">\n            <label for=\"name\">Recipient</label> \n            <input name=\"name\" type=\"text\" class=\"form-control here\" placeholder=\"Autocomplete + dropdown\" [(ngModel)]=\"selectedUser.name\" disabled />\n          </div>\n          <div class=\"form-group\">\n            <label for=\"description\">Comments</label> \n            <textarea #description id=\"description\" name=\"description\" cols=\"40\" rows=\"5\" class=\"form-control\" placeholder=\"Good job broooo!!!!!!\" required ></textarea>\n          </div> \n          <div class=\"form-group\">\n            <label for=\"amount\">Carrot</label> \n            <input #amount (input)=\"changeAmount(amount.value)\" name=\"amount\" type=\"number\" class=\"form-control here\" [min]=1 [max]=\"config.carrotLimit\" value={{amountValue}} required >\n          </div> \n        </form>\n      </div>\n      <div class=\"modal-footer\">\n        <mat-progress-spinner *ngIf=\"isExchangeLoading\"\n          class=\"text-center m-auto\"\n          color=\"warn\"\n          diameter=34\n          mode=\"indeterminate\" >\n        </mat-progress-spinner>\n        <div *ngIf=\"!isExchangeLoading\" >\n          <button type=\"button\" class=\"btn btn-link float-left\" data-dismiss=\"modal\">Cancel</button>\n          <button (click)=\"sendCarrot(description.value)\" type=\"button\" class=\"btn btn-carrot radius-5 float-right\">Send Carrot</button>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n  \n\n<!-- Modal Profile -->\n<div *ngIf=\"selectedUser && !modalShare\" class=\"modal fade\" id=\"modalProfile\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5 class=\"modal-title\" id=\"exampleModalLabel\">{{selectedUser.name}}</h5>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <form>\n          <div class=\"form-group\">\n            <label for=\"text\">Date of birth</label> \n            <input name=\"dob\" type=\"text\" class=\"form-control here\" [(ngModel)]=\"selectedUser.dob\" disabled />\n          </div>\n          <div class=\"form-group\">\n            <label for=\"text\">Join Date</label> \n            <input name=\"joinDate\" type=\"text\" class=\"form-control here\" [(ngModel)]=\"selectedUser.joinDate\" disabled />\n          </div>\n          <div class=\"form-group\">\n            <label for=\"textarea\">Address</label> \n            <textarea id=\"address\" name=\"address\" cols=\"40\" rows=\"5\" class=\"form-control\" [(ngModel)]=\"selectedUser.address\" disabled ></textarea>\n          </div> \n          <div class=\"form-group\">\n            <label for=\"text1\">Carrot</label> \n            <input id=\"carrot\" name=\"carrot\" type=\"number\" class=\"form-control here\"  [(ngModel)]=\"selectedUser.carrot\" disabled />\n          </div>\n        </form>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-link float-left\" data-dismiss=\"modal\">Close</button>\n        <!-- <button type=\"button\" class=\"btn btn-carrot radius-5 float-right\">Send Carrot</button> -->\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/employee/group/group.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_transaction_service__ = __webpack_require__("./src/app/service/transaction.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_utility__ = __webpack_require__("./src/app/config/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_config_service__ = __webpack_require__("./src/app/service/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var GroupComponent = /** @class */ (function () {
    function GroupComponent(userService, transactionService, configService, appComponent) {
        this.userService = userService;
        this.transactionService = transactionService;
        this.configService = configService;
        this.appComponent = appComponent;
        this.amountValue = 1;
        this.isExchangeLoading = false;
        this.Constanta = __WEBPACK_IMPORTED_MODULE_4__config_constanta__;
        this.Utility = __WEBPACK_IMPORTED_MODULE_3__config_utility__;
    }
    GroupComponent.prototype.ngOnInit = function () {
        this.loggedInUser = this.userService.getLoggedInUser();
        this.getUsers();
        this.config = this.configService.getConfigData();
        if (this.loggedInUser && this.loggedInUser.carrot < this.config.carrotLimit) {
            this.config.carrotLimit = this.loggedInUser.carrot;
        }
    };
    GroupComponent.prototype.getUsers = function () {
        // this.users = this.userService.getAll();
        var _this = this;
        if (!this.users) {
            this.userService.fetchUsersBySupervisor(this.loggedInUser.supervisorId)
                .subscribe(function (users) {
                console.log(users);
                _this.users = users.filter(function (user) {
                    return user.id != _this.loggedInUser.id;
                });
            });
        }
    };
    GroupComponent.prototype.selectUser = function (user, modalShare) {
        if (!this.loggedInUser) {
            this.loggedInUser = this.userService.getLoggedInUser();
        }
        console.log(modalShare);
        this.selectedUser = user;
        if (modalShare)
            this.modalShare = true;
        else
            this.modalShare = false;
    };
    GroupComponent.prototype.sendCarrot = function (description) {
        var _this = this;
        this.isExchangeLoading = true;
        if (!this.loggedInUser) {
            this.loggedInUser = this.userService.getLoggedInUser();
        }
        var senderId = this.loggedInUser.id;
        var receiverId = this.selectedUser.id;
        var type = __WEBPACK_IMPORTED_MODULE_4__config_constanta__["SHARED_TYPE"];
        var amount = this.amountValue;
        if (amount > 0) {
            this.transactionService.postTransaction({ type: type, senderId: senderId, receiverId: receiverId, amount: amount, description: description })
                .subscribe(function (res) {
                console.log(res);
                _this.isExchangeLoading = false;
                _this.closeModal();
                if (res) {
                    _this.loggedInUser.carrot -= amount;
                    _this.selectedUser.carrot += amount;
                    _this.appComponent.showSuccess('Send Success!');
                }
                else {
                    _this.appComponent.showError('Send Failed!');
                }
            });
        }
        else {
            this.appComponent.showError('Send Failed!');
        }
    };
    GroupComponent.prototype.changeAmount = function (amount) {
        this.amountValue = amount;
        if (this.amountValue > this.config.carrotLimit) {
            this.amountValue = this.config.carrotLimit;
        }
        console.log(this.amountValue);
    };
    GroupComponent.prototype.closeModal = function () {
        this.closeBtn.nativeElement.click();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('closeBtn'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], GroupComponent.prototype, "closeBtn", void 0);
    GroupComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-group',
            template: __webpack_require__("./src/app/employee/group/group.component.html"),
            styles: [__webpack_require__("./src/app/employee/group/group.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_2__service_transaction_service__["a" /* TransactionService */],
            __WEBPACK_IMPORTED_MODULE_6__service_config_service__["a" /* ConfigService */],
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]])
    ], GroupComponent);
    return GroupComponent;
}());



/***/ }),

/***/ "./src/app/employee/social/social.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee/social/social.component.html":
/***/ (function(module, exports) {

module.exports = "\n\n<section class=\"bazaar-2-items mb-4\">\n  <div class=\"container search-box pb-4\">\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n        <hr class=\"box-title-hr\">\n        <h4 class=\"my-2 box-title\">Social Foundation</h4>\n      </div>\n      <mat-progress-spinner *ngIf=\"!contents\"\n        class=\"text-center m-auto\"\n        color=\"warn\"\n        mode=\"indeterminate\" >\n      </mat-progress-spinner>\n      <div *ngFor=\"let content of contents\" class=\"col-md-6 br-1\">\n        <div class=\"text-center\">\n          <img src=\"{{content.picture ? content.picture : '../../assets/img/na.jpg'}}\" class=\"img-fluid p-3 bazaar-item mb-3\" alt=\"\">\n        </div>\n        <div class=\"px-3\">\n          <h3>{{content.name}}</h3>\n          <h4><strong class=\"carrot-orange\">{{content.carrot}} Carrots</strong></h4>\n          <p>\n            {{content.description ? content.description : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse eius velit dolores exercitationem porro tempore, ipsa suscipit quia. Libero voluptate quibusdam neque numquam error quas ipsa hic voluptatem aliquam, necessitatibus.'}}\n          </p>\n          <button (click)=\"onSelect(content)\" data-toggle=\"modal\" data-target=\"#modalContent\" class=\"btn btn-carrot radius-5\" >\n            {{'Donate'}}\n          </button>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>\n\n<!-- Modal -->\n<div *ngIf=\"selectedContent\" class=\"modal fade\" id=\"modalContent\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalContentLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\" id=\"modalContentLabel\">Donate</h5>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" #closeBtn >\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          <form>\n            <div class=\"form-group\">\n              <label for=\"name\">Social</label> \n              <input name=\"name\" type=\"name\" class=\"form-control here\" [(ngModel)]=\"selectedContent.name\" disabled />\n            </div>\n            <div class=\"form-group\">\n              <label for=\"description\">Description</label> \n              <textarea id=\"description\" name=\"description\" cols=\"40\" rows=\"8\" class=\"form-control\" placeholder=\"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse eius velit dolores exercitationem porro tempore, ipsa suscipit quia. Libero voluptate quibusdam neque numquam error quas ipsa hic voluptatem aliquam, necessitatibus.\" >\n                {{selectedContent.description && selectedContent.description}}\n              </textarea>\n            </div> \n            <div class=\"form-group\">\n              <label for=\"amount\">Carrot</label> \n              <input #amount (input)=\"changeAmount(amount.value)\" name=\"amount\" type=\"number\" class=\"form-control here\" [min]=1 [max]=\"user.carrot\" [(ngModel)]=\"amountValue\" required />\n            </div> \n          </form>\n        </div>\n        <div class=\"modal-footer\">\n          <mat-progress-spinner *ngIf=\"isExchangeLoading\"\n            class=\"text-center m-auto\"\n            color=\"warn\"\n            diameter=34\n            mode=\"indeterminate\" >\n          </mat-progress-spinner>\n          <div *ngIf=\"!isExchangeLoading\" >\n            <button type=\"button\" class=\"btn btn-link float-left\" data-dismiss=\"modal\">Cancel</button>\n            <button (click)=\"exchange(selectedContent)\" type=\"button\" class=\"btn btn-carrot radius-5 float-right\">Donate</button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/employee/social/social.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocialComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_transaction_service__ = __webpack_require__("./src/app/service/transaction.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_content_service__ = __webpack_require__("./src/app/service/content.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__model_transaction__ = __webpack_require__("./src/app/model/transaction.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("./src/app/app.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SocialComponent = /** @class */ (function () {
    function SocialComponent(transactionService, userService, contentService, appComponent) {
        this.transactionService = transactionService;
        this.userService = userService;
        this.contentService = contentService;
        this.appComponent = appComponent;
        this.amountValue = 1;
        this.isExchangeLoading = false;
    }
    SocialComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.userService.getLoggedInUser();
        var groupId = this.user && this.user.groupId ? this.user.groupId : null;
        this.contentService.fetchContentsByType(__WEBPACK_IMPORTED_MODULE_4__config_constanta__["SOCIAL_TYPE"], groupId)
            .subscribe(function (res) {
            console.log(res);
            // this.contents = res.filter(content => {
            //   return content.active && !content.deleted
            // });
            _this.contents = res;
        });
    };
    SocialComponent.prototype.onSelect = function (content) {
        if (!this.user) {
            this.user = this.userService.getLoggedInUser();
        }
        this.selectedContent = content;
    };
    SocialComponent.prototype.exchange = function (content) {
        var _this = this;
        if (content.carrot > this.user.carrot) {
            this.appComponent.showError('Not enough carrot!');
            return;
        }
        this.isExchangeLoading = true;
        var transaction = new __WEBPACK_IMPORTED_MODULE_5__model_transaction__["a" /* Transaction */]();
        transaction.type = __WEBPACK_IMPORTED_MODULE_4__config_constanta__["SOCIAL_TYPE"];
        transaction.senderId = this.user.id;
        transaction.contentId = content.id;
        transaction.amount = this.amountValue;
        transaction.description = 'Social: ' + content.name;
        this.transactionService.postTransaction(transaction)
            .subscribe(function (res) {
            console.log(res);
            _this.isExchangeLoading = false;
            _this.closeModal();
            if (res) {
                _this.user.carrot -= transaction.amount;
                content.currentCarrot -= 1;
                _this.appComponent.showSuccess('Donate Success!');
            }
            else {
                _this.appComponent.showError('Donate Failed!');
            }
        });
    };
    SocialComponent.prototype.changeAmount = function (amount) {
        console.log(amount);
        this.amountValue = amount;
        if (this.amountValue > this.user.carrot) {
            this.amountValue = this.user.carrot;
        }
        console.log(this.amountValue);
    };
    SocialComponent.prototype.closeModal = function () {
        this.closeBtn.nativeElement.click();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('closeBtn'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], SocialComponent.prototype, "closeBtn", void 0);
    SocialComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-social',
            template: __webpack_require__("./src/app/employee/social/social.component.html"),
            styles: [__webpack_require__("./src/app/employee/social/social.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__service_transaction_service__["a" /* TransactionService */],
            __WEBPACK_IMPORTED_MODULE_1__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_3__service_content_service__["a" /* ContentService */],
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]])
    ], SocialComponent);
    return SocialComponent;
}());



/***/ }),

/***/ "./src/app/employee/transaction/transaction.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/employee/transaction/transaction.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Begin page content -->\n<main role=\"main\" class=\"container mt-3\">\n  <div class=\"row d-flex\">\n    <div class=\"col-md-6\">\n      <h2 class=\"mt-4 pl-0 text-grey ml-0\">\n        <span class=\"back-button\">\n          <a routerLink=\"/employee\" ><img src=\"../../../assets/img/back.png\" alt=\"\" class=\"back\" /></a>\n        </span> \n        TRANSACTION HISTORY\n      </h2>\n    </div>\n  </div>\n</main>\n\n\n<section class=\"mini-dashboard my-4\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-md-3\">\n        <div class=\"row box-reward px-0 mr-0\">\n          <div class=\"col-md-4 my-auto\">\n            <img src=\"../../../assets/img/mc-icon-carrot.png\" alt=\"\" class=\"img-fluid rounded-circle\">\n          </div>\n          <div class=\"col-md-8 my-auto\">\n            <p class=\"sub-title\">Earning</p>\n            <mat-progress-spinner *ngIf=\"!user || user.carrotOfTheYear == null\"\n              diameter=50\n              color=\"warn\"\n              mode=\"indeterminate\" >\n            </mat-progress-spinner>\n            <h2 class=\"text-white\" *ngIf=\"user && user.carrotOfTheYear != null\" >{{user.carrotOfTheYear}}</h2>\n            <!-- <a class=\"badge badge-white\" data-toggle=\"modal\" data-target=\"#exampleModal\">\n          Share carrot!</a> -->\n          </div>\n        </div>\n      </div>\n      <div class=\"col-md-3\">\n        <div class=\"row box-shared px-0 mr-0\">\n          <div class=\"col-md-4 my-auto\">\n            <img src=\"../../../assets/img/mc-icon-carrot.png\" alt=\"\" class=\"img-fluid rounded-circle\">\n          </div>\n          <div class=\"col-md-8 my-auto\">\n            <p class=\"sub-title\">Reward</p>\n            <mat-progress-spinner *ngIf=\"!user || user.carrotToReward == null\"\n              diameter=50\n              color=\"warn\"\n              mode=\"indeterminate\" >\n            </mat-progress-spinner>\n            <h2 class=\"text-white\" *ngIf=\"user && user.carrotToReward != null\" >{{user.carrotToReward}}</h2>\n            <!-- <a class=\"badge badge-white\" data-toggle=\"modal\" data-target=\"#exampleModal\">\n          Share carrot!</a> -->\n          </div>\n        </div>\n      </div>\n      <div class=\"col-md-3\">\n        <div class=\"row box-bazaar px-0 mr-0\">\n          <div class=\"col-md-4 my-auto\">\n            <img src=\"../../../assets/img/mc-icon-carrot.png\" alt=\"\" class=\"img-fluid rounded-circle\">\n          </div>\n          <div class=\"col-md-8 my-auto\">\n            <p class=\"sub-title\">Social</p>\n            <mat-progress-spinner *ngIf=\"!user || user.carrotToSocial == null\"\n              diameter=50\n              color=\"warn\"\n              mode=\"indeterminate\" >\n            </mat-progress-spinner>\n            <h2 class=\"text-white\" *ngIf=\"user && user.carrotToSocial != null\" >{{user.carrotToSocial}}</h2>\n            <!-- <a class=\"badge badge-white\" data-toggle=\"modal\" data-target=\"#exampleModal\">\n          Share carrot!</a> -->\n          </div>\n        </div>\n      </div>\n      <div class=\"col-md-3\">\n        <div class=\"row box-reward px-0 mr-0\">\n          <div class=\"col-md-4 my-auto\">\n            <img src=\"../../../assets/img/mc-icon-carrot.png\" alt=\"\" class=\"img-fluid rounded-circle\">\n          </div>\n          <div class=\"col-md-8 my-auto\">\n            <p class=\"sub-title\">Shared</p>\n            <mat-progress-spinner *ngIf=\"!user || user.carrotToOther == null\"\n              diameter=50\n              color=\"warn\"\n              mode=\"indeterminate\" >\n            </mat-progress-spinner>\n            <h2 class=\"text-white\" *ngIf=\"user && user.carrotToOther != null\">{{user.carrotToOther}}</h2>\n            <!-- <a class=\"badge badge-white\" data-toggle=\"modal\" data-target=\"#exampleModal\">\n          Share carrot!</a> -->\n          </div>\n        </div>\n      </div>\n    </div>\n    \n  </div> \n</section>\n\n<section class=\"transaction-history my-4\">\n  <div class=\"container search-box\">\n    <div class=\"row d-flex align-content-end\">\n      <div class=\"col-md-12\">\n        <!-- <form>\n          <div class=\"row\">\n            <div class=\"col-md-3\">\n              <div class=\"form-group\">\n                <label for=\"carrot-type\">Type</label> \n                <div>\n                  <select id=\"carrot-type\" name=\"carrot-type\" class=\"custom-select\">\n                    <option value=\"\">Choose Type</option>\n                    <option value=\"Reward\">Achievement</option>\n                    <option value=\"Bazaar\">Reward</option>\n                    <option value=\"Bazaar\">Social</option>\n                    <option value=\"Shared\">Shared</option>\n                  </select>\n                </div>\n              </div>\n            </div>\n            <div class=\"col-md-3\">\n              <div class=\"form-group\">\n                <label for=\"\">Date From</label> \n                <input id=\"\" name=\"\" type=\"date\" class=\"form-control here\">\n              </div>\n            </div>\n            <div class=\"col-md-3\">\n              <div class=\"form-group\">\n                <label for=\"date-to\">Date To</label> \n                <input id=\"date-to\" name=\"date-to\" type=\"date\" class=\"form-control here\">\n              </div> \n            </div>\n            <div class=\"col-md-3\">\n              <div class=\"form-group\">\n                <label for=\"holder\" class=\"vis-none\">Date To</label> \n                <input onclick=\"search()\" name=\"submit\" value=\"Search\" class=\"btn btn-primary btn-block\">\n              </div>\n            </div>\n          </div>\n        </form> -->\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-md-12 p-0 my-4\">\n        <div >\n          <mat-progress-spinner *ngIf=\"!transactions\"\n            class=\"text-center m-auto\"\n            color=\"warn\"\n            mode=\"indeterminate\" >\n          </mat-progress-spinner>\n        </div>\n        <table datatable [dtOptions]=\"dtOptions\" [dtTrigger]=\"dtTrigger\" class=\"table table-hover mt-3\">\n          <thead *ngIf=\"transactions\" >\n            <tr>\n              <th scope=\"col\">Type</th>\n              <th scope=\"col\">To/From</th>\n              <th scope=\"col\">Description</th>\n              <th scope=\"col\">Carrot</th>\n              <th scope=\"col\">Date</th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor=\"let transaction of transactions\">\n              <td>{{utility.getType(transaction.type)}}</td>\n              <td>\n                {{\n                  transaction.sender && user ? (\n                    transaction.sender.id != user.id ? \n                      transaction.sender.name : transaction.receiver.name\n                  ) : 'Mitrais'\n                }}\n              </td>\n              <!-- <td>{{transaction.sender.name}}</td> -->\n              <td>{{transaction.description ? transaction.description : '-'}}</td>\n              <td>\n                {{\n                  transaction.senderId != user.id ?\n                    transaction.amount : '-' + transaction.amount\n                }}\n                </td>\n              <td>{{utility.transformDate(transaction.dateOccured)}}</td>\n            </tr>\n          </tbody>\n        </table>\n      </div>\n    </div>\n  </div>\n</section>"

/***/ }),

/***/ "./src/app/employee/transaction/transaction.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransactionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_transaction_service__ = __webpack_require__("./src/app/service/transaction.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config_utility__ = __webpack_require__("./src/app/config/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_datatables_net__ = __webpack_require__("./node_modules/datatables.net/js/jquery.dataTables.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_datatables_net___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_datatables_net__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TransactionComponent = /** @class */ (function () {
    function TransactionComponent(transactionService, userService, router) {
        this.transactionService = transactionService;
        this.userService = userService;
        this.router = router;
        this.utility = __WEBPACK_IMPORTED_MODULE_5__config_utility__;
        this.dtOptions = {};
        // We use this trigger because fetching the list of persons can be quite long,
        // thus we ensure the data is fetched before rendering
        this.dtTrigger = new __WEBPACK_IMPORTED_MODULE_2_rxjs__["Subject"]();
    }
    TransactionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.userService.getLoggedInUser();
        if (!this.user) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_6__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        console.log(res);
                        _this.user = res;
                        _this.userService.setLoggedInUser(res);
                        if (res.role != __WEBPACK_IMPORTED_MODULE_6__config_constanta__["EMPLOYEE_ROLE"] || res.admin)
                            _this.router.navigate(['/']);
                        else
                            _this.fetchData();
                    }
                    else
                        _this.router.navigate(['/']);
                });
            }
            else
                this.router.navigate(['/']);
        }
        else {
            this.fetchData();
        }
        this.dtOptions = {};
    };
    TransactionComponent.prototype.fetchData = function () {
        var _this = this;
        this.getTransactions();
        this.userService.fetchUserPlus(this.user)
            .subscribe(function (user) {
            _this.user = user;
            _this.userService.setLoggedInUser(user);
        });
    };
    TransactionComponent.prototype.getTransactions = function () {
        var _this = this;
        // this.transactions = this.transactionService.getAll();
        // if (!this.transactions) {
        this.transactionService.fetchTransactionsByUser(this.user ? this.user.id : null)
            .subscribe(function (transactions) {
            console.log(transactions);
            // for(let transaction of transactions){
            //   transaction.dateOccured = Utility.transformDate(transaction.dateOccured, 'longDate');
            // }
            _this.transactions = transactions;
            _this.dtTrigger.next();
            // this.transactionService.setAll(transactions);
        });
        // }
    };
    // getType(type: number){
    //   return Utility.getType(type);
    // }
    // transformDate(date: string, format: string){
    //   return Utility.transformDate(date, format);
    // }
    TransactionComponent.prototype.selectTransaction = function (transaction) {
        this.selectedTransaction = transaction;
    };
    TransactionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-transaction',
            template: __webpack_require__("./src/app/employee/transaction/transaction.component.html"),
            styles: [__webpack_require__("./src/app/employee/transaction/transaction.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__service_transaction_service__["a" /* TransactionService */],
            __WEBPACK_IMPORTED_MODULE_4__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], TransactionComponent);
    return TransactionComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Begin page content -->\r\n<main role=\"main\" class=\"container\">\r\n  <h2 class=\"mt-4 pl-0 text-grey ml-0\">LOGIN</h2>\r\n</main>\r\n\r\n<section class=\"mini-dashboard mt-4 mb-4\">\r\n  <div class=\"container search-box pb-4\">\r\n    <div class=\"row d-flex\">\r\n      <div class=\"text-center col-md-12\">\r\n        <mat-progress-spinner *ngIf=\"isLoading\"\r\n          class=\"text-center m-auto\"\r\n          color=\"warn\"\r\n          mode=\"indeterminate\" >\r\n        </mat-progress-spinner>\r\n        <form autocomplete=\"off\" class=\"form-signin\" *ngIf=\"!isLoading\" >\r\n          <!-- <img class=\"mt-5 mb-4 rounded-circle\" src=\"../../assets/img/mc-icon-carrot.png\" alt=\"\" width=\"72\" height=\"72\"> -->\r\n          <img class=\"mt-5 mb-4\" src=\"../../assets/img/carrot-gift.png\" alt=\"\" width=\"72\" height=\"72\">\r\n          \r\n          <div class=\"form-group\">\r\n            <input #email id=\"email\" name=\"email\" type=\"text\" class=\"form-control here\" placeholder=\"Email address\" required=\"\" autofocus=\"\">\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <input #password id=\"password\" name=\"password\" type=\"password\" class=\"form-control here\" placeholder=\"Password\" required=\"\">\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <p style=\"color: red\" >{{errMsg}}</p>\r\n          </div>\r\n          <mat-progress-spinner *ngIf=\"isLoadingLogin\"\r\n            class=\"text-center m-auto\"\r\n            color=\"warn\"\r\n            mode=\"indeterminate\" >\r\n          </mat-progress-spinner>\r\n          <div *ngIf=\"!isLoadingLogin\" >\r\n            <button (click)=\"login(email.value, password.value)\" class=\"btn btn-block btn-primary radius-5 mt-5 mb-4\">Sign in</button>\r\n            <!-- <a routerLink=\"/manager\" class=\"btn btn-block btn-primary radius-5 mt-5 mb-4\">Sign in</a> -->\r\n            <button routerLink=\"/register\" class=\"btn btn-block btn-link radius-5\">Create account</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n\r\n<div *ngIf=\"user\">\r\n  {{user}}\r\n</div>"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_notification_service__ = __webpack_require__("./src/app/service/notification.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = /** @class */ (function () {
    function LoginComponent(userService, router, notificationService) {
        this.userService = userService;
        this.router = router;
        this.notificationService = notificationService;
        this.isLoading = true;
        this.isLoadingLogin = false;
        this.errMsg = null;
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.userService.getLoggedInUser();
        if (this.user) {
            this.move(this.user);
            this.isLoading = false;
        }
        else {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_3__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        _this.user = res;
                        _this.isLoading = false;
                        _this.move(_this.user);
                    }
                    else
                        _this.isLoading = false;
                });
            }
            else
                this.isLoading = false;
        }
    };
    LoginComponent.prototype.login = function (email, password) {
        var _this = this;
        this.isLoadingLogin = true;
        email = email.trim();
        if (!email) {
            this.isLoadingLogin = false;
            return;
        }
        this.userService.login({ email: email, password: password })
            .subscribe(function (user) {
            if (user) {
                console.log(user);
                localStorage.setItem(__WEBPACK_IMPORTED_MODULE_3__config_constanta__["TOKEN_KEY_NAME"], user.token);
                _this.isLoadingLogin = false;
                _this.move(user);
            }
            else {
                _this.errMsg = 'user not found!';
                _this.isLoadingLogin = false;
            }
        });
    };
    LoginComponent.prototype.move = function (user) {
        if (user) {
            this.userService.setLoggedInUser(user);
            // let appComponent = new AppComponent(this.notificationService, this.userService, this.router);
            // appComponent.ngOnInit();
            if (user.admin)
                this.router.navigate(['admin']);
            else {
                switch (user.role) {
                    case __WEBPACK_IMPORTED_MODULE_3__config_constanta__["MANAGER_ROLE"]:
                        this.router.navigate(['manager']);
                        break;
                    case __WEBPACK_IMPORTED_MODULE_3__config_constanta__["EMPLOYEE_ROLE"]:
                        this.router.navigate(['employee']);
                        break;
                    case __WEBPACK_IMPORTED_MODULE_3__config_constanta__["ROOT_ADMIN_ROLE"]:
                        this.router.navigate(['root-admin']);
                        break;
                    case __WEBPACK_IMPORTED_MODULE_3__config_constanta__["SENIOR_MANAGER_ROLE"]:
                        this.router.navigate(['senior-manager']);
                        break;
                    default:
                        break;
                }
            }
        }
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("./src/app/login/login.component.html"),
            styles: [__webpack_require__("./src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__service_notification_service__["a" /* NotificationService */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/manager/manager.component.css":
/***/ (function(module, exports) {

module.exports = ".actionmanage {\r\n  color: #156fca !important; \r\n}\r\n\r\n.actioncarrot {\r\n  color: #e95321  !important; \r\n}\r\n\r\n.carrotLeft{\r\n  color: #ff5722 !important;\r\n}\r\n\r\n/* managersComponent's private CSS styles */\r\n\r\n.selected {\r\n  /* background-color: #CFD8DC !important; */\r\n  color: #607D8B;\r\n}\r\n\r\n.managers {\r\n  text-transform: capitalize;\r\n  color: #444;\r\n  font-family: Arial, Helvetica, sans-serif;\r\n  font-weight: lighter;\r\n  margin: 0 0 2em 0;\r\n  list-style-type: none;\r\n  padding: .3em 0;\r\n  width: 15em;\r\n}\r\n\r\n.managers li {\r\n  cursor: pointer;\r\n  position: relative;\r\n  left: 0;\r\n  /* background-color: #EEE; */\r\n  margin: 1em;\r\n  /* padding: .3em 0; */\r\n  height: 1.6em;\r\n  border-radius: 4px;\r\n}\r\n\r\n.managers li.selected:hover {\r\n  /* background-color: #BBD8DC !important; */\r\n  color: white;\r\n}\r\n\r\n.managers li:hover {\r\n  color: #607D8B;\r\n  /* background-color: #DDD; */\r\n  left: .1em;\r\n}\r\n\r\n.managers .text {\r\n  position: relative;\r\n  top: -3px;\r\n}\r\n\r\n.managers .badge {\r\n  display: inline-block;\r\n  font-size: small;\r\n  color: white;\r\n  padding: 0.8em 0.7em 0 0.7em;\r\n  /* background-color: #607D8B; */\r\n  line-height: 1em;\r\n  position: relative;\r\n  left: -1px;\r\n  top: -4px;\r\n  height: 1.8em;\r\n  margin-right: .8em;\r\n  border-radius: 4px 0 0 4px;\r\n}\r\n\r\n.mat-form-field {\r\n  font-size: 14px;\r\n  width: 100%;\r\n}\r\n\r\n.mat-checkbox-inner-container .mat-checkbox-inner-container-no-side-margin{\r\n  background-color: #ff5722;\r\n}\r\n\r\n.mat-header-row {\r\n  top: 0;\r\n  position: -webkit-sticky;\r\n  position: sticky;\r\n  z-index: 1;\r\n  background-color: inherit;\r\n}\r\n\r\nh6.a{\r\n  white-space: nowrap; \r\n  width: 200px; \r\n  overflow: hidden;\r\n  text-overflow: ellipsis; \r\n  /* border: 1px solid #000000; */\r\n}"

/***/ }),

/***/ "./src/app/manager/manager.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"admin-tabs py-3\">\r\n    <div class=\"container\">\r\n        <div class=\"row\" *ngIf=\"loggedInManager\" style=\"margin-bottom: 20px\" >\r\n            <div class=\"col-md-6\">\r\n            <div class=\"row box-profile soft-shadow px-0 mr-0\">\r\n                <div class=\"col-md-4 my-auto circular--landscape    \">\r\n                        <img src=\"{{loggedInManager && loggedInManager.picture ? loggedInManager.picture : '../../assets/img/user.png'}}\" alt=\"\" class=\"img-fluid rounded-circle crop\">\r\n                    </div>\r\n                <div class=\"col-md-8 my-auto\">\r\n                <h4 class=\"mb-0 text-white\">{{loggedInManager ? loggedInManager.name : 'Name'}}</h4>\r\n                <p class=\"text-white\">Mitrais Manager</p>\r\n                <a routerLink=\"/profile\" class=\"badge badge-white\">Edit Profile</a>\r\n                </div>\r\n            </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n            <div class=\"row box-carrot px-0 mr-0\">\r\n                <div class=\"col-md-4 my-auto\">\r\n                <img src=\"../../assets/img/mc-icon-carrot.png\" alt=\"\" class=\"img-fluid rounded-circle\">\r\n                </div>\r\n                <div class=\"col-md-8 my-auto\">\r\n                <h4 class=\"text-white\">You have {{loggedInManager ? loggedInManager.carrot : '0'}} carrots!</h4>\r\n                </div>\r\n            </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12 px-md-0\">\r\n                <ul class=\"nav nav-pills mb-3\" id=\"myTab\" role=\"tablist\">\r\n                    <li class=\"nav-item\">\r\n                        <a class=\"nav-link active\" id=\"home-tab\" data-toggle=\"tab\" href=\"#home\" role=\"tab\" aria-controls=\"home\" aria-selected=\"true\" (click)=\"loadFirstGroup()\">Your Groups</a>\r\n                    </li>\r\n                    <li class=\"nav-item\">\r\n                        <a class=\"nav-link\" id=\"other-tab\" data-toggle=\"tab\" href=\"#otherGroups\" role=\"tab\" aria-controls=\"otherGroups\" aria-selected=\"false\" (click)=\"loadOtherGroups()\">Employee Managed By Others</a>\r\n                    </li>\r\n                    <li class=\"nav-item\">\r\n                        <a class=\"nav-link\" id=\"available-tab\" data-toggle=\"tab\" href=\"#avEmployee\" role=\"tab\" aria-controls=\"available\" aria-selected=\"false\" (click)=\"loadUnmanagedUsers()\">Available Employee</a>\r\n                    </li>\r\n                </ul>\r\n                <div class=\"tab-content search-box\" id=\"myTabContent\">\r\n                    <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">\r\n                        <div class=\"row\">\r\n                            \r\n                            <div class=\"col-md-12\"  *ngIf = \"selectedGroup\">\r\n                                <hr class=\"box-title-hr\">\r\n                                <h4 class=\"my-2 box-title\">{{selectedGroup.name}} Employee List</h4>\r\n                            </div>\r\n                            <div class=\"col-md-12\">\r\n                                <div class=\"row col-md-12\">\r\n                                    <div class=\"col-md-10\">\r\n                                        <mat-form-field class=\"ml-auto\">\r\n                                            <input matInput (keyup)=\"applyFilter($event.target.value, 1)\" placeholder=\"Click Me To Search\">\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                    <div class=\"col-md-2 form-group ml-auto\">\r\n                                        <button class=\"btn btn-secondary dropdown-toggle form-control here\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" \r\n                                            aria-haspopup=\"true\" aria-expanded=\"false\" style=\"padding:6px;background-color: #17a2b8;font-size: 0.7rem;border-left-width: 0px;width: 163px;\">\r\n                                                Select Group\r\n                                        </button>\r\n                                        <div class=\"dropdown-menu form-group\" aria-labelledby=\"dropdownMenuButton\" style=\"position: absolute;transform: translate3d(9px, 30px, 0px);top: 0px;left: 0px;will-change: transform;width: 174px;\">\r\n                                                <a class=\"dropdown-item form-control here\" style=\"padding : 6px\" href=\"#\" *ngFor=\"let group of groups\" (click)=\"onSelectGroup(group)\"\r\n                                                data-toggle=\"modal\" data-dismiss=\"modal\">{{group.name}}</a>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <mat-table [dataSource]=\"dataSource\" matSort>\r\n\r\n                                    <ng-container matColumnDef=\"Name\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> Name </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\"> {{row.name}} </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"E-mail\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> E-mail </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\"> {{row.email}} </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Join Date\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> Join Date </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\"> {{row.joinDate}} </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Role\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> Role </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\"> {{utility.getRole(row.role)}} </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Active\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> Active </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\"> {{row.active}} </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Action\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> Action </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\">\r\n                                            <a class=\"actionmanage\" data-toggle=\"modal\" data-target=\"#actionManager\" style=\"color : #868e96\" (click)=\"onSelect(row)\">Manage </a>|\r\n                                            <a class=\"actioncarrot\" data-toggle=\"modal\" data-target=\"#actionCarrot\" style=\"color : #868e96\" (click)=\"onSelect(row)\" (click)=\"loadContents()\"> Send Carrot</a>\r\n                                        </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n                                    <mat-row *matRowDef=\"let row; columns: displayedColumns;\">\r\n                                    </mat-row>\r\n                                </mat-table>\r\n                                <mat-paginator [pageSizeOptions]=\"[10, 15, 25]\"></mat-paginator>\r\n                                <div class=\"text-center mb-3\">\r\n                                    <!-- Button trigger modal -->\r\n                                    <button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#addEmployee\" (click)=\"loadAddEmpl()\">\r\n                                        <i class=\"fa fa-plus-circle\"></i> ADD EMPLOYEE\r\n                                    </button>\r\n                                    \r\n                                    <button type=\"button\" class=\"btn btn-carrot radius-5\" data-toggle=\"modal\" data-target=\"#editBirthday\" style=\"width: 50px; height: 50px\">\r\n                                        <!-- SET CARROT <br/> FOR THIS GROUP -->\r\n                                        <img src=\"../../assets/img/carr2.png\" alt=\"\" class=\"img-fluid\">\r\n                                    </button>\r\n\r\n                                    <button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#setAchievement\" (click)=\"loadContents()\">\r\n                                        SET ACHIEVEMENT <i class=\"fa fa-plus-circle\"></i>\r\n                                    </button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"tab-pane fade\" id=\"otherGroups\" role=\"tabpanel\" aria-labelledby=\"other-tab\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-12\">\r\n                                <hr class=\"box-title-hr\">\r\n                                <h4 class=\"my-2 box-title\">Employee Managed By Others</h4>\r\n                            </div>\r\n                            <div class=\"col-md-12\">\r\n                                <mat-form-field class=\"ml-auto\">\r\n                                    <input matInput (keyup)=\"applyFilter($event.target.value, 1)\" placeholder=\"Click Me To Search\">\r\n                                </mat-form-field>\r\n\r\n                                <mat-table [dataSource]=\"dataSource\" matSort>\r\n\r\n                                    <ng-container matColumnDef=\"Name\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> Name </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\"> {{row.name}} </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"E-mail\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> E-mail </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\"> {{row.email}} </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Join Date\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> Join Date </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\"> {{row.joinDate}} </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Active\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> Active </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\"> {{row.active}} </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Action\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> Action </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\">\r\n                                            <a class=\"actionmanage\" data-toggle=\"modal\" data-target=\"#actionView\" style=\"color : #868e96\" (click)=\"onSelect(row)\">View</a>\r\n                                        </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <mat-header-row *matHeaderRowDef=\"displayedColumnsOther\"></mat-header-row>\r\n                                    <mat-row *matRowDef=\"let row; columns: displayedColumnsOther;\">\r\n                                    </mat-row>\r\n                                </mat-table>\r\n                                <mat-paginator [pageSizeOptions]=\"[10, 15, 25]\"></mat-paginator>\r\n\r\n                                <!-- <table class=\"table table-hover mt-3\" datatable [dtOptions]=\"dtOptions\" class=\"table table-hover mt-3\">\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th rowspan=\"1\">Name</th>\r\n                                            <th colspan=\"1\">E-mail</th>\r\n                                            <th rowspan=\"1\">Join Date</th>\r\n                                            <th rowspan=\"1\">Role</th>\r\n                                            <th rowspan=\"1\">Active</th>\r\n                                            <th rowspan=\"1\">Group</th>\r\n                                            <th rowspan=\"1\">Managed By</th>\r\n                                        </tr>\r\n                                    </thead>\r\n                                    <tbody *ngFor=\"let groupe of otherGroups\">\r\n                                        <tr *ngFor=\"let user of groupe.members\">\r\n                                            <td>{{user.name}}</td>\r\n                                            <td>{{user.email}}</td>\r\n                                            <td>{{user.joinDate}}</td>\r\n                                            <td>{{utility.getRole(user.role)}}</td>\r\n                                            <td>{{user.active}}</td>\r\n                                            <td>{{groupe.name}}</td>\r\n                                            <td>{{groupe.manager.name}}</td>\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table> -->\r\n                            </div>\r\n                        </div>\r\n                    </div>      \r\n                    <div class=\"tab-pane fade\" id=\"avEmployee\" role=\"tabpanel\" aria-labelledby=\"available-tab\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-12\">\r\n                                <hr class=\"box-title-hr\">\r\n                                <h4 class=\"my-2 box-title\">Available Employee LIST</h4>\r\n                            </div>\r\n                            <div class=\"col-md-12\">\r\n                                <mat-form-field class=\"ml-auto\">\r\n                                    <input matInput (keyup)=\"applyFilter($event.target.value, 1)\" placeholder=\"Click Me To Search\">\r\n                                </mat-form-field>\r\n\r\n                                <mat-table [dataSource]=\"dataSource\" matSort>\r\n\r\n                                    <!-- <mat-input-container>\r\n                                        <input matInput placeholder=\"Search lessons\" #input>\r\n                                    </mat-input-container> -->\r\n\r\n                                    <ng-container matColumnDef=\"Name\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> Name </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\"> {{row.name}} </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"E-mail\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> E-mail </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\"> {{row.email}} </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Join Date\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> Join Date </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\"> {{row.joinDate}} </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Role\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> Role </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\"> {{utility.getRole(row.role)}} </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"Active\">\r\n                                        <mat-header-cell *matHeaderCellDef mat-sort-header> Active </mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row\"> {{row.active}} </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <mat-header-row *matHeaderRowDef=\"displayedColumnsUnGroup\"></mat-header-row>\r\n                                    <mat-row *matRowDef=\"let row; columns: displayedColumnsUnGroup;\">\r\n                                    </mat-row>\r\n                                </mat-table>\r\n                                <mat-paginator [pageSizeOptions]=\"[10, 15, 25]\"></mat-paginator>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n    <!-- Action Manager Modal -->\r\n<div class=\"modal fade\" id=\"actionManager\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"actionManager\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Manage Employee</h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div *ngIf =\"selectedUser\" class=\"modal-body\">\r\n        <form>\r\n            <div class=\"col-md-12\">\r\n                <div class=\"row box-profile soft-shadow px-0 mr-0\">\r\n                <div class=\"col-md-4 my-auto radius-5 circular--landscape\">\r\n                    <img src=\"{{selectedUser && selectedUser.picture ? selectedUser.picture : '../../assets/img/user.png'}}\" alt=\"\" class=\"img-fluid rounded-circle crop\">\r\n                </div>\r\n                <div class=\"col-md-8 my-auto\">\r\n                    <h4 class=\"mb-0 text-white\">{{selectedUser ? selectedUser.name : 'Name'}}</h4>\r\n                    <h5 class=\"text-white badge badge-white\">Member of {{selectedGroup.name}}</h5>\r\n                </div>\r\n                </div>\r\n            </div>\r\n            <br>\r\n            <div class=\"form-group col-md-12\" *ngIf=\"groups.length > 1\">\r\n                <button class=\"btn btn-secondary dropdown-toggle form-control here\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\"\r\n                    aria-haspopup=\"true\" aria-expanded=\"false\" style=\" background-color: #119bd2; font-size: 0.7rem\">\r\n                        Move Group\r\n                </button>\r\n                <div class=\"dropdown-menu form-group\" aria-labelledby=\"dropdownMenuButton\">\r\n                        <a class=\"dropdown-item form-control here\" style=\"padding : 6px\" href=\"#\" *ngFor=\"let foot of groupsAftFiltered\" (click)=\"onSelectNewGroup(foot)\"\r\n                        data-toggle=\"modal\" data-target=\"#moveEmployee\" data-dismiss=\"modal\">{{foot.name}}</a>\r\n                </div>\r\n            </div>\r\n            <div class=\"text-center form-group col-md-12\">\r\n                <a class=\"text-center\" id=\"removeButton\" data-toggle=\"modal\" data-dismiss=\"modal\"\r\n                    aria-haspopup=\"true\" aria-expanded=\"false\" style=\"padding:6px; color: #ff2222; font-size: 0.8rem; width: 100%\" data-target=\"#removeEmployee\" data-target=\"#removeEmployee\">\r\n                        Remove from Group\r\n                </a>\r\n            </div>\r\n        </form>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-link \" data-dismiss=\"modal\">Cancel</button>\r\n        <!-- <button type=\"button\" class=\"btn btn-carrot radius-5\">Submit</button> -->\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n    <!-- Action View Modal -->\r\n<div class=\"modal fade\" id=\"actionView\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"actionView\" aria-hidden=\"true\">\r\n        <div class=\"modal-dialog\" role=\"document\">\r\n          <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n              <h5 class=\"modal-title\" id=\"exampleModalLabel\">Manage Employee</h5>\r\n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                <span aria-hidden=\"true\">&times;</span>\r\n              </button>\r\n            </div>\r\n            <div *ngIf =\"selectedUser\" class=\"modal-body\">\r\n              <form>\r\n                  <div class=\"col-md-12\">\r\n                      <div class=\"row box-profile soft-shadow px-0 mr-0\">\r\n                      <div class=\"col-md-4 my-auto radius-5 circular--landscape\">\r\n                          <img src=\"{{selectedUser && selectedUser.picture ? selectedUser.picture : '../../assets/img/user.png'}}\" alt=\"\" class=\"img-fluid rounded-circle crop\">\r\n                      </div>\r\n                      <div class=\"col-md-8 my-auto\">\r\n                            <h3 class=\"mb-0 text-white\">{{selectedUser ? selectedUser.name : 'Name'}}</h3>\r\n                            <h5 class=\"text-white\">Member of {{selectedGroup.name}}</h5>\r\n                            <h6 class=\"text-white\">Managed by : {{selectedGroup.manager.name}}</h6>\r\n                          <!-- <h5 class=\"text-white\">Managed by : {{selectedGroup.manager.name}}</h5> -->\r\n                      </div>\r\n                      </div>\r\n                  </div>\r\n                  <br>\r\n              </form>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n              <button type=\"button\" class=\"btn btn-link \" data-dismiss=\"modal\">Cancel</button>\r\n              <!-- <button type=\"button\" class=\"btn btn-carrot radius-5\">Submit</button> -->\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n\r\n    <!-- Action Carrot Modal -->\r\n<div class=\"modal fade\" id=\"actionCarrot\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"actionSeniorCarrot\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n            <h5 class=\"modal-title\" id=\"exampleModalLabel\">Send Carrot</h5>\r\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n            </button>\r\n            </div>\r\n            <div *ngIf =\"selectedUser\" class=\"modal-body\">\r\n                <form>\r\n                    <div class=\"form-group my-auto\">\r\n                        <label for=\"text1\">Recipient</label> \r\n                        <input id=\"text1\" name=\"text1\" type=\"text\" class=\"form-control here\" [(ngModel)]=\"selectedUser.name\" placeholder=\"name\" disabled>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"text2\">E-mail</label> \r\n                        <input id=\"text2\" name=\"text2\" type=\"text\" class=\"form-control here\" [(ngModel)]=\"selectedUser.email\" placeholder=\"email\" disabled>\r\n                    </div>\r\n                    <!-- <div class=\"form-group\">\r\n                        <label for=\"text3\">Description</label>\r\n                        <textarea #description id=\"description\" name=\"textarea\" cols=\"40\" rows=\"5\" class=\"form-control\"></textarea>\r\n                    </div> -->\r\n                    <div class=\"form-group\">\r\n                        <button class=\"btn btn-secondary dropdown-toggle form-control here\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" \r\n                            aria-haspopup=\"true\" aria-expanded=\"false\" style=\"padding:6px;background-color: #119bd2;font-size: 0.7rem;\">\r\n                                Select Achievement\r\n                        </button>\r\n                        <div class=\"dropdown-menu form-group\" aria-labelledby=\"dropdownMenuButton\" style=\"position: absolute;transform: translate3d(9px, 30px, 0px);top: 0px;left: 0px;will-change: transform;width: 174px;\">\r\n                                <a class=\"dropdown-item form-control here\" style=\"padding : 6px\" href=\"#\" *ngFor=\"let group of contentsDisp\" (click)=\"calCarrot(group)\"\r\n                                data-toggle=\"modal\">{{group.name}}</a>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"form-group\" *ngIf=\"selectedContent\">\r\n                        <label for=\"text4\">Achievement</label> \r\n                        <input id=\"carrotVal\" #conName [(ngModel)]=\"selectedContent.name\" name=\"textz\" type=\"text\" class=\"form-control here\" placeholder=\"0\" value=0 disabled>\r\n\r\n                        <label for=\"text4\">Send Carrot</label> \r\n                        <input id=\"carrotVal\" #carrotVal [(ngModel)]=\"selectedContent.carrot\" name=\"text4\" type=\"text\" class=\"form-control here\" placeholder=\"0\" value=0 disabled>\r\n                        <!-- <input id=\"carrotValue\" #carrotValue (change)=\"calCarrot(carrotValue.value)\" name=\"text4\" type=\"number\" class=\"form-control here\" placeholder=\"0\" value=0 disabled> -->\r\n                        <label id=\"carrotLeft\" for=\"text\" *ngIf=\"inputedCarrot\">You Have : {{loggedInManager.carrot - carrotVal.value}} Carrot(s) Left</label>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-link \" data-dismiss=\"modal\" (click)=\"re()\">Cancel</button>\r\n                <button type=\"button\" class=\"btn btn-carrot radius-5\" (click)=\"sendCarrot()\" data-toggle=\"modal\" data-target=\"#confirmCarrot\" data-dismiss=\"modal\">Submit</button>\r\n            </div>\r\n        </div>\r\n    </div >\r\n</div>\r\n\r\n    <!-- Action Confirmation Carrot Modal -->\r\n<div class=\"modal fade\" id=\"confirmCarrot\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"actionCarrot\" aria-hidden=\"true\" *ngIf=\"selectedManager\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n            <h5 class=\"modal-title\" id=\"exampleModalLabel\">Send Carrot</h5>\r\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n            </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <form>\r\n                    <div class=\"form-group my-auto\">\r\n                        <label for=\"text1\">You already sent {{ carrotvalue }} carrots to {{ selectedUser.name }}</label>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-carrot radius-5\" data-dismiss=\"modal\">OK</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n    <!-- Action Add Employee Modal -->\r\n<div class=\"modal fade\" id=\"addEmployee\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"addEmployee\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\" style=\"margin: auto; width : 750px\">\r\n        <div class=\"modal-header\">\r\n            <h5 class=\"modal-title\" id=\"exampleModalLabel\">Add Employee</h5>\r\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n            </button>\r\n        </div>\r\n        <!-- <div class=\"row col-md-12\"> -->\r\n            <mat-form-field class=\"ml-auto\">\r\n                <input matInput (keyup)=\"applyFilter($event.target.value, 2)\" placeholder=\"Click Me To Search\">\r\n            </mat-form-field>\r\n            <div style=\"overflow:scroll; height:400px;\">\r\n\r\n            <mat-table [dataSource]=\"dataSourceAddEmp\" matSort>\r\n\r\n                <ng-container matColumnDef=\"select\">\r\n                    <th mat-header-cell *matHeaderCellDef>\r\n                        <!-- <mat-checkbox (change)=\"$event ? masterToggle() : null\"\r\n                                    [checked]=\"selection.hasValue() && isAllSelected()\"\r\n                                    [indeterminate]=\"selection.hasValue() && !isAllSelected()\">\r\n                        </mat-checkbox> -->\r\n                    </th>\r\n                    <td mat-cell *matCellDef=\"let row\">\r\n                        <mat-checkbox (click)=\"$event.stopPropagation()\"\r\n                                    (change)=\"$event ? selection.toggle(row) : null\"\r\n                                    [checked]=\"selection.isSelected(row)\">\r\n                        </mat-checkbox>\r\n                    </td>\r\n                </ng-container>\r\n                <!-- <mat-input-container>\r\n                    <input matInput placeholder=\"Search lessons\" #input>\r\n                </mat-input-container> -->\r\n\r\n                <ng-container matColumnDef=\"Name\">\r\n                    <mat-header-cell *matHeaderCellDef mat-sort-header> Name </mat-header-cell>\r\n                    <mat-cell *matCellDef=\"let row\"> {{row.name}} </mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"E-mail\">\r\n                    <mat-header-cell *matHeaderCellDef mat-sort-header> E-mail </mat-header-cell>\r\n                    <mat-cell *matCellDef=\"let row\"> {{row.email}} </mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"Join Date\">\r\n                    <mat-header-cell *matHeaderCellDef mat-sort-header> Join Date </mat-header-cell>\r\n                    <mat-cell *matCellDef=\"let row\"> {{row.joinDate}} </mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"Role\">\r\n                    <mat-header-cell *matHeaderCellDef mat-sort-header> Role </mat-header-cell>\r\n                    <mat-cell *matCellDef=\"let row\"> {{utility.getRole(row.role)}} </mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"Active\">\r\n                    <mat-header-cell *matHeaderCellDef mat-sort-header> Active </mat-header-cell>\r\n                    <mat-cell *matCellDef=\"let row\"> {{row.active}} </mat-cell>\r\n                </ng-container>\r\n\r\n                <mat-header-row *matHeaderRowDef=\"displayedColumnsAddEmp\"></mat-header-row>\r\n                <mat-row *matRowDef=\"let row; columns: displayedColumnsAddEmp;\">\r\n                </mat-row>\r\n            </mat-table>\r\n            <!-- <mat-paginator [pageSizeOptions]=\"[10, 15, 25]\"></mat-paginator> -->\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n            <button type=\"button\" class=\"btn btn-link \" data-dismiss=\"modal\">Cancel</button>\r\n            <button type=\"button\" class=\"btn btn-carrot radius-5\" data-dismiss=\"modal\" (click)=\"onSubmit()\">Submit</button>\r\n        </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n    <!-- Action Add Achievement Modal -->\r\n<div class=\"modal fade\" id=\"setAchievement\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"setAchievement\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\" style=\"margin: auto; width : 750px\">\r\n        <div class=\"modal-header\">\r\n            <h5 class=\"modal-title\" id=\"exampleModalLabel\">Set Achievement</h5>\r\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                <span aria-hidden=\"true\">&times;</span>\r\n            </button>\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n                <hr class=\"box-title-hr\" style=\"margin-top: 0px\">\r\n                <h4 class=\"my-2 box-title\" style=\"color: mediumseagreen; text-transform: none; margin-bottom: 0px;margin-top: 0px;\">How to use it?</h4>\r\n                <p style=\"color: #868e96; font-size: 13px\">Click options below the UNAVAILABLE ACHIEVEMENT to make it move to the AVAILABLE ACHIEVEMENT,\r\n                    it will make system available to send carrots to your group members based on options you choose.\r\n                    If you want to remove the existing available achievements that have been set on your groups,\r\n                    you can do it by clicking the achievement options below the AVAILABLE ACHIEVEMENT.\r\n                    Don't worry, the system will do it for you automatically. Enjoy :)\r\n                </p>\r\n            </div>\r\n        <div class=\"row col-md-12\">\r\n            <div class=\"col-md-6\">\r\n                <table class=\"table table-hover mt-3\">\r\n                    <thead>\r\n                        <tr>\r\n                            <th rowspan=\"1\" class=\"my-2 box-title\" style=\"text-align: center\">Available <br> Achievement</th>\r\n                        </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                        <div *ngFor=\"let content of contentsDisp\"\r\n                            (click)=\"onSelectContent(content)\" class=\"col-md-12\"\r\n                            style=\"height: 50px; padding: 10px\">\r\n                                <div class=\"row p-0 m-2\" title=\"{{content.name}}\">\r\n                                    <div>\r\n                                        <img src=\"{{user && user.picture ? user.picture : '../../assets/img/carr-icon.png'}}\" alt=\"\" class=\"img-fluid rounded-circle\"\r\n                                        style=\"width: 50px;height: 50px;padding-bottom: 5px;padding-right: 5px;padding-left: 5px;padding-top: 5px;margin-bottom: 0px;\">\r\n                                    </div>\r\n                                    <div>\r\n                                        <a style=\"color: dimgray !important\"><h6 class=\"a\" style=\"margin-bottom: 0px;margin-top: 5px;\">{{content.name}}</h6></a>\r\n                                        <a style=\"color: dimgray !important\">{{content.carrot}} carrots</a>\r\n                                    </div>\r\n                                </div>\r\n                        </div>\r\n                    </tbody>\r\n                </table>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n                <table class=\"table table-hover mt-3\">\r\n                    <thead>\r\n                        <tr>\r\n                            <th rowspan=\"1\" class=\"my-2 box-title\" style=\"text-align: center\">Unavailable Achievement</th>\r\n                        </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                        <div *ngFor=\"let content of unableContentsDisp\"\r\n                            (click)=\"onRemoveContent(content)\" class=\"col-md-12\"\r\n                            style=\"height: 50px; padding: 10px\">\r\n                                <div class=\"row p-0 m-2\" title=\"{{content.name}}\">\r\n                                    <div>\r\n                                        <img src=\"{{user && user.picture ? user.picture : '../../assets/img/carr-icon.png'}}\" alt=\"\" class=\"img-fluid rounded-circle\"\r\n                                        style=\"width: 50px;height: 50px;padding-bottom: 5px;padding-right: 5px;padding-left: 5px;padding-top: 5px;margin-bottom: 0px;\">\r\n                                    </div>\r\n                                    <div>\r\n                                        <a style=\"color: dimgray !important\"><h6 class=\"a\" style=\"margin-bottom: 0px;margin-top: 5px;\">{{content.name}}</h6></a>\r\n                                        <a style=\"color: dimgray !important\">{{content.carrot}} carrots</a>\r\n                                    </div>\r\n                                </div>\r\n                        </div>\r\n                    </tbody>\r\n                </table>\r\n            </div>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n            <button type=\"button\" class=\"btn btn-link \" data-dismiss=\"modal\">Cancel</button>\r\n            <button type=\"button\" class=\"btn btn-carrot radius-5\" data-dismiss=\"modal\" (click)=\"onSubmitContent()\">Submit</button>\r\n        </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n    <!-- Move Employee Confirmation Modal -->\r\n<div class=\"modal fade\" id=\"moveEmployee\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"removeEmployee\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n      <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n          <h5 class=\"modal-title\" id=\"exampleModalLabel\">Move Employee</h5>\r\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n          </button>\r\n        </div>\r\n        <div *ngIf =\"selectedUser\" class=\"modal-body\">\r\n          <form>\r\n            <div class=\"form-group my-auto\">\r\n              <label for=\"text1\" *ngIf=\"selectedNewGroup\">Do you really want to move {{ selectedUser.name }} from {{ selectedGroup.name }} to {{ selectedNewGroup.name }} ?</label>\r\n            </div>\r\n          </form>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n          <button type=\"button\" class=\"btn btn-link \" data-dismiss=\"modal\" (click) = \"selectNewGroup(selectedUser.id, selectedGroup, selectedNewGroup)\">Yes</button>\r\n          <button type=\"button\" class=\"btn btn-carrot radius-5\" data-dismiss=\"modal\" data-toggle=\"modal\" data-target=\"#actionManageEmp\">No</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n</div>\r\n\r\n    <!-- Remove Employee Confirmation Modal -->\r\n<div class=\"modal fade\" id=\"removeEmployee\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"removeEmployee\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n      <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n          <h5 class=\"modal-title\" id=\"exampleModalLabel\">Remove Employee</h5>\r\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n          </button>\r\n        </div>\r\n        <div *ngIf =\"selectedUser\" class=\"modal-body\">\r\n          <form>\r\n            <div class=\"form-group my-auto\">\r\n              <label for=\"text1\">Do you really want to remove {{ selectedUser.name }} from {{ selectedGroup.name }} ?</label>\r\n            </div>\r\n          </form>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n          <button type=\"button\" class=\"btn btn-link \" data-dismiss=\"modal\" (click)=\"onRemoveEmployee()\" href=\"#home\" role=\"tab\">Yes</button>\r\n          <button type=\"button\" class=\"btn btn-carrot radius-5\" data-dismiss=\"modal\" data-toggle=\"modal\" data-target=\"#actionManageEmp\">No</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n</div>\r\n\r\n    <!-- Action Auto Set Carrot Modal -->\r\n<div class=\"modal fade\" id=\"editBirthday\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Edit Birthday Configuration</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\" *ngIf = \"selectedGroup\">\r\n                <form>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"text1\">Auto-send carrot on {{selectedGroup.name}}'s staff birthday</label>\r\n                        <input #autoStatus type=\"checkbox\" [checked]=\"selectedGroup.autoCarrotManager\" (change)=\"selectedGroup.autoCarrotManager = !selectedGroup.autoCarrotManager\">\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"textz\">Amount of carrot sent</label><br>\r\n                        <input #carrotAmount [(ngModel)]=\"selectedGroup.birthdayCarrotManager\" [attr.disabled]=\"selectedGroup.autoCarrotManager ? null : selectedGroup.birthdayCarrotManager\" id=\"textz\" name=\"textz\" type=\"text\" class=\"form-control here\">\r\n                    </div>\r\n                </form>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\">Cancel</button>\r\n                <button type=\"button\" class=\"btn btn-carrot radius-5\" (click)=\"saveBirthdayStatus()\" data-dismiss=\"modal\">SAVE CONFIG</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/manager/manager.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManagerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config_utility__ = __webpack_require__("./src/app/config/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_cdk_collections__ = __webpack_require__("./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__model_user__ = __webpack_require__("./src/app/model/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__model_group__ = __webpack_require__("./src/app/model/group.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__service_group_service__ = __webpack_require__("./src/app/service/group.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__service_transaction_service__ = __webpack_require__("./src/app/service/transaction.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__service_content_service__ = __webpack_require__("./src/app/service/content.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















// import { exists } from 'fs';
var ManagerComponent = /** @class */ (function () {
    function ManagerComponent(route, userService, groupService, transactionService, contentService, appComponent, location, changeDetectorRefs, router) {
        this.route = route;
        this.userService = userService;
        this.groupService = groupService;
        this.transactionService = transactionService;
        this.contentService = contentService;
        this.appComponent = appComponent;
        this.location = location;
        this.changeDetectorRefs = changeDetectorRefs;
        this.router = router;
        this.displayedColumns = ['Name', 'E-mail', 'Join Date', 'Role', 'Active', 'Action'];
        this.displayedColumnsUnGroup = ['Name', 'E-mail', 'Join Date', 'Role', 'Active'];
        this.displayedColumnsAddEmp = ['select', 'Name', 'Join Date', 'Role', 'Active'];
        this.displayedColumnsOther = ['Name', 'E-mail', 'Join Date', 'Active', 'Action'];
        this.selection = new __WEBPACK_IMPORTED_MODULE_8__angular_cdk_collections__["b" /* SelectionModel */](true, []);
        this.dtOptions = {};
        this.dtTrigger = new __WEBPACK_IMPORTED_MODULE_4_rxjs__["Subject"]();
        this.parentComponent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.groupTemp = new __WEBPACK_IMPORTED_MODULE_10__model_group__["a" /* Group */]();
        this.groupDisp = new Array();
        this.groupsAftFiltered = new Array();
        this.allGroups = new Array();
        this.otherGroups = new Array();
        this.utility = __WEBPACK_IMPORTED_MODULE_6__config_utility__;
        this.contentsDisp = new Array();
        this.unableContentsDisp = new Array();
        this.inputedCarrot = false;
        this.page = 1;
        this.amountValue = 1;
    }
    ManagerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.groupDisp = new Array();
        this.selectedUGUser = new __WEBPACK_IMPORTED_MODULE_9__model_user__["a" /* User */];
        // this.newUsers = new Array<User>();
        this.loggedInManager = this.userService.getLoggedInUser();
        if (!this.loggedInManager)
            this.router.navigate(['/']);
        this.userService.fetchUsers()
            .subscribe(function (users) {
            _this.users = users;
            _this.userLength = users.length;
        });
        this.refresh();
        this.contentService.fetchContentsByType(1)
            .subscribe(function (conts) {
            _this.contents = conts;
            _this.contentLength = _this.contents.length;
            // this.loadContents();
        });
        this.dtOptions = {};
        // this.loadContents();
    };
    ManagerComponent.prototype.applyFilter = function (filterValue, div) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        if (div == 1) {
            this.dataSource.filter = filterValue;
        }
        else if (div == 2) {
            this.dataSourceAddEmp.filter = filterValue;
        }
    };
    ManagerComponent.prototype.refresh = function () {
        var _this = this;
        this.groupService.getAllGroups()
            .subscribe(function (groups) {
            _this.allGroups = groups;
            if (!_this.changeDetectorRefs['destroyed']) {
                _this.changeDetectorRefs.detectChanges();
            }
        });
        this.groupService.getGroupByManID(this.loggedInManager.id)
            .subscribe(function (groups) {
            console.log(groups);
            _this.groups = groups;
            // console.log("my groups", this.groups)
            // if (!this.changeDetectorRefs['destroyed']) {
            _this.changeDetectorRefs.detectChanges();
            // }
            _this.loadFirstGroup();
        });
        this.userService.fetchUGUsers()
            .subscribe(function (users) {
            _this.unGroupUsers = users;
            if (!_this.changeDetectorRefs['destroyed']) {
                _this.changeDetectorRefs.detectChanges();
            }
        });
    };
    ManagerComponent.prototype.createMatTable = function (groups) {
        if (groups.managerId) {
            this.dataSource = new __WEBPACK_IMPORTED_MODULE_7__angular_material__["h" /* MatTableDataSource */](groups.members);
        }
        else {
            this.dataSource = new __WEBPACK_IMPORTED_MODULE_7__angular_material__["h" /* MatTableDataSource */](groups);
        }
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sortAddEmp;
    };
    ManagerComponent.prototype.loadFirstGroup = function () {
        this.selection.clear();
        this.onSelectGroup(this.groups[0]);
    };
    ManagerComponent.prototype.loadUnmanagedUsers = function () {
        this.createMatTable(this.unGroupUsers);
    };
    ManagerComponent.prototype.loadOtherGroups = function () {
        var _this = this;
        var members = new Array();
        this.otherGroups = new Array();
        this.allGroups.forEach(function (gr) {
            if (gr.managerId != _this.loggedInManager.id) {
                _this.otherGroups.push(gr);
                // console.log("og", this.otherGroups);
            }
        });
        // this.otherGroups.forEach(g => {
        //   this.createMatTable(g.members);
        // })
        for (var i = 0; i < this.otherGroups.length; i++) {
            for (var j = 0; j < this.otherGroups[i].members.length; j++) {
                members.push(this.otherGroups[i].members[j]);
            }
        }
        this.createMatTable(members);
        // this.createMatTable(this.otherGroups);
        // console.log(members);
    };
    ManagerComponent.prototype.onSelect = function (user) {
        var _this = this;
        console.log(user);
        this.selectedUser = user;
        this.groups.forEach(function (g) {
            if (g.id != _this.selectedGroup.id) {
                _this.groupsAftFiltered.push(g);
            }
        });
        this.userService.fetchUserPlus(user).subscribe(function (us) {
            _this.selectedUser = us;
            _this.selectedGroup = us.group;
        });
    };
    ManagerComponent.prototype.onSelectGroup = function (group) {
        console.log("group1 ", group);
        this.selectedGroup = group;
        console.log("group2 ", this.selectedGroup);
        this.createMatTable(this.selectedGroup.members);
    };
    ManagerComponent.prototype.onSelectNewGroup = function (group) {
        this.selectedNewGroup = group;
    };
    ManagerComponent.prototype.selectNewGroup = function (idEmployee, oldGroup, newGroup) {
        newGroup.membersId.push(idEmployee);
        this.groupService.updateGroup(newGroup).subscribe();
        this.onRemoveEmployee();
        // this.selectedNewManagerGroup = newManagerGroup;
    };
    ManagerComponent.prototype.onRemoveEmployee = function () {
        var _this = this;
        var membersId = [];
        // let members : User[] = new Array<User>();
        this.selectedGroup.membersId.forEach(function (mem) {
            if (mem != _this.selectedUser.id) {
                membersId.push(mem);
            }
        });
        this.selectedGroup.membersId = membersId;
        this.selectedGroup.manager = null;
        this.groupService.updateGroup(this.selectedGroup).subscribe(function (sg) {
            if (sg) {
                _this.appComponent.showSuccess(_this.selectedUser.name + " is now removed from " + sg.name);
                _this.selectedGroup = sg;
                _this.refresh();
            }
            else {
                _this.appComponent.showSuccess("Failed to remove" + _this.selectedUser.name);
            }
        });
        // this.unGroupUsers.push(this.selectedUser);
        console.log("removed");
    };
    ManagerComponent.prototype.calCarrot = function (con) {
        // this.selectedContent = con;
        this.carrotvalue = con.carrot;
        // console.log(con.carrot);
        // console.log(this.selectedContent);
        var min = this.loggedInManager.carrot - con.carrot;
        if (min < 0) {
            this.appComponent.showError("We cannot process your activity now. You need more carrots.");
            // this.selectedContent = null;
        }
        else {
            this.selectedContent = con;
            if (min <= 500) {
                this.appComponent.showWarning("You have " + min + " carrots left");
            }
        }
    };
    ManagerComponent.prototype.re = function () {
        this.selectedContent = null;
    };
    // selectContent( content : Content){
    //   this.selectedContent = content;
    // }
    // sendCarrot(description : string){
    ManagerComponent.prototype.sendCarrot = function () {
        var _this = this;
        if (!this.loggedInManager) {
            this.loggedInManager = this.userService.getLoggedInUser();
        }
        var senderId = this.loggedInManager.id;
        var receiverId = this.selectedUser.id;
        var type = __WEBPACK_IMPORTED_MODULE_3__config_constanta__["ACHIEVEMENT_TYPE"];
        var contentId = this.selectedContent.id;
        var amount = this.carrotvalue;
        var description = this.selectedContent.description;
        if (amount > 0) {
            this.transactionService.postTransaction({ type: type, senderId: senderId, receiverId: receiverId, amount: amount, description: description, contentId: contentId })
                .subscribe(function (res) {
                console.log(res);
                if (res) {
                    _this.loggedInManager.carrot -= amount;
                    _this.selectedUser.carrot += amount;
                    _this.appComponent.showSuccess('Send Success!');
                }
                else {
                    _this.appComponent.showError('Send Failed!');
                }
            });
        }
        else {
            this.appComponent.showError('Send Failed!');
        }
    };
    // ngAfterViewInit(){
    //   this.dtTrigger.next();
    // }
    ManagerComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.selection.selected.length < 1) {
            this.appComponent.showError("Please Choose At Least One Employee");
        }
        else {
            for (var i = 0; i < this.selection.selected.length; i++) {
                this.selectedGroup.membersId.push(this.selection.selected[i].id);
            }
            this.selectedGroup.manager = null;
            this.groupService.updateGroup(this.selectedGroup).subscribe(function (sg) {
                if (sg) {
                    for (var i = 0; i < _this.selection.selected.length; i++) {
                        _this.appComponent.showSuccess(_this.selection.selected[i].name + " added to " + _this.selectedGroup.name + " successfully");
                    }
                    _this.selectedGroup = sg;
                    // this.selection = null;
                    _this.refresh();
                }
                else {
                    _this.appComponent.showError("Adding Employee(s) unsuccess");
                }
            });
        }
    };
    // ----------------------------------------------------------------------S
    ManagerComponent.prototype.saveBirthdayStatus = function () {
        // this.selectedGroup.birthdayCarrotManager = amount;
        console.log("auto Carrot set : ", this.selectedGroup.autoCarrotManager);
        console.log("mount of Carrot set : ", this.selectedGroup.birthdayCarrotManager);
        this.groupService.updateGroup(this.selectedGroup).subscribe();
        alert("Birthday Carrot Updated");
    };
    ManagerComponent.prototype.loadContents = function () {
        var _this = this;
        var exist = false;
        this.contentsDisp = new Array();
        this.unableContentsDisp = new Array();
        this.contents.forEach(function (c) {
            if (c.deleted == false) {
                exist = false;
                c.groupsId.forEach(function (g) {
                    if (g == _this.selectedGroup.id) {
                        exist = true;
                    }
                });
                if (exist == false) {
                    _this.unableContentsDisp.push(c);
                }
                else if (exist == true) {
                    _this.contentsDisp.push(c);
                }
            }
        });
    };
    ManagerComponent.prototype.onSelectContent = function (cont) {
        this.selectedContent = cont;
        this.contentsDisp = this.contentsDisp.filter(function (cG) {
            return cG.id != cont.id;
        });
        this.unableContentsDisp.push(cont);
    };
    ManagerComponent.prototype.onRemoveContent = function (cont) {
        this.selectedContent = cont;
        this.unableContentsDisp = this.unableContentsDisp.filter(function (cG) {
            return cG.id != cont.id;
        });
        this.contentsDisp.push(cont);
    };
    ManagerComponent.prototype.onSubmitContent = function () {
        var groupsId = [];
        for (var i = 0; i < this.unableContentsDisp.length; i++) {
            for (var j = 0; j < this.unableContentsDisp[i].groupsId.length; j++) {
                if (this.unableContentsDisp[i].groupsId[j] == this.selectedGroup.id) {
                    groupsId.pop();
                }
            }
            this.unableContentsDisp[i].groupsId = groupsId;
            this.contentService.postContent(this.unableContentsDisp[i]).subscribe();
        }
        groupsId = [];
        for (var i = 0; i < this.contentsDisp.length; i++) {
            for (var j = 0; j < this.contentsDisp[i].groupsId.length; j++) {
                if (this.contentsDisp[i].groupsId[j] == this.selectedGroup.id) {
                    groupsId.push(this.selectedGroup.id);
                }
            }
            this.contentsDisp[i].groupsId = groupsId;
            console.log(this.contentsDisp[i].name);
            this.contentService.postContent(this.contentsDisp[i]).subscribe();
        }
    };
    // loadUnGroupUsers(){
    //   this.selectedUGUser = new User;
    //   this.unGroupUsers = new Array<User>();
    //   // this.newUsers = new Array<User>();
    // }
    ManagerComponent.prototype.changeAmount = function (amount) {
        this.amountValue = amount;
        if (this.amountValue > this.loggedInManager.carrot) {
            this.amountValue = this.loggedInManager.carrot;
        }
    };
    ManagerComponent.prototype.isAllSelected = function () {
        var numSelected = this.selection.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    ManagerComponent.prototype.masterToggle = function () {
        var _this = this;
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(function (row) { return _this.selection.select(row); });
    };
    ManagerComponent.prototype.loadAddEmpl = function () {
        this.dataSourceAddEmp = new __WEBPACK_IMPORTED_MODULE_7__angular_material__["h" /* MatTableDataSource */](this.unGroupUsers);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_7__angular_material__["c" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_7__angular_material__["c" /* MatPaginator */])
    ], ManagerComponent.prototype, "paginator", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_7__angular_material__["f" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_7__angular_material__["f" /* MatSort */])
    ], ManagerComponent.prototype, "sort", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_7__angular_material__["f" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_7__angular_material__["f" /* MatSort */])
    ], ManagerComponent.prototype, "sortAddEmp", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], ManagerComponent.prototype, "parentComponent", void 0);
    ManagerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-manager',
            template: __webpack_require__("./src/app/manager/manager.component.html"),
            styles: [__webpack_require__("./src/app/manager/manager.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_11__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_12__service_group_service__["a" /* GroupService */],
            __WEBPACK_IMPORTED_MODULE_13__service_transaction_service__["a" /* TransactionService */],
            __WEBPACK_IMPORTED_MODULE_14__service_content_service__["a" /* ContentService */],
            __WEBPACK_IMPORTED_MODULE_5__app_app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], ManagerComponent);
    return ManagerComponent;
}());



/***/ }),

/***/ "./src/app/model/content.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Content; });
var Content = /** @class */ (function () {
    function Content() {
        this.changes = new Array();
        this.groupsId = new Array();
    }
    return Content;
}());



/***/ }),

/***/ "./src/app/model/group.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Group; });
var Group = /** @class */ (function () {
    function Group() {
    }
    return Group;
}());



/***/ }),

/***/ "./src/app/model/manager-group.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManagerGroup; });
var ManagerGroup = /** @class */ (function () {
    function ManagerGroup() {
        this.membersId = new Array();
    }
    return ManagerGroup;
}());



/***/ }),

/***/ "./src/app/model/notification.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Notification; });
var Notification = /** @class */ (function () {
    function Notification() {
    }
    return Notification;
}());



/***/ }),

/***/ "./src/app/model/transaction.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Transaction; });
var Transaction = /** @class */ (function () {
    function Transaction() {
    }
    return Transaction;
}());



/***/ }),

/***/ "./src/app/model/user.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "./src/app/password/password.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/password/password.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\r\n  password works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/password/password.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PasswordComponent = /** @class */ (function () {
    function PasswordComponent() {
    }
    PasswordComponent.prototype.ngOnInit = function () {
    };
    PasswordComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-password',
            template: __webpack_require__("./src/app/password/password.component.html"),
            styles: [__webpack_require__("./src/app/password/password.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PasswordComponent);
    return PasswordComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile.component.css":
/***/ (function(module, exports) {

module.exports = ".card {\r\n    -webkit-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\r\n            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\r\n    max-width: 500px;\r\n    margin: auto;\r\n    text-align: center;\r\n}\r\n\r\n.title {\r\n    color: grey;\r\n    font-size: 18px;\r\n}\r\n\r\nbutton {\r\n    border: none;\r\n    outline: 0;\r\n    display: inline-block;\r\n    padding: 8px;\r\n    color: white;\r\n    background-color: #e95321;\r\n    text-align: center;\r\n    cursor: pointer;\r\n    width: 100%;\r\n    font-size: 18px;\r\n}\r\n\r\na {\r\n    text-decoration: none;\r\n    font-size: 22px;\r\n    color: #e95321;\r\n}\r\n\r\nbutton:hover, a:hover {\r\n    opacity: 0.7;\r\n}\r\n\r\ndiv .prof {\r\n    color: #3d4f5e;\r\n    text-align: left;\r\n    font-family:  'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;\r\n}\r\n\r\n.head {\r\n    color: #2295f3 !important;\r\n    margin-bottom: 20px;\r\n}\r\n\r\ndiv .body {\r\n    color: #919ca4;\r\n}\r\n\r\ndiv .about {\r\n    /* color: #919ca4; */\r\n    margin-bottom: 10px;\r\n}\r\n\r\n.blue {\r\n    color: #2295f3;\r\n}\r\n\r\n.tooltip {\r\n    position: relative;\r\n    display: inline-block;\r\n    border-bottom: 1px dotted black;\r\n}\r\n\r\n.tooltip .tooltiptext {\r\n    visibility: hidden;\r\n    width: 120px;\r\n    /* background-color: black; */\r\n    color: #e95321;\r\n    text-align: center;\r\n    border-radius: 6px;\r\n    padding: 5px 0;\r\n    \r\n    /* Position the tooltip */\r\n    position: absolute;\r\n    z-index: 1;\r\n    top: -5px;\r\n    left: 105%;\r\n}\r\n\r\n.tooltip:hover .tooltiptext {\r\n    visibility: visible;\r\n}"

/***/ }),

/***/ "./src/app/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Begin page content -->\r\n<main role=\"main\" class=\"container mt-3\">\r\n  <div class=\"row d-flex\">\r\n    <div class=\"col-md-6\">\r\n      <h2 class=\"mt-4 pl-0 text-grey ml-0\">\r\n        <span class=\"back-button\">\r\n          <a (click)=\"back()\" ><img src=\"../../../assets/img/back.png\" alt=\"\" class=\"back\" /></a>\r\n        </span> \r\n        PROFILE\r\n      </h2>\r\n    </div>\r\n  </div>\r\n</main>\r\n\r\n<section *ngIf=\"user\" class=\"mini-dashboard mt-4 mb-4\">\r\n  <div class=\"container search-box pb-4\">\r\n    <div class=\"row d-flex\">\r\n      <div class=\"col-md-12\">\r\n\r\n          <div class=\"row col-md-12\">\r\n            <div class=\"col col-md-3\">\r\n                <img class=\"\" src=\"{{user && user.picture ? user.picture : '../../assets/img/user.png'}}\" alt=\"\" width=\"200\" height=\"200\">\r\n            </div>\r\n            <div class=\"prof col col-md-9\">\r\n              <div class=\"col-md-12 head\">\r\n                <h2 class=\"prof mb-0\">{{user ? user.name : 'Name'}}</h2>\r\n                <p class=\"prof mb-0\" style=\"color: #2295f3 !important;\">Mitrais {{user && Utility.getRole(user.role)}}</p>\r\n              </div>\r\n              <div class=\"col-md-12\" style=\"margin-bottom: 30px\">\r\n                <p class=\"prof mb-0 body\">Joined On {{user.joinDate}}</p>\r\n                <!-- <h5 class=\"prof\">Reports To {{(user && user.supervisor) && user.supervisor.name | uppercase}}</h5> -->\r\n                <!-- <div class=\"tooltip\"> <h5>{{user.address}}</h5>\r\n                  <span class=\"tooltiptext\">Tooltip text</span>\r\n                  <a class=\"tooltiptext\" id=\"home-tab\" data-toggle=\"tab\" href=\"#changeAdd\" role=\"tab\" aria-controls=\"changeAdd\" aria-selected=\"false\" style=\"font-size: 10px !important;\">Change Address</a>\r\n                </div> -->\r\n                <h5 class=\"prof\">{{user.address}}\r\n                    <ul class=\"nav nav-pills mb-3\" id=\"myTab\" role=\"tablist\">\r\n                        <li class=\"nav-item\">\r\n                            <a id=\"home-tab\" data-toggle=\"tab\" href=\"#changeAdd\" role=\"tab\" aria-controls=\"changeAdd\" aria-selected=\"false\" style=\"font-size: 10px !important;\">Change Address</a>\r\n                        </li>\r\n                    </ul>\r\n                </h5>\r\n              </div>\r\n              <div class=\"row col-md-12\">\r\n                <ul class=\"nav nav-pills mb-3\" id=\"myTab\" role=\"tablist\">\r\n                    <li class=\"nav-item\">\r\n                        <a class=\"nav-link\" id=\"other-tab\" data-toggle=\"tab\" href=\"#changePass\" role=\"tab\" aria-controls=\"changePass\" aria-selected=\"false\" style=\"font-size: 15px !important;\">Change Password</a>\r\n                    </li>\r\n                </ul>\r\n              </div>\r\n              <div class=\"tab-content col-md-12\" id=\"myTabContent\">\r\n                <div class=\"tab-pane fade\" id=\"changePass\" role=\"tabpanel\" aria-labelledby=\"changePass-tab\">\r\n                    <!-- <div class=\"row\"> -->\r\n                        <div class=\"row\">\r\n                          <div class=\"col-md-6\">\r\n                            <div class=\"form-group\">\r\n                              <label for=\"password\">Old Password</label>\r\n                              <input #oldPassword id=\"oldPassword\" name=\"oldPassword\" type=\"password\" class=\"form-control here\" required=\"\">\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"row\">\r\n                          <div class=\"col-md-6\">\r\n                            <div class=\"form-group\">\r\n                              <label for=\"password\">New Password</label>\r\n                              <input #newPassword id=\"newPassword\" name=\"newPassword\" type=\"password\" class=\"form-control here\" required=\"\">\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"col-md-6\">\r\n                            <div class=\"form-group\">\r\n                              <label for=\"cpassword\">Confirm Password</label>\r\n                              <input #confirmPassword id=\"confirmPassword\" name=\"confirmPassword\" type=\"password\" class=\"form-control here\" required=\"\">\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col-md-12\" *ngIf=\"errMsg\">\r\n                          {{errMsg}}\r\n                        </div>\r\n                \r\n                        <mat-progress-spinner *ngIf=\"isLoading\"\r\n                          class=\"text-center m-auto\"\r\n                          color=\"warn\"\r\n                          diameter=28\r\n                          mode=\"indeterminate\" >\r\n                        </mat-progress-spinner>\r\n                        <div class=\"col-md-12\">\r\n                          <input *ngIf=\"!isLoading\" (click)=\"update(oldPassword.value, newPassword.value, confirmPassword.value)\" class=\"btn btn-block btn-primary radius-5 mt-5 mb-4\" value=\"Update\"/>\r\n                        </div>\r\n                    <!-- </div> -->\r\n                </div>\r\n                <div class=\"tab-pane fade\" id=\"changeAdd\" role=\"tabpanel\" aria-labelledby=\"changeAdd-tab\">      \r\n                  <div class=\"row\">\r\n                    <div class=\"col-md-12\">\r\n                      <div class=\"form-group\">\r\n                        <label for=\"address\">Address</label>\r\n                        <input #address name=\"address\" type=\"text\" [(ngModel)]=\"user.address\" class=\"form-control here\" required=\"\" [(ngModel)]=\"user.address\">\r\n                      </div>\r\n                      <div class=\"col-md-12\">\r\n                        <input *ngIf=\"!isLoading\" (click)=\"updateAddress(address.value)\" class=\"btn btn-block btn-primary radius-5 mt-5 mb-4\" value=\"Update\"/>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <!-- <a routerLink=\"/password\" class=\"prof badge badge-white\">Change Password</a> -->\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          \r\n<!-- \r\n          <img class=\"mt-5 mb-4\" src=\"{{user && user.picture ? user.picture : '../../assets/img/user.png'}}\" alt=\"\" width=\"200\" height=\"200\">\r\n\r\n          <div class=\"form-group\">\r\n            <label for=\"file\">Choose File</label>\r\n            <input type=\"file\" id=\"file\" (change)=\"handleFileInput($event.target.files)\">\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"name\">Name</label>\r\n                <input id=\"name\" name=\"name\" type=\"text\" [(ngModel)]=\"user.name\" class=\"form-control here\" required=\"\" autofocus=\"\" >\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"email\">Email</label>\r\n                <input id=\"email\" name=\"email\" type=\"text\" [(ngModel)]=\"user.email\" class=\"form-control here\" required=\"\" >\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"dob\">Date of birth</label>\r\n                <input id=\"dob\" name=\"dob\" type=\"date\" [(ngModel)]=\"user.dob\" class=\"form-control here\" required=\"\">\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n                <div class=\"form-group\">\r\n                  <label for=\"joinDate\">Join date</label>\r\n                  <input id=\"joinDate\" name=\"joinDate\" type=\"date\" [(ngModel)]=\"user.joinDate\" class=\"form-control here\" required=\"\">\r\n                </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"role\">As A</label>\r\n                <input id=\"role\" name=\"role\" type=\"text\" value=\"{{user && Utility.getRole(user.role)}}\" class=\"form-control here\" disabled >\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"supervisor\">Supervisor</label>\r\n                <input id=\"supervisor\" name=\"supervisor\" type=\"text\" value=\"{{(user && user.supervisor) && user.supervisor.name}}\" class=\"form-control here\" disabled >\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <div class=\"form-group\">\r\n                <label for=\"password\">Old Password</label>\r\n                <input #oldPassword id=\"oldPassword\" name=\"oldPassword\" type=\"password\" class=\"form-control here\" required=\"\">\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"password\">New Password</label>\r\n                <input #newPassword id=\"newPassword\" name=\"newPassword\" type=\"password\" class=\"form-control here\" required=\"\">\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"cpassword\">Confirm Password</label>\r\n                <input #confirmPassword id=\"confirmPassword\" name=\"confirmPassword\" type=\"password\" class=\"form-control here\" required=\"\">\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <div class=\"form-group\">\r\n                <label for=\"address\">Address</label>\r\n                <input name=\"address\" type=\"text\" [(ngModel)]=\"user.address\" class=\"form-control here\" required=\"\">\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div *ngIf=\"errMsg\">\r\n            {{errMsg}}\r\n          </div>\r\n\r\n          <mat-progress-spinner *ngIf=\"isLoading\"\r\n            class=\"text-center m-auto\"\r\n            color=\"warn\"\r\n            diameter=28\r\n            mode=\"indeterminate\" >\r\n          </mat-progress-spinner>\r\n          <input *ngIf=\"!isLoading\" (click)=\"update(oldPassword.value, newPassword.value, confirmPassword.value)\" class=\"btn btn-block btn-primary radius-5 mt-5 mb-4\" value=\"Update\"/> -->\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n\r\n<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">"

/***/ }),

/***/ "./src/app/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__config_utility__ = __webpack_require__("./src/app/config/utility.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(userService, location, router) {
        this.userService = userService;
        this.location = location;
        this.router = router;
        this.fileToUpload = null;
        this.Constanta = __WEBPACK_IMPORTED_MODULE_4__config_constanta__;
        this.Utility = __WEBPACK_IMPORTED_MODULE_5__config_utility__;
        this.isLoading = false;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        // this.user = this.userService.getLoggedInUser();
        // if (!this.user)
        //   this.router.navigate(['/']);
        var _this = this;
        this.user = this.userService.getLoggedInUser();
        if (!this.user) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_4__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        console.log(res);
                        _this.user = res;
                        _this.userService.setLoggedInUser(res);
                        // if (res.role != Constanta.EMPLOYEE_ROLE || res.admin)
                        //   this.router.navigate(['/']);
                    }
                    else {
                        _this.router.navigate(['/']);
                    }
                });
            }
            else {
                this.router.navigate(['/']);
            }
        }
    };
    ProfileComponent.prototype.update = function (oldPassword, newPassword, confirmPassword) {
        if (!__WEBPACK_IMPORTED_MODULE_5__config_utility__["isValidEmail"](this.user.email)) {
            this.errMsg = 'Email is invalid!';
            return;
        }
        if (oldPassword != this.user.password) {
            this.errMsg = 'Old password is invalid!';
            return;
        }
        if (confirmPassword != newPassword) {
            this.errMsg = 'New password is not equal!';
            return;
        }
        if (newPassword.length < 1) {
            this.errMsg = 'Please input new password!';
            return;
        }
        this.user.password = newPassword;
        this.updateUser();
        // this.userService.signUp(this.user)
        //   .subscribe(user => {
        //     if (user)
        //       this.location.back();
        //     else
        //       this.errMsg = 'Failed to update';
        //   });
    };
    ProfileComponent.prototype.updateAddress = function (address) {
        this.user.address = address;
        this.updateUser();
    };
    ProfileComponent.prototype.back = function () {
        this.location.back();
    };
    ProfileComponent.prototype.updateUser = function () {
        var _this = this;
        this.isLoading = true;
        console.log(this.user);
        if (this.fileToUpload) {
            this.userService.uploadFile(this.fileToUpload)
                .subscribe(function (res) {
                console.log(res);
                if (_this.user && res) {
                    _this.user.picture = res.url;
                    _this.userService.addEdit(_this.user)
                        .subscribe(function (user) {
                        console.log(user);
                        if (user)
                            _this.router.navigate(['/']);
                        else
                            _this.errMsg = 'Failed to update';
                        _this.isLoading = false;
                    });
                }
                else {
                    _this.errMsg = 'Failed to upload photo';
                    _this.isLoading = false;
                }
            });
        }
        else {
            this.userService.addEdit(this.user)
                .subscribe(function (user) {
                console.log(user);
                if (user)
                    _this.router.navigate(['/']);
                else
                    _this.errMsg = 'Failed to update';
                _this.isLoading = false;
            });
        }
    };
    ProfileComponent.prototype.handleFileInput = function (files) {
        // this.isLoading = true;
        console.log(files);
        if (files.length < 1) {
            // this.isLoading = false;
            return;
        }
        this.fileToUpload = files[0];
        // this.userService.uploadFile(this.fileToUpload)
        //   .subscribe(res => {
        //     console.log(res);
        //     if (this.user && res) {
        //       this.user.picture = res.url as string;
        //       this.isLoading = false;
        //       // this.updateUser();
        //     } else {
        //       this.errMsg = 'Failed to upload photo';
        //       this.isLoading = false;
        //     }
        //   });
    };
    ProfileComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__("./src/app/profile/profile.component.html"),
            styles: [__webpack_require__("./src/app/profile/profile.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/register/register.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Begin page content -->\r\n<main role=\"main\" class=\"container\">\r\n  <h2 class=\"mt-4 pl-0 text-grey ml-0\">REGISTER</h2>\r\n</main>\r\n\r\n<section class=\"mini-dashboard mt-4 mb-4\">\r\n  <div class=\"container search-box pb-4\">\r\n    <div class=\"row d-flex\">\r\n      <div class=\"text-center col-md-12\">\r\n        <form autocomplete=\"off\" class=\"form-signup\">\r\n          <img class=\"mt-5 mb-4 rounded-circle\" src=\"../../assets/img/mc-icon-carrot.png\" alt=\"\" width=\"72\" height=\"72\">\r\n          \r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"name\">Name</label>\r\n                <input #name id=\"name\" name=\"name\" type=\"text\" class=\"form-control here\" placeholder=\"Name\" required=\"\">\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"email\">Email</label>\r\n                <input #email id=\"email\" name=\"email\" type=\"text\" class=\"form-control here\" placeholder=\"Email address\" required=\"\" autofocus=\"\">\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"dob\">Date of birth</label>\r\n                <input #dob id=\"dob\" name=\"dob\" type=\"date\" class=\"form-control here\" placeholder=\"Date of birth\" required=\"\">\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n                <div class=\"form-group\">\r\n                  <label for=\"joinDate\">Join date</label>\r\n                  <input #joinDate id=\"joinDate\" name=\"joinDate\" type=\"date\" class=\"form-control here\" placeholder=\"Join date\" required=\"\">\r\n                </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n              <div class=\"form-group\">\r\n                <label for=\"role\">Role</label>\r\n                <select #role id=\"role\" name=\"role\" class=\"form-control here\" >\r\n                  <option value={{Constanta.EMPLOYEE_ROLE}}>{{Utility.getRole(Constanta.EMPLOYEE_ROLE)}}</option>\r\n                  <option value={{Constanta.MANAGER_ROLE}}>{{Utility.getRole(Constanta.MANAGER_ROLE)}}</option>\r\n                </select>\r\n              </div>\r\n            </div>\r\n            <!-- <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"supervisor\">Supervisor</label>\r\n                <input #supervisor id=\"supervisor\" name=\"supervisor\" type=\"text\" class=\"form-control here\" placeholder=\"Supervisor id\" required=\"\" value=\"\">\r\n              </div>\r\n            </div> -->\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"password\">Password</label>\r\n                <input #password id=\"password\" name=\"password\" type=\"password\" class=\"form-control here\" placeholder=\"Password\" required=\"\">\r\n              </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n              <div class=\"form-group\">\r\n                <label for=\"joinDate\">Confirm Password</label>\r\n                <input #cpassword id=\"cpassword\" name=\"cpassword\" type=\"password\" class=\"form-control here\" placeholder=\"Confirm Password\" required=\"\">\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <p style=\"color: red\" >{{errMsg}}</p>\r\n\r\n          <!-- <a routerLink=\"/employee\" class=\"btn btn-block btn-primary radius-5 mt-5 mb-4\" onclick=\"login()\">Sign in</a> -->\r\n          <input (click)=\"signUp(\r\n              name.value, email.value, dob.value, joinDate.value, role.value, password.value, cpassword.value\r\n            )\" class=\"btn btn-block btn-primary radius-5 mt-5 mb-4\" value=\"Sign up\"/>\r\n\r\n          <!-- <a routerLink=\"/employee\" class=\"btn btn-block btn-primary radius-5 mt-5 mb-4\" onclick=\"login()\">Sign up</a> -->\r\n          <input routerLink=\"/login\" class=\"btn btn-block btn-link radius-5\" value=\"Login\"/>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>"

/***/ }),

/***/ "./src/app/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_utility__ = __webpack_require__("./src/app/config/utility.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(userService, router) {
        this.userService = userService;
        this.router = router;
        this.errMsg = null;
        this.Constanta = __WEBPACK_IMPORTED_MODULE_3__config_constanta__;
        this.Utility = __WEBPACK_IMPORTED_MODULE_4__config_utility__;
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.user = this.userService.getLoggedInUser();
        if (this.user)
            this.move(this.user);
    };
    RegisterComponent.prototype.signUp = function (name, email, dob, joinDate, role, password, cpassword) {
        var _this = this;
        if (!name || !email || !dob || !joinDate || !role || !password || !cpassword) {
            this.errMsg = 'input all field!';
            return;
        }
        email = email.trim();
        if (!__WEBPACK_IMPORTED_MODULE_4__config_utility__["isValidEmail"](email)) {
            this.errMsg = 'input valid email!';
            return;
        }
        if (password != cpassword) {
            this.errMsg = 'password is not equal!';
            return;
        }
        var approved = true;
        if (role == __WEBPACK_IMPORTED_MODULE_3__config_constanta__["MANAGER_ROLE"])
            approved = false;
        this.userService.addEdit({ name: name, email: email, dob: dob, joinDate: joinDate, role: role, password: password, approved: approved })
            .subscribe(function (user) {
            _this.router.navigate(['login']);
        });
    };
    RegisterComponent.prototype.move = function (user) {
        if (user) {
            this.userService.setLoggedInUser(user);
            if (user.admin)
                this.router.navigate(['admin']);
            else {
                switch (user.role) {
                    case __WEBPACK_IMPORTED_MODULE_3__config_constanta__["MANAGER_ROLE"]:
                        this.router.navigate(['manager']);
                        break;
                    case __WEBPACK_IMPORTED_MODULE_3__config_constanta__["EMPLOYEE_ROLE"]:
                        this.router.navigate(['employee']);
                        break;
                    case __WEBPACK_IMPORTED_MODULE_3__config_constanta__["ROOT_ADMIN_ROLE"]:
                        this.router.navigate(['root-admin']);
                        break;
                    case __WEBPACK_IMPORTED_MODULE_3__config_constanta__["SENIOR_MANAGER_ROLE"]:
                        this.router.navigate(['senior-manager']);
                        break;
                    default:
                        break;
                }
            }
        }
    };
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',
            template: __webpack_require__("./src/app/register/register.component.html"),
            styles: [__webpack_require__("./src/app/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/root-admin/admin-list/admin-list.component.css":
/***/ (function(module, exports) {

module.exports = "\r\n  a:not([href]):not([tabindex]) {\r\n    color: #FFF;\r\n    cursor: pointer;\r\n  }"

/***/ }),

/***/ "./src/app/root-admin/admin-list/admin-list.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"admin-tabs py-3\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12 px-md-0\">\r\n\r\n                <div class=\"tab-content search-bo\" id=\"myTabContent\">\r\n                    <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-12\">\r\n                                <hr class=\"box-title-hr\">\r\n                                <h4 class=\"my-2 box-title\">ADMIN LIST</h4>\r\n                            </div>\r\n                            <div class=\"col-md-12\">\r\n                                <mat-progress-spinner *ngIf=\"loadingAdmin\"\r\n                                class=\"text-center m-auto\"\r\n                                color=\"warn\"\r\n                                mode=\"indeterminate\" >\r\n                                </mat-progress-spinner> \r\n                                <table id=\"admin-table\" class=\"table table-hover mt-3\" datatable [dtOptions]=\"dtOptionsAdmin\" [dtTrigger]=\"dtTriggerAdmin\">\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th scope=\"col\">Name</th>\r\n                                            <th scope=\"col\">Email</th>\r\n                                            <th scope=\"col\">Action</th>\r\n                                        </tr>\r\n                                    </thead>\r\n                                    <tbody>       \r\n                                        <tr *ngFor=\"let user of admin\">\r\n                                            <td>{{user.name}}</td>\r\n                                            <td>{{user.email}}</td>\r\n                                            <td><a class=\"btn btn-danger\" (click)=\"revoke(user)\">Revoke</a></td>\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/root-admin/admin-list/admin-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_user__ = __webpack_require__("./src/app/model/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_root_admin_service__ = __webpack_require__("./src/app/service/root-admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AdminListComponent = /** @class */ (function () {
    function AdminListComponent(rootAdminService, userService, router, appComponent) {
        this.rootAdminService = rootAdminService;
        this.userService = userService;
        this.router = router;
        this.appComponent = appComponent;
        this.admin = [];
        this.nonAdmin = [];
        this.loadingAdmin = true;
        this.dtOptionsAdmin = {};
        this.dtTriggerAdmin = new __WEBPACK_IMPORTED_MODULE_7_rxjs__["Subject"]();
        this.userTemp = new __WEBPACK_IMPORTED_MODULE_2__model_user__["a" /* User */];
        this.loggedInUser = new __WEBPACK_IMPORTED_MODULE_2__model_user__["a" /* User */];
    }
    AdminListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInUser = this.userService.getLoggedInUser();
        if (!this.loggedInUser) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_3__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        _this.loggedInUser = res;
                        _this.userService.setLoggedInUser(res);
                    }
                    else
                        _this.router.navigate(['/']);
                });
            }
            else
                this.router.navigate(['/']);
        }
        this.getUsers();
    };
    AdminListComponent.prototype.getUsers = function () {
        var _this = this;
        this.rootAdminService.getUsers().subscribe(function (users) {
            _this.users = users;
            _this.users.forEach(function (user) {
                if (user.admin == true)
                    _this.admin.push(user);
                else
                    _this.nonAdmin.push(user);
            });
            _this.loadingAdmin = false;
            _this.dtTriggerAdmin.next();
        });
    };
    AdminListComponent.prototype.onSelect = function (user) {
        this.selectedUser = user;
    };
    AdminListComponent.prototype.revoke = function (user) {
        var _this = this;
        user.admin = false;
        this.rootAdminService.updateUser(user).subscribe(function (user) {
            _this.nonAdmin.push(user);
            _this.admin = _this.admin.filter(function (r) {
                return r.id != user.id;
            });
            _this.appComponent.showSuccess(user.name + " is not an Admin anymore.");
        });
    };
    AdminListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin-list',
            template: __webpack_require__("./src/app/root-admin/admin-list/admin-list.component.html"),
            styles: [__webpack_require__("./src/app/root-admin/admin-list/admin-list.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__service_root_admin_service__["a" /* RootAdminService */],
            __WEBPACK_IMPORTED_MODULE_5__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]])
    ], AdminListComponent);
    return AdminListComponent;
}());



/***/ }),

/***/ "./src/app/root-admin/approval-admin/approval-admin.component.css":
/***/ (function(module, exports) {

module.exports = "a:not([href]):not([tabindex]) {\r\n  color: #FFF;\r\n  cursor: pointer;\r\n}"

/***/ }),

/***/ "./src/app/root-admin/approval-admin/approval-admin.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"admin-tabs py-3\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12 px-md-0\">\r\n\r\n                <div class=\"tab-content search-bo\" id=\"myTabContent\">\r\n                    <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-12\">\r\n                                <hr class=\"box-title-hr\">\r\n                                <h4 class=\"my-2 box-title\">STAFF LIST</h4>\r\n                            </div>\r\n                            <div class=\"col-md-12\">\r\n                                <mat-progress-spinner *ngIf=\"loadingStaff\"\r\n                                class=\"text-center m-auto\"\r\n                                color=\"warn\"\r\n                                mode=\"indeterminate\" >\r\n                                </mat-progress-spinner> \r\n                                <table id=\"nonadmin-table\" class=\"table table-hover mt-3\" datatable [dtOptions]=\"dtOptionsStaff\" [dtTrigger]=\"dtTriggerStaff\">\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th scope=\"col\">Name</th>\r\n                                                <th scope=\"col\">Email</th>\r\n                                                <th scope=\"col\">Action</th>\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody>       \r\n                                            <tr *ngFor=\"let user of nonAdmin\">\r\n                                                <td>{{user.name}}</td>\r\n                                                <td>{{user.email}}</td>\r\n                                                <td><a class=\"btn btn-success\" (click)=\"approveAdmin(user)\">Approve</a></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/root-admin/approval-admin/approval-admin.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApprovalAdminComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_user__ = __webpack_require__("./src/app/model/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_root_admin_service__ = __webpack_require__("./src/app/service/root-admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ApprovalAdminComponent = /** @class */ (function () {
    function ApprovalAdminComponent(rootAdminService, userService, router, appComponent) {
        this.rootAdminService = rootAdminService;
        this.userService = userService;
        this.router = router;
        this.appComponent = appComponent;
        this.admin = [];
        this.nonAdmin = [];
        this.loadingStaff = true;
        this.dtOptionsStaff = {};
        this.dtTriggerStaff = new __WEBPACK_IMPORTED_MODULE_7_rxjs__["Subject"]();
        this.userTemp = new __WEBPACK_IMPORTED_MODULE_2__model_user__["a" /* User */];
        this.loggedInUser = new __WEBPACK_IMPORTED_MODULE_2__model_user__["a" /* User */];
    }
    ApprovalAdminComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInUser = this.userService.getLoggedInUser();
        if (!this.loggedInUser) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_3__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        _this.loggedInUser = res;
                        _this.userService.setLoggedInUser(res);
                    }
                    else
                        _this.router.navigate(['/']);
                });
            }
            else
                this.router.navigate(['/']);
        }
        this.getUsers();
    };
    ApprovalAdminComponent.prototype.getUsers = function () {
        var _this = this;
        this.rootAdminService.getUsers().subscribe(function (users) {
            _this.users = users;
            _this.users.forEach(function (user) {
                if (user.admin == true)
                    _this.admin.push(user);
                else
                    _this.nonAdmin.push(user);
            });
            _this.loadingStaff = false;
            _this.dtTriggerStaff.next();
        });
    };
    ApprovalAdminComponent.prototype.onSelect = function (user) {
        this.selectedUser = user;
    };
    ApprovalAdminComponent.prototype.approveAdmin = function (user) {
        var _this = this;
        user.admin = true;
        this.rootAdminService.updateUser(user).subscribe(function (user) {
            _this.admin.push(user);
            _this.nonAdmin = _this.nonAdmin.filter(function (r) {
                return r.id != user.id;
            });
            _this.appComponent.showSuccess(user.name + " is now an Admin.");
        });
    };
    ApprovalAdminComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-approval-admin',
            template: __webpack_require__("./src/app/root-admin/approval-admin/approval-admin.component.html"),
            styles: [__webpack_require__("./src/app/root-admin/approval-admin/approval-admin.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__service_root_admin_service__["a" /* RootAdminService */],
            __WEBPACK_IMPORTED_MODULE_5__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]])
    ], ApprovalAdminComponent);
    return ApprovalAdminComponent;
}());



/***/ }),

/***/ "./src/app/root-admin/approval-manager/approval-manager.component.css":
/***/ (function(module, exports) {

module.exports = "a:not([href]):not([tabindex]) {\r\n    color: #FFF;\r\n    cursor: pointer;\r\n}"

/***/ }),

/***/ "./src/app/root-admin/approval-manager/approval-manager.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"admin-tabs py-3\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12 px-md-0\">\r\n\r\n                <div class=\"tab-content search-bo\" id=\"myTabContent\">\r\n                    <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-12\">\r\n                                <hr class=\"box-title-hr\">\r\n                                <h4 class=\"my-2 box-title\">MANAGER APPROVAL LIST</h4>\r\n                            </div>\r\n                            <div class=\"col-md-12\"> \r\n                                <mat-progress-spinner *ngIf=\"loadingManager\"\r\n                                class=\"text-center m-auto\"\r\n                                color=\"warn\"\r\n                                mode=\"indeterminate\" >\r\n                                </mat-progress-spinner> \r\n                                <table id=\"manager-table\" class=\"table table-hover mt-3\" datatable [dtOptions]=\"dtOptionsManager\" [dtTrigger]=\"dtTriggerManager\">\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th scope=\"col\">Name</th>\r\n                                                <th scope=\"col\">Email</th>\r\n                                                <th scope=\"col\">Action</th>\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody  >       \r\n                                            <tr *ngFor=\"let user of manager\" >\r\n                                                <td>{{user.name}}</td>\r\n                                                <td>{{user.email}}</td>\r\n                                                <td><a class=\"btn btn-success\" (click)=\"approveManager(user)\">Approve</a></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n"

/***/ }),

/***/ "./src/app/root-admin/approval-manager/approval-manager.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApprovalManagerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_user__ = __webpack_require__("./src/app/model/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_root_admin_service__ = __webpack_require__("./src/app/service/root-admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ApprovalManagerComponent = /** @class */ (function () {
    function ApprovalManagerComponent(rootAdminService, userService, router, appComponent) {
        this.rootAdminService = rootAdminService;
        this.userService = userService;
        this.router = router;
        this.appComponent = appComponent;
        this.manager = [];
        this.loadingManager = true;
        this.dtOptionsManager = {};
        this.dtTriggerManager = new __WEBPACK_IMPORTED_MODULE_7_rxjs__["Subject"]();
        this.userTemp = new __WEBPACK_IMPORTED_MODULE_2__model_user__["a" /* User */];
        this.loggedInUser = new __WEBPACK_IMPORTED_MODULE_2__model_user__["a" /* User */];
    }
    ApprovalManagerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInUser = this.userService.getLoggedInUser();
        if (!this.loggedInUser) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_3__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        _this.loggedInUser = res;
                        _this.userService.setLoggedInUser(res);
                    }
                    else
                        _this.router.navigate(['/']);
                });
            }
            else
                this.router.navigate(['/']);
        }
        this.getUsers();
    };
    ApprovalManagerComponent.prototype.getUsers = function () {
        var _this = this;
        this.rootAdminService.getUsers().subscribe(function (users) {
            _this.users = users;
            _this.users.forEach(function (user) {
                if (user.admin == false)
                    if (user.role == 4 && user.approved == false)
                        _this.manager.push(user);
            });
            _this.loadingManager = false;
            _this.dtTriggerManager.next();
        });
    };
    ApprovalManagerComponent.prototype.approveManager = function (user) {
        var _this = this;
        user.approved = true;
        this.userService.addEdit(user).subscribe(function (user) {
            _this.manager = _this.manager.filter(function (r) {
                return r.id != user.id;
            });
            _this.appComponent.showSuccess(user.name + " is now registered as Manager.");
        });
    };
    ApprovalManagerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-approval-manager',
            template: __webpack_require__("./src/app/root-admin/approval-manager/approval-manager.component.html"),
            styles: [__webpack_require__("./src/app/root-admin/approval-manager/approval-manager.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__service_root_admin_service__["a" /* RootAdminService */],
            __WEBPACK_IMPORTED_MODULE_5__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]])
    ], ApprovalManagerComponent);
    return ApprovalManagerComponent;
}());



/***/ }),

/***/ "./src/app/root-admin/approval-social/approval-social.component.css":
/***/ (function(module, exports) {

module.exports = "a:not([href]):not([tabindex]) {\r\n    color: #FFF;\r\n    cursor: pointer;\r\n}"

/***/ }),

/***/ "./src/app/root-admin/approval-social/approval-social.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"admin-tabs py-3\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12 px-md-0\">\r\n\r\n                <div class=\"tab-content search-bo\" id=\"myTabContent\">\r\n                    <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-12\">\r\n                                <hr class=\"box-title-hr\">\r\n                                <h4 class=\"my-2 box-title\">REACHED-TARGET SOCIAL FOUNDATION LIST</h4>\r\n                            </div>\r\n                            <div class=\"col-md-12\">\r\n                                <mat-progress-spinner *ngIf=\"loadingSocial\"\r\n                                class=\"text-center m-auto\"\r\n                                color=\"warn\"\r\n                                mode=\"indeterminate\" >\r\n                                </mat-progress-spinner> \r\n                                <table id=\"social-table\" class=\"table table-hover mt-3\" datatable [dtOptions]=\"dtOptionsSocial\" [dtTrigger]=\"dtTriggerSocial\">\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th scope=\"col\">Name</th>\r\n                                                <th scope=\"col\">Current Carrot</th>\r\n                                                <th scope=\"col\">Target Carrot</th>\r\n                                                <th scope=\"col\">Action</th>\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody >       \r\n                                            <tr *ngFor=\"let content of social\">\r\n                                                <td>{{content.name}}</td>\r\n                                                <td>{{content.currentCarrot}}</td>\r\n                                                <td>{{content.carrot}}</td>\r\n                                                <td><a class=\"btn btn-success\" (click)=\"approveSocial(content)\">Approve</a></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>"

/***/ }),

/***/ "./src/app/root-admin/approval-social/approval-social.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApprovalSocialComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_user__ = __webpack_require__("./src/app/model/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_root_admin_service__ = __webpack_require__("./src/app/service/root-admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_admin_service__ = __webpack_require__("./src/app/service/admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_transaction_service__ = __webpack_require__("./src/app/service/transaction.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var ApprovalSocialComponent = /** @class */ (function () {
    function ApprovalSocialComponent(rootAdminService, adminService, userService, transactionService, router, appComponent) {
        this.rootAdminService = rootAdminService;
        this.adminService = adminService;
        this.userService = userService;
        this.transactionService = transactionService;
        this.router = router;
        this.appComponent = appComponent;
        this.social = [];
        this.loadingSocial = true;
        this.dtOptionsSocial = {};
        this.dtTriggerSocial = new __WEBPACK_IMPORTED_MODULE_9_rxjs__["Subject"]();
        this.userTemp = new __WEBPACK_IMPORTED_MODULE_2__model_user__["a" /* User */];
        this.loggedInUser = new __WEBPACK_IMPORTED_MODULE_2__model_user__["a" /* User */];
    }
    ApprovalSocialComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInUser = this.userService.getLoggedInUser();
        if (!this.loggedInUser) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_3__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        _this.loggedInUser = res;
                        _this.userService.setLoggedInUser(res);
                    }
                    else
                        _this.router.navigate(['/']);
                });
            }
            else
                this.router.navigate(['/']);
        }
        this.getSocial();
    };
    ApprovalSocialComponent.prototype.getSocial = function () {
        var _this = this;
        this.social = [];
        console.log(this.social);
        this.adminService.getRewards().subscribe(function (rewards) {
            _this.rewards = rewards;
            _this.rewards.forEach(function (reward) {
                if (reward.type == 3 && reward.currentCarrot >= reward.carrot) {
                    _this.social.push(reward);
                }
            });
            _this.loadingSocial = false;
            _this.dtTriggerSocial.next();
        });
    };
    ApprovalSocialComponent.prototype.approveSocial = function (content) {
        var _this = this;
        content.currentCarrot -= content.carrot;
        this.adminService.addReward(content).subscribe(function (content) {
            _this.appComponent.showSuccess(content.carrot + " Carrot(s) has been donated to '" + content.name + "' Social Foundation.");
        });
    };
    ApprovalSocialComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-approval-social',
            template: __webpack_require__("./src/app/root-admin/approval-social/approval-social.component.html"),
            styles: [__webpack_require__("./src/app/root-admin/approval-social/approval-social.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__service_root_admin_service__["a" /* RootAdminService */],
            __WEBPACK_IMPORTED_MODULE_5__service_admin_service__["a" /* AdminService */],
            __WEBPACK_IMPORTED_MODULE_6__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_7__service_transaction_service__["a" /* TransactionService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */]])
    ], ApprovalSocialComponent);
    return ApprovalSocialComponent;
}());



/***/ }),

/***/ "./src/app/root-admin/carrot-give/carrot-give.component.css":
/***/ (function(module, exports) {

module.exports = "a:not([href]):not([tabindex]) {\r\n    color: #FFF;\r\n    cursor: pointer;\r\n}"

/***/ }),

/***/ "./src/app/root-admin/carrot-give/carrot-give.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"admin-tabs py-3\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12 px-md-0\">\r\n\r\n                <div class=\"tab-content search-bo\" id=\"myTabContent\">\r\n                    <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-12\">\r\n                                <hr class=\"box-title-hr\">\r\n                                <h4 class=\"my-2 box-title\">SENIOR MANAGER LIST</h4>\r\n                            </div>\r\n                            <div class=\"col-md-12\">\r\n                                <mat-progress-spinner *ngIf=\"loadingSeniorManager\"\r\n                                class=\"text-center m-auto\"\r\n                                color=\"warn\"\r\n                                mode=\"indeterminate\" >\r\n                                </mat-progress-spinner> \r\n                                <table id=\"senior-manager-table\"class=\"table table-hover mt-3\" datatable [dtOptions]=\"dtOptionsSeniorManager\" [dtTrigger]=\"dtTriggerSeniorManager\">\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th scope=\"col\">Name</th>\r\n                                                <th scope=\"col\">Email</th>\r\n                                                <th scope=\"col\">Action</th>\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody>       \r\n                                            <tr *ngFor=\"let user of sManager\">\r\n                                                <td>{{user.name}}</td>\r\n                                                <td>{{user.email}}</td>\r\n                                                <td><a (click)=\"onSelect(user)\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#sendCarrot\">Give Carrot</a></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n<!-- MODAL -->\r\n<div class=\"modal fade\" id=\"sendCarrot\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Give Carrot</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <form>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"text1\">Amount of Carrot(s)</label>\r\n                      <input #carrotSent name=\"carrotSent\" type=\"text\" class=\"form-control here\"  >\r\n                    </div>\r\n                </form>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\" (click)=\"cancelEdit()\">Cancel</button>\r\n                <button type=\"button\" class=\"btn btn-carrot radius-5\" (click)=\"send(carrotSent.value)\" data-dismiss=\"modal\">Send</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/root-admin/carrot-give/carrot-give.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarrotGiveComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_user__ = __webpack_require__("./src/app/model/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_transaction__ = __webpack_require__("./src/app/model/transaction.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_root_admin_service__ = __webpack_require__("./src/app/service/root-admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_transaction_service__ = __webpack_require__("./src/app/service/transaction.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_config_service__ = __webpack_require__("./src/app/service/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_rxjs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var CarrotGiveComponent = /** @class */ (function () {
    function CarrotGiveComponent(rootAdminService, userService, transactionService, configService, router, appComponent) {
        this.rootAdminService = rootAdminService;
        this.userService = userService;
        this.transactionService = transactionService;
        this.configService = configService;
        this.router = router;
        this.appComponent = appComponent;
        this.sManager = [];
        this.transactionTemp = new __WEBPACK_IMPORTED_MODULE_3__model_transaction__["a" /* Transaction */];
        this.loadingSeniorManager = true;
        this.dtOptionsSeniorManager = {};
        this.dtTriggerSeniorManager = new __WEBPACK_IMPORTED_MODULE_10_rxjs__["Subject"]();
        this.userTemp = new __WEBPACK_IMPORTED_MODULE_2__model_user__["a" /* User */];
        this.loggedInUser = new __WEBPACK_IMPORTED_MODULE_2__model_user__["a" /* User */];
    }
    CarrotGiveComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInUser = this.userService.getLoggedInUser();
        if (!this.loggedInUser) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_4__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        _this.loggedInUser = res;
                        _this.userService.setLoggedInUser(res);
                    }
                    else
                        _this.router.navigate(['/']);
                });
            }
            else
                this.router.navigate(['/']);
        }
        this.getUsers();
        this.getConfig();
        // this.getSocial()
    };
    CarrotGiveComponent.prototype.getUsers = function () {
        var _this = this;
        this.rootAdminService.getUsers().subscribe(function (users) {
            _this.users = users;
            _this.users.forEach(function (user) {
                if (user.admin == false)
                    if (user.role == 8 && user.approved == true)
                        _this.sManager.push(user);
            });
            _this.loadingSeniorManager = false;
            _this.dtTriggerSeniorManager.next();
        });
    };
    CarrotGiveComponent.prototype.getConfig = function () {
        var _this = this;
        this.configs = [];
        this.configService.getConfig().subscribe(function (configs) { return _this.config = configs[0]; });
    };
    CarrotGiveComponent.prototype.onSelect = function (user) {
        this.selectedUser = user;
    };
    CarrotGiveComponent.prototype.send = function (carrot) {
        var _this = this;
        if (carrot != null) {
            if (isNaN(carrot))
                this.appComponent.showError("Only number is permitted.");
            else {
                if (this.config.stockCarrot >= carrot) {
                    this.transactionTemp.senderId = this.loggedInUser.id;
                    this.transactionTemp.receiverId = this.selectedUser.id;
                    this.transactionTemp.amount = carrot;
                    this.transactionTemp.description = "Senior Manager Supply from Root Admin";
                    this.transactionTemp.approved = true;
                    this.transactionTemp.type = 4;
                    this.transactionService.postTransaction(this.transactionTemp).subscribe(function (transaction) {
                        if (transaction) {
                            _this.config.stockCarrot -= carrot;
                            _this.configService.updateConfig(_this.config).subscribe(function (config) {
                                _this.appComponent.showSuccess(carrot + " carrot(s) has been sent to " + _this.selectedUser.name + ".");
                            });
                        }
                        else
                            _this.appComponent.showError("Sending Failed.");
                    });
                }
                else
                    this.appComponent.showError("Not Enough Stock Carrot");
            }
        }
        else
            this.appComponent.showError("Please fill the carrot number.");
    };
    CarrotGiveComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-carrot-give',
            template: __webpack_require__("./src/app/root-admin/carrot-give/carrot-give.component.html"),
            styles: [__webpack_require__("./src/app/root-admin/carrot-give/carrot-give.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__service_root_admin_service__["a" /* RootAdminService */],
            __WEBPACK_IMPORTED_MODULE_6__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_7__service_transaction_service__["a" /* TransactionService */],
            __WEBPACK_IMPORTED_MODULE_8__service_config_service__["a" /* ConfigService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* AppComponent */]])
    ], CarrotGiveComponent);
    return CarrotGiveComponent;
}());



/***/ }),

/***/ "./src/app/root-admin/carrot-set/carrot-set.component.css":
/***/ (function(module, exports) {

module.exports = "a:not([href]):not([tabindex]) {\r\n    color: #FFF;\r\n    cursor: pointer;\r\n}"

/***/ }),

/***/ "./src/app/root-admin/carrot-set/carrot-set.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"admin-tabs py-3\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12 px-md-0\">\r\n\r\n                <div class=\"tab-content search-bo\" id=\"myTabContent\">\r\n                    <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">\r\n                        <div class=\"row\">\r\n                            <div class=\"col-md-12\">\r\n                                <hr class=\"box-title-hr\">\r\n                                <h4 class=\"my-2 box-title\">CARROT CONFIGURATIONS</h4>\r\n                            </div>\r\n                            <div class=\"col-md-12\" *ngIf = \"config\">\r\n                                <label>This Year Carrot Supply :</label>{{config.stockCarrot}}\r\n                                <table class=\"table table-hover mt-3\">                                         \r\n                                        <tr>\r\n                                            <th scope=\"col\">Annual carrot per year</th>\r\n                                            <th scope=\"col\"><input #yearlyCarrot [(ngModel)]=\"config.yearlyCarrot\" type=\"text\" name=\"yearlyCarrot\"></th>\r\n                                        </tr>                                                                               \r\n                                        <tr>\r\n                                            <th scope=\"col\">Maximum carrot per sharing</th>\r\n                                            <th scope=\"col\"><input #carrotLimit [(ngModel)]=\"config.carrotLimit\" type=\"text\" name=\"carrotLimit\"></th>\r\n                                        </tr>                 \r\n                                </table>\r\n                                <div class=\"text-center mb-3\">\r\n                                    <button type=\"button\" class=\"btn btn-info\" (click)=\"updateConfig(yearlyCarrot.value, carrotLimit.value)\">\r\n                                        <i class=\"fa fa-plus-circle\"></i> SAVE CONFIG\r\n                                    </button>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>"

/***/ }),

/***/ "./src/app/root-admin/carrot-set/carrot-set.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarrotSetComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_user__ = __webpack_require__("./src/app/model/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_config_service__ = __webpack_require__("./src/app/service/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("./src/app/app.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CarrotSetComponent = /** @class */ (function () {
    function CarrotSetComponent(userService, configService, router, appComponent) {
        this.userService = userService;
        this.configService = configService;
        this.router = router;
        this.appComponent = appComponent;
        this.userTemp = new __WEBPACK_IMPORTED_MODULE_2__model_user__["a" /* User */];
        this.loggedInUser = new __WEBPACK_IMPORTED_MODULE_2__model_user__["a" /* User */];
    }
    CarrotSetComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInUser = this.userService.getLoggedInUser();
        if (!this.loggedInUser) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_3__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        _this.loggedInUser = res;
                        _this.userService.setLoggedInUser(res);
                    }
                    else
                        _this.router.navigate(['/']);
                });
            }
            else
                this.router.navigate(['/']);
        }
        this.getConfig();
    };
    CarrotSetComponent.prototype.getConfig = function () {
        var _this = this;
        this.configs = [];
        this.configService.getConfig().subscribe(function (configs) { return _this.config = configs[0]; });
    };
    CarrotSetComponent.prototype.updateConfig = function (yearlyCarrot, carrotLimit) {
        var _this = this;
        this.config.yearlyCarrot = yearlyCarrot;
        this.config.carrotLimit = carrotLimit;
        this.configService.updateConfig(this.config).subscribe(function (config) {
            if (config) {
                _this.appComponent.showSuccess("Config Updated");
                _this.ngOnInit;
            }
            else
                _this.appComponent.showError("Update Failed.");
        });
    };
    CarrotSetComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-carrot-set',
            template: __webpack_require__("./src/app/root-admin/carrot-set/carrot-set.component.html"),
            styles: [__webpack_require__("./src/app/root-admin/carrot-set/carrot-set.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_5__service_config_service__["a" /* ConfigService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]])
    ], CarrotSetComponent);
    return CarrotSetComponent;
}());



/***/ }),

/***/ "./src/app/root-admin/root-admin.component.css":
/***/ (function(module, exports) {

module.exports = "a:not([href]):not([tabindex]) {\r\n  color: #ff5722;\r\n  cursor: pointer;\r\n}"

/***/ }),

/***/ "./src/app/root-admin/root-admin.component.html":
/***/ (function(module, exports) {

module.exports = "<html lang=\"en\">\r\n<head>\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\r\n    <meta name=\"description\" content=\"\">\r\n    <meta name=\"author\" content=\"\">\r\n</head>\r\n\r\n<body>\r\n    <div id=\"header\"></div>\r\n\r\n    <!-- Begin page content -->\r\n    <main role=\"main\" class=\"container\">\r\n        <h2 class=\"mt-4 pl-0 text-grey ml-0\">\r\n        ROOT-ADMINISTRATOR DASHBOARD\r\n       </h2>\r\n    </main>\r\n    <section class=\"admin-tabs py-3\">\r\n        <div class=\"container\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-12 px-md-0\">\r\n                    <ul class=\"nav nav-pills mb-3\" id=\"myTab\" role=\"tablist\">\r\n                        <li class=\"nav-item\">\r\n                            <a class=\"nav-link\" (click)=\" openAdminList()\" [ngClass]=\"{'btn-success': isAdmin, 'btn-outline-success': !isAdmin }\">Admin List</a>\r\n                        </li>\r\n                        <li class=\"nav-item\">\r\n                            <a class=\"nav-link\" (click)=\" openManagerList()\" [ngClass]=\"{'btn-success': isManager, 'btn-outline-success': !isManager }\">Approve Manager</a>\r\n                        </li>\r\n                        <li class=\"nav-item\">\r\n                            <a class=\"nav-link\" (click)=\" openStaffList()\" [ngClass]=\"{'btn-success': isStaff, 'btn-outline-success': !isStaff }\">Approve Admin</a>\r\n                        </li>\r\n                        <li class=\"nav-item\">\r\n                            <a class=\"nav-link\" (click)=\" openSocial()\" [ngClass]=\"{'btn-success': isSocial, 'btn-outline-success': !isSocial }\">Approve Social Foundation Carrot</a>\r\n                        </li>\r\n                        <li class=\"nav-item\">\r\n                            <a class=\"nav-link\" (click)=\" openGiveCarrot()\" [ngClass]=\"{'btn-success': isGiveCarrot, 'btn-outline-success': !isGiveCarrot }\">Give Carrot</a>\r\n                        </li>\r\n                        <li class=\"nav-item\">\r\n                            <a class=\"nav-link\" (click)=\" openSetCarrot()\" [ngClass]=\"{'btn-success': isSetCarrot, 'btn-outline-success': !isSetCarrot }\">Set Carrot</a>\r\n                        </li>\r\n                    </ul>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section>\r\n\r\n    <div>\r\n        <app-admin-list *ngIf=\"isAdmin\" ></app-admin-list>\r\n        <app-approval-manager *ngIf=\"isManager\" ></app-approval-manager>\r\n        <app-approval-admin *ngIf=\"isStaff\" ></app-approval-admin>\r\n        <app-approval-social *ngIf=\"isSocial\" ></app-approval-social>\r\n        <app-carrot-give *ngIf=\"isGiveCarrot\" ></app-carrot-give>\r\n        <app-carrot-set *ngIf=\"isSetCarrot\" ></app-carrot-set>\r\n    </div>\r\n    \r\n    <div id=\"footer\"></div>\r\n</body>\r\n\r\n</html>"

/***/ }),

/***/ "./src/app/root-admin/root-admin.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RootAdminComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_user__ = __webpack_require__("./src/app/model/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_root_admin_service__ = __webpack_require__("./src/app/service/root-admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_admin_service__ = __webpack_require__("./src/app/service/admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_transaction_service__ = __webpack_require__("./src/app/service/transaction.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_config_service__ = __webpack_require__("./src/app/service/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_component__ = __webpack_require__("./src/app/app.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var RootAdminComponent = /** @class */ (function () {
    function RootAdminComponent(rootAdminService, adminService, userService, transactionService, configService, router, appComponent) {
        this.rootAdminService = rootAdminService;
        this.adminService = adminService;
        this.userService = userService;
        this.transactionService = transactionService;
        this.configService = configService;
        this.router = router;
        this.appComponent = appComponent;
        this.userTemp = new __WEBPACK_IMPORTED_MODULE_2__model_user__["a" /* User */];
        this.loggedInUser = new __WEBPACK_IMPORTED_MODULE_2__model_user__["a" /* User */];
    }
    RootAdminComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInUser = this.userService.getLoggedInUser();
        if (!this.loggedInUser) {
            var token = localStorage.getItem(__WEBPACK_IMPORTED_MODULE_3__config_constanta__["TOKEN_KEY_NAME"]);
            if (token) {
                this.userService.loginWithToken(token)
                    .subscribe(function (res) {
                    if (res) {
                        _this.loggedInUser = res;
                        _this.userService.setLoggedInUser(res);
                    }
                    else
                        _this.router.navigate(['/']);
                });
            }
            else
                this.router.navigate(['/']);
        }
        this.openAdminList();
    };
    RootAdminComponent.prototype.openAdminList = function () {
        this.isAdmin = true;
        this.isStaff = false;
        this.isManager = false;
        this.isSocial = false;
        this.isSetCarrot = false;
        this.isGiveCarrot = false;
    };
    RootAdminComponent.prototype.openManagerList = function () {
        this.isAdmin = false;
        this.isStaff = false;
        this.isManager = true;
        this.isSocial = false;
        this.isSetCarrot = false;
        this.isGiveCarrot = false;
    };
    RootAdminComponent.prototype.openStaffList = function () {
        this.isAdmin = false;
        this.isStaff = true;
        this.isManager = false;
        this.isSocial = false;
        this.isSetCarrot = false;
        this.isGiveCarrot = false;
    };
    RootAdminComponent.prototype.openSocial = function () {
        this.isAdmin = false;
        this.isStaff = false;
        this.isManager = false;
        this.isSocial = true;
        this.isSetCarrot = false;
        this.isGiveCarrot = false;
    };
    RootAdminComponent.prototype.openSetCarrot = function () {
        this.isAdmin = false;
        this.isStaff = false;
        this.isManager = false;
        this.isSocial = false;
        this.isSetCarrot = true;
        this.isGiveCarrot = false;
    };
    RootAdminComponent.prototype.openGiveCarrot = function () {
        this.isAdmin = false;
        this.isStaff = false;
        this.isManager = false;
        this.isSocial = false;
        this.isSetCarrot = false;
        this.isGiveCarrot = true;
    };
    RootAdminComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root-admin',
            template: __webpack_require__("./src/app/root-admin/root-admin.component.html"),
            styles: [__webpack_require__("./src/app/root-admin/root-admin.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__service_root_admin_service__["a" /* RootAdminService */],
            __WEBPACK_IMPORTED_MODULE_5__service_admin_service__["a" /* AdminService */],
            __WEBPACK_IMPORTED_MODULE_6__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_7__service_transaction_service__["a" /* TransactionService */],
            __WEBPACK_IMPORTED_MODULE_8__service_config_service__["a" /* ConfigService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* AppComponent */]])
    ], RootAdminComponent);
    return RootAdminComponent;
}());



/***/ }),

/***/ "./src/app/senior-manager/senior-manager.component.css":
/***/ (function(module, exports) {

module.exports = ".actionmanage {\r\n    color: #156fca !important; \r\n}\r\n\r\n.actioncarrot {\r\n    color: #e95321  !important; \r\n}\r\n\r\n.carrotLeft{\r\n    color: #ff5722 !important;\r\n}\r\n\r\n/* managersComponent's private CSS styles */\r\n\r\n.selected {\r\n    background-color: #CFD8DC !important;\r\n    color: white;\r\n  }\r\n\r\n.managers {\r\n    color: #444;\r\n    font-family: Arial, Helvetica, sans-serif;\r\n    font-weight: lighter;\r\n    margin: 0 0 2em 0;\r\n    list-style-type: none;\r\n    padding: .3em 0;\r\n    width: 15em;\r\n  }\r\n\r\n.managers li {\r\n    cursor: pointer;\r\n    position: relative;\r\n    left: 0;\r\n    background-color: #EEE;\r\n    margin: 1em;\r\n    /* padding: .3em 0; */\r\n    height: 1.6em;\r\n    border-radius: 4px;\r\n  }\r\n\r\n.managers li.selected:hover {\r\n    background-color: #BBD8DC !important;\r\n    color: white;\r\n  }\r\n\r\n.managers li:hover {\r\n    color: #607D8B;\r\n    background-color: #DDD;\r\n    left: .1em;\r\n  }\r\n\r\n.managers .text {\r\n    position: relative;\r\n    top: -3px;\r\n  }\r\n\r\n.managers .badge {\r\n    display: inline-block;\r\n    font-size: small;\r\n    color: white;\r\n    padding: 0.8em 0.7em 0 0.7em;\r\n    background-color: #607D8B;\r\n    line-height: 1em;\r\n    position: relative;\r\n    left: -1px;\r\n    top: -4px;\r\n    height: 1.8em;\r\n    margin-right: .8em;\r\n    border-radius: 4px 0 0 4px;\r\n  }\r\n\r\n.crop {\r\n    width: 128px;\r\n    height: 128px;\r\n    overflow: hidden;\r\n}\r\n\r\n.crop img {\r\n    width: 400px;\r\n    height: 300px;\r\n    margin: -75px 0 0 -100px;\r\n}"

/***/ }),

/***/ "./src/app/senior-manager/senior-manager.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"mini-dashboard my-4\">\r\n    <div class=\"container\">\r\n        <mat-progress-spinner *ngIf=\"!loggedInManager\"\r\n        class=\"text-center m-auto\"\r\n        color=\"warn\"\r\n        mode=\"indeterminate\" >\r\n        </mat-progress-spinner>\r\n\r\n        <div class=\"row\" *ngIf=\"loggedInManager\" >\r\n            <div class=\"col-md-6\">\r\n                <div class=\"row box-profile soft-shadow px-0 mr-0\">\r\n                <div class=\"col-md-4 my-auto radius-5 circular--landscape\">\r\n                    <img src=\"{{loggedInManager && loggedInManager.picture ? loggedInManager.picture : '../../assets/img/user.png'}}\" alt=\"\" class=\"img-fluid rounded-circle crop\">\r\n                </div>\r\n                <div class=\"col-md-8 my-auto\">\r\n                    <h4 class=\"mb-0 text-white\">{{loggedInManager ? loggedInManager.name : 'Name'}}</h4>\r\n                    <p class=\"text-white\">Mitrais Senior Manager</p>\r\n                    <a routerLink=\"/profile\" class=\"badge badge-white\">Edit Profile</a>\r\n                </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n                <div class=\"row box-carrot px-0 mr-0\">\r\n                <div class=\"col-md-4 my-auto\">\r\n                    <img src=\"../../assets/img/mc-icon-carrot.png\" alt=\"\" class=\"img-fluid rounded-circle\">\r\n                </div>\r\n                <div class=\"col-md-8 my-auto\">\r\n                    <h4 class=\"text-white\">You have {{loggedInManager ? loggedInManager.carrot : '0'}} carrots!</h4>\r\n                    <h6 class=\"text-red\" *ngIf=\"loggedInManager.carrot <= 500\">Your carrots are getting low</h6>\r\n                </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div> \r\n</section>\r\n<section class=\"my-4\">\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12 px-md-0\">\r\n                <ul class=\"nav nav-pills mb-3\" id=\"myTab\" role=\"tablist\">\r\n                    <li class=\"nav-item\">\r\n                        <a class=\"nav-link active\" id=\"home-tab\" data-toggle=\"tab\" href=\"#home\" role=\"tab\" aria-controls=\"home\" aria-selected=\"true\" (click)=\"loadFirstGroup()\">Your Groups</a>\r\n                    </li>\r\n                    <li class=\"nav-item\">\r\n                        <a class=\"nav-link\" id=\"available-tab\" data-toggle=\"tab\" href=\"#avManager\" role=\"tab\" aria-controls=\"available\" aria-selected=\"false\" (click)=\"loadGroupAvManager()\">Available Manager</a>\r\n                    </li>\r\n                </ul>\r\n                <div class=\"tab-content search-box\" id=\"myTabContent\">\r\n                        <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">\r\n                            <div class=\"row\">\r\n                                <div class=\"form-group ml-auto\">\r\n                                    <label for=\"text2\" style=\"margin-bottom: 0px;\">Select Group</label>\r\n                                    <select name=\"user\" #user>\r\n                                        <option *ngFor = \"let group of myGroups\"><a class=\"dropdown-item form-control here\" style=\"padding : 6px\" href=\"#\" (click)=\"onSelectGroup(group)\"\r\n                                            data-toggle=\"modal\" data-dismiss=\"modal\">{{group.name}}</a></option>\r\n                                    </select>\r\n                                </div>\r\n                                <div class=\"col-md-12\"  *ngIf = \"selectedManagerGroup\">\r\n                                    <hr class=\"box-title-hr\" style=\"margin-top: 0px;\">\r\n                                    <h4 class=\"my-2 box-title\">{{selectedManagerGroup.name}} Manager List</h4>\r\n                                </div>\r\n                                <div class=\"col-md-12\" *ngIf = \"selectedManagerGroup\">\r\n                                    <mat-form-field class=\"ml-auto\">\r\n                                        <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Click Me To Search\">\r\n                                    </mat-form-field>\r\n                                    <mat-table [dataSource]=\"dataSource\" matSort>\r\n                                        <ng-container matColumnDef=\"Name\">\r\n                                            <mat-header-cell *matHeaderCellDef mat-sort-header> Name </mat-header-cell>\r\n                                            <mat-cell *matCellDef=\"let row\"> {{row.name}} </mat-cell>\r\n                                        </ng-container>\r\n\r\n                                        <ng-container matColumnDef=\"E-mail\">\r\n                                            <mat-header-cell *matHeaderCellDef mat-sort-header> E-mail </mat-header-cell>\r\n                                            <mat-cell *matCellDef=\"let row\"> {{row.email}} </mat-cell>\r\n                                        </ng-container>\r\n\r\n                                        <ng-container matColumnDef=\"Join Date\">\r\n                                            <mat-header-cell *matHeaderCellDef mat-sort-header> Join Date </mat-header-cell>\r\n                                            <mat-cell *matCellDef=\"let row\"> {{row.joinDate}} </mat-cell>\r\n                                        </ng-container>\r\n\r\n                                        <ng-container matColumnDef=\"Role\">\r\n                                            <mat-header-cell *matHeaderCellDef mat-sort-header> Role </mat-header-cell>\r\n                                            <mat-cell *matCellDef=\"let row\"> {{utility.getRole(row.role)}} </mat-cell>\r\n                                        </ng-container>\r\n\r\n                                        <ng-container matColumnDef=\"Active\">\r\n                                            <mat-header-cell *matHeaderCellDef mat-sort-header> Active </mat-header-cell>\r\n                                            <mat-cell *matCellDef=\"let row\"> {{row.active}} </mat-cell>\r\n                                        </ng-container>\r\n\r\n                                        <ng-container matColumnDef=\"Action\">\r\n                                            <mat-header-cell *matHeaderCellDef mat-sort-header> Action </mat-header-cell>\r\n                                            <mat-cell *matCellDef=\"let row\">\r\n                                                <a class=\"actionmanage\" data-toggle=\"modal\" data-target=\"#actionSeniorManager\" style=\"color : #868e96\" (click)=\"onSelect(row)\">Manage </a>|\r\n                                                <a class=\"actioncarrot\" data-toggle=\"modal\" data-target=\"#actionSeniorCarrot\" style=\"color : #868e96\" (click)=\"onSelect(row)\"> Send Carrot</a>\r\n                                            </mat-cell>\r\n                                        </ng-container>\r\n\r\n                                        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n                                        <mat-row *matRowDef=\"let row; columns: displayedColumns;\">\r\n                                        </mat-row>\r\n                                    </mat-table>\r\n                                    <mat-paginator [pageSizeOptions]=\"[10, 15, 25]\"></mat-paginator> \r\n                                    <div class=\"text-center mb-3\">\r\n                                        <!-- Button trigger modal -->\r\n                                        <button type=\"button\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#addManager\">\r\n                                            <i class=\"fa fa-plus-circle\"></i> Add Manager\r\n                                        </button>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"tab-pane fade\" id=\"avManager\" role=\"tabpanel\" aria-labelledby=\"available-tab\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-12\">\r\n                                    <hr class=\"box-title-hr\">\r\n                                    <h4 class=\"my-2 box-title\">Available Manager LIST</h4>\r\n                                </div>\r\n                                <div class=\"col-md-12\">\r\n                                    <mat-form-field class=\"ml-auto\">\r\n                                        <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Click Me To Search\">\r\n                                    </mat-form-field>\r\n                                    <mat-table [dataSource]=\"dataSource\" matSort>\r\n                                        <ng-container matColumnDef=\"Name\">\r\n                                            <mat-header-cell *matHeaderCellDef mat-sort-header> Name </mat-header-cell>\r\n                                            <mat-cell *matCellDef=\"let row\"> {{row.name}} </mat-cell>\r\n                                        </ng-container>\r\n\r\n                                        <ng-container matColumnDef=\"E-mail\">\r\n                                            <mat-header-cell *matHeaderCellDef mat-sort-header> E-mail </mat-header-cell>\r\n                                            <mat-cell *matCellDef=\"let row\"> {{row.email}} </mat-cell>\r\n                                        </ng-container>\r\n\r\n                                        <ng-container matColumnDef=\"Join Date\">\r\n                                            <mat-header-cell *matHeaderCellDef mat-sort-header> Join Date </mat-header-cell>\r\n                                            <mat-cell *matCellDef=\"let row\"> {{row.joinDate}} </mat-cell>\r\n                                        </ng-container>\r\n\r\n                                        <ng-container matColumnDef=\"Role\">\r\n                                            <mat-header-cell *matHeaderCellDef mat-sort-header> Role </mat-header-cell>\r\n                                            <mat-cell *matCellDef=\"let row\"> {{utility.getRole(row.role)}} </mat-cell>\r\n                                        </ng-container>\r\n\r\n                                        <ng-container matColumnDef=\"Active\">\r\n                                            <mat-header-cell *matHeaderCellDef mat-sort-header> Active </mat-header-cell>\r\n                                            <mat-cell *matCellDef=\"let row\"> {{row.active}} </mat-cell>\r\n                                        </ng-container>\r\n\r\n                                        <mat-header-row *matHeaderRowDef=\"displayedColumnsAvManager\"></mat-header-row>\r\n                                        <mat-row *matRowDef=\"let row; columns: displayedColumnsAvManager;\">\r\n                                        </mat-row>\r\n                                    </mat-table>\r\n                                    <mat-paginator [pageSizeOptions]=\"[10, 15, 25]\"></mat-paginator>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n\r\n    <!-- Action Manager Modal -->\r\n<div class=\"modal fade\" id=\"actionSeniorManager\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"actionSeniorManager\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Manage Manager</h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div *ngIf =\"selectedManager\" class=\"modal-body\">\r\n        <form>\r\n            <div class=\"form-group my-auto\">\r\n                <label for=\"text1\">ID</label> \r\n                <input id=\"text1\" name=\"text1\" type=\"text\" class=\"form-control here\" [(ngModel)]=\"selectedManager.id\" placeholder=\"id\" disabled>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"text2\">Name</label> \r\n                <input id=\"text2\" name=\"text2\" type=\"text\" class=\"form-control here\" [(ngModel)]=\"selectedManager.name\" placeholder=\"name\" disabled>\r\n            </div>\r\n            \r\n            <div class=\"form-group\">\r\n                <label for=\"text4\">Current Group</label> \r\n                <input id=\"text4\" name=\"text4\" type=\"text\" class=\"form-control here\" [(ngModel)]=\"selectedManagerGroup.name\" placeholder=\"Current Group\" disabled>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n                <label for=\"text5\">Managing Staff Group</label> \r\n                <input id=\"text5\" name=\"text5\" type=\"text\" class=\"form-control here\" [(ngModel)]=\"groups.name\" placeholder=\"this manager has no managing a staff group yet\" disabled>\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n                <button *ngIf=\"groupsAftFiltered.length > 1\" class=\"btn btn-secondary dropdown-toggle form-control here\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\"\r\n                    aria-haspopup=\"true\" aria-expanded=\"false\" style=\"padding:6px; background-color: #17a2b8; font-size: 0.7rem\" (click)=\"moveToNoSelected(selectedManagerGroup.id)\">\r\n                        Move From Group\r\n                </button>\r\n                <div class=\"dropdown-menu form-group\" aria-labelledby=\"dropdownMenuButton\">\r\n                        <a class=\"dropdown-item form-control here\" style=\"padding : 6px\" href=\"#\" *ngFor=\"let foot of groupsAftFiltered\" (click)=\"onSelectNewManagerGroup(foot)\"\r\n                        data-toggle=\"modal\" data-target=\"#moveManager\" data-dismiss=\"modal\">{{foot.name}}</a>\r\n                </div>\r\n            </div>\r\n            <div class=\"text-center form-group col-md-12\">\r\n                <a class=\"text-center\" id=\"removeButton\" data-toggle=\"modal\" data-dismiss=\"modal\" \r\n                    aria-haspopup=\"true\" aria-expanded=\"false\" style=\"padding:6px; color: #ff2222; font-size: 0.8rem; width: 100%\" data-target=\"#removeManager\">\r\n                        Remove from Group\r\n                </a>\r\n            </div>\r\n        </form>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-link \" data-dismiss=\"modal\">Cancel</button>\r\n        <!-- <button type=\"button\" class=\"btn btn-carrot radius-5\">Submit</button> -->\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n    <!-- Action Carrot Modal -->\r\n<!-- <div *ngIf=\"selectedManager\" class=\"modal fade\" id=\"actionSeniorCarrot\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"actionSeniorCarrot\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Send Carrot</h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n        </div>\r\n        <div *ngIf =\"selectedManager\" class=\"modal-body\">\r\n            <form>\r\n                <div class=\"form-group\">\r\n                    <label for=\"name\">Recipient</label> \r\n                    <input name=\"name\" type=\"text\" class=\"form-control here\" placeholder=\"Autocomplete + dropdown\" [(ngModel)]=\"selectedManager.name\" disabled />\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <label for=\"description\">Comments</label> \r\n                    <textarea #description id=\"description\" name=\"description\" cols=\"40\" rows=\"5\" class=\"form-control\" placeholder=\"Happy birthday broooo!!!!!!\" required ></textarea>\r\n                </div> \r\n                <div class=\"form-group\">\r\n                    <label for=\"amount\">Carrot</label> \r\n                    <input #amount (input)=\"changeAmount(amount.value)\" name=\"amount\" type=\"number\" class=\"form-control here\" [min]=1 [max]=\"loggedInManager.carrot\" value={{amountValue}} required >\r\n                </div> \r\n            </form>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n            <button type=\"button\" class=\"btn btn-link float-left\" data-dismiss=\"modal\">Cancel</button>\r\n            <button (click)=\"sendCarrot(description.value)\" data-dismiss=\"modal\" type=\"button\" class=\"btn btn-carrot radius-5 float-right\">Send Carrot</button>\r\n        </div>\r\n    </div>\r\n  </div>\r\n</div> -->\r\n\r\n<!-- Action Carrot Modal -->\r\n<div *ngIf=\"selectedManager\" class=\"modal fade\" id=\"actionSeniorCarrot\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"actionCarrotLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n            <h5 class=\"modal-title\" id=\"actionCarrotLabel\">Share your carrot!</h5>\r\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n            </button>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n            <form>\r\n            <div class=\"form-group\">\r\n                <label for=\"name\">Recipient</label> \r\n                <input name=\"name\" type=\"text\" class=\"form-control here\" placeholder=\"Autocomplete + dropdown\" [(ngModel)]=\"selectedManager.name\" disabled />\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"description\">Comments</label> \r\n                <textarea #description id=\"description\" name=\"description\" cols=\"40\" rows=\"5\" class=\"form-control\" placeholder=\"Happy birthday broooo!!!!!!\" required ></textarea>\r\n            </div> \r\n            <div class=\"form-group\">\r\n                <label for=\"amount\">Carrot</label> \r\n                <input #amount (input)=\"changeAmount(amount.value)\" name=\"amount\" type=\"number\" class=\"form-control here\" [min]=1 [max]=\"loggedInManager.carrot\" value={{amountValue}} required >\r\n            </div> \r\n            </form>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n            <button type=\"button\" class=\"btn btn-link float-left\" data-dismiss=\"modal\">Cancel</button>\r\n            <button (click)=\"sendCarrot(description.value)\" data-dismiss=\"modal\" type=\"button\" class=\"btn btn-carrot radius-5 float-right\">Send Carrot</button>\r\n        </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n    <!-- Action Confirmation Carrot Modal -->\r\n<div class=\"modal fade\" id=\"confirmCarrot\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"actionCarrot\" aria-hidden=\"true\" *ngIf=\"selectedManager\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n            <h5 class=\"modal-title\" id=\"exampleModalLabel\">Send Carrot</h5>\r\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n            </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <form>\r\n                    <div class=\"form-group my-auto\">\r\n                        <label for=\"text1\">You already sent {{ carrotvalue }} carrots to {{ selectedManager.name }}</label>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-carrot radius-5\" data-dismiss=\"modal\">OK</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n    <!-- Action Add Manager Modal -->\r\n<div class=\"modal fade\" id=\"addManager\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"addManager\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\" style=\"margin: auto; width : 750px\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">Add Employee</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"row col-md-12\">\r\n                <div class=\"col-md-6\">\r\n                    <table class=\"table table-hover mt-3\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th rowspan=\"1\">Available Manager</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <div *ngFor=\"let user of unGroupManagers\"\r\n                                [class.selected]=\"user === selectedManager\"\r\n                                (click)=\"onSelectManagerToMove(user)\" class=\"col-md-12\"\r\n                                style=\"height: 50px; padding: 10px\">\r\n                                    <div class=\"row soft-shadow p-0 m-2\">\r\n                                        <div>\r\n                                            <img src=\"{{user && user.picture ? user.picture : '../../assets/img/user.png'}}\" alt=\"\" class=\"img-fluid rounded-circle\" style=\"width: 50px; height: 50px; padding: 10px\">\r\n                                        </div>\r\n                                        <div>\r\n                                            <a style=\"color: dimgray !important\"><h6 style=\"margin-bottom: 0px\">{{user.name}}</h6></a>\r\n                                            <a style=\"color: dimgray !important\">{{user.email}}</a>\r\n                                        </div>\r\n                                    </div>\r\n                            </div>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n                <div class=\"col-md-6\">\r\n                    <table class=\"table table-hover mt-3\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th rowspan=\"1\">Added Manager</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <div *ngFor=\"let user of newManagers\"\r\n                                [class.selected]=\"user === selectedManager\"\r\n                                (click)=\"onRemoveManagerToMove(user)\" class=\"col-md-12\"\r\n                                style=\"height: 50px; padding: 10px\">\r\n                                    <div class=\"row soft-shadow p-0 m-2\">\r\n                                        <div>\r\n                                            <img src=\"{{user && user.picture ? user.picture : '../../assets/img/user.png'}}\" alt=\"\" class=\"img-fluid rounded-circle\" style=\"width: 50px; height: 50px; padding: 10px\">\r\n                                        </div>\r\n                                        <div>\r\n                                            <a style=\"color: dimgray !important\"><h6 style=\"margin-bottom: 0px\">{{user.name}}</h6></a>\r\n                                            <a style=\"color: dimgray !important\">{{user.email}}</a>\r\n                                        </div>\r\n                                    </div>\r\n                            </div>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-link \" data-dismiss=\"modal\">Cancel</button>\r\n                <button type=\"button\" class=\"btn btn-carrot radius-5\" data-dismiss=\"modal\" (click)=\"onSubmit()\">Submit</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n    <!-- Remove Manager Modal -->\r\n<div class=\"modal fade\" id=\"removeManager\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"removeManager\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n            <!-- <h5 class=\"modal-title\" id=\"exampleModalLabel\">Manage Manager</h5> -->\r\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n            </button>\r\n        </div>\r\n        <div *ngIf =\"selectedManager\" class=\"modal-body\">\r\n            <form>\r\n            <div class=\"form-group my-auto\">\r\n                <label for=\"text1\">Do you really want to remove {{ selectedManager.name }} from {{ selectedManagerGroup.name }} ?</label>\r\n            </div>\r\n            </form>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n            <button type=\"button\" class=\"btn btn-link\" data-dismiss=\"modal\" (click)=\"onRemoveManager()\">Yes</button>\r\n            <button type=\"button\" class=\"btn btn-carrot radius-5\" data-toggle=\"modal\" data-dismiss=\"modal\" data-target=\"#actionSeniorManager\">No</button>\r\n        </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n    <!-- Move Manager Modal -->\r\n<div class=\"modal fade\" id=\"moveManager\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"removeManager\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n            <!-- <h5 class=\"modal-title\" id=\"exampleModalLabel\">Manage Manager</h5> -->\r\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n            </button>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n            <div class=\"form-group my-auto\">\r\n                <label for=\"text1\" *ngIf=\"selectedNewManagerGroup\">Do you really want to Move {{ selectedManager.name }} from {{ selectedManagerGroup.name }} to {{selectedNewManagerGroup.name}}?</label>\r\n            </div>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n            <button type=\"button\" class=\"btn btn-link\" (click)=\"selectNewManagerGroup()\" data-dismiss=\"modal\">Yes</button>\r\n            <button type=\"button\" class=\"btn btn-carrot radius-5\" data-toggle=\"modal\" data-dismiss=\"modal\" data-target=\"#actionSeniorManager\">No</button>\r\n        </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/senior-manager/senior-manager.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SeniorManagerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__model_manager_group__ = __webpack_require__("./src/app/model/manager-group.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_user_service__ = __webpack_require__("./src/app/service/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_group_service__ = __webpack_require__("./src/app/service/group.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_manager_group_service__ = __webpack_require__("./src/app/service/manager-group.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_manager_service__ = __webpack_require__("./src/app/service/manager.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__service_transaction_service__ = __webpack_require__("./src/app/service/transaction.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angular_datatables__ = __webpack_require__("./node_modules/angular-datatables/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__config_utility__ = __webpack_require__("./src/app/config/utility.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var SeniorManagerComponent = /** @class */ (function () {
    function SeniorManagerComponent(route, userService, groupService, managerGroupService, managerService, transactionService, appComponent, location, changeDetectorRefs, router) {
        this.route = route;
        this.userService = userService;
        this.groupService = groupService;
        this.managerGroupService = managerGroupService;
        this.managerService = managerService;
        this.transactionService = transactionService;
        this.appComponent = appComponent;
        this.location = location;
        this.changeDetectorRefs = changeDetectorRefs;
        this.router = router;
        this.displayedColumns = ['Name', 'E-mail', 'Join Date', 'Role', 'Active', 'Action'];
        this.displayedColumnsAvManager = ['Name', 'E-mail', 'Join Date', 'Role', 'Active'];
        this.dtOptions = {};
        this.dtTrigger = new __WEBPACK_IMPORTED_MODULE_13_rxjs__["Subject"]();
        this.utility = __WEBPACK_IMPORTED_MODULE_14__config_utility__;
        this.managerStaffGroups = [];
        this.newManagers = new Array();
        this.groupsAftFiltered = new Array();
        this.amountValue = 0;
        this.inputedCarrot = false;
    }
    SeniorManagerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInManager = this.userService.getLoggedInUser();
        if (!this.loggedInManager)
            this.router.navigate(['/']);
        this.loggedInManager = this.userService.getLoggedInUser();
        this.managerGroupService.getAllManGroups()
            .subscribe(function (managerGroups) {
            console.log(managerGroups);
            _this.managerGroups = managerGroups;
        });
        this.managerService.getAllManager()
            .subscribe(function (managers) {
            console.log(managers);
            _this.managers = managers;
            _this.manLength = _this.managers.length;
        });
        // used ----------------------------------------------------------------
        this.refresh();
        // this.loadOtherGroups();
    };
    // used --------------------------------------------------------------------
    SeniorManagerComponent.prototype.refresh = function () {
        var _this = this;
        this.managerGroupService.fetchMyGroup(this.loggedInManager.id)
            .subscribe(function (manGroup) {
            _this.myGroups = manGroup;
            // console.log("my group", this.myGroups);
            _this.loadFirstGroup();
            _this.changeDetectorRefs.detectChanges();
        });
        this.managerService.getUnGroupManagers()
            .subscribe(function (managers) {
            _this.unGroupManagers = managers;
            _this.changeDetectorRefs.detectChanges();
            // this.loadGroupAvManager();
        });
    };
    SeniorManagerComponent.prototype.createMatTable = function (users) {
        this.dataSource = new __WEBPACK_IMPORTED_MODULE_2__angular_material__["h" /* MatTableDataSource */](users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    SeniorManagerComponent.prototype.onSelectGroup = function (group) {
        var _this = this;
        this.selectedManagerGroup = group;
        this.createMatTable(group.members);
        this.myGroups.forEach(function (mg) {
            if (mg.id != _this.selectedManagerGroup.id) {
                _this.groupsAftFiltered.push(mg);
            }
        });
    };
    SeniorManagerComponent.prototype.onSelect = function (manager) {
        var _this = this;
        this.selectedManager = manager;
        this.groups = [];
        this.groupService.getGroupByManID(this.selectedManager.id).subscribe(function (gr) {
            _this.groups = gr;
        });
    };
    SeniorManagerComponent.prototype.loadFirstGroup = function () {
        this.selectedManagerGroup = this.myGroups[0];
        this.createMatTable(this.selectedManagerGroup.members);
    };
    // -------------- Add Manager ------------------------------------------
    SeniorManagerComponent.prototype.onSelectManagerToMove = function (manager) {
        this.selectedUnGroupManager = manager;
        this.unGroupManagers = this.unGroupManagers.filter(function (man) {
            return man.id != manager.id;
        });
        this.newManagers.push(manager);
        console.log(this.newManagers);
    };
    SeniorManagerComponent.prototype.onRemoveManagerToMove = function (manager) {
        this.selectedUnGroupManager = manager;
        this.onRemoveManagerToMoves(this.selectedUnGroupManager);
    };
    SeniorManagerComponent.prototype.onRemoveManagerToMoves = function (manager) {
        this.newManagers = this.newManagers.filter(function (man) {
            return man.id != manager.id;
        });
        this.unGroupManagers.push(manager);
    };
    SeniorManagerComponent.prototype.onSubmit = function () {
        var _this = this;
        this.newManagers.forEach(function (nm) {
            _this.selectedManagerGroup.membersId.push(nm.id);
        });
        // this.parentComponent.emit(true);
        this.managerGroupService.addEdit(this.selectedManagerGroup).subscribe(function (sm) {
            _this.selectedManagerGroup = sm;
            if (sm) {
                _this.appComponent.showSuccess("SuccessFully added");
            }
            else {
                _this.appComponent.showError("Unsuccessfully");
            }
            _this.refresh();
        });
        this.newManagers = null;
    };
    SeniorManagerComponent.prototype.onRemoveManager = function () {
        var _this = this;
        var membersId = [];
        this.unGroupManagers.push(this.selectedManager);
        if (this.selectedManagerGroup.members.length > 0) {
            this.selectedManagerGroup.members.forEach(function (member) {
                if (_this.selectedManager.id != member.id) {
                    membersId.push(member.id);
                }
            });
            this.selectedManagerGroup.membersId = membersId;
            this.managerGroupService.addEdit(this.selectedManagerGroup).subscribe(function (man) {
                _this.selectedManagerGroup = man;
                if (man) {
                    _this.appComponent.showSuccess("Successfully Removed");
                }
                else {
                    _this.appComponent.showError('Unsuccessfully Removed');
                }
                _this.refresh();
            });
        }
    };
    ;
    SeniorManagerComponent.prototype.loadOtherGroups = function () {
        var _this = this;
        var manGroup = new Array();
        this.managerGroups.forEach(function (mg) {
            if (mg.seniorManagerId != _this.loggedInManager.id) {
                manGroup.push(mg);
            }
        });
        this.managerGroups = new Array();
        this.managerGroups = manGroup;
        var mana = new __WEBPACK_IMPORTED_MODULE_4__model_manager_group__["a" /* ManagerGroup */];
        this.managerGroups.forEach(function (mg) {
            mg.members.forEach(function (mem) {
                mana.members.push(mem);
            });
        });
        // this.createMatTable(mana.members);
    };
    // --- --------------------------------------------------------------------
    SeniorManagerComponent.prototype.loadGroupAvManager = function () {
        var membersid = [];
        var members = new Array();
        for (var i = 0; i < this.unGroupManagers.length; i++) {
            membersid.push(this.unGroupManagers[i].id);
            members.push(this.unGroupManagers[i]);
        }
        this.createMatTable(members);
    };
    SeniorManagerComponent.prototype.onSelectNewManagerGroup = function (managerGroup) {
        this.selectedNewManagerGroup = managerGroup;
    };
    SeniorManagerComponent.prototype.selectManagerGroup = function (managerGroup) {
        this.selectedManagerGroup = new __WEBPACK_IMPORTED_MODULE_4__model_manager_group__["a" /* ManagerGroup */];
        this.selectedManagerGroup = managerGroup;
        var groupMembers = new Array();
        this.selectedManagerGroup.members.forEach(function (member) {
            groupMembers.push(member);
        });
    };
    SeniorManagerComponent.prototype.selectNewManagerGroup = function () {
        this.selectedNewManagerGroup.membersId.push(this.selectedManager.id);
        this.managerGroupService.addEdit(this.selectedNewManagerGroup);
        this.onRemoveManager();
    };
    SeniorManagerComponent.prototype.calCarrot = function (value) {
        this.carrotvalue = value;
        console.log(value);
        if (value > 0)
            this.inputedCarrot = true;
        else
            this.inputedCarrot = false;
    };
    SeniorManagerComponent.prototype.sendCarrot = function (description) {
        var _this = this;
        if (!this.loggedInManager) {
            this.loggedInManager = this.userService.getLoggedInUser();
        }
        var senderId = this.loggedInManager.id;
        var receiverId = this.selectedManager.id;
        var type = __WEBPACK_IMPORTED_MODULE_11__config_constanta__["SHARED_TYPE"];
        var amount = this.amountValue;
        if (amount > 0) {
            this.transactionService.postTransaction({ type: type, senderId: senderId, receiverId: receiverId, amount: amount, description: description })
                .subscribe(function (res) {
                console.log(res);
                if (res) {
                    _this.loggedInManager.carrot -= amount;
                    _this.selectedManager.carrot += amount;
                    _this.appComponent.showSuccess('Send Success!');
                }
                else {
                    _this.appComponent.showError('Send Failed!');
                }
            });
        }
        else {
            this.appComponent.showError('Send Failed!');
        }
    };
    SeniorManagerComponent.prototype.moveToNoSelected = function (idManager) {
        var groupsAftFiltered = new Array();
        // this.groupsAftFiltered = (this.managerGroups);
        console.log(idManager);
        for (var index = 0; index < this.manGroupLength; index++) {
            if (this.managerGroups[index].id != idManager) {
                console.log(this.managerGroups[index]);
                groupsAftFiltered.push(this.managerGroups[index]);
            }
        }
        console.log(this.groupsAftFiltered);
        this.groupsAftFiltered = groupsAftFiltered;
    };
    SeniorManagerComponent.prototype.changeAmount = function (amount) {
        this.amountValue = amount;
        if (this.amountValue > this.loggedInManager.carrot) {
            this.amountValue = this.loggedInManager.carrot;
        }
    };
    SeniorManagerComponent.prototype.applyFilter = function (filterValue, div) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["c" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__angular_material__["c" /* MatPaginator */])
    ], SeniorManagerComponent.prototype, "paginator", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["f" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__angular_material__["f" /* MatSort */])
    ], SeniorManagerComponent.prototype, "sort", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_12_angular_datatables__["a" /* DataTableDirective */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_12_angular_datatables__["a" /* DataTableDirective */])
    ], SeniorManagerComponent.prototype, "dtElement", void 0);
    SeniorManagerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-senior-manager',
            template: __webpack_require__("./src/app/senior-manager/senior-manager.component.html"),
            styles: [__webpack_require__("./src/app/senior-manager/senior-manager.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_5__service_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_6__service_group_service__["a" /* GroupService */],
            __WEBPACK_IMPORTED_MODULE_7__service_manager_group_service__["a" /* ManagerGroupService */],
            __WEBPACK_IMPORTED_MODULE_8__service_manager_service__["a" /* ManagerService */],
            __WEBPACK_IMPORTED_MODULE_9__service_transaction_service__["a" /* TransactionService */],
            __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], SeniorManagerComponent);
    return SeniorManagerComponent;
}());



/***/ }),

/***/ "./src/app/service/admin.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' })
};
var AdminService = /** @class */ (function () {
    function AdminService(http, datepipe) {
        this.http = http;
        this.datepipe = datepipe;
        this.rewardUrl = __WEBPACK_IMPORTED_MODULE_3__config_constanta__["API_URL"] + 'content/';
        this.rewardUrlPlus = __WEBPACK_IMPORTED_MODULE_3__config_constanta__["API_URL"] + 'content/plus';
        //private approvalUrl = 'api/adminApprovals';
        this.userUrl = __WEBPACK_IMPORTED_MODULE_3__config_constanta__["API_URL"] + 'user/plus';
    }
    AdminService.prototype.getRewards = function () {
        return this.http.get(this.rewardUrlPlus);
    };
    AdminService.prototype.getUsers = function () {
        return this.http.get(this.userUrl);
    };
    // getApprovals(): Observable<AdminApproval[]> 
    // {
    //   return this.http.get<AdminApproval[]>(this.approvalUrl);
    // }
    AdminService.prototype.addReward = function (reward) {
        console.log(reward);
        return this.http.post(this.rewardUrl, reward, httpOptions);
    };
    AdminService.prototype.deleteReward = function (content) {
        console.log(content);
        return this.http.post(this.rewardUrl, content, httpOptions);
    };
    AdminService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["DatePipe"]])
    ], AdminService);
    return AdminService;
}());



/***/ }),

/***/ "./src/app/service/config.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' })
};
var ConfigService = /** @class */ (function () {
    function ConfigService(http) {
        this.http = http;
        this.configUrl = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'config';
        this.config = null;
    }
    ConfigService.prototype.getConfigData = function () {
        var _this = this;
        if (!this.config) {
            this.getConfig().subscribe(function (res) {
                if (res) {
                    console.log(res);
                    _this.config = res[0];
                }
                return _this.config;
            });
        }
        else
            return this.config;
    };
    ConfigService.prototype.getConfig = function () {
        return this.http.get(this.configUrl);
    };
    ConfigService.prototype.updateConfig = function (config) {
        return this.http.post(this.configUrl, config, httpOptions);
    };
    ConfigService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], ConfigService);
    return ConfigService;
}());



/***/ }),

/***/ "./src/app/service/content.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContentService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' })
};
var ContentService = /** @class */ (function () {
    function ContentService(http) {
        // if (!this.contents) {
        //   this.fetchContents()
        //   .subscribe(contents => {
        //     console.log(contents);
        //     for(let content of contents){
        //       content.expired = Utility.transformDate(content.expired, 'longDate');
        //     }
        //     this.contents = contents;
        //   });
        // }
        this.http = http;
        this.contentsUrl = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'content/';
        this.contentsByTypeUrl = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'content/type/';
        this.contentPlusUrl = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'content/plus/';
    }
    // getAll(): Content[] {
    //   return this.contents;
    // }
    ContentService.prototype.fetchContents = function () {
        try {
            var res = this.http.get(this.contentsUrl);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    ContentService.prototype.fetchContentsByType = function (type, groupId) {
        if (groupId === void 0) { groupId = null; }
        var url = this.contentsByTypeUrl + type + '/';
        if (groupId)
            url += groupId;
        console.log(url);
        try {
            var res = this.http.get(url);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    ContentService.prototype.fetchContentPlus = function (type, userId) {
        var url = this.contentPlusUrl + type + '/' + userId;
        console.log(url);
        try {
            var res = this.http.get(url);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    ContentService.prototype.postContent = function (content) {
        try {
            var res = this.http.post(this.contentsUrl, content, httpOptions);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    ContentService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], ContentService);
    return ContentService;
}());



/***/ }),

/***/ "./src/app/service/group.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' })
};
var GroupService = /** @class */ (function () {
    function GroupService(http) {
        this.http = http;
        this.groupsUrl = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'group/';
        this.groupByManIDUrl = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'group/supervisor/';
        this.managerGroupsUrl = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'manager-group/';
    }
    GroupService.prototype.getAllGroups = function () {
        try {
            var res = this.http.get(this.groupsUrl);
            return res;
        }
        catch (error) {
            return error;
        }
    };
    GroupService.prototype.getAllManagerGroups = function () {
        try {
            var res = this.http.get(this.managerGroupsUrl);
            return res;
        }
        catch (error) {
            return error;
        }
    };
    GroupService.prototype.getGroupById = function (id) {
        var url = this.groupsUrl + id;
        return this.http.get(url);
    };
    GroupService.prototype.getGroupByManID = function (manID) {
        try {
            var res = this.http.get(this.groupByManIDUrl + manID);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    GroupService.prototype.updateGroup = function (group) {
        return this.http.post(this.groupsUrl, group, httpOptions);
    };
    GroupService.prototype.updateManagerGroup = function (group) {
        return this.http.post(this.managerGroupsUrl, group, httpOptions);
    };
    GroupService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], GroupService);
    return GroupService;
}());



/***/ }),

/***/ "./src/app/service/manager-group.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManagerGroupService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' })
};
var ManagerGroupService = /** @class */ (function () {
    function ManagerGroupService(http) {
        this.http = http;
        this.manGroupsUrl = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'manager-group';
        this.fetchManGroupsBySMIDUrl = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'manager-group/supervisor/';
    }
    ManagerGroupService.prototype.getAllManGroups = function () {
        try {
            var res = this.http.get(this.manGroupsUrl);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    // removeManagerFromGroup (managerGroup: ManagerGroup): ManagerGroup 
    // {
    //   try {
    //     this.http.post<ManagerGroup>(this.manGroupsUrl, managerGroup, httpOptions).subscribe(res => {
    //       return res;
    //     });      
    //   } catch (error) {
    //     return error;
    //   }
    // }
    ManagerGroupService.prototype.addEdit = function (managerGroup) {
        try {
            var res = this.http.post(this.manGroupsUrl, managerGroup, httpOptions);
            return res;
        }
        catch (error) {
            return error;
        }
    };
    // addManager(managerGroup: ManagerGroup): ManagerGroup
    // {
    //   try {
    //     this.http.post<ManagerGroup>(this.manGroupsUrl, managerGroup, httpOptions).subscribe(res =>{
    //       return res;
    //     });
    //   } catch (error) {
    //     return error;
    //   }
    // }
    ManagerGroupService.prototype.fetchMyGroup = function (manID) {
        try {
            var res = this.http.get(this.fetchManGroupsBySMIDUrl + manID);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    ManagerGroupService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], ManagerGroupService);
    return ManagerGroupService;
}());



/***/ }),

/***/ "./src/app/service/manager.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManagerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' })
};
var ManagerService = /** @class */ (function () {
    function ManagerService(http) {
        this.http = http;
        this.getAllManUrl = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'user/manager';
        this.getUGMans = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'user/ungroup-manager';
    }
    ManagerService.prototype.getAllManager = function () {
        try {
            var res = this.http.get(this.getAllManUrl);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    ManagerService.prototype.getUnGroupManagers = function () {
        try {
            var res = this.http.get(this.getUGMans);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    ManagerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], ManagerService);
    return ManagerService;
}());



/***/ }),

/***/ "./src/app/service/notification.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NotificationService = /** @class */ (function () {
    function NotificationService(http) {
        this.http = http;
        this.notificationUrl = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'notification';
        this.notificationByUserUrl = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'notification/user/';
        // // Create an Observable out of a promise
        // const data = fromPromise(fetch(this.getAllUsersUrl));
        // // Subscribe to begin listening for async result
        // data.subscribe({
        //   next(response) { console.log(response); },
        //   error(err) { console.error('Error: ' + err); },
        //   complete() { console.log('Completed'); }
        // });
    }
    NotificationService.prototype.getAll = function () {
        try {
            var res = this.http.get(this.notificationUrl);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    NotificationService.prototype.getByUser = function (userId) {
        try {
            var res = this.http.get(this.notificationByUserUrl + userId);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    NotificationService.prototype.addEdit = function (notification) {
        try {
            var res = this.http.post(this.notificationUrl, notification);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
        // return this.http.post<User>(this.signUpUrl, user, httpOptions)
        //   .pipe(
        //     catchError(this.handleError('signUp', user))
        //   );
    };
    NotificationService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], NotificationService);
    return NotificationService;
}());



/***/ }),

/***/ "./src/app/service/root-admin.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RootAdminService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' })
};
var RootAdminService = /** @class */ (function () {
    //private approvalUrl = 'api/rootAdminApprovals';
    function RootAdminService(http) {
        this.http = http;
        this.userUrl = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'user';
    }
    RootAdminService.prototype.getUsers = function () {
        return this.http.get(this.userUrl);
    };
    RootAdminService.prototype.updateUser = function (user) {
        return this.http.post(this.userUrl, user, httpOptions);
    };
    RootAdminService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], RootAdminService);
    return RootAdminService;
}());



/***/ }),

/***/ "./src/app/service/transaction.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransactionService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' })
};
var TransactionService = /** @class */ (function () {
    function TransactionService(http) {
        this.http = http;
        this.transactionsUrl = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'transaction';
        this.transactionsUrlReject = __WEBPACK_IMPORTED_MODULE_2__config_constanta__["API_URL"] + 'transaction/approve';
    }
    // getAll(): Transaction[] {
    //     return this.transactions;
    // }
    // setAll(transactions: Transaction[]){
    //   this.transactions = transactions;
    // }
    TransactionService.prototype.fetchTransactions = function () {
        try {
            var res = this.http.get(this.transactionsUrl);
            return res;
        }
        catch (error) {
            return error;
        }
    };
    TransactionService.prototype.fetchTransactionsByUser = function (userId) {
        /**
         * for not logged in user
         */
        if (!userId)
            return this.fetchTransactions();
        try {
            var res = this.http.get(this.transactionsUrl + '/user/' + userId);
            return res;
        }
        catch (error) {
            return error;
        }
    };
    TransactionService.prototype.postTransaction = function (transaction) {
        try {
            var res = this.http.post(this.transactionsUrl, transaction, httpOptions);
            return res;
        }
        catch (error) {
            return error;
        }
    };
    TransactionService.prototype.postTransactionReject = function (transaction) {
        try {
            var res = this.http.post(this.transactionsUrlReject, transaction, httpOptions);
            return res;
        }
        catch (error) {
            return error;
        }
    };
    TransactionService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], TransactionService);
    return TransactionService;
}());



/***/ }),

/***/ "./src/app/service/user.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__ = __webpack_require__("./node_modules/rxjs/_esm5/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constanta__ = __webpack_require__("./src/app/config/constanta.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' })
};
var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
        this.userUrl = __WEBPACK_IMPORTED_MODULE_3__config_constanta__["API_URL"] + 'user/';
        this.userPlusUrl = __WEBPACK_IMPORTED_MODULE_3__config_constanta__["API_URL"] + 'user/plus';
        this.birthdayUsersUrl = __WEBPACK_IMPORTED_MODULE_3__config_constanta__["API_URL"] + 'user/birthday/';
        this.usersBySupervisorUrl = __WEBPACK_IMPORTED_MODULE_3__config_constanta__["API_URL"] + 'user/supervisor/';
        this.loginUrl = __WEBPACK_IMPORTED_MODULE_3__config_constanta__["API_URL"] + 'session/login';
        this.loginWithTokenUrl = __WEBPACK_IMPORTED_MODULE_3__config_constanta__["API_URL"] + 'session/token';
        this.uGUserUrl = __WEBPACK_IMPORTED_MODULE_3__config_constanta__["API_URL"] + 'user/ungroup-employee';
        this.ungrouppedManagerUrl = __WEBPACK_IMPORTED_MODULE_3__config_constanta__["API_URL"] + 'user/ungroup-manager';
        this.loggedInUser = null;
        // // Create an Observable out of a promise
        // const data = fromPromise(fetch(this.getAllUsersUrl));
        // // Subscribe to begin listening for async result
        // data.subscribe({
        //   next(response) { console.log(response); },
        //   error(err) { console.error('Error: ' + err); },
        //   complete() { console.log('Completed'); }
        // });
    }
    // getAll(): User[] {
    //   return this.users;
    // }
    // setAll(users: User[]): void {
    //   if (users && users.length > 0) {
    //     this.users = users;
    //   }
    // }
    UserService.prototype.logout = function () {
        this.loggedInUser = null;
        localStorage.removeItem(__WEBPACK_IMPORTED_MODULE_3__config_constanta__["TOKEN_KEY_NAME"]);
    };
    UserService.prototype.setLoggedInUser = function (user) {
        // user.subscribe(user => {
        //   console.log(user);
        // if(user && user.id){
        this.loggedInUser = user;
        // }
        // });
    };
    UserService.prototype.getLoggedInUser = function () {
        return this.loggedInUser;
    };
    UserService.prototype.getUngroupManager = function () {
        return this.http.get(this.ungrouppedManagerUrl);
    };
    UserService.prototype.fetchUsers = function () {
        try {
            var res = this.http.get(this.userUrl);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    UserService.prototype.fetchUsersById = function (id) {
        try {
            var res = this.http.get(this.userUrl + id);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    UserService.prototype.fetchUsersBySupervisor = function (supervisorId) {
        try {
            var res = this.http.get(this.usersBySupervisorUrl + supervisorId);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    UserService.prototype.fetchBirthdayUsers = function (day) {
        if (!day)
            day = 0;
        try {
            var res = this.http.get(this.birthdayUsersUrl + day);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    UserService.prototype.fetchUsersPlus = function () {
        try {
            var res = this.http.get(this.userPlusUrl);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    UserService.prototype.fetchUserPlus = function (user) {
        try {
            var res = this.http.post(this.userPlusUrl, user);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    UserService.prototype.fetchUGUsers = function () {
        try {
            var res = this.http.get(this.uGUserUrl);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    };
    /* POST */
    UserService.prototype.login = function (user) {
        try {
            var res = this.http.post(this.loginUrl, user);
            console.log(res);
            // this.setLoggedInUser(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
        // return this.http.post<User>(this.loginUrl, myParams)
        //   .pipe(
        //     catchError(this.handleError<User>('login'))
        //   );
    };
    /* POST */
    UserService.prototype.loginWithToken = function (token) {
        try {
            var formData = new FormData();
            formData.append('token', token);
            // return this.http.post<User>(this.loginWithTokenUrl, formData)
            //   .map(res => {
            //     return res;
            //   });
            return this.http.post(this.loginWithTokenUrl, formData);
            // this.setLoggedInUser(res);
            // return res;
        }
        catch (error) {
            console.log(error);
            return null;
            ;
        }
        // return this.http.post<User>(this.loginUrl, myParams)
        //   .pipe(
        //     catchError(this.handleError<User>('login'))
        //   );
    };
    /**
     *
     * @param user
     * can be used for add or update
     */
    UserService.prototype.addEdit = function (user) {
        user.supervisor = null;
        try {
            var res = this.http.post(this.userUrl, user, httpOptions);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
        // return this.http.post<User>(this.signUpUrl, user, httpOptions)
        //   .pipe(
        //     catchError(this.handleError('signUp', user))
        //   );
    };
    UserService.prototype.uploadFile = function (file) {
        try {
            var formData = new FormData();
            formData.append('file', file);
            formData.append('upload_preset', __WEBPACK_IMPORTED_MODULE_3__config_constanta__["UNSIGNED_UPLOAD_PRESET"]);
            var res = this.http.post(__WEBPACK_IMPORTED_MODULE_3__config_constanta__["UPLOAD_URL"], formData);
            console.log(res);
            return res;
        }
        catch (error) {
            console.log(error);
            return error;
        }
        // var xhr = new XMLHttpRequest();
        // var fd = new FormData();
        // xhr.open('POST', url, true);
        // xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        // // // Reset the upload progress bar
        // //  document.getElementById('progress').style.width = 0;
        // // // Update progress (can be used to show progress indicator)
        // xhr.upload.addEventListener("progress", function(e) {
        //   // var progress = Math.round((e.loaded * 100.0) / e.total);
        //   // document.getElementById('progress').style.width = progress + "%";
        //   console.log(`fileuploadprogress data.loaded: ${e.loaded}, data.total: ${e.total}`);
        // });
        // xhr.onreadystatechange = function(e) {
        //   if (xhr.readyState == 4 && xhr.status == 200) {
        //     // File uploaded successfully
        //     var response = JSON.parse(xhr.responseText);
        //     console.log(response);
        //     // https://res.cloudinary.com/cloudName/image/upload/v1483481128/public_id.jpg
        //     var url = response.secure_url;
        //     // // Create a thumbnail of the uploaded image, with 150px width
        //     // var tokens = url.split('/');
        //     // tokens.splice(-2, 0, 'w_150,c_scale');
        //     // var img = new Image(); // HTML5 Constructor
        //     // img.src = tokens.join('/');
        //     // img.alt = response.public_id;
        //     // document.getElementById('gallery').appendChild(img);
        //   }
        // };
        // fd.append('upload_preset', Constanta.UNSIGNED_UPLOAD_PRESET);
        // // fd.append('tags', 'browser_upload'); // Optional - add tag for image admin in Cloudinary
        // fd.append('file', file);
        // xhr.send(fd);
    };
    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    UserService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // TODO: better job of transforming error for user consumption
            // this.log(`${operation} failed: ${error.message}`);
            // Let the app keep running by returning an empty result.
            return Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__["a" /* of */])(result);
        };
    };
    UserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map